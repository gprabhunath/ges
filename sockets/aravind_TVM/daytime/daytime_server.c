/*A simple Daytime server*/

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define MAXLINE 4096
#define SA struct sockaddr
#define LISTENQ 5

int main(int argc,char **argv)
{
    int listenfd, connfd;
    struct sockaddr_in servaddr;
    struct sockaddr_in cliaddr;
    socklen_t addrlen;
    char buff[MAXLINE];
    char cliipaddr[20];
    time_t ticks;
    int a=1;
    const char *ret_val;
     

    listenfd = socket(AF_INET,SOCK_STREAM,0);
    if(listenfd==-1)
    {
        printf("listenfd:%s\n",strerror(listenfd));
        exit(1);
    }
    
    memset(&servaddr,0, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);                  //INADDR will expand to all 0's implies all incoming addresses are  accepted
    servaddr.sin_port = htons(13);                                //htonl(host to network long) :what ever we are sending convert it to network byte order,

    setsockopt(listenfd, SOL_SOCKET, SO_REUSEADDR, &a, sizeof(a));
    bind(listenfd,(SA *)&servaddr,sizeof(servaddr));           //associate socket to an address
    
    listen(listenfd,LISTENQ);                                //LISTENQ says that it can take 5 requests,discard all other requests


    for(;;){                                               //server is always listening in an infinite loop
    connfd = accept(listenfd,(SA *)&cliaddr,&addrlen);     //once accepted converts listening socket to transaction socket(each socket is a session)
    if(connfd==-1)
    {
        printf("connfd error:%s\n",strerror(connfd));
        exit(1);
    }
    ret_val=inet_ntop(AF_INET, &cliaddr.sin_addr, cliipaddr, sizeof (cliipaddr));
    if(ret_val==NULL)
    {
        perror("inet_ntop");
        exit(1);
    }	
    printf("Client ip address = %s\n",cliipaddr);
    printf("client port = %d\n",ntohs(cliaddr.sin_port));
    ticks = time(NULL);
    snprintf(buff,sizeof(buff),"%.24s\r\n",ctime(&ticks));
    write(connfd,buff,strlen(buff));
    close(connfd);     //we are closing the connected socket
    }
}
