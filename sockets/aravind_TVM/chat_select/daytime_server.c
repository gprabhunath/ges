
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define MAXLINE 4096
#define SA struct sockaddr
#define LISTENQ 5


int listenfd, connfd;
struct sockaddr_in servaddr;
struct sockaddr_in cliaddr;
socklen_t addrlen;
char buff[MAXLINE];
char cliipaddr[20];
time_t ticks;
int a=1;
const char *ret_val;

int main(int argc,char **argv)
{
    listenfd = socket(AF_INET,SOCK_STREAM,0);
    if(listenfd==-1)
    {
        printf("listenfd:%s\n",strerror(listenfd));
        exit(1);
    }
    
    memset(&servaddr,0, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);                  
    servaddr.sin_port = htons(13);                               
    setsockopt(listenfd, SOL_SOCKET, SO_REUSEADDR, &a, sizeof(a));
    bind(listenfd,(SA *)&servaddr,sizeof(servaddr));           
    listen(listenfd,LISTENQ);                                

    for(;;){                                               
    connfd = accept(listenfd,(SA *)&cliaddr,&addrlen);     
    if(connfd==-1)
    {
        printf("connfd error:%s\n",strerror(connfd));
        exit(1);
    }
    ret_val=inet_ntop(AF_INET, &cliaddr.sin_addr, cliipaddr, sizeof (cliipaddr));
    if(ret_val==NULL)
    {
        perror("inet_ntop");
        exit(1);
    }	
    printf("Client ip address = %s\n",cliipaddr);
    printf("client port = %d\n",ntohs(cliaddr.sin_port));
    ticks = time(NULL);
    snprintf(buff,sizeof(buff),"%.24s\r\n",ctime(&ticks));
    write(connfd,buff,strlen(buff));
    close(connfd);     
    }
}
