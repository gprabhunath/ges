
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>

#define MAXLINE 4096
#define SA struct sockaddr


int main(int argc,char **argv)
{
    int sockfd,n;
    char recvline[MAXLINE+1];
    char chatmsg[MAXLINE+1];
    struct sockaddr_in servaddr;
    char *myname;
    char *prompt_msg;
    myname =  argv[0];
    fd_set readfds;
    int nfds;
    
    if(argc!=2)
    {
        fprintf(stderr,"usage: a.out <IPaddress>%s\n",myname);
        exit(1);
    }

    if((sockfd = socket(AF_INET,SOCK_STREAM,0))<0){
        perror("socket");
        exit(1);
    }
    memset(&servaddr,0,sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_port = htons(9876);
    if(inet_pton(AF_INET,argv[1],&servaddr.sin_addr)<=0){ 
        perror("inet_pton");
        exit(1);
    }
    
    if(connect(sockfd,(SA *)&servaddr,sizeof(servaddr))<0){
       perror("connect");
       exit(1);
    }
    write(1,"\nclient>",sizeof("\nclient>"));
    while(1)
    {
         FD_ZERO(&readfds);
         FD_SET(0,&readfds);
         FD_SET(sockfd,&readfds);
         
         nfds = sockfd + 1;
         
         if(select(nfds,&readfds,NULL,NULL,NULL)!=-1)
         {
         	if(FD_ISSET(0,&readfds))
            {
                if(read(0,chatmsg,MAXLINE)<=0)
                {
                    perror("read error1:");
                    exit(1);
          
                }
                write(1,"\nclient>",sizeof("\nclient"));
                if(write(sockfd,chatmsg,strlen(chatmsg))<=0)
                {
                    perror("write failed");
                      
                }
            }
            else if(FD_ISSET(sockfd,&readfds))
            {
            	if(read(sockfd,chatmsg,MAXLINE)<=0)
      			{
           			perror("read failed");
                    exit(1);
      			}
                write(1,"\n\nserver>",sizeof("\nserver>")); 
                write(1,chatmsg,strlen(chatmsg));
                write(1,"\nclient>",sizeof("\nclient>"));
			}
         }
    }
                                           
}
