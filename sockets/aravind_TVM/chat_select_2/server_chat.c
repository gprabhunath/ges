
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define MAXLINE 4096
#define SA struct sockaddr
#define LISTENQ 5

int main(int argc,char **argv)
{
    int listenfd, connfd;
    struct sockaddr_in servaddr;
    struct sockaddr_in cliaddr;
    socklen_t addrlen;
    char buff[MAXLINE];
    char cliipaddr[20];
    time_t ticks;
    int a=1;
    const char *ret_val;
    char server_msg[10]; 
    char *prompt_msg;
    int nfds;
    fd_set readfds;

    listenfd = socket(AF_INET,SOCK_STREAM,0);
    if(listenfd==-1)
    {
        printf("listenfd:%s\n",strerror(listenfd));
        exit(1);
    }
    
    memset(&servaddr,0, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);          
    servaddr.sin_port = htons(9877);                       

    setsockopt(listenfd, SOL_SOCKET, SO_REUSEADDR, &a, sizeof(a));
    bind(listenfd,(SA *)&servaddr,sizeof(servaddr));          
    listen(listenfd,LISTENQ);          


    for(;;)
    {                                             
    	connfd = accept(listenfd,(SA *)NULL,NULL);     
      	if(connfd==-1)
    	{
            printf("connfd error:%s\n",strerror(connfd));
            exit(1);
        }
        write(1,"\nserver>",sizeof("\nserver>"));
        while(1)
    	{
    	    FD_ZERO(&readfds);
            FD_SET(0,&readfds);
            FD_SET(connfd,&readfds);

            nfds=connfd+1;
            if(select(nfds,&readfds,NULL,NULL,NULL)!=-1)
            {
                if(FD_ISSET(0,&readfds))
                {
		    if(read(0,server_msg,MAXLINE)<=0)
                    {
		        perror("read error1:");
                        exit(1);
                    }
                    write(1,"\nserver>",sizeof("\nserver>"));
                   if(write(connfd,server_msg,strlen(server_msg))<=0)
                   {
                       perror("write error");
                       exit(1);
                   }
                    

		}
		else if(FD_ISSET(connfd,&readfds))
                {
		    if(read(connfd,buff,MAXLINE)<=0)
                    {
               	        perror("read error2:");
                        exit(1);
                    }
                    write(1,"\n\nclient>",sizeof("\nclient>"));
                    write(1,buff,strlen(buff));
                    write(1,"\nserver>",sizeof("\nserver>"));
                    
		}
            }
            
        }            
        
        close(connfd);
    }
}
