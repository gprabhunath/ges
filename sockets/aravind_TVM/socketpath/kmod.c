
/** System Includes **/
#include <linux/init.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/sched.h>
#include <linux/cdev.h>
#include <asm/uaccess.h>
#include <linux/device.h>
#include <linux/slab.h>
#include <linux/fdtable.h>
#include <linux/socket.h>
#include <asm/socket.h>
#include <linux/net.h>
#include <linux/dcache.h>
MODULE_LICENSE("Dual BSD/GPL");

/** Constants **/
#define FIRST_MINOR 	0
#define NR_DEVS	1 // Number of device numbers
#define MSB10 0xffc00000
#define MID10 0x003ff000
#define LSB12 0x00000fff







// Function Declarations for syscall definitions
int myOpen (struct inode *inode, struct file *filep);
int myRelease (struct inode *in, struct file *fp);
long myioctl(struct file *fp,unsigned int pid,unsigned long listenfd);
void *kmap(struct page *page);
// Initialization routines
static int myInit (void);
static void myExit (void);
struct file_operations fops = {
		owner:THIS_MODULE,
		open:myOpen,
		unlocked_ioctl:myioctl,
		release:myRelease
		
};

/* Global variables */
char *devname; // contains device name
int majNo;     
static dev_t mydev; // encodes major number and minor number
struct cdev *my_cdev; // holds character device driver descriptor

/* To accept input from the command line */
module_param(devname, charp, 0000);

// class and device structures 
static struct class *mychar_class;
static struct device *mychar_device;
static struct task_struct *ptr;


//unsigned long pgd_addr;





/*
 * myOpen: open function of the pseudo driver
 *
 */

int myOpen (struct inode *inode, struct file *filep)
{
        printk(KERN_INFO "Open successful\n");
        return 0;
}

/*
 * myRelease: close function of the pseudo driver
 *
 */

int myRelease (struct inode *in, struct file *fp)
{
   printk(KERN_INFO "File released \n");
   return 0;
}
/*
 * myioctl:to pass the pid to kernel
 *
 */

long myioctl (struct file *fp, unsigned int pid, unsigned long listenfd)
{
        int i;
	struct files_struct *files_task;
        struct fdtable *fd1;
        struct file ** fd_ptr;
        struct file *session;
        struct path sock_path;
        struct socket * sock_desc;
        struct dentry *sock_dentry=NULL;

     	printk(KERN_INFO "\nMy ioctl called\n");
	ptr=find_task_by_vpid(pid);

        files_task=ptr->files;
        printk("\nfiles %x",(unsigned int)files_task);

        fd1=files_task->fdt;
        printk("\nfdt %x",(unsigned int)fd1);

        fd_ptr=fd1->fd;
        session = fd_ptr[listenfd];

        printk("\nSession %x",(unsigned int)session);
        sock_desc=(struct socket*)session->private_data;
        
        switch(sock_desc->state)
        {
         case SS_FREE:
                      printk("\nSS_FREE");
         break;
         case SS_UNCONNECTED:
                      printk("\nSS_UNCONNECTED");
         break;
         case SS_CONNECTING:
                      printk("\nSS_CONNECTING");
         break;
         case SS_CONNECTED:
                    printk("\nSS_CONNECTED");
         break;
         case SS_DISCONNECTING:
                      printk("\nSS_DISCONNECTING");
         break;
       }
        
        printk("\nstate = %d",(unsigned int)sock_desc->state);
        sock_path = (session->f_path);
        sock_dentry=sock_path.dentry->d_parent;
        printk("\nSock_denty %x",sock_path.dentry->d_parent);
        printk("\nSock_path.dentry %x\n",sock_path.dentry);
        for(i=0;i<DNAME_INLINE_LEN;i++)
            printk("%c",sock_path.dentry->d_iname[i]);



       /* while(sock_path.dentry!=sock_dentry)
        {
            
            printk("%s",sock_path.dentry->d_iname);
            sock_path.dentry=sock_path.dentry->d_parent;
            sock_dentry=sock_path.dentry->d_parent;
            
        }*/
        
        
        
        
        return 0;


}
/*
 * myInit : init function of the kernel module
 *
 */
static int __init myInit (void)
{
	int ret = -ENODEV;
	int status;

	printk(KERN_INFO "Initializing Character Device \n");

	// Allocating Device Numbers
	status = alloc_chrdev_region (&mydev, FIRST_MINOR, NR_DEVS, devname);
	if (status < 0)
	{
		printk (KERN_NOTICE "Device numbers allocation failed: %d \n", 
								status);
		goto err;
	}

	printk (KERN_INFO "Major number allocated = %d \n", MAJOR(mydev));
	my_cdev = cdev_alloc(); // Allocate memory for my_cdev
	if (my_cdev == NULL) {
		printk (KERN_ERR "cdev_alloc failed \n");	
		goto err_cdev_alloc;
	}

	
	cdev_init(my_cdev, &fops); // Initialize my_cdev with fops
	my_cdev->owner = THIS_MODULE;

	status = cdev_add (my_cdev, mydev, NR_DEVS);// Add my_cdev to the list
	if (status) {
		printk (KERN_ERR "cdev_add failed  \n");
		goto err_cdev_add;
	}

	// Create a class and an entry in sysfs
	mychar_class = class_create(THIS_MODULE, devname);
	if (IS_ERR(mychar_class)) {
		printk (KERN_ERR "class_create() failed \n");
		goto err_class_create;
	}

	// creates mychar_device in sysfs and an 
	// device entry will be made in /dev directory
	mychar_device = device_create (mychar_class, NULL, mydev, NULL, devname);
	if (IS_ERR(mychar_device)) {
		printk (KERN_ERR "device_create() failed \n");
		goto err_device_create;
		
	}

	return 0;

err_device_create:
	class_destroy (mychar_class);

err_class_create:
	cdev_del(my_cdev);

err_cdev_add:
	kfree (my_cdev);
	
err_cdev_alloc:
	unregister_chrdev_region (mydev, NR_DEVS);

err:
   return ret;
}

/*
 * myExit : cleanup function of the kernel module
 */

static void myExit (void)
{
	printk (KERN_INFO "Exiting the Character Driver \n");
	
	device_destroy (mychar_class, mydev);
	class_destroy(mychar_class);
	cdev_del(my_cdev);
	unregister_chrdev_region(mydev, NR_DEVS);	

	return;
}

module_init(myInit);
module_exit(myExit);
