#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <stdlib.h>

struct sockaddr_un un;
char *path = "/home/prabhu/training/sockets/unix_domain/prabhu/mysocket";

int sockfd;	
int get_fd(void)
{
    int status;
    struct msghdr msg = {0};
    struct cmsghdr *cmsg;
	struct iovec iov[1];
	char ch;
    int *fdptr;
	char buf[CMSG_SPACE(sizeof *fdptr)] = {0};
	int flags;

    iov[0].iov_base = &ch;
//    iov[0].iov_len = 1;
    msg.msg_iov = iov;
  //  msg.msg_iovlen = 1;

	msg.msg_control = buf;
	msg.msg_controllen = sizeof (buf);

    status = recvmsg (sockfd, &msg, flags);
    if (status < 0)
    {
        perror ("recvmsg failed");
    }

#if 0
    cmsg->cmsg_level = SOL_SOCKET;
    cmsg->cmsg_type = SCM_RIGHTS;
    cmsg->cmsg_len = CMSG_LEN(sizeof myfd);
#endif

    cmsg = CMSG_FIRSTHDR (&msg);
    fdptr = (int *) CMSG_DATA (cmsg);

	printf ("fd received %d \n", *fdptr);
    //memcpy (fdptr, &myfd, sizeof (myfd));

    return *fdptr;
}

edit_file (int fd)
{
	char *buf = " Keep Smiling";

	write (fd, buf, strlen(buf));

	return;
}

int main()
{
	int fd;
	int connfd;
	int status;
	char buf[256] = {0};

	sockfd = socket (AF_LOCAL, SOCK_STREAM, 0);
	if (sockfd < 0)
	{
		perror ("socket failed");
		exit (1);
	}
	
	un.sun_family = AF_LOCAL;
	strncpy (un.sun_path, path, strlen (path));

	status = connect (sockfd, (struct sockaddr *) &un, sizeof (struct sockaddr_un));
	if (status < 0)
	{
		perror ("connect failed");
		exit (1);
	}

	read (sockfd, buf, sizeof (buf));
	write (1, buf, strlen (buf));

	fd = get_fd();
	edit_file(fd);

	close (sockfd);

	return 0;

}
	
	
