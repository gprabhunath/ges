#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <stdlib.h>
#include <fcntl.h>


struct sockaddr_un un;
//char *path = "/tmp/mysocket";
char *path = "/home/prabhu/training/sockets/unix_domain/prabhu/mysocket";
char *file_path = "/home/prabhu/training/sockets/unix_domain/prabhu/myfile";

pass_fd (int fd, int connfd)
{
	int status = 0;
	struct msghdr msg = {0};
	struct cmsghdr *cmsg;
	int myfd = fd;
	char buf[CMSG_SPACE(sizeof myfd)];
	int *fdptr;
	struct iovec iov[1];

	iov[0].iov_base = "";
	iov[0].iov_len = 1;
	msg.msg_iov = iov;
	msg.msg_iovlen = 1;

	msg.msg_control = buf;
	msg.msg_controllen = sizeof (buf);
	cmsg = CMSG_FIRSTHDR (&msg);
	cmsg->cmsg_level = SOL_SOCKET;
	cmsg->cmsg_type = SCM_RIGHTS;
	cmsg->cmsg_len = CMSG_LEN(sizeof myfd);
	fdptr = (int *) CMSG_DATA (cmsg);
	//memcpy (fdptr, &myfd, sizeof (myfd));	
	*fdptr = myfd;

	msg.msg_controllen = cmsg->cmsg_len;
	
	status = sendmsg (connfd, &msg, 0);
	if (status < 0)
	{
		perror ("sendmsg failed");
	} if (status > 0)
	{
		printf ("Number of characters sent = %d \n", status);
		printf ("fd %d sent \n", myfd);
	} else
	{
		printf ("Number of characters sent = %d \n", status);
	}
	
	return;	
}
int open_file(void)
{
	int fd;
	fd = open (file_path, O_RDWR);
	if (fd < 0)
	{
		perror ("open failed:");
		return -1;
	}
	
	return fd;
}

int main()
{
	int fd;
	int sockfd;	
	int connfd;
	int status;

	sockfd = socket (AF_LOCAL, SOCK_STREAM, 0);
	if (sockfd < 0)
	{
		perror ("socket failed");
		exit (1);
	}

	unlink (path);
	un.sun_family = AF_LOCAL;
	strncpy (un.sun_path, path, strlen (path));
	
	status = bind (sockfd, (struct sockaddr *)&un, sizeof (struct sockaddr_un));
	if (status != 0)
	{
		perror ("bind failed");
		exit (1);
	}

	listen (sockfd, 5);

	for (;;)
	{
		connfd = accept(sockfd, NULL, NULL);
		if (sockfd < 0)
		{
			perror ("socket failed");
			exit (1);
		}

		write (connfd, "Hello\n", sizeof ("Hello\n"));
	
		fd = open_file();		
	
		pass_fd(fd,connfd);
		close(connfd);
		
	}
		
	
}
