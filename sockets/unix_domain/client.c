#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <linux/un.h>


int main()
{
    int sock_fd;
    int status;
	int fd;
    struct cmsghdr *cmsg;

    char buf[20];
    struct sockaddr_un un = {0};
    struct msghdr msg = {0};
    char buf1[CMSG_SPACE(sizeof fd)];
	struct iovec iov;
	int flags;
	char c;

	
    char *str = "/home/prabhu/training/sockets/unix_domain/my_socket";
    //char *str = "/home/ramya/LINUX/unix_domain/my_socket";


    sock_fd = socket(AF_LOCAL, SOCK_STREAM, 0);
    if (sock_fd < 0) {
		perror("socket failed");
		exit(1);
    }

    un.sun_family = AF_LOCAL;
    strncpy(un.sun_path, str, strlen(str));

    status = connect(sock_fd, (struct sockaddr *) &un,
		sizeof(struct sockaddr_un));
    if (status != 0) {
		perror("connect failed");
		exit(1);
    }

    read(sock_fd, buf, sizeof(buf));
    write(1, buf, strlen(buf));

	iov.iov_base = &c;
    //msg.msg_name = NULL;
    //msg.msg_namelen = 0;
    msg.msg_control = buf1;
    msg.msg_controllen = sizeof(buf1);
	msg.msg_iov = &iov;

	printf("Above recvmsg %d\n", getpid());

#if 1
    status = recvmsg(sock_fd, &msg, flags);
    if (status < 0) {
		perror("recvmsg");
		exit(1);
    }
#endif
    cmsg = CMSG_FIRSTHDR(&msg);
	fd = *((int *)CMSG_DATA(cmsg));
	//fd = cmsg->cmsg_level;

	printf("received fd %d\n", fd);

	write (fd, "Hi Putta, Keep smiling", sizeof ("Hi Putta, Keep Smiling"));

    return 0;
}
