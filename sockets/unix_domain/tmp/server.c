# include <stdio.h>
# include <stdlib.h>
# include <string.h>
# include <sys/types.h>
# include <sys/socket.h>
# include <linux/un.h>
# include <fcntl.h>
int open_file (void) {
	int fd;
	char *path = "/home/prabhu/training/sockets/unix_domain/myfile";
	fd = open(path, O_RDWR);
	if (fd < 0) {
		perror("open failed");
		exit(1);
	}
	return fd;
}


int main()
{
	int fd;
	int sock_fd;
	int con_fd;
	int status;
	
	struct sockaddr_un un ;
	char *str = "/home/prabhu/training/sockets/unix_domain/my_socket";
//	char *str = "/tmp/my_socket";

	sock_fd = socket (AF_LOCAL, SOCK_STREAM, 0);
	if (sock_fd < 0) {
		perror("socket failed");
		exit(1);
	}
	unlink (str);
	un.sun_family = AF_LOCAL;
	strncpy (un.sun_path, str, strlen(str)+1);

	status = bind (sock_fd, (struct sockaddr *) &un, sizeof(struct sockaddr_un));
	
	if (status != 0) {
		perror("bind failed");
		exit(1);
	}
	
	status = listen (sock_fd, 5);

	if (status != 0) {
		perror("listen failed");
		exit(1);
	}


	struct msghdr msg = {0};
	struct cmsghdr *cmsg;
	int myfd;
	char buf[CMSG_SPACE(sizeof myfd)];
	int *fdptr = NULL;

	myfd = open_file();	
	msg.msg_control = buf;
	msg.msg_controllen = sizeof(buf);
	cmsg = CMSG_FIRSTHDR(&msg);
	cmsg->cmsg_level = SOL_SOCKET;
	cmsg->cmsg_type = SCM_RIGHTS;
	cmsg->cmsg_len = CMSG_LEN(sizeof(int));
	fdptr = (int*) CMSG_DATA(cmsg);
	*fdptr = myfd;
	
//	printf ("Value of fd = %d \n", *fdtr);
	msg.msg_controllen = cmsg->cmsg_len;

	for (;;) {

		con_fd = accept (sock_fd, NULL, NULL);	
		if (!con_fd) {
			perror("accept failed");
			exit(1);
		}
		write (con_fd, "Hello world\n", sizeof ("Hello world\n")); 
		printf ("After write \n");
#if 1
		status = sendmsg(con_fd, &msg, 0);		
		if (status < 0) {
			perror("sendmsg failed");
			exit(1);
		}
		printf ("Num of msg = %d\n", status);	
#endif
	
	}

	return 0;
}
