# include <stdio.h>
# include <stdlib.h>
# include <string.h>
# include <sys/types.h>
# include <sys/socket.h>
# include <linux/un.h>


int main()
{
	int sock_fd;
	int status;
	struct sockaddr_un un = {0};
	char buf[20];
	char *str = "/home/prabhu/training/sockets/unix_domain/my_socket";
//	char *str = "/tmp/my_socket";

	sock_fd = socket (AF_LOCAL, SOCK_STREAM, 0);
	if (sock_fd < 0) {
		perror("socket failed");
		exit(1);
	}
	un.sun_family = AF_LOCAL;
	strncpy (un.sun_path, str, strlen(str));

    status = connect(sock_fd, (struct sockaddr *) &un, sizeof (struct sockaddr_un));

    if (status != 0) {
        perror("connect failed");
        exit(1);
    }

#if 1
    read (sock_fd, buf, sizeof(buf));
    write (1, buf, strlen(buf));
#endif

	struct msghdr msg;
    struct cmsghdr *cmsg;
    int myfd;

    char buf1[CMSG_SPACE(sizeof myfd)];
    int *fdptr = NULL;
#if 0
    msg.msg_control = buf1;
    msg.msg_controllen = sizeof(buf1);
    cmsg = CMSG_FIRSTHDR(&msg);
    cmsg->cmsg_level = SOL_SOCKET;
    cmsg->cmsg_type = SCM_RIGHTS;
    //cmsg->cmsg_len = CMSG_LEN(sizeof(int));
    msg.msg_controllen = cmsg->cmsg_len;
	int flags;
#endif

	status = recvmsg(sock_fd, &msg, flags);
	if (status < 0) {
		perror("recvmsg failed");
		exit(1);
	}
    cmsg = CMSG_FIRSTHDR(&msg);
	printf ("originating protocol = %d\n", cmsg->cmsg_level);
	
#endif
	return 0;
}
