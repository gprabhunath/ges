#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <linux/un.h>

int open_file(void)
{
    int fd;
    //char *path = "/home/ramya/LINUX/unix_domain/myfile";
    char *path = "/home/prabhu/training/sockets/unix_domain/myfile";

    fd = open(path, O_RDWR);
	if (fd < 0) {
		perror("open failed");
		exit(1);
    }

    return fd;
}


int main()
{
    int myfd;
    int sock_fd;
    int con_fd;
    int status;
	int errno;

    struct sockaddr_un un;
    struct msghdr msg = { 0 };
    struct cmsghdr *cmsg;
	struct iovec iov;

    char buf[CMSG_SPACE(sizeof myfd)];

    char *str = "/home/prabhu/training/sockets/unix_domain/my_socket";

    sock_fd = socket(AF_LOCAL, SOCK_STREAM, 0);
    if (sock_fd < 0) {
		perror("socket failed");
		exit(1);
    }

    unlink(str);
    un.sun_family = AF_LOCAL;
    strncpy(un.sun_path, str, strlen(str) + 1);

    status =
	bind(sock_fd, (struct sockaddr *) &un, sizeof(struct sockaddr_un));
    if (status != 0) {
		perror("bind failed");
		exit(1);
    }

    status = listen(sock_fd, 1);
    if (status != 0) {
		perror("listen failed");
		exit(1);
    }

    myfd = open_file();
	int c = ' ' ;	
    msg.msg_name = NULL;
    msg.msg_namelen = 0;
	iov.iov_base = &c;
	iov.iov_len = 1;
    msg.msg_iov = &iov;
    msg.msg_iovlen = 1;

    msg.msg_control = buf;
    msg.msg_controllen = sizeof(buf);


    cmsg = CMSG_FIRSTHDR(&msg);
    cmsg->cmsg_len = CMSG_LEN(sizeof(int));
    cmsg->cmsg_level = SOL_SOCKET;
    cmsg->cmsg_type = SCM_RIGHTS;
    *((int *) CMSG_DATA(cmsg)) = myfd;


    printf("passing fd %d\n", myfd);


    for (;;) {

		con_fd = accept(sock_fd, NULL, NULL);
		if (!con_fd) {
	    	perror("accept failed");
	    	exit(1);
		}

		write(con_fd, "Hello world\n", sizeof("Hello world\n"));

		printf("Above sendmsg %d\n", getpid());

		status = sendmsg(con_fd, &msg, 0);
		if (status < 0) {
	    	perror("sendmsg failed");
	    	exit(1);
		}

		printf("Num of msg = %d\n", status);

    }
    return 0;
}
