
#include <linux/init.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/device.h>
#include <linux/sched.h>
#include <linux/fdtable.h>
#include <asm/pgtable.h>
#include <linux/mm_types.h>

MODULE_LICENSE("Dual BSD/GPL");

#define FIRST_MINOR 	0
#define NR_DEVS	1 // Number of device numbers


char *devname;
module_param(devname, charp, 0000);

// Function Declarations for syscall definitions
int myOpen 	  (struct inode *inode, struct file *filep);
int myRelease (struct inode *in, struct file *fp);
long myIoctl  (struct file *filep, unsigned int, unsigned long);

// Initialization routines
static int myInit (void);
static void myExit (void);

struct file_operations fops = {
		.owner = THIS_MODULE,
		.open = myOpen,
		.release = myRelease,
		.unlocked_ioctl = myIoctl
};

int majNo;
static dev_t dev_id;
struct cdev *my_cdev;

static unsigned int c_major;
static unsigned int c_minor;

struct class *my_class;	// Tie with the device model

static int __init myInit (void)
{
	int err;
	
	printk (KERN_INFO "Initializing Character Device \n");

	// Allocating Device Numbers
	if (c_major)
	{
		dev_id = MKDEV(c_major, c_minor);	
		register_chrdev_region (dev_id, NR_DEVS, devname);
	}
	else
	{
		err = alloc_chrdev_region (&dev_id, FIRST_MINOR, NR_DEVS, devname);
		if (err < 0)
			printk (KERN_NOTICE "Device numbers allocation failed: %d \n", 
								err);
	}

	printk (KERN_INFO "Major number allocated = %d \n", MAJOR(dev_id));
	my_cdev = cdev_alloc(); // Allocate memory for my_cdev
	cdev_init(my_cdev, &fops); // Initialize my_cdev with fops
	my_cdev->owner = THIS_MODULE;

	err = cdev_add (my_cdev, dev_id, NR_DEVS);// Add my_cdev to the list
	if (err)
		printk (KERN_NOTICE "Error adding my_cdev \n");
	else
		printk (KERN_NOTICE "Device Added \n");

	my_class = class_create(THIS_MODULE, devname);
	device_create (my_class, NULL, dev_id, NULL, devname);
	
	printk (KERN_INFO "No. of CPUs are %d \n", NR_CPUS);
	
   return 0;
}


static void myExit (void)
{
	printk (KERN_INFO "Exiting the Character Driver \n");
	unregister_chrdev_region (dev_id, NR_DEVS);	
	device_destroy (my_class, dev_id);
	cdev_del(my_cdev);
	class_destroy(my_class);

	return;
}

int myOpen (struct inode *inode, struct file *filep)
{
    printk (KERN_INFO "Open successful\n");

#if 0
	printk (KERN_INFO "Address of myOpen = 0x%08x \n", (unsigned int) myOpen);
	printk (KERN_INFO "Address of fops   = 0x%08x \n", (unsigned int) &fops);
#endif

		return 0;
}

int myRelease (struct inode *in, struct file *fp)
{
   printk(KERN_INFO "File released \n");
   return 0;
}

long myIoctl (struct file *filep, unsigned int sockfd, unsigned long u_data)
{

	struct task_struct *my_task;
	
	my_task = current;

	printk (KERN_INFO "0x%08x\n", my_task->files->fdt->fd[sockfd]->private_data);

	
	return 0;
}

module_init(myInit);
module_exit(myExit);
