# include <stdio.h>
# include <stdlib.h>
# include <string.h>
# include <errno.h>
# include <sys/types.h>
# include <sys/socket.h>
# include <netinet/ip.h> /* superset of previous */
# include <netinet/tcp.h> /* superset of previous */


# define MAX 1024


int main()
{
	int sock_fd;
	int status;
	char buf[MAX];
	char src[16] = {0};
	char dst[16] = {0};
	struct iphdr *hdr;
	struct tcphdr *tcp;


	sock_fd = socket(AF_INET, SOCK_RAW, IPPROTO_RAW);
	if(sock_fd < 0){
		perror("socket failed");
		exit(1);
	}
	printf("  =====SERVER ====\n\n");
	hdr = (struct iphdr *) buf;
	tcp = (struct tcphdr *) (hdr + 1);
	while (1) {
		printf("======================= Waiting ======================\n\n");
		status = read(sock_fd, buf, sizeof(buf));
		if (status <= 0){
			perror("read failed");
			exit(2);
		}

		printf("protocol = %d\n", hdr->protocol);
		printf("src = 0x%08x\n", ntohl(hdr->saddr));
		printf("dst = 0x%08x\n", ntohl(hdr->daddr));
		inet_ntop(AF_INET, &hdr->saddr, src, sizeof(src));
		inet_ntop(AF_INET, &hdr->daddr, dst, sizeof(dst));
		printf("source ip address = %s\n", src);
		printf("destination ip address = %s\n", dst);
#if 0
		printf("source port num = %d\n", ntohs(tcp->source));
		printf("destination port num = %d\n", ntohs(tcp->dest));
#endif
		
	}
	
	return 0;
}
