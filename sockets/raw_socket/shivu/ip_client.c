# include <stdio.h>
# include <stdlib.h>
# include <string.h>
# include <sys/types.h>
# include <sys/socket.h>
# include <netinet/ip.h>
#include <arpa/inet.h>
int main()
{
	int sock_fd;
	int status;
	int op_value = 1;
	socklen_t op_len;
	struct iphdr *ip;
	struct sockaddr_in saddr,daddr;
	unsigned char buf[256] = {0};

#if 0
	const char *src = "172.16.5.164";
	char *dst = "172.16.5.171";
#endif

#if 0
	char *src = "127.0.0.1";
	char *dst = "127.0.0.1";
#endif

	memset(&ip, 0, sizeof(struct iphdr));
	memset(&saddr, 0, sizeof(struct sockaddr_in));

//	printf("hi ");

	saddr.sin_addr.s_addr = inet_addr("172.16.2.241");
	daddr.sin_addr.s_addr = inet_addr("172.16.5.44");
	daddr.sin_family = AF_INET;


	ip = (struct iphdr *)buf;
	ip->version = 4;
	ip->ihl = 5;
	ip->tos = 0;
	ip->tot_len = htons(25);
	ip->protocol = IPPROTO_RAW;
	ip->saddr = saddr.sin_addr.s_addr;
	ip->daddr = daddr.sin_addr.s_addr;

//	inet_pton (AF_INET, src, &saddr.sin_addr);
//	inet_pton (AF_INET, dst, &ip->daddr);

	//memcpy (buf, &ip, sizeof (struct iphdr));	
	
	
	sock_fd = socket (AF_INET, SOCK_RAW, IPPROTO_RAW);
	if (sock_fd < 0){
		perror("socket failed");
		exit(1);
	}
//	saddr.sin_addr.s_addr = ip->saddr;
//	daddr.sin_addr.s_addr = ip->daddr;
	op_len = sizeof(op_value);

	
	printf("src ==> %s",inet_ntoa(saddr.sin_addr));
	printf ("dest ip addr = %s \n", inet_ntoa(daddr.sin_addr));

	status = getsockopt(sock_fd, IPPROTO_IP, IP_HDRINCL, &op_value, &op_len);
	if (status < 0){
		perror("getsockopt failed");
		exit(1);
	}
	printf("op_value %d\n", op_value);
#if 0
	op_value = 0;
	status = setsockopt(sock_fd, IPPROTO_IP, IP_HDRINCL, &op_value, op_len);
	if (status < 0){
		perror("getsockopt failed");
		exit(1);
	}
#endif

	strncpy ((char *)(ip+1), "Hello", 5);	
	//status = sendto(sock_fd, (char *)(ip+1), 5, 0, (struct sockaddr *)&saddr, sizeof(saddr));
	status = sendto(sock_fd, buf, 25, 0, (struct sockaddr *)&daddr, sizeof(struct sockaddr_in));

	if (status < 0) {
		perror("sendto failed");
		exit(1);
	}

	printf ("status = %d \n", status);
		
	return 0;

} //End of main
