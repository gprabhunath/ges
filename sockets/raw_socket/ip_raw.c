#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/ip.h>
#include <stdlib.h>
#include <string.h>

int main()
{
	int sockfd;
	int status;
	int bytes;
	struct sockaddr_in sa; 
	char buf[4096] = {0};
	struct iphdr *ip = NULL;
	unsigned char srcip[32] = {0};

	sockfd = socket (PF_INET, SOCK_RAW, IPPROTO_TCP);
	if (sockfd < 0)
	{
		perror ("socket failed");
		exit (1);
	}

	memset (&sa, 0, sizeof (struct sockaddr_in));

	sa.sin_family = PF_INET;
	sa.sin_addr.s_addr = INADDR_ANY;

	status = bind (sockfd, (struct sockaddr *)&sa, sizeof (struct sockaddr_in));	
	if (status < 0)
	{
		perror ("bind failed");
		exit (1);
	}

	for (;;)	
	{		
		bytes = recv (sockfd, buf, sizeof(buf), 0);
		if (bytes <= 0)
		{
			perror ("recv failed");
			exit (1);
		}

		ip = (struct iphdr *)buf;
		if (inet_ntop (PF_INET, (struct in_addr *)&ip->saddr, 
				srcip, sizeof (srcip)) == NULL)
		{
			perror ("inet_ntop failed");
			exit (1);
			
		}
		
		printf ("Source IP address = %s ", srcip);
		printf ("Encapsulated protocol = %d \n", ip->protocol);
		
		
	}
	
	return 0;
	
}
	
