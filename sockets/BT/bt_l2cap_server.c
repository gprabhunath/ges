#include <stdio.h>
#include <sys/types.h>         
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h> 
#include <unistd.h>
#include <bluetooth/bluetooth.h>
#include <bluetooth/l2cap.h>

/*server program*/

int main(int argc, char **argv)
{
	struct sockaddr_l2 loc_addr = {0};
	struct sockaddr_l2 rem_addr = {0};
	char buf[1024] = {0};
	int sockfd;
	int client;
	int bytes_read;
	unsigned int opt = sizeof(rem_addr);

	if(-1 == (sockfd = socket(PF_BLUETOOTH, SOCK_SEQPACKET, BTPROTO_L2CAP))) { 

		perror("socket() failed");
		exit(EXIT_FAILURE);
	}

	loc_addr.l2_family = PF_BLUETOOTH;
	loc_addr.l2_bdaddr = *BDADDR_ANY;
	loc_addr.l2_psm = htobs(0x2005);

	if(-1 == bind(sockfd, (struct sockaddr *)&loc_addr, sizeof(loc_addr))) { 
		perror("bind() failed");
		exit(1); 
	}

	if(-1 == listen(sockfd, 1)) { /*listen for connections on a socket*/
		perror("listen() failed");
		exit(EXIT_FAILURE);
	}
	
	client = accept(sockfd, (struct sockaddr *)&rem_addr, &opt);

	ba2str(&rem_addr.l2_bdaddr, buf);

	fprintf(stderr, "accepted connection from %s\n", buf);

	memset(buf, 0, sizeof(buf));

	while(1) {
	bytes_read = recv(client, buf, sizeof(buf), 0);

	if(bytes_read > 0) {
		printf("received [%s]\n", buf);
	}

	}
	close(client);
	close(sockfd);

	return 0;

}
