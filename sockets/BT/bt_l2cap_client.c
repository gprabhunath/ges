#include <stdio.h> 
#include <unistd.h>                                                             
#include <sys/types.h>                                                          
#include <sys/socket.h>                                                         
#include <stdlib.h>                                                             
#include <netinet/in.h>
#include <bluetooth/bluetooth.h>
#include <bluetooth/l2cap.h>           

/*client program*/
                                                                     
int main(int argc, char **argv)                                                 
{
	struct sockaddr_l2 addr = {0};
	int sockfd;
	int status;   
	char dest[18] = "";                      
	if(argc < 2) {
		fprintf(stderr, "usage : %s <bt_addr>\n", argv[0]);
		return 1;
	}

	strncpy(dest, argv[1], 18);

	if(-1 == (sockfd = socket(PF_BLUETOOTH, SOCK_SEQPACKET, BTPROTO_L2CAP))) { 
        perror("socket() failed");                                              
        exit(EXIT_FAILURE);                                                                
    }   

   	addr.l2_family = PF_BLUETOOTH;                                               	
	addr.l2_psm = htobs(0x1001);
	str2ba(dest, &addr.l2_bdaddr);  

	if(-1 == connect(sockfd, (struct sockaddr *)&addr, sizeof(addr))) { 
		perror("connect failed");
		exit(1);
	}

	
	if(0 == status) {
		status = send(sockfd, "hello!", 6, 0);

	}

	if(status < 0)
		perror("Error");

	close(sockfd);

	return 0;

}

