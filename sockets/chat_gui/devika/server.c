/*Server programme*/
#include <stdio.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/select.h>
#include <netinet/in.h>
#include <string.h>
#include <pthread.h>
#include <stdlib.h>
#include <sys/types.h>

#define MAXLINE 4096
#define SA struct sockaddr
#define LISTENQ 5
#define TCPPORT 13
#define SIZE 100 //!size of string buffer


int main (int argc, char **argv)
{
        socklen_t len;
        struct sockaddr_in servaddr, cliaddr;
        char buff[MAXLINE];
        time_t ticks;
        int connfd;
        int listenfd;
        int ret, sel; 
        char chr[SIZE]; 
        fd_set rdfdr, wrfdr;
        struct timeval tim;
        
        listenfd = socket (AF_INET, SOCK_STREAM, 0);
        memset (&servaddr, 0, sizeof(servaddr));
        servaddr.sin_family = AF_INET;
        servaddr.sin_addr.s_addr = inet_addr("172.16.5.96");
        servaddr.sin_port = htons (TCPPORT);

        bind (listenfd, (SA *)&servaddr, sizeof(servaddr));

        listen (listenfd, LISTENQ);
	len = sizeof (servaddr);
//        getchar();
        printf ("waiting for client\n");
        connfd = accept (listenfd, (SA *)&servaddr, &len);
        printf ("client received\n");
        
        for(;;){
                FD_ZERO (&rdfdr);
                FD_SET (connfd, &rdfdr);
                FD_SET (0, &rdfdr);
                sel = select (connfd + 1, &rdfdr, NULL, NULL, NULL);

                if (FD_ISSET(0, &rdfdr)){
                        fgets(chr,SIZE,stdin);
                        chr[strlen(chr)-1]='\0';
                        write (connfd, chr, 100);
                        if(!strcmp(chr,"exit"))
                                exit(1);
                        FD_SET (0, &rdfdr);
                }

                else if (FD_ISSET(connfd, &rdfdr)){
                        memset(chr, 0, SIZE);
                        ret = read (connfd, chr, 100);
                        if(ret > 0){
                                printf ("\t\t%s\n",chr);
                                if(strcmp(chr,"exit") == 0){
                                        printf("client wants to terminate\n");
                                        exit(1);
                                }
                        }
                        FD_SET (connfd, &rdfdr);
                }
        }       
        close(connfd);
}
