#include <stdio.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <string.h>
#include <stdlib.h>

#define MAXLINE 4096
#define SA struct sockaddr
#define TCPPORT 13
#define SIZE 100

int main(int argc, char **argv)
{
        struct sockaddr_in servaddr;
        int sockfd,n,conn;
        char recvline[MAXLINE + 1];
        char *myname;
        char chr[SIZE];
        fd_set rdfdr;
        int ret, sel;


        myname = argv[0];
        if (argc != 2){
                fprintf (stderr, "usage:%s <IPaddress>\n", myname);
                exit (0);
        }

        if ((sockfd = socket (AF_INET, SOCK_STREAM, 0)) < 0){
                perror ("socket");
                exit (1);
        }

        memset (&servaddr, 0, sizeof(servaddr));
        servaddr.sin_family = AF_INET;
        servaddr.sin_port = htons (TCPPORT);

        if (inet_pton (AF_INET, argv[1], &servaddr.sin_addr) <= 0){
                perror ("inet_pton");
                exit (1);
        }
        printf ("Requesting connection\n");
        if ((conn = connect (sockfd, (SA *)&servaddr, sizeof(servaddr))) <0){
                perror ("connect");
                exit (1);
        }
        printf ("connection established\n");
        for(;;) {
                FD_ZERO (&rdfdr);
                FD_SET (sockfd, &rdfdr);
                FD_SET (0, &rdfdr);
                sel = select (sockfd + 1, &rdfdr, NULL, NULL, NULL);

                if (FD_ISSET(0, &rdfdr)){
                        fgets(chr,SIZE,stdin);
                        chr[strlen(chr)-1]='\0';
                        write (sockfd, chr, 100);
                        if(!strcmp(chr,"exit"))
                                exit(1);
                        FD_SET (0, &rdfdr);
                }
                else if (FD_ISSET(sockfd, &rdfdr)){
              
                        memset(chr, 0, SIZE);
                        ret = read (sockfd, chr, 100);
                        if(ret > 0){
                                printf ("\t\t%s\n",chr);
                                if(!strcmp(chr,"exit\n")){
                                        printf("client wants to terminate\n");
                                        exit(1);
                                }
                        }
                        FD_SET (sockfd, &rdfdr);
                }
        }       
        close(sockfd);
}
