#include <stdio.h>
#include <poll.h>
#include <stdlib.h>

int get_status (char *);
typedef int (*fd_callback)(int);

static struct pollfd fds[20];
static int nfds;

struct cb_param {
	int fd;
	fd_callback cb;
	int data;
} r_funcs[9];


void register_fds(int fd, fd_callback cb, int data)
{
	r_funcs[fd].fd = fd;
	r_funcs[fd].cb = cb;
	r_funcs[fd].data = data;

	fds[fd].fd = fd;
	fds[fd].events = POLLIN;
	if (fd > nfds)
		nfds = fd;

	printf ("fds[fd].fd = %d \n", fds[fd].fd);
	printf ("nfds = %d \n", nfds);
	return;
}

int main()
{
	int status;
	int i;

	nfds = 3;
	for (i=0; i<20; i++)
	{
		fds[i].fd = -1;
	}

	init();	
	get_status("172.16.6.21");

	while (1)
	{
		printf ("Entering poll \n");
		status = poll(fds, nfds+1, -1);
		if (status < 0)
		{
			perror ("poll failed");
			exit (-1);
		}

//		printf ("Processing events %d \n", status);
		for (i=0; i<=nfds; i++)
		{
			if (fds[i].revents == POLLIN)
			{
				r_funcs[i].cb(i);
			} else
			if (fds[i].revents)
			{
				printf ("Stuck at %d \n", i);
				fds[i].revents = 0;
			}

		}
	}

	return 0;
	
}


