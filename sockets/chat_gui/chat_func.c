#include "chat.h"
static int cfd;
static int sfd;
static char dpaddr[16] = {0};

int init(void)
{
	int status;
	struct sockaddr_in saddr;
	
	sfd = socket (PF_INET, SOCK_STREAM, 0);
	if (sfd < 0)
	{
		perror ("socket failed");
		return -1;
	}

	memset (&saddr, 0, sizeof (struct sockaddr_in));
	saddr.sin_family = PF_INET;
	saddr.sin_port = htons(13);

	status = bind(sfd, (const struct sockaddr *)&saddr, 
						sizeof (struct sockaddr_in));
	if (status < 0)
	{
		perror ("listen failed");
		return -1;
	}

	status = listen (sfd, 5);
	if (status < 0)
	{
		perror ("listen failed");
		return -1;
	}

	register_fds(sfd, sfd_callback, sfd);

	return 0;
}

int get_status(char *ip)
{
	int status;
	struct sockaddr_in daddr;
	int peer_status = 0;

	cfd = socket (PF_INET, SOCK_STREAM, 0);
	if (cfd < 0)
	{
		perror ("socket failed");
		return -1;
	}
	
	memset(&daddr, 0, sizeof (sizeof daddr));
	daddr.sin_family = PF_INET;
	daddr.sin_port = htons(13);
	status = inet_pton (PF_INET, ip, &daddr.sin_addr);
	if (status < 1)
	{
		perror ("inet_pton failed");
		return -1;
	}
	
	status = connect (cfd, (const struct sockaddr *)&daddr,
				 sizeof(struct sockaddr_in));
	if (status < 0)
	{
		perror("connect");
		return (-1);
	}

	printf ("Connection successful \n");
	read (cfd, &peer_status, sizeof (peer_status));
	printf ("Status = %d \n", peer_status);

	return 0;
}

int sfd_callback(fd)
{
	int host_status = 3;
	int connfd;
	struct sockaddr_in psockaddr;
	int saddr_size = sizeof(struct sockaddr_in);
	int in_addr_size = sizeof (struct in_addr);

	memset (&psockaddr, 0, sizeof (psockaddr));

	connfd = accept (sfd, (struct sockaddr *)&psockaddr, &saddr_size);
	if (connfd < 0)
	{
		perror("accept");
		return (-1);

	}
#if 0
    if (inet_ntop(PF_INET, &psockaddr.sin_addr, dpaddr,
					in_addr_size) == NULL)
	{
		perror ("inet_ntop");
	}
#endif
	strcpy (dpaddr, inet_ntoa(psockaddr.sin_addr));
	register_fds(connfd, pfd_callback, connfd);
	printf ("connfd = %d : peer ip address = %s \n", connfd, dpaddr);
	write (connfd, &host_status, sizeof (host_status));

	return 0;
}

int pfd_callback(fd)
{
	int status;
	char buf[32] = {0};

	status = read (fd, buf, sizeof(buf));
	printf ("%s \n", buf);

	return 0;
}
