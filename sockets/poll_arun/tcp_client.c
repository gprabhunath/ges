/* Sample TCP client */

#include <sys/socket.h>
#include <netinet/in.h>
#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <stdlib.h>
#include <sys/poll.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <fcntl.h>
#include <sys/syscall.h>

#define CONTEXT	0x10

int sock=-1;
int fd;
int flag = 0;
pthread_t thread1, thread2;

void *client_connect(void *x )
{
     char sendline[1000]="Hello\n";
     char recvline[1000];
     int n,res;
     struct sockaddr_in servaddr,cliaddr;
     struct pollfd input_fd;
     int timeout_ms;
   
     struct  hostent *hp;
#if 0
	if (forWrite)
		input_fd.events |= POLLOUT;
#endif

    //hp = gethostbyname("warsaw.tva.tvworks.com");
	//bcopy ( hp->h_addr, &servaddr.sin_addr.s_addr, hp->h_length);

	
	sleep (2);
    printf("%s: %d : Entry \n", __func__, syscall(SYS_gettid)); 

    //bzero((char *) &servaddr, sizeof(servaddr));
    memset((char *) &servaddr, 0, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
  //  servaddr.sin_addr.s_addr="192.168.12.97";
    servaddr.sin_addr.s_addr= htonl(INADDR_ANY);
    servaddr.sin_port=htons(32000);

   input_fd.fd = sock;
   //input_fd.events = POLLERR;
   input_fd.revents = 0;
   input_fd.events |= POLLIN;
   timeout_ms = -1;

   connect(sock, (struct sockaddr *)&servaddr, sizeof(servaddr));

   sendto(sock,sendline,strlen(sendline),0,
             (struct sockaddr *)&servaddr,sizeof(servaddr));
   printf("Msg Sent OK \n");
	ioctl (fd, sock, syscall(SYS_gettid));
   if ( (res=poll(&input_fd, 1, timeout_ms)) > 0 )
   {
          printf("Msg Recv OK \n");
			ioctl (fd, sock, syscall(SYS_gettid));
           n=recvfrom(sock,recvline,10000,0,NULL,NULL);
           recvline[n]=0;
			flag = 1;
			sleep (2);
           fputs(recvline,stdout);
   }
   else {
   		fprintf(stderr, "Error: %d\n",res);
   }
   close(sock);	 
   printf("Client Connect() Exit!\n");
}

void *client_close(void *x )
{

  printf("%s:%d Going to close Socket\n", __func__, syscall(SYS_gettid));
  //sleep(3);
  /* This will work properly */
  //shutdown(sock,SHUT_RDWR);
  ioctl (fd, sock, syscall(SYS_gettid));
  //close(sock);
  //ioctl (fd, sock, syscall(SYS_gettid));
  
  printf("%s: Socket closed\n", __func__);
}
  
int main(int argc, char **argv)
{
     int iret1, iret2;
     int sockfd;	 

    /* Create independent threads each of which will execute function */
     sockfd = socket(AF_INET, SOCK_STREAM, 0);
	 printf ("sockfd = %d \n", sockfd);
	 fd = open ("/dev/myChar", O_RDWR);
	 if (fd < 0)
	 	perror("Unable to open the device");
	 else
		printf("File opened Successfully %d\n", fd);
     
     sock=sockfd;

  	 //ioctl (fd, sock, getpid());
     iret1=pthread_create( &thread1, NULL, client_connect, NULL);
  	 //ioctl (fd, sock, getpid());
#if 1
     iret2=pthread_create( &thread2, NULL, client_close, NULL);
  	 //ioctl (fd, sock, getpid());
	 
     /* Wait till threads are complete before main continues. Unless we  */
     /* wait we run the risk of executing an exit which will terminate   */
     /* the process and all threads before the threads have completed.   */

	 sleep(2);
     iret2 = pthread_join( thread2, NULL);
     printf("Thread 2 returns: %d\n",iret2);
#endif
  	 ioctl (fd, sock, getpid());
	 while (!flag) { };
  	 ioctl (fd, sock, getpid());

//	 close(sock);	 

     iret1 = pthread_join( thread1, NULL);
     printf("Thread 1 returns: %d\n",iret1);

	 sleep(3);
  	 ioctl (fd, sock, getpid());
     close (fd);
     exit(0);
}
