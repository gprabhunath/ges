# include "ser.h"
#define TCPPORT 15 

int main (int argc, char **argv)
{
		struct sockaddr_in serv, clinaddr;
		int sockfd, n;
		fd_set rfds, wrfds;
		char recvline[MAXLINE + 1];
		char *myname;

		myname = argv[0];


		if (argc != 2) {
				fprintf (stderr, "Usage:a.out <IPaddr>\n", myname);
				exit (1);
		}

		if ( (sockfd = socket (AF_INET, SOCK_STREAM, 0)) < 0) {
				perror ("Socket");
				exit (1);
		}


		memset (&serv, 0, sizeof(serv));
		serv.sin_family = AF_INET;
		serv.sin_port = htons (TCPPORT);
		
		if (inet_pton (AF_INET, argv[1], &serv.sin_addr) <= 0) {
				perror ("Inet_pton");
				exit (1);
		}
		

		if (connect (sockfd, (SA *)&serv, sizeof(serv) ) < 0)  {
				perror ("Connect");
				exit (1);
		}
		

		while ( 1 ) {
			
			FD_ZERO(&rfds);
			FD_SET(sockfd, &rfds);
			FD_SET(0, &rfds);

			select(sockfd+1, &rfds, NULL, NULL, 0);

			if(FD_ISSET(0, &rfds)){

				fgets(recvline, MAXLINE, stdin);
				if(!write (sockfd, recvline, strlen(recvline)))
					perror("WRITE");

			} else {

				if(!(n = read (sockfd, recvline, MAXLINE)))
					perror("READ");
				recvline[n] = '\0';


				if (fputs (recvline, stdout) == EOF)
					fprintf (stderr, "fputs error\n");

			}


		}

}
