#ifndef __ser_h
#define __ser_h

# include <stdio.h>
# include <sys/socket.h>
# include <sys/types.h>
# include <sys/select.h>
# include <stdlib.h>
# include <string.h>
# include <netinet/in.h>
# include <poll.h>


#define MAXLINE 4096
#define SA struct sockaddr /* max text line length */

#endif
