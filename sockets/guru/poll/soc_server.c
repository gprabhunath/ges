						/*...............server code for two services............*/

#include "ser.h"
#define LISTENQ 5
#define TCPPORT 13 
#define TCPPORT1 15 


int main(int argc, char **argv)
{

	int listenfd[2], connfd[2], n, retval;
	fd_set rfds;
	socklen_t len;
	struct sockaddr_in servaddr[2], cliaddr[2];
	char buff[MAXLINE];
	struct timeval ticks;
	struct pollfd server[2];

	/* creating socket with port number TCPPORT*/

	listenfd[0] = socket(AF_INET, SOCK_STREAM, 0);
	memset(&servaddr[0], 0, sizeof(servaddr));
	
	servaddr[0].sin_family = AF_INET;
	servaddr[0].sin_addr.s_addr = htonl(INADDR_ANY);
	servaddr[0].sin_port = htons(TCPPORT);

	bind(listenfd[0], (SA *)&servaddr[0], sizeof(servaddr));
	listen(listenfd[0], LISTENQ);

	/* creating socket with port number TCPPORT1*/

	listenfd[1] = socket(AF_INET, SOCK_STREAM, 0);
	
	memset(&servaddr[1], 0, sizeof(servaddr));
	
	servaddr[1].sin_family = AF_INET;
	servaddr[1].sin_addr.s_addr = htonl(INADDR_ANY);

	servaddr[1].sin_port = htons(TCPPORT1);

	bind(listenfd[1], (SA *)&servaddr[1], sizeof(servaddr));
	listen(listenfd[1], LISTENQ);

	printf ("PID = %d \n", getpid());

	server[0].fd = listenfd[0];
	server[0].events = POLLIN;
	server[1].fd = listenfd[1];
	server[1].events = POLLIN;

	for(;;) {	
	
		

		printf ("Blocking on poll\n");
		//select(listenfd[1]+1, &rfds, NULL, NULL, 0);
		poll(server, 2, -1);

		//if(FD_ISSET(listenfd[0], &rfds))
		if (server[0].revents & POLLIN) {
			
			if ( (connfd[0] = accept(listenfd[0], (SA *)&cliaddr[0], &len)) < 0 )/*accpeting the request from socket 1*/
				perror("accept by socket 1");
		
			printf ("Blocking on fgets, Waiting for user input \n");
			fgets(buff, MAXLINE, stdin);

			if(!write(connfd[0], buff, strlen(buff)))/*writing message to the accepted connection from socket 1*/

				perror("WRITE");


		} else if (server[1].revents & POLLIN) {
			
			if ( (connfd[1] = accept(listenfd[1], (SA *)&cliaddr[1], &len) ) < 0)/*accpeting the request from socket 2*/
				perror("accept by socket 2");
		
			printf ("Blocking on fgets, Waiting for user input \n");
			fgets(buff, MAXLINE, stdin);

			if(!write(connfd[1], buff, strlen(buff)))/*writing message to the accepted connection from socket 2*/
				perror("WRITE");

		}

	}

	return 0;
}

