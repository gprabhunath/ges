
#include <stdio.h>
//#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
//#include <linux/in.h>

int main()
{
	printf ("Size of sa_family_t = %d \n", sizeof (sa_family_t));
	printf ("Size of in_port_t = %d \n", sizeof (in_port_t));
	printf ("Size of struct sockaddr_in = %d \n", sizeof (struct sockaddr_in));
	printf ("Size of struct in_addr = %d \n", sizeof (struct in_addr));
	printf ("Size of socklen_t = %d \n", sizeof (socklen_t));

	return 0;
}
