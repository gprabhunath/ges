
 /* A simple TCP Server */

#include <stdio.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>
#include <arpa/inet.h>

#define MAXLINE 16	/* max text line length*/
#define SA struct sockaddr
#define LISTENQ	5	/* requests in queue */	
#define TCPPORT 13

char buff[MAXLINE];
int main(int argc, char **argv)
{
 	int 	listenfd, connfd;
	socklen_t	len;
 	struct 	sockaddr_in servaddr, cliaddr;
 	time_t 	ticks;
	int a = 1;				/* Set SO_REUSEADDR */
	int status;
 	
 	listenfd = socket(AF_INET, SOCK_STREAM, 0);
	if (listenfd < 0)
	{
		perror ("socket failed:");
		exit (1);
	}
 	
 	memset(&servaddr, 0, sizeof(servaddr));
 	servaddr.sin_family = AF_INET;
 	servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
 	servaddr.sin_port = htons(TCPPORT);          /* day time server */

 	setsockopt(listenfd, SOL_SOCKET, SO_REUSEADDR, &a, sizeof(a));

 	status = bind(listenfd, (SA *)&servaddr, sizeof(servaddr));
	if (status < 0)
	{
		perror ("bind failed:");
		exit (1);
	}

 	status = listen(listenfd, LISTENQ);
	if (status < 0)
	{
		perror ("listen failed:");
		exit (1);
	}
 
 	for (;;) {
 		connfd = accept(listenfd, (SA *) &cliaddr, &len);
		if (connfd <= 0)
		{
			perror ("accept failed:");
			exit (1);
		}
		printf ("Connection establishment successfully");
		getchar();	

		strncpy (buff, "Hello CR-16 \n", sizeof (buff));
	 	write(connfd, buff, sizeof(buff));
		printf ("Data Sent successfully");

	 	close(connfd);
		

 	} // End of for(;;)

} // End of main()	
