
 /* A Simple Daytime client */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>

#define MAXLINE 6000	/* max text line length*/
#define SA struct sockaddr
#define PORT 5000

int
main(int argc, char **argv)
{
	int	sockfd, n;
	char	recvline[MAXLINE + 1];
	struct sockaddr_in servaddr;
	char *myname;
	
	myname = argv[0];
	if (argc != 2) {
		fprintf(stderr, "usage: a.out <IPaddress>\n", myname);
		exit(1);
	}
	
	if ( (sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
	 	perror("socket");
	 	exit(1);
	}
	
	memset(&servaddr, 0, sizeof (servaddr));
	servaddr.sin_family = AF_INET;
	servaddr.sin_port = htons(PORT);
	
	if (inet_pton(AF_INET, argv[1], &servaddr.sin_addr) <= 0) {
		perror("inet_pton");
		exit(1);
	}	
		
	if (connect(sockfd, (SA *)&servaddr, sizeof(servaddr)) < 0) {
		perror("connect");
		exit(1);
	}

	while( (n = recv(sockfd, recvline, MAXLINE, MSG_WAITALL)) > 0) {
	#if 0
		recvline[n] = '\0';
		if (fputs(recvline, stdout) == EOF)
			fprintf(stderr, "fputs error\n");
	#endif

		printf ("Number of bytes received = %d \n", n);
	}
	if (n < 0)
	fprintf(stderr, "read error");
	exit(0);
        
}	



