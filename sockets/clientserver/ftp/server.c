
 /* A Simple Daytime Server */

#include <stdio.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <string.h>

#define MAXLINE 4096	/* max text line length*/
#define SA struct sockaddr
#define LISTENQ	5	/* requests in queue */	

int
main(int argc, char **argv)
{
 	int 	listenfd, connfd;
 	struct 	sockaddr_in servaddr;
 	char 	buff[MAXLINE];
 	time_t 	ticks;
	int a = 1;				/* Set SO_REUSEADDR */
	int fd = 0;
 	
	
 	listenfd = socket(AF_INET, SOCK_STREAM, 0);
 	
 	memset(&servaddr, 0, sizeof(servaddr));
 	servaddr.sin_family = AF_INET;
 	servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
 	servaddr.sin_port = htons(80);          /* day time server */

 	setsockopt(listenfd, SOL_SOCKET, SO_REUSEADDR, &a, sizeof(a));
 		
 	bind(listenfd, (SA *)&servaddr, sizeof(servaddr));
 	
 	listen(listenfd, LISTENQ);
 	
 	for (;;) {
 		connfd = accept(listenfd, (SA *) NULL, NULL);

 		write(connfd, buff, strlen(buff));
 		close(connfd);
 	}
}	
