  /* 	WHOIS SERVER- OBTAIN Information from the user of diff. machine */

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <pwd.h>



#define BACKLOG	 	05	/* of requests we are willing to queue */
#define MAXHOSTNAME	32	/* maximum host name length we tolerate */

main(int argc, char **argv)
{
	int s, t;			/* socket descriptor */
	int i;	 			/* general purpose integers */
	struct sockaddr_in sa, isa;	/* Internet socket addr. structure */
	struct hostent *hp; 		/* result of host name lookup */
	struct servent *sp;		/* result of service lookup */
	char *myname;			/* pointer to name of this program  */
	char localhost[MAXHOSTNAME+1]; 	/* local host name as charecter string */
/*	char c;				/ * Takes care of exit of server.c */
	int a = 1;				/* Set SO_REUSEADDR */
	
		
	myname = argv[0];
		
	/*
	 * Look up the WHOIS service
	 */
	if ((sp = getservbyname("whois", "tcp")) == NULL)  {
		fprintf(stderr, "%s: No whois service on this host \n", myname);
		exit(1);
	}

	/*
	 * Get our own host information
	 */
	gethostname(localhost, MAXHOSTNAME);
	if ((hp = gethostbyname(localhost)) == NULL)  {
		fprintf(stderr, "%s: cannot get local host info ?\n", myname);
		exit(1);
	}
	
	/*
	 * Put WHOIS socket number and our address info
	 * into the socket structure
	 */
	
	sa.sin_port = sp->s_port;  	
	bcopy((char *)hp->h_addr, (char *)&sa.sin_addr, hp->h_length);
	sa.sin_family = hp->h_addrtype;
	
	/*
	 * Allocate an open socket for incoming connections
	 */
	if((s = socket(hp->h_addrtype, SOCK_STREAM, 0)) < 0) {
		perror("socket");
		exit(1);
	}
	setsockopt(s, SOL_SOCKET, SO_REUSEADDR, &a, sizeof(a));
	/*
	 * Bind the socket to the service port so we hear
	 * incoming connections
	 */
	if (bind(s, &sa, sizeof sa) < 0)  {
	 	perror("bind");
	 	exit(1);
	}

	/*
	 * Set maximum connections we will fall behind
	 */
	listen(s, BACKLOG);
	/*
	 * Go into an infinite loop waiting for new connections
	 */
	while(1) {
		i = sizeof isa;
		/*
		 * We hang in accept () while waiting for new customers
		 */
		if ((t = accept(s, &isa, &i)) < 0) {
			perror("accept");
			exit(1);
		}
		whois(t); 	/* perform the actual WHOIS service */
	}
	close(t);
	close(s);
	
}

/*
 * Get the WHOIS request from remote host and format a reply.
 */
 whois(sock)
 int sock;
 {
 	struct passwd *p;
 	char buf[BUFSIZ+1];
 	int i;
 	
 	/*
 	 * Get one line request
 	 */
 	if ((i = read(sock, buf, BUFSIZ)) <= 0)
 		return;
 	buf[i] = '\0'; 	/* Null terminates */
 	/*
 	 * Look up the requested user and format reply
 	 */
 	if ((p = getpwnam(buf)) == NULL)
 		strcpy(buf, "User not found\n");
 	else
 		sprintf (buf, "%s: %s\n", p->pw_name, p->pw_passwd);
 	
 	/*
 	 * Return reply
 	 */
 	write (sock, buf, strlen(buf));
 	return;
 	
 }


  	 	
	
	
