
 /* A Simple Daytime client */

#include <stdio.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <time.h>
#include <curses.h>
#include <signal.h>


#define MAXLINE 20	/* max text line length*/
#define SA struct sockaddr

void intr_handle(int);
void proper_exit(void);
void calctime();


int
main(int argc, char **argv)
{
	int	sockfd, n;
	char	recvline[MAXLINE + 1];
	struct sockaddr_in servaddr;
	char *myname;
	time_t 	t ;
	struct tm *loc;	
	
	initscr();
	myname = argv[0];
	if (argc != 2) {
		fprintf(stderr, "usage: a.out <IPaddress>\n", myname);
		exit(1);
	}
	
	if ( (sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
	 	perror("socket");
	 	exit(1);
	}
	
	bzero(&servaddr, sizeof (servaddr));
	servaddr.sin_family = AF_INET;
	servaddr.sin_port = htons(13);
	
	if (inet_pton(AF_INET, argv[1], &servaddr.sin_addr) <= 0) {
		perror("inet_pton");
		exit(1);
	}	
		
	if (connect(sockfd, (SA *)&servaddr, sizeof(servaddr)) < 0) {
		perror("connect");
		exit(1);
	}
	signal(SIGTERM, intr_handle);
	while( (n = read(sockfd, recvline, MAXLINE)) > 0) {
		recvline[n] = '\0';
		if (fputs(recvline, stdout) == EOF)
			fprintf(stderr, "fputs error\n");
	
		t = time(NULL);
        	loc = localtime(&t);
        	mvprintw(5,10,"\nLogin Time : %2d : %2d : %2d\n", loc->tm_hour, loc->tm_min, loc->tm_sec);
        	refresh();
        	calctime();
        	atexit(proper_exit);
        }	
	if (n < 0)
		fprintf(stderr, "read error");
	endwin();	
	exit(0);
}	

void proper_exit(void)
{
  printf("Properly exited\n");
  endwin();
  exit(0);
}


void calctime()
{
	  time_t tp, tp1;
	  int hr= -1, min= 0, sec=0, i=1;

	  tp = time(NULL);

	  while(1) {
		
		  tp1 = time(NULL);
		  	
		  if ((tp1 - tp) % 3600 == 0 && i) {
		  	hr += 1;
		  	min = 0;
		  	sec = 0;
		  	i = 0;
		  }else
		   if ((tp1 - tp) % 60 == 0 && i) {
	          	min += 1;
	          	sec = 0;
	          	i = 0;
	           }else
	            if ((tp1-tp) % 60 != 0) {
	                sec = (tp1-tp) % 60;
	            	i = 1;
	            }	
	            	
                mvprintw(8,10,"Time used -> %2d : %2d : %2d \n", hr, min, sec);	
                refresh();
	 }
}	          	


void intr_handle(int n)
{
   endwin();
   exit(0);
}


