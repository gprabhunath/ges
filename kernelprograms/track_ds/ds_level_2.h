// Level 2 Data Structures of task_struct

struct list_head {
	struct list_head *next;
	struct list_head *prev;
}

struct sched_class {
	sched_class *next
	task_struct
}

struct sched_entity {
	load_weight
	rb_node
	#ifdef CONFIG_FAIR_GROUP_SCHED
		sched_entity
		cfs_rq
}

struct hlist_head {
	struct hlist_node *first;
}

struct sched_info {
}

// include/linux/sched.h
struct mm_struct {
	vm_area_struct
	rb_root
	pgd_t
	atomic_t
	rw_semaphore
	spinlock_t
	completion
	kioctx
}

struct linux_binfmt {
	struct linux_binfmt *next;
	module
}

struct pid_link {
	hlist_node
	pid
}

struct completion {
	wait_queue_head_t
}

struct timespec {
}

struct group_info {
	atomic_t
}

struct user_struct {
	atomic_t
	#ifdef CONFIG_KEYS
		key
	hlist_node
}

struct sysv_sem {
	sem_undo_list
}

struct thread_struct {
	desc_struct
	vm86_struct
}

struct fs_struct {
	rwlock_t
	dentry
	vfsmount
}

struct files_struct {
	atomic_t
	fdtable
	embedded_fd_set
	file
}

struct nsproxy {
	atomic_t
	spinlock_t
	uts_namespace
	ipc_namespace
	mnt_namespace
	pid_namespace
	user_namespace
}

struct signal_struct {
	atomic_t
	wait_queue_head_t
	task_struct
	sigpending
	list_head
	hrtimer
	pid
	tty_struct
	rlimit
	key
	#ifdef CONFIG_BSD_PROCESS_ACCT
		pacct_struct
	#ifdef CONFIG_TASKSTATS
		taskstats
	#ifdef CONFIG_AUDIT
		tty_audit_buf
}

struct sighand_struct {
	atomic_t
	k_sigaction
	spinlock_t
	wait_queue_head_t
}

struct sigpending {
	list_head
	sigset_t
}

struct audit_context {
	timespec
	audit_names
	dentry
	vfsmount
	audit_context
	audit_aux_data
}

struct plist_head {
	list_head
	list_head
	spinlock_t
}

struct rt_mutex_waiter {
	plist_node
	task_struct
	rt_mutex
}

struct mutex_waiter {
	list_head
	task_struct
	mutex
}

struct held_lock {
	lock_class
	lockdep_map
}

struct bio {
	bio
	block_device
	bio_vec
}

struct reclaim_state {
}

struct backing_dev_info {
}

struct io_context {
	atomic_t
	task_struct
	as_io_context
	rb_root
}

struct task_io_accounting {
}

struct mempolicy {
	atomic_t
	zonelist
	nodemask_t
}

struct cpuset {
	cpumask_t
	nodemask_t
	list_head
	cpuset
	dentry
	fmeter
}

struct robust_list_head {
	robust_list
}

struct compat_robust_list_head {
	compat_robust_list
}

struct futex_pi_state {
	list_head
	rt_mutex
	task_struct
	atomic_t
}

struct rcu_head {
	rcu_head
}

struct pipe_inode_info {
	wait-queue_head_t
	page
	fasync_struct
	inode
	pipe_buffer
}

struct task_delay_info {
	spinlock_t
	timespec
}
