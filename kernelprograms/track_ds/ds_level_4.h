
// 4th Level data structures of task_struct

struct rq {
	spinlock_t
	load_stat
	cfs_rq
	#ifdef CONFIG_FAIR_GROUP_SCHED
		list_head	leaf_cfs_rq_list
	rt_rq
	task_struct
	mm_struct
	#ifdef CONFIG_SMP
		sched_domain
		task_struct
		list_head
	#ifdef CONFIG_SCHEDSTATS
		sched_info
	lock_class_key
}

struct raw_prio_tree_node {
	prio_tree_node
	prio_tree_node
	prio_tree_node
}

struct anon_vma {
	spinlock_t
}

struct vm_operations_struct {
	page
	#ifdef CONFIG_NUMA
		mempolicy
}

struct raw_spinlock_t {
}

struct aio_ring_info {
	page
	spinlock_t
	page
}

struct delayed_work {
	work_struct
	timer_list
}

struct module_kobject {
	kobject
	module
	kobject
}

struct module_param_attrs {
	attribute_group
	param_attribute
}

struct module_attribute {
	attribute
}

struct kobject {
	kref
	list_head
	kobject
	kset
	kobj_type	
	sysfs_dirent
	wait_queue_head_t
}

struct kernel_symbol {
}

struct exception_table_entry {
}

struct mod_arch_specific {
}

struct bug_entry {
}

struct module_ref {
}

struct module_sect_attrs {
	attribute_group
	module_sect_attr
}

struct sem_undo {
	sem_undo
	sem_undo
}

struct vm86_regs {
}

struct revectored_struct {
}

struct raw_rwlock_t {
}

struct lockdep_map {
	lock_class_key
	lock_class
}

struct qstr {
}

struct dentry_operations {
}

struct super_block {
	list_head
	file_system_type
	super_operations
	dquot_operations
	quotactl_ops
	export_operations
	dentry
	rw_semaphore
	mutex
	xattr_handler
	list_head	s_inodes
	list_head 	s_dirty
	list_head	s_io
	hlist_head 	s_anon
	list_head	s_files
	block_device
	mtd_info
	list_head	s_instances
	quota_info
	mutex
}

struct dcookie_struct {
	dentry	
	vfsmount
	list_head	hash_list
}

struct fd_set {
}

struct path {
	vfsmount
	dentry
}

struct file_operations {
	module
}

struct fown_struct {
	rwlock_t
	pid
}

struct file_ra_state {
}

struct address_space {
	inode
	radix_tree_root
	prio_tree_root
	list_head
	address_space_operations
	backing_dev_info
	list_head
	address_space
}
	
struct kref {
	atomic_t
}

struct new_utsname {
}

struct ipc_ids {
	mutex
	ipc_id_ary
	ipc_id_ary
}

struct pidmap {
}

struct hrtimer_clock_base {
	hrtimer_cpu_base
	rb_root
	rb_node
}

struct tty_driver {
	cdev
	module
	ktermios
	proc_dir_entry
	tty_driver
	tty_struct
	ktermios	
	ktermios
	list_head tty_drivers
}

struct tty_ldisc {
	module
}

struct ktermios {
}

struct winsize {
}

struct tty_bufhead {
	delayed_work
	semaphore
	tty_buffer
	tty_buffer
	tty_buffer
}

struct work_struct {
	atomic_long_t
	list_head entry
}

struct thread_info {
	task_struct
	exec_domain
	restart_block
}

struct lockdep_subclass_key {
}

struct stack_trace {
}

struct lock_class_key {
	lockdep_subclass_key
}

struct semaphore {
	atomic_t
	wait_queue_head_t
}

struct hd_struct {
	kobject
	kobject
}

struct gendisk {
	hd_struct
	block_device_operations
	request_queue
	device
	kobject
	kobject
	kobject
	timer_rand_state
	disk_stats
	work_struct
}

struct backing_dev_info {
}

struct zonelist_cache {
}

struct zone {
	per_cpu_pageset
	free_area
	list_head	active_list
	list_head	inactive_list
	pglist_data
}

struct kmem_cache {
	kmem_cache_node
	list_head list
	#ifdef CONFIG_SLUB_DEBUG
		kobject
	page
}

struct inode {
	dentry
}

struct file_lock {
	file_lock
	list_head	fl_link
	list_head	fl_block
	file 	*fl_pid
	fasync_struct
	file_lock_operations
	lock_manager_operations
	nfs_lock_info
	nfs4_lock_info
	list_head	link
}

struct dquot {
	hlist_node
	list_head
	list_head
	list_head
	mutex
	super_block
	mem_dqblk
}

struct cdev {
	kobject
	module
	file_operations
	list_head
}

struct dnotify_struct {
	dnotify_struct
	file
	fl_owner_t // files_struct
}

struct pipe_buf_operations {
}
	
