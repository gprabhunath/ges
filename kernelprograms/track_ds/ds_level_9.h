// Data Structures of level 9

struct in_addr {
}

struct rpc_xprt {
	kref
	rpc_xprt_ops
	rpc_timeout
	sockaddr_storage
	rpc_wait_queue
	rpc_wait_queue
	rpc_wait_queue
	rpc_wait_queue
	rpc_wait_queue
	list_head
	rpc_rqst
	work_struct
	timer_list
	rpc_task
	list_head
	stat
}

struct rpc_procinfo {
}

struct rpc_auth {
	rpc_authops
	rpc_cred_cache
}

struct rpc_stat {
	rpc_program
}

struct rpc_iostats {
}

struct rpc_rtt {
}

struct rpc_program {
	rpc_version
	rpc_stat
}

struct nfs_client {
	sockaddr_in
	list_head
	list_ead
	rpc_clnt
	nfs_rpc_ops
	rb_root
	rb_root
	rw_semaphore
	list_head
	rb_root
	spinlock_t
	delayed_work
	rpc_wait_queue
	timespec
	idmap
}

struct nfs_server {
	nfs_client
	list_head
	list_head
	rpc_clnt
	rpc_clnt
	nfs_iostats
	backing_dev_info
	nfs_fsid
}

struct rpc_cred {
	hlist_node
	list_head
	rcu_head
	rpc_auth
	rpc_credops
}

struct rpc_wait_queue {
	list_head
}

struct nfs4_client {
	list_head
	list_head
	list_head
	list_head
	list_head
	xdr_netobj
	svc_cred
	nfs4_callback
}

struct xdr_netobj {
}

struct nfs4_replay {
}

struct stateid_opaque_t {
}

	
