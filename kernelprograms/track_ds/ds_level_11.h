// Data Structures of 11 level


struct xdr_buf {
	kvec
	page
}

struct rpc_message {
	rpc_procinfo
	rpc_cred
}

struct rpc_call_ops {
}

struct workqueue_struct {
	cpu_workqueue_struct
	list_head
}

struct rpc_wait {
	list_head
	list_head
	rpc_wait_queue
}

struct inode_operations {
	dentry
}

struct idmap_msg {
}

struct idmap_hashtable {
	idmap_hashent
}

