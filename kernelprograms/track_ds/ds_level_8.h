// Data Structures of 8 level

struct elv_fs_entry {
	attribute
}

struct rchan_callbacks {
	dentry
}

struct rchan_buf {
	rchan
	timer_list
	dentry
	kref
	page
}

struct sockaddr_in {
	in_addr
}

struct rpc_clnt {
	kref
	list_head	cl_clients
	list_head	cl_tasks
	spinlock_t
	rpc_xprt
	rpc_procinfo
	rpc_auth
	rpc_stat
	rpc_iostats
	rpc_rtt
	vfsmount
	dentry
	rpc_clnt
	rpc_rtt
	rpc_program
}

struct nsm_handle {
	list_head	sm_link	
	atomic_t
	sockaddr_in
}

struct nfs4_state_owner {
	nfs_unique_id
	nfs_client
	nfs_server
	rb_node
	rpc_cred
	list_head	so_states
	list_head	so_delegations
	nfs_seqid_counter
	rpc_sequence
}

struct rpc_sequence {
	rpc_wait_queue
	spinlock_t
	list_head list
}

struct nfs4_stateowner {
	kref
	list_head	
	list_head	
	list_head	
	list_head	
	list_head	
	list_head	
	nfs4_client
	xdr_netobj
	nfs4_replay
}

struct nfs4_file {
	kref
	list_head	
	list_head
	list_head
	inode
}

struct stateid_t {
	stateid_opaque_t
}
