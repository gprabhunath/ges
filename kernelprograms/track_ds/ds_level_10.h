// Data Structures of 10 level

struct rpc_xprt_ops {
}

struct rpc_timeout {
}

struct sockaddr_storage {
}

struct rpc_rqst {
	rpc_xprt
	xdr_buf
	xdr_buf
	rpc_task
	page
	list_head
	xdr_buf
}

struct rpc_task {
	list_head
	rpc_clnt
	rpc_rqst
	rpc_message
	rpc_call_ops
	timer_list
	workqueue_struct
	work_struct
	rpc_wait
	rcu_head
}

struct stat {
}

struct rpc_authops {
	module
	rpc_auth
	rpc_cred
	rpc_cred
}

struct rpc_cred_cache {
	hlist_head
	spinlock_t
}

struct rpc_version {
	rpc_procinfo
}

struct nfs_rpc_ops {
	dentry
	inode_operations
	inode_operations
}

struct idmap {
	dentry
	wait_queue_head_t
	idmap_msg
	mutex
	mutex
	idmap_hashtable
	idmap_hashtable
}

struct nfs_iostats {
}

struct nfs_fsid {
}

struct rpc_credops {
}

struct svc_cred {
	group_info
}

struct nfs4_callback {
	rpc_program
	rpc_stat
	rpc_clnt
}
