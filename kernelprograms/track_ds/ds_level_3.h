// Level 3 Data Structures of task_struct

struct load_weight {
}

struct key {
	// search
}
// include/linux/rbtree.h
struct rb_node {
	rb_node
	rb_node
}

struct cfs_rq {
	load_weight
	rb_root
	rb_node
	sched_entity
	rq
	list_head leaf_cfs_rq_list
}

struct vm_area_struct {
	mm_struct
	vm_area_struct
	rb_node
	raw_prio_tree_node
	list_head
	anon_vma
	vm_operations_struct
	file
	mempolicy
}

struct rb_root {
	rb_node
}

struct pdg_t {
}

struct atomic_t {
}

struct rw_semaphore {
	list_head
	lockdep_map
}

struct spinlock_t {
	raw_spinlock_t
	lockdep_map
}

struct kioctx {
	atomic_t
	mm_struct
	kioctx
	wait_queue_head_t
	spinlock_t
	list_head
	list_head
	aio_ring_info
	delayed_work
}

struct module {
	list_head
	module_kobject
	module_param_attrs
	module_attribute
	kobject
	kernel_symbol
	exception_table_entry
	mod_arch_specific
	list_head
	bug_entry
	module_ref
	list_head
	task_struct
	module_sect_attrs
}

struct hlist_node {
	hlist_node *next, **pprev
}

struct pid {
	// search
}

struct wait_queue_head_t {	
	spinlock_t
	list_head
}

struct sem_undo_list {
	atomic_t
	spinlock_t
	sem_undo
}

struct desc_struct {
}

struct vm86_struct {
	vm86_regs
	revectored_struct
}

struct rwlock_t {
	raw_rwlock_t
	lockdep_map
}	

struct dentry {
	atomic_t
	spinlock_t
	inode
	hlist_node
	dentry
	qstr
	list_head
	list_head
	rcu_head
	list_head
	list_head
	dentry_operations
	super_block
	dcookie_struct
}

struct vfsmount {
	list_head
	vfsmount
	dentry
	dentry
	super_block
	list_head mnt_mounts
	list_head mnt_child
	list_head mnt_list
	list_head mnt_expire
	list_head mnt_share
	list_head mnt_slave_list
	list_head mnt_slave
	vfsmount
	mnt_namespace
	atomic_t
}

struct fdtable {
	file
	fd_set
	rcu_head
	fdtable
}

struct embedded_fd_set {
}

struct file {	
	list_head fu_list
	rcu_head
	path
	file_operations
	fown_struct
	file_ra_state
	list_head f_ep_links
	spinlock_t
	address_space
}

struct uts_namespace {
	kref
	new_utsname
}

struct ipc_namespace {
	kref
	ipc_ids
}

struct mnt_namespace {
	atomic_t
	vfsmount
	list_head
	wait_queue_head_t
}

struct pid_namespace {
	kref
	pidmap
	task_struct
}

struct user_namespace {
	kref
	hlist_head
	user_struct
}

struct sigpending {
	list_head list
}

struct hrtimer {
	rb_node
	hrtimer_clock_base
	list_head cb_entry
}

struct tty_struct {
	tty_driver
	tty_ldisc
	mutex
	ktermios
	pid
	pid
	winsize
	tty_struct
	fasync_struct
	tty_bufhead
	work_struct
	list_head
	mutex
	mutex
	work_struct
}

struct rlimit {
}

struct pacct_struct {
	k_sigaction
	spinlock_t
	wait_queue_head_t
}

struct taskstats {
}

struct tty_audit_buf {
	atomic_t
	mutex
}

struct k_sigaction {
	sigaction
}

struct sigset_t {
}

struct timespec {
}

struct audit_names {
}

struct audit_context {
	timespec
	audit_names
	dentry
	vfsmount
	audit_context
	audit_aux_data
	audit_aux_data
}

struct audit_aux_data {
	audit_aux_data
}

struct plist_node {
	plist_head list
} 	

struct rt_mutex {
	spinloc_t	
	plist_head
	task_struct
}

struct mutex {
	atomic_t
	spinlock_t
	list_head
	thread_info
	lockdep_map
}

struct lock_class {
	list_head hash_entry
	list_head lock_entry
	lockdep_subclass_key
	stack_trace
	list_head
}

struct lockdep_map {
	lock_class_key
	lock_class
}

struct block_device {
	inode
	mutex
	semaphore
	list_head bd_inodes
	#ifdef CONFIG_SYSFS
		list_head bd_holder_list

	block_device
	hd_struct
	gendisk
	list_head bd_lisk
	backing_dev_info
}

struct bio_vec {
	page
}

struct as_io_context {
}

struct zonelist {
	zonelist_cache
	zone
}

struct nodemask_t {
}

struct cpumask_t {
}

struct fmeter {
	spinlock_t
}

struct robust_list {
	robust_list
}

struct compat_robust_list {
}

struct rcu_head {
	rcu_head
}

struct page {
	address_space
	kmem_cache
	page
	list_head
}

struct fasync_struct {
	fasync_struct
	file
}

struct inode {
	hlist_node	i_hash
	list_head	i_list
	list_head	i_sb_list
	list_head	i_dentry
	timespec
	timespec
	timespec
	mutex
	rw_semaphore	
	inode_operations
	file_operations
	super_block
	file_lock
	address_space	i_mapping
	address_space	i_data
	#ifdef CONFIG_QUOTA
		dquot
	list_head
	pipe_inode_info
	block_device
	cdev
	#ifdef CONFIG_DNOTIFY
		dnotify_struct
	#ifdef CONFIG_INOTIFY
		list_head
		mutex
}

struct pipe_buffer {
	page
	pipe_buf_operations
}

	
	
	

	
 
