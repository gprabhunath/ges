// Level 1 Data Structures of task_struct

// include/linux/sched.h

struct task_struct {

	list_head run_list
	sched_class
	sched_entity

	#ifdef CONFIG_PREEMPT_NOTIFIERS
		struct hlist_head preempt_notifiers

	#if defined(CONFIG_SCHEDSTATS) || defined(CONFIG_TASK_DELAY_ACCT)
		struct sched_info	

	list_head tasks
	list_head ptrace_children
	list_head ptrace_list
	mm_struct
	linux_binfmt
	task_struct *real_parent
	task_struct *parent
	list_head children
	list_head sibling
	task_struct *group_leader
	pid_link
	list_head thread_group
	completion
	timespec
	list_head cpu_timers[3]
	group_info
	user_struct
	#ifdef CONFIG_KEYS
		key
	
	#ifdef CONFIG_SYSVIPC
		sysv_sem

	thread_struct
	fs_struct
	files_struct
	nsproxy
	signal_struct
	sighand_struct
	sigpending
	audit_context

	#ifdef CONFIG_RT_MUTEXES
		plist_head
		rt_mutex_waiter

	#ifdef CONFIG_DEBUG_MUTEXES
		mutex_waiter

	#ifdef CONFIG_LOCKDEP
		held_lock

	bio
	reclaim_state
	backing_dev_info
	io_context
	task_io_accounting

	#ifdef CONFIG_NUMA
		mempolicy

	#ifdef CONFIG_CPUSETS
		cpuset

	robust_list_head
	
	#ifdef CONFIG_COMPAT
		compat_robust_list_head

	list_head pi_state_list
	futex_pi_state
	rcu_head
	pipe_inode_info

	#ifdef  CONFIG_TASK_DELAY_ACCT
		task_delay_info

} // End of struct task_struct

