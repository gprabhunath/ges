// Data Structures of level 7

struct bin_attribute {
	attribute
}

struct nand_oobfree {
}

struct quota_format_type {
	quota_format_ops
	module
	quota_format_type
}

struct v1_mem_dqinfo {
}

struct v2_mem_dqinfo {
}

struct elevator_ops {
}

struct elevator_type {
	list_head list
	elevator_ops
	elv_fs_entry
	module
}

struct mempool_t {
	spinlock_t
	wait_queue_head_t
}

struct rchan {
	rchan_callbacks
	kref
	rchan_buf
	list_head list
	dentry
}

struct class_device {
	list_head 	node
	kobject
	class
	device
	class_device
	attribute_group
}

struct blocking_notifier_head {
	rw_semaphore
	notifier_block
}

struct bus_attribute {
	attribute
}

struct device_attribute {
	attribute
}

struct pm_message_t {
}

struct nlm_host {
	hlist_node
	sockaddr_in
	sockaddr_in
	rpc_clnt
	rw_semaphore
	mutex
	list_head	h_lockowners
	list_head	h_granted
	list_head	h_reclaim
	nsm_handle
}

struct nfs4_state {
	list_head open_states
	list_head inode_states
	list_head lock_states
	nfs4_state_owner
	inode
}

struct nfs_seqid_counter {
	rpc_sequence
}

struct nfs_unique_id {
	rb_node
}

struct nfs4_stateid {
	list_head	st_has
	list_head	st_perfile
	list_head	st_perstateowner
	list_head	st_lockowners
	nfs4_stateowner
	nfs4_file
	stateid_t
	file
	nfs4_stateid
}
