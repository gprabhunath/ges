/*********************************************************
 * FILE  	:- inout1.c
 * AUTHOR	:- Mihir R. Shah
 * PURPOSE	:- To generate output same as lspci
 * PROBLEM	:- One class of Display Controllers not recognized
 * *******************************************************/



#include<stdio.h>
#include<sys/io.h>
#include<unistd.h>

#define DATABASE_FILE "/usr/share/hwdata/pci.ids"

/*******************************************************************
 * Function 	:- 	readline()
 * AUTHOR	:- 	Mihir R. Shah
 * Return Type	:-	void
 * Arguments	:- 	File pointer localfp, Character pointer localstr
 * Application	:-	To read a single line from the file including 
 * 			escape characters and store in the character pointer
 * *****************************************************************/

void readline(FILE *localfp,char *localstr)
{
	char c;
//	char str[100];
	int i=0;
//	printf("\nHere\n");
//	fscanf(localfp,"%[^\n]\n",localstr);
//	printf("\nHere 3\n");
//	printf("\nAlso Here\n");
	c=fgetc(localfp);
//	printf("\nHere 3\n");
	while(c!='\n' && !(feof(localfp)))
	{
//		printf("%d ",i);
		localstr[i]=c;
		c=fgetc(localfp);
		i++;
	}
	localstr[i]='\0';
//	printf("\nHere 4\n");
//	printf("%s",localstr);
	return;

}


/****************************************************************
 * Function	:-	convert()
 * AUTHOR	:-	Mihir R. Shah
 * Return Type	:-	unsigned short int
 * Arguments	:-	Character pointer localstr, integer option
 * Application	:-	To convert a segment of string into a
 * 			hexadecimal number of 16 bits
 ****************************************************************/
unsigned short int convert(char * localstr,int option)
{
	unsigned short int return_val;
	int i=0;
	int val;
	return_val=0x0000;

	if(localstr[i+option] < 'a')
		return_val += localstr[i+option]-48;
	else 
		return_val += localstr[i+option]-87;
	i++;	
	while(i<4)
	{
	return_val *= 0x10;
	if(localstr[i+option] < 'a')
		return_val += localstr[i+option]-48;
	else 
		return_val += localstr[i+option]-87;
	i++;
	}
	return return_val;
}
/****************************************************************
 * Function	:-	makereadyforprint()
 * AUTHOR	:-	Mihir R. Shah
 * Return Type	:-	int
 * Arguments	:-	Character pointer str, integer option
 * Application	:-	To remove the ID from the string and then
 * 			store it back to print the string without
 * 			the ID in it.
 ****************************************************************/

int makereadyforprint(char * str,int option)
{
	char * str1[500];
	int i;
	for(i=0;str[i];i++)
	{
		str1[i]=str[i+option+5];
	}

	for(i=0;str1[i];i++)
	{
		str[i]=str1[i];
	}
	str[i]='\0';
}

/****************************************************************
 * Function	:-	checkdevice()
 * AUTHOR	:-	Mihir R. Shah
 * Return Type	:-	int
 * Arguments	:-	unsigned short int devid(DEVICE ID), 
 * 			File pointer localfp(To the database file)
 * Application	:-	To search for the name of device from the
 * 			Device ID
 ****************************************************************/
int checkdevice(unsigned short int devid,FILE *localfp)
{
//	FILE *fp;
	char str[500];
	unsigned short int did=0xffff;
	int temp;
//	localfp=fopen("pci.ids","r");
	
	while(did!=devid)
	{
	readline(localfp,str);
//	printf("%s\n",str);
		if(str[0]=='\t')
			switch(str[1])
			{
				case '0' :
				case '1' :
				case '2' :
				case '3' :
				case '4' :
				case '5' :
				case '6' :
				case '7' :
				case '8' :
				case '9' :
				case 'a' :
				case 'b' :
				case 'c' :
				case 'd' :
				case 'e' :	did=convert(str,1);
						break;
				case 'f' :	if(str[1] == 'f' && str[2] == 'f' && str[3] == 'f')
							break;
						did=convert(str,1);
							break;
	
				default :// 	printf("Here 1\n");
						break;
			}
		if(str[1] == 'f' && str[2] == 'f' && str[3] == 'f')
		{
			break;	
		}
	}
//	printf("HERE\n");
	if(did==devid)
	{
//		printf("\"%x\"",vid);
		makereadyforprint(str,1);
	//	printf(" %x ",devid);
		printf("%s",str);
//		scanf("%d",&temp);
	}
	else
	{
		printf(" Unknown device %x",devid);
	}
}

/****************************************************************
 * Function	:-	checkvendor()
 * AUTHOR	:-	Mihir R. Shah
 * Return Type	:-	int
 * Arguments	:-	unsigned short int vevid(VENDOR ID), 
 * 			File pointer localfp(To the database file)
 * Application	:-	To search for the name of Vendor from the
 * 			Vendor ID
 ****************************************************************/
int checkvendor(unsigned short int venid,unsigned short int devid)
{
	FILE *fp;
	char str[500];
	unsigned short int vid=0xffff;

	fp=fopen(DATABASE_FILE,"r");
	
	while(vid!=venid)
	{
	readline(fp,str);
//	printf("%s\n",str);
		switch(str[0])
		{
			case '0' :
			case '1' :
			case '2' :
			case '3' :
			case '4' :
			case '5' :
			case '6' :
			case '7' :
			case '8' :
			case '9' :
			case 'a' :
			case 'b' :
			case 'c' :
			case 'd' :
			case 'e' :	vid=convert(str,0);
					break;
			case 'f' :	if(str[1] == 'f' && str[2] == 'f' && str[3] == 'f')
						return;
					vid=convert(str,0);
					break;
	
			default :// 	printf("Here 1\n");
					break;
		}
	}
//	printf("HERE\n");
	if(vid==venid)
	{
//		printf("\"%x\"",vid);
		
		makereadyforprint(str,0);
		printf("%s",str);
		checkdevice(devid,fp);
	}
	fclose(fp);
}

int checkdev(unsigned short int devid,unsigned short int venid)
{
	
	checkvendor(venid,devid);
//	printf("\nHere 2\n");
}

/****************************************************************
 * Function	:-	convert_class()
 * AUTHOR	:-	Mihir R. Shah
 * Return Type	:-	int
 * Arguments	:-	Character Pointer localstr, 
 * 			integer option
 * Application	:-	To convert the segment of string to
 * 			8 bit hexadecimal number
 ****************************************************************/
int convert_class(char * localstr,int option)
{
	unsigned short int return_val;
	int i=0;
	int val;
	option+=2;
	return_val=0x0000;

	if(localstr[i+option] < 'a')
		return_val += localstr[i+option]-48;
	else 
		return_val += localstr[i+option]-87;
	i++;	
	while(i<2)
	{
	return_val *= 0x10;
	if(localstr[i+option] < 'a')
		return_val += localstr[i+option]-48;
	else 
		return_val += localstr[i+option]-87;
	i++;
	}
	return return_val;
	

}
/****************************************************************
 * Function	:-	checkclass()
 * AUTHOR	:-	Mihir R. Shah
 * Return Type	:-	int
 * Arguments	:-	unsigned int classid(CLASS ID)
 * Application	:-	To search for the name of Class from the
 * 			Class ID in the Database
 ****************************************************************/
int checkclass(unsigned int classid)
{
	int i;
	unsigned int new_classid=0;
	FILE *fp;
	char str[500];
	char str_backup[500];
	unsigned short int classid1=0;
	unsigned short int classid2=0;
	unsigned short int clid1=0xffff;
	unsigned short int clid2=0xffff;
//	printf("CL:-\t %x \t",classid);
	for(i=0;i<7;i++)
	{
		new_classid <<=4;
		new_classid|=(classid & 0xf);
		classid>>=4;
	}

	
//	printf("\t %x\t",new_classid);

	classid1 = (new_classid & 0xff);
	classid2 = ((new_classid & 0xff00) >> 8);
	fp=fopen(DATABASE_FILE,"r");
	
	while(clid1!=classid1)
	{
	if(feof(fp))
		{
			printf(" : ");
			return;
		}
	readline(fp,str);
//	printf("%s\n",str);
	if(str[0] == 'C')
		switch(str[2])
		{
			case '0' : switch(str[3])
						{
							case 'd' :
							case 'b' : 	//clid1=convert_class(str,0);
										break;
							default :	classid2 &= 0x0f;
									//	clid1=convert_class(str,0);
										break;
						}
						//break;

			case '1' :		clid1=convert_class(str,0);
							break;

			default : break;
		}
	}

	for(i=0;str[i];i++)
	{
		str_backup[i] = str[i];
	}
	if(clid1==classid1)
	{
		while(clid2!=classid2)
		{
			if(feof(fp))
			{
				makereadyforprint(str_backup,1);
				printf("%s : ",str_backup);	
				return;
			}
		readline(fp,str);
//		printf("%s\n",str);
		if(str[0] == '\t')
			switch(str[2])
			{
				case '0' :
				case '1' :
				case '2' :
				case '3' :
				case '4' :
				case '5' :
				case '6' :
				case '7' :
				case '8' : str[3]='0';
				case '9' :
				case 'a' :
				case 'b' :
				case 'c' :
				case 'd' :
				case 'e' :
				case 'f' :	clid2=convert_class(str,-1);
	//					printf("\n\t%x\t",clid2);
						break;
		
				default : break;
			}
		}
	
	}
	if(clid2==classid2)
	{
		makereadyforprint(str,0);
		printf("%s:",str);	
	}
	fclose(fp);
}

int main()
{
	unsigned char En_Reserved=0x80;
	unsigned char BusNo=0x00;
	unsigned char DeviceNo=0x00;
	unsigned char FunNo=0x00;
	unsigned char DoubleWord=0x09;
	unsigned int  value=0x00;
	unsigned short int port=0x0000;
	unsigned int ret=0x00;
	unsigned short int DeviceID=0x0000;
	unsigned short int VendorID=0x0000;
	unsigned short int Prev_DeviceID=0x0000;
	unsigned short int Prev_VendorID=0x0000;
	iopl(3);

	for(BusNo=0x00;BusNo < 0xff;BusNo++)
	{
		for(DeviceNo=0x00;DeviceNo<=0x1f;DeviceNo++)
		{
			for(FunNo=0x00;FunNo<0x08;FunNo++)
			{

				value=0x00;
		
				value = En_Reserved;

				value<<=8;
				value |=BusNo;
r
	
				value<<=5;
				value |=DeviceNo;
	
				value<<=3;
				value |=FunNo;
		
				value<<=8;
			
				DoubleWord = 0x00;
				value &= 0xffffff00;
				value |= DoubleWord;
				
				port=0x0cf8;
				asm("outl %%eax,%%dx;": :"d"(port),"a"(value));
				port=0x0cfc;
				asm("inl %%dx,%%eax;" :"=a"(ret) :"d"(port));

				if(ret!=0xffffffff)
				{
					VendorID = (ret & 0x0000ffff);
					DeviceID = ((ret & 0xffff0000) >> 16);
//					printf("%x\t%x\t",VendorID,DeviceID);
				}
				if(Prev_VendorID != VendorID || Prev_DeviceID!=DeviceID)
				{
			
					DoubleWord = 0x09;			
					value &= 0xffffff00;
					value |=DoubleWord;
		
						port=0x0cf8;
					asm("outl %%eax,%%dx;": :"d"(port),"a"(value));
					port=0x0cfc;
					asm("inl %%dx,%%eax;" :"=a"(ret) :"d"(port));
					if(ret!=0xffffffff)
					{
						printf("\n%02x:%02x.%1x ",BusNo,DeviceNo,FunNo);
						checkclass(ret);
		//				printf("%x\n",ret);	
						}
//					printf("\nHere 1\n");	
	
					DoubleWord = 0x00;
					value &= 0xffffff00;
					value |= DoubleWord;
					port=0x0cf8;
					asm("outl %%eax,%%dx;": :"d"(port),"a"(value));
					port=0x0cfc;
					asm("inl %%dx,%%eax;" :"=a"(ret) :"d"(port));
	
					if(ret!=0xffffffff)
					{
						VendorID = (ret & 0x0000ffff);
						DeviceID = ((ret & 0xffff0000) >> 16);
//					printf("%x\t%x\t",VendorID,DeviceID);
						checkvendor(VendorID,DeviceID);
						Prev_VendorID = VendorID;
						Prev_DeviceID = DeviceID;
					}
				}
			}
		}
	}
	printf("\n\n");
}
