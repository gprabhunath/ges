
/** System Includes **/
#include <linux/init.h>
#include <linux/module.h>
#include <linux/sched.h>
#include <linux/device.h>
#include <linux/net.h>
#include <net/sock.h>

#define PF_GES	28

MODULE_LICENSE("Dual BSD/GPL");

/** Constants **/
#define FIRST_MINOR 	10
#define NR_DEVS	1 // Number of device numbers


int my_sock_create (struct net *, struct socket *, int, int);
int my_sock_release (struct socket *sock);
int my_sock_connect (struct socket *sock, struct sockaddr *vaddr,
                      int sockaddr_len, int flags);
int my_sock_bind  (struct socket *sock, struct sockaddr *myaddr,
                      int sockaddr_len);
int my_sock_listen (struct socket *sock, int len);
int my_sock_accept (struct socket *sock,
                      struct socket *newsock, int flags);
int my_sock_sendmsg (struct kiocb *iocb, struct socket *sock,
                      struct msghdr *m, size_t total_len);
int my_sock_recvmsg (struct kiocb *iocb, struct socket *sock,
                      struct msghdr *m, size_t total_len,
                      int flags);

	
// Initialization routines
static int myInit (void);
static void myExit (void);

static const struct proto_ops my_sock_ops = {
	.family = PF_GES,
	.owner = THIS_MODULE,
	.release = my_sock_release,
	.connect = my_sock_connect,
	.bind = my_sock_bind,
	.listen = my_sock_listen,
	.accept = my_sock_accept,
	.sendmsg = my_sock_sendmsg,
	.recvmsg = my_sock_recvmsg
};

static const struct net_proto_family my_sock_family_ops = {
		.family = PF_GES,
		.owner = THIS_MODULE,
		.create = my_sock_create
};

int my_sock_release (struct socket *sock)
{
	printk (KERN_INFO "Releasing socket \n");
	
	return 0;
}

int my_sock_create (struct net *net, struct socket *sock, int protocol, int kern)
{
	printk (KERN_INFO "Creating my socket \n");
	
	sock->state = SS_UNCONNECTED;
	sock->ops = &my_sock_ops;

	return 0;
}


int my_sock_connect (struct socket *sock, struct sockaddr *vaddr,
                      int sockaddr_len, int flags)
{
	printk (KERN_INFO "Entering my_sock_connect\n");
	return 0;
}
int my_sock_bind  (struct socket *sock, struct sockaddr *myaddr,
                      int sockaddr_len)
{
	printk (KERN_INFO "Entering my_sock_bind\n");
	return 0;
}
int my_sock_listen (struct socket *sock, int len)
{
	printk (KERN_INFO "Entering my_sock_listen\n");
	return 0;
}
int my_sock_accept (struct socket *sock,
                      struct socket *newsock, int flags)
{
	printk (KERN_INFO "Entering my_sock_accept\n");
	return 0;
}
int my_sock_sendmsg (struct kiocb *iocb, struct socket *sock,
                      struct msghdr *m, size_t total_len)
{
	printk (KERN_INFO "Entering my_sock_sendmsg\n");
	return 0;
}
int my_sock_recvmsg (struct kiocb *iocb, struct socket *sock,
                      struct msghdr *m, size_t total_len,
                      int flags)
{
	printk (KERN_INFO "Entering my_sock_recvmsg\n");
	return 0;
}
/*
 * myInit : init function of the kernel module
 *
 */
static int __init myInit (void)
{
	int status;

	printk(KERN_INFO "Initializing Pseudo protocol stack \n");
	
	status = sock_register(&my_sock_family_ops);
	if (status < 0)
	{		
		return status;
	}

	return 0;

}

/*
 * myExit : cleanup function of the kernel module
 */

static void myExit (void)
{
	printk (KERN_INFO "Exiting Pseudo stack \n");
	sock_unregister(PF_GES);
	

	return;
}

module_init(myInit);
module_exit(myExit);
