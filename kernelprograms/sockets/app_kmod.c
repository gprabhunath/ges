#include <stdio.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/socket.h>

#define PF_GES	28

int main()
{

   int fd;

   fd = socket (PF_GES, SOCK_STREAM, 0);
	if (fd < 0)
	{
		perror ("socket failed:");
	}

	bind (fd, NULL, 0);
	connect (fd, NULL, 0);
	//listen (fd, 0);
	//accept (fd, NULL, NULL);
	write (fd, &fd, sizeof  (int));
	read (fd, &fd, sizeof  (int));

   close(fd);

   return 0;
}
