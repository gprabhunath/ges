#include <stdlib.h>
#include <stdio.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>

int A=400;
int B=500;
int main (void)

{
	int fd;
	fd = open ( "/dev/pgWalk",O_RDWR);
	if (fd < 0)
	{
		perror("Open failed");
		exit (1);
	}
	A=A+1;
	printf("Before Kernel Exec &A = %p\n",&A);
	ioctl(fd,getpid(),&A);
	printf("After Kernel Exec &A = %p\n",&A);
	printf ("Value of A after Kernel Exec = %d \n", A);

	close (fd);
	return 0;


}
