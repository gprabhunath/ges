#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/sched.h>
#include <linux/highmem.h>
#include <asm-i386/page.h>
MODULE_LICENSE("Dual BSD/GPL");

#define VA(addr) addr+0xc0000000
int Majno;
char *devname;
int myioctl (struct inode *inode, struct file *filep, 
		unsigned int command, unsigned long arg);
int myopen (struct inode *inode, struct file *filep);
struct file_operations fops = { ioctl:myioctl,open:myopen};
//module_param (Majno,int,0000);
module_param (devname,charp,0000);
struct page *pgaddr;
int *kaddr;

EXPORT_SYMBOL(Majno);
static int module_initialization(void)
{
	Majno = register_chrdev (0,devname,&fops);
	printk(KERN_INFO "Hello world\n");
	return 0;
}

int myopen (struct inode *inode, struct file *filep)
{
	printk("<0> open successful\n");
	return 0;
}

int myioctl (struct inode *inode, struct file *filep, 
		unsigned int command, unsigned long arg)

{
 	pgd_t *pgdbase;
 	pgd_t *pgd_indexed_addr;
	pte_t *pt_base;
	unsigned long pfn;
	unsigned long virt_addr;
 	pte_t *pt_indexed_addr;
	struct task_struct *task;
	int *addr;
	int pgd_offset;
	int pt_offset;
	int page_offset;
	int value;
		
	addr=(int *)arg;
	pgd_offset=(unsigned int )addr >> 22;
	pt_offset=((unsigned int)addr<<10) >> 22;
	page_offset=((unsigned int)addr&0x00000fff);
	
	printk("<0>pgd_offset=%x\t pt_offset=%x\t page_offset=%x \n",pgd_offset,pt_offset,page_offset);
	printk("<0>addr = %p \n",addr);
	printk("<0>addr_val = %d \n",*addr);
        task=find_task_by_pid(command);	
        pgdbase=task->mm->pgd;
	printk("<0>pgdbase = %p \n",(unsigned long *)pgdbase);
	pgd_indexed_addr=pgdbase+pgd_offset;
	printk("<0>pgd_indexed_addr = %p \n",(unsigned long *)pgd_indexed_addr);
	pt_base= (pte_t *)((pgd_indexed_addr->pgd&0xFFFFF000)+0xC0000000);
	printk("<0>pt_base = %p \n",(unsigned long *)pt_base);
	pt_indexed_addr=pt_base+pt_offset;
	printk("<0>pt_indexed_addr = %p \n",(unsigned long *)pt_indexed_addr);
	pfn = pt_indexed_addr->pte_low&0xFFFFF000;
	printk("<0>pfn = %p \n",(unsigned long *)pfn);
	pgaddr=pfn_to_page(pfn>>12);
	printk("<0>pgaddr = %p \n",(unsigned long *)pgaddr);
        if (NULL==(kaddr=(int *)kmap(pgaddr))) 
                 printk("<0>kmap failed\n");	
	else 
		printk("<0>kaddr = %p \n",(int *)kaddr);
	virt_addr=(unsigned long)kaddr|page_offset;
        value=*((int *)virt_addr);
	printk("<0>address = %p \n",(int *)virt_addr);
	printk("<0>value = %d \n",value);
	return 0;


}
static void  module_cleanup(void)
{
	printk(KERN_INFO"BYE\n");
	if (kaddr!=NULL) kunmap(pgaddr);
	unregister_chrdev(Majno,devname);
}

module_init(module_initialization);
module_exit(module_cleanup);
