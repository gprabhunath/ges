#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/sched.h>
#include <linux/highmem.h>
MODULE_LICENSE("Dual BSD/GPL");

#define VA(addr) addr+0xc0000000
int Majno;
char *devname;

int myioctl (struct inode *inode, struct file *filep, 
		unsigned int pid, unsigned long arg);
int myopen (struct inode *inode, struct file *filep);

struct file_operations fops = { ioctl:myioctl,open:myopen};
//module_param (Majno,int,0000);
module_param (devname,charp,0000);
struct page *pgaddr;
int *kaddr;

EXPORT_SYMBOL(Majno);
static int module_initialization(void)
{
	Majno = register_chrdev (0,devname,&fops);
	printk(KERN_INFO KERN_INFO "Hello world\n");
	return 0;
}

int myopen (struct inode *inode, struct file *filep)
{
	printk(KERN_INFO " open successful\n");
	return 0;
}

static void  module_cleanup(void)
{
	printk(KERN_INFO "BYE\n");
	if (kaddr!=NULL) 
		kunmap(pgaddr);
	unregister_chrdev(Majno,devname);
	
	return;
}

int myioctl (struct inode *inode, struct file *filep, 
		unsigned int pid, unsigned long arg)

{
 	pgd_t *pgdbase;
 	pgd_t *pgd_indexed_addr;
	pte_t *pt_base;
	unsigned long pfn;
	unsigned long virt_addr;
 	pte_t *pt_indexed_addr;
	struct task_struct *task;
	int *addr;
	int pgd_offset;
	int pt_offset;
	int page_offset;
	int value;
		
	addr=(int *)arg;
	*(addr) = 501;
	pgd_offset=(unsigned int )addr >> 22;
	pt_offset=((unsigned int)addr<<10) >> 22;
	page_offset=((unsigned int)addr&0x00000fff);
	
	printk(KERN_INFO "pgd_offset=%x\t pt_offset=%x\t page_offset=%x \n",
					pgd_offset,pt_offset,page_offset);
	printk(KERN_INFO "Virtual addr = %p \n",addr);
	printk(KERN_INFO "addr_val = %d \n",*addr);
	// Get the pointer of task_struct
    task=find_task_by_vpid(pid);	
    pgdbase=task->mm->pgd; // Get the base address of PGD
	printk(KERN_INFO "pgdbase = %p \n",(unsigned long *)pgdbase);

	pgd_indexed_addr=pgdbase+pgd_offset; // Get the entry in the PGD
	printk(KERN_INFO "pgd_indexed_addr = %p \n",
				(unsigned long *)pgd_indexed_addr);

	// Get Page Table Base address
	pt_base= (pte_t *)((pgd_indexed_addr->pgd&0xFFFFF000)+0xC0000000);
	printk(KERN_INFO "pt_base = %p \n",(unsigned long *)pt_base);
	pt_indexed_addr=pt_base+pt_offset; // Get the entry in the Page Table
	printk(KERN_INFO "pt_indexed_addr = %p \n",
				(unsigned long *)pt_indexed_addr);
	pfn = pt_indexed_addr->pte_low&0xFFFFF000; // Get the page base address
	printk(KERN_INFO "pfn = %p \n",(unsigned long *)pfn);

#if 0
	pgaddr=pfn_to_page(pfn>>12); // Get struct page address
	printk(KERN_INFO "pgaddr = %p \n",(unsigned long *)pgaddr);

	// Get the mapped Kernel Virtual Address of the page base
    if (NULL==(kaddr=(int *)kmap(pgaddr))) 
    	printk(KERN_INFO "kmap failed\n");	
	else 
		printk(KERN_INFO "kaddr = %p \n",(int *)kaddr);

	virt_addr=(unsigned long)kaddr|page_offset; // Index into the base
    value=*((int *)virt_addr); // Access the value of the memory
	printk(KERN_INFO "address = %p \n",(int *)virt_addr);
	printk(KERN_INFO "value = %d \n",value);
	*((int *)virt_addr) = value + 20; // Change the value in the location
#endif

	return 0;
}

module_init(module_initialization);
module_exit(module_cleanup);
