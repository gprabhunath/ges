#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/sched.h>
#include <linux/file.h>
#include <linux/mount.h>
MODULE_LICENSE("Dual BSD/GPL");

#define VA(addr) addr+0xc0000000
int Majno;
char *devname;
int myioctl (struct inode *inode, struct file *filep, 
		unsigned int command, unsigned long arg);
int myopen (struct inode *inode, struct file *filep);

// User defined functions

void files_struct_parse (int);
void vmaParse (void);

struct file_operations fops = { ioctl:myioctl,open:myopen};
//module_param (Majno,int,0000);
module_param (devname,charp,0000);
struct page *pgaddr;
int *kaddr;

EXPORT_SYMBOL(Majno);
static int module_initialization(void)
{
	Majno = register_chrdev (0,devname,&fops);
	printk(KERN_INFO "Hello world\n");
	return 0;
}

int myopen (struct inode *inode, struct file *filep)
{
	printk(KERN_INFO " open successful\n");
	return 0;
}

void files_struct_parse (int fd)
{
	struct task_struct *myTask;
	struct inode *pwdInode;
	struct inode *fdInode; // inode pertaining to the open file /dev/myChar
	struct files_struct *myFiles; // pointer to open descriptor
	//struct file *myFile;	
	struct fdtable *myFdt;
	struct fdtable myFdt1;
	int i; // loop count

	//myTask = find_task_by_pid(pid);
	myTask = current; 
	
	// Checking for the inode of pwd	
	pwdInode = myTask->fs->pwd->d_inode;
	printk (KERN_INFO "Inode of pwd = %lu \n", pwdInode->i_ino);

    //fdInode = myTask->fles->fd_array[fd]->f_mapping->host;
	//printk (KERN_INFO "Inode info of /dev/myChar= %lu \n", fdInode->i_ino);

    myFiles = myTask->files;
    printk (KERN_INFO "\nValue of File Descriptor %d \n", fd); 
    printk (KERN_INFO "\nchecking for the elements in files_struct \n");
	printk (KERN_INFO "Size of files_struct = %d \n", 
				sizeof (struct files_struct));
	printk (KERN_INFO "Value of count = %d \n", myFiles->count.counter);
	printk (KERN_INFO "Value of next fd = %d \n", myFiles->next_fd);
	i = 0;
	while (i < 32)
	{
		if (myFiles->fd_array[i] != NULL)
		{
        	fdInode = myFiles->fd_array[i]->f_mapping->host;
			printk ("Index = %d Inode = %lu \n", i, fdInode->i_ino);
		}
        i++;
	}

    // Accessing *fdt member of files_struct
	printk (KERN_INFO "\nAccessing *fdt member of files_struct \n");

	if (myFiles->fdt != NULL)
	{
		printk(KERN_INFO "fdtable is present \n");
		myFdt = myFiles->fdt;
		printk(KERN_INFO "Value of max_fds = %d \n", myFdt->max_fds);
		i = 0;
		while (i < 32)
		{
			if (myFdt->fd[i] != NULL)
			{
				fdInode = myFdt->fd[i]->f_mapping->host;
				printk ("Index = %d Inode = %lu \n", i, fdInode->i_ino);
			}
			i++;
			
		}

		// Checking for the next node of the fdtable
		if (myFiles->fdt->next != NULL)
			printk(KERN_INFO "fdt->next is present \n");

		// Printing the filesystem type
		printk (KERN_INFO "File System type = %s \n",
		        myFdt->fd[3]->f_vfsmnt->mnt_sb->s_type->name);

	} 
	
	// Accessing fdt member of files_struct
	printk (KERN_INFO "\nAccessing fdtab member of files_struct \n");
	myFdt1 = myFiles->fdtab;
	printk(KERN_INFO "Value of max_fds = %d \n", myFdt1.max_fds);
  	i = 0;
	while (i < 32)	
	{
		if (myFdt1.fd[i] != NULL)
		{
        	fdInode = myFdt1.fd[i]->f_mapping->host;
			printk ("Index = %d Inode = %lu \n", i, fdInode->i_ino);
		}
        i++;
	}
	
	
	return ;

	
}

int myioctl (struct inode *inode, struct file *filep, 
		unsigned int pid, unsigned long fd)

{
	files_struct_parse (fd);

	return 0;
}


static void  module_cleanup(void)
{
	printk(KERN_INFO "BYE\n");
	unregister_chrdev(Majno,devname);
}

module_init(module_initialization);
module_exit(module_cleanup);
