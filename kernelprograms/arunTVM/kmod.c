
#include <linux/init.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/sched.h>
#include <linux/fdtable.h>

MODULE_LICENSE("Dual BSD/GPL");

char *devname;
int majNo;

module_param(devname, charp, 0000);
// Function Declarations for syscall definitions
int myOpen (struct inode *inode, struct file *filep);
int myRelease (struct inode *in, struct file *fp);
long myIoctl (struct file *fp, unsigned int cmd, unsigned long pid);

// Initialization routines
static int myInit (void);
static void myExit (void);

struct file_operations fops = {
			.open = myOpen,
			.release = myRelease, 
			.unlocked_ioctl = myIoctl
			};

static int myInit (void)
{
   printk(KERN_INFO "Initializing the World \n");
#if 1
   majNo = register_chrdev(0,devname,&fops);
   if (majNo < 0)
   {
      printk(KERN_INFO "register_chrdev failed \n");
   } else {
      printk(KERN_INFO "Major Number of the device = %d \n", majNo);
   }
  
#endif
	
   return 0;
}

int myOpen (struct inode *inode, struct file *filep)
{
        printk(KERN_INFO "Open successful\n");
        return 0;
}

int myRelease (struct inode *in, struct file *fp)
{
   printk(KERN_INFO "File released \n");
	if (fp)
        printk (KERN_INFO "Fd ref. count = %ld \n", fp->f_count);
   return 0;
}


static void myExit (void)
{
   printk (KERN_INFO "Exiting the World \n");
   unregister_chrdev(majNo, devname);
   return;
}

long myIoctl (struct file *fp, unsigned int sockfd, unsigned long pid)
{
	struct task_struct *myTask;
	struct file *sockfp;

	printk (KERN_INFO "--- Executing for pid = %lu \n", pid);
	myTask = find_task_by_vpid(pid);
	sockfp = myTask->files->fdt->fd[sockfd];

	printk (KERN_INFO "myTask->files = 0x%08x \n", myTask->files);
	if (fp)
		printk (KERN_INFO "Fd ref. count = %ld \n", fp->f_count);
	if (sockfp)
		printk (KERN_INFO "sockfd ref. count = %ld \n", sockfp->f_count);

	printk (KERN_INFO "--------------------------- \n");

	return 0;
}

module_init(myInit);
module_exit(myExit);
