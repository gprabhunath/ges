#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <sched.h>
#include <sys/syscall.h>
#include <sys/types.h>

/**********************
 * Function protos
**********************/

int child_process();
int parent_process();
int create_fork();
int create_pthread();

/*********************/

int gettid()
{
	return syscall(SYS_gettid);
}

int create_fork()
{
	pid_t pid = 0;
	void *ptr;
	ptr = malloc(0x1000 * 2);
	ptr = ptr + (0x1000 * 2);
	pid = clone(NULL, ptr, SIGCHLD, NULL);
	if (pid > 0) {
		printf("Child has started\n");
		child_process();
		exit(EXIT_SUCCESS);
	} else {
		perror ("clone failed");
		exit (1);
	}
	parent_process();
}

int create_pthread()
{
	pid_t pid;
	void *ptr;
	ptr = malloc(0x1000);
	ptr = ptr + 0x1000;
	//pid =
	/*clone(&child_process, ptr, CLONE_VM | CLONE_SIGHAND | CLONE_THREAD,
	   NULL); */
	if (!pid) {
		printf("Thread is created \n");
	} else {
		sleep(2);
		//      printf("PID %d\n",pid);
		parent_process();
	}
}

int child_process()
{
	printf("I'm a child\n");
	printf("PID %d TGID %d PPID %d\n", gettid(), getpid(), getppid());
	//exit(EXIT_SUCCESS);
}

int parent_process()
{
	printf("I'm the parent\n");
	wait (NULL);
}

int main()
{
	printf("PID %d TGID %d PPID %d\n", gettid(), getpid(), getppid());
	create_fork();
	//create_pthread();
}
