//#define _GNU_SOURCE
#include <stdio.h>
#include <unistd.h>
#if 0
#include <signal.h>
#include <sched.h>
#include <stdlib.h>
#endif


int main()
{
	int ret;

#if 0
	ret = clone (NULL, NULL, SIGCHLD, NULL);
	if (ret < 0)
	{
		perror ("clone failed");
		exit (1);
	}

#endif 

#if 1
	ret = fork();
#endif
	
	return 0;
}
