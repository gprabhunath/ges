#include <stdio.h>
#include <fcntl.h>


int main()
{

	int fd;
	int regfd;
	int ret;

	fd = open("/dev/myChar", O_RDWR);
	
	if (fd < 0)
	   perror("Unable to open the device");
	else
	   printf("File opened Successfully %d\n", fd);

	regfd = open("../kscript.sh", O_RDWR);
	if (regfd < 0)
	   perror("Unable to open the device");
	else
	   printf("File opened Successfully %d\n", regfd);

	ret = ioctl (fd, getpid(), (unsigned long)regfd);
	if (ret < 0)
		perror ("ioctl failed");
	else
		printf ("ioctl successful %d \n", ret);
	

	close (regfd);
	close(fd);

	return 0;
}
