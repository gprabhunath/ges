
#include <linux/init.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/mount.h>
#include <linux/namespace.h>
#include <linux/file.h>

MODULE_LICENSE("Dual BSD/GPL");

char *devname;
int majNo;

module_param(devname, charp, 0000);
// Function Declarations for syscall definitions
int myOpen (struct inode *inode, struct file *filep);
int myRelease (struct inode *in, struct file *fp);
int myIoctl (struct inode *in, struct file *fp, unsigned int, unsigned long);

// Initialization routines
static int myInit (void);
static void myExit (void);

struct file_operations fops = {open:myOpen,release:myRelease,ioctl:myIoctl};

static int myInit (void)
{
   printk(KERN_INFO "Initializing the World \n");

   majNo = register_chrdev(0,devname,&fops);
   if (majNo < 0)
   {
      printk(KERN_INFO "register_chrdev failed \n");
   } else {
      printk(KERN_INFO "Major Number of the device = %d \n", majNo);
   }
   
   return 0;
}

int myOpen (struct inode *inode, struct file *filep)
{
        printk(KERN_INFO "Open successful\n");
        return 0;
}

int myRelease (struct inode *in, struct file *fp)
{
   printk(KERN_INFO "File released \n");
   return 0;
}


static void myExit (void)
{
   printk (KERN_INFO "Exiting the World \n");
   unregister_chrdev(majNo, devname);
   return;
}

module_init(myInit);
module_exit(myExit);

/*
	Experiment : To parse the following vfs related data structures 
	           : super_blocks, inode_unused, inode_in_use, file_systems
*/

#if 0
int myIoctl (struct inode *in, struct file *fp, unsigned int pid, 
             unsigned long A)
{
	struct super_block *sb;
	struct list_head *sbh;
	struct file_system_type *fs, *tfs;

	sbh = get_sb_head(sbh);
	printk (KERN_INFO "Major Minor    dev_name \t fs_name\n");
	printk (KERN_INFO "----- -----    -------- \t -------\n");
	list_for_each_entry(sb, sbh, s_list)
	{
		printk (KERN_INFO "%4u    %u ", 
		                   MAJOR(sb->s_dev), MINOR(sb->s_dev));
		printk ("\t%-11s \t %-11s\n", sb->s_id, sb->s_type->name);
	}
	fs = get_fs_head(fs);
	tfs = fs;
	while (tfs)
	{	
		printk (KERN_INFO "fs_name = %s \n", tfs->name);
		tfs = tfs->next;
	}
	
} // End of myIoctl

#endif

/*
	Experiment : To look into the vfsmount from task_struct
*/

#if 0
int myIoctl (struct inode *in, struct file *fp, unsigned int pid, 
             unsigned long A)
{
	struct task_struct *myTask;
	struct vfsmount *mntHead;
	struct vfsmount *rootmnt;
	struct vfsmount *pwdmnt;
	struct vfsmount *mnt_parent;

	myTask = find_task_by_pid(pid);
	rootmnt = myTask->fs->rootmnt;
	pwdmnt = myTask->fs->pwdmnt;
	mnt_parent = rootmnt->mnt_parent;


	printk (KERN_INFO "\n");
	printk (KERN_INFO "Dev filename of the mounted filesystem descriptor of the root directory = %s \n", rootmnt->mnt_devname);

	printk (KERN_INFO "Name of the mount point directory where this file system is mounted = %s \n", rootmnt->mnt_mountpoint->d_name.name);

	printk (KERN_INFO "Name of the root directory of this file system is = %s \n", rootmnt->mnt_root->d_name.name);

//	printk (KERN_INFO "Dev filename of the mounted filesystem descriptor of the cwd = %s \n", pwdmnt->mnt_devname);

	//printk (KERN_INFO "Name of the mount point directory where the file system is mounted = %s \n", mnt_mntpoint->d_name.name);

	printk (KERN_INFO "\n");

	printk (KERN_INFO "Dev filename of parent filesystem on which this filesystem is mounted = %s \n", mnt_parent->mnt_devname);
	
	printk (KERN_INFO "Name of the mount point directory where this file system is mounted = %s \n", mnt_parent->mnt_mountpoint->d_name.name);

	printk (KERN_INFO "Name of the root directory of the parent file system = %s \n", mnt_parent->mnt_mountpoint->d_name.name);


	// List of dev filename, mnt point directory and root directory of 
	// the filesystem descriptors mounted on this filesystem
	printk (KERN_INFO "\nList of dev filename, mount point directory and root directory of the filesystem descriptors mounted on the native filesystem \n\n");

	list_for_each_entry(mntHead, &(rootmnt->mnt_mounts), mnt_child)
	{
		printk (KERN_INFO "Dev name = %-9s | ", mntHead->mnt_devname);
		printk ("Mnt pt. dir = %-7s | ", mntHead->mnt_mountpoint->d_name.name);
		printk ("Root dir = %s \n", mntHead->mnt_root->d_name.name);
		
	} // End of list_for_each_entry

	// List of dev filename, mnt point directory and root directory of 
	// the filesystem descriptors mounted on rootfs filesystem
	printk (KERN_INFO "\nList of dev filename, mount point directory and root directory of the filesystem descriptors mounted on rootfs filesystem \n\n");
	
	list_for_each_entry(mntHead, &(mnt_parent->mnt_mounts), mnt_child)
	{
		printk (KERN_INFO "Dev name = %s \t", mntHead->mnt_devname);
		printk ("Mnt pt. dir = %s \t", mntHead->mnt_mountpoint->d_name.name);
		printk ("Root dir = %s \n", mntHead->mnt_root->d_name.name);
		
	} // End of list_for_each_entry

	// List of dev filename, mnt point directory and root directory of 
	// the mounted filesystem descriptors belonging to the namespace.

	printk (KERN_INFO "\nList of dev filename, mount point directory and root directory of the mounted filesystem descriptors belonging to the namespace \n\n");
	
	list_for_each_entry(mntHead, &(myTask->namespace->list), mnt_list)
	{
		printk (KERN_INFO "Dev name = %-13s | ", mntHead->mnt_devname);
		printk ("Mnt pt. dir = %-11s | ", mntHead->mnt_mountpoint->d_name.name);
		printk ("Root dir = %s \n", mntHead->mnt_root->d_name.name);
		
	} // End of list_for_each_entry

	printk (KERN_INFO "\n");
	return 0;
} // End of myIoctl

#endif


/*
	Experiment : To get the address of namespace associted to a task, 
	  			 address of the vfsmount, superblock and dentry of the 
				 mounted native filesystem accessed thru the namespace of 
				 all the tasks.
*/

#if 1
int myIoctl (struct inode *in, struct file *fp, unsigned int pid, 
             unsigned long A)
{
	struct task_struct *myTask;
	struct task_struct *curTask;
	struct vfsmount *vfsmnt;

	myTask = find_task_by_pid(pid);

	printk (KERN_INFO "\n");
	printk (KERN_INFO " pid \t namespace \ttask_name      vfsmnt \t sb \t dentry\n\n");

	list_for_each_entry(curTask, &(myTask->tasks), tasks)
	{
		printk (KERN_INFO "%5d \t 0x%08x \t %-10s ", curTask->pid, 
		         curTask->namespace, curTask->comm);

    	list_for_each_entry(vfsmnt, &(curTask->namespace->list), mnt_list)
	    {
			if (!(strcmp (vfsmnt->mnt_devname, "/dev/root")))
		        printk ("0x%08x   0x%08x   0x%08x \n", 
						vfsmnt, vfsmnt->mnt_sb, vfsmnt->mnt_root);

	    }
	}
	printk ("\n");

	printk (KERN_INFO "%5d \t 0x%08x \t %-10s ", curTask->pid, 
		         curTask->namespace, curTask->comm);
    list_for_each_entry(vfsmnt, &(curTask->namespace->list), mnt_list)
	{
		if (!(strcmp (vfsmnt->mnt_devname, "/dev/root")))
	        printk ("0x%08x   0x%08x   0x%08x \n", 
					vfsmnt, vfsmnt->mnt_sb, vfsmnt->mnt_root);
	}
	printk ("\n\n");
	
	return 0;

} // End of myIoctl

#endif

/*
	Experiment : To get the address of vfsmounts in the list of vfsmount 
				 accessed from the namespace of the task structs
*/

#if 0
int myIoctl (struct inode *in, struct file *fp, unsigned int pid, 
             unsigned long A)
{
	struct task_struct *myTask;
	struct task_struct *curTask;
	struct vfsmount *vfsmnt;

	myTask = find_task_by_pid(pid);

	printk (KERN_INFO "\n");
#if 0
	printk (KERN_INFO " pid \t namespace \t task_name \n\n");
	list_for_each_entry(curTask, &(myTask->tasks), tasks)
	{
		printk (KERN_INFO "%5d \t 0x%08x \t %s\n", curTask->pid, 
		         curTask->namespace, curTask->comm);

	}
		printk (KERN_INFO "%5d \t 0x%08x \t %s\n", curTask->pid, 
		         curTaskurTask->namespace, curTask->comm);
#endif

	printk (KERN_INFO "Addresses of vfsmount parsed thru pid = %d \n", pid);
	list_for_each_entry(vfsmnt, &(myTask->namespace->list), mnt_list)
	{
		printk (KERN_INFO "0x%08x -> %s \n", vfsmnt, vfsmnt->mnt_devname);
	}

	printk ("\n");
	printk (KERN_INFO "\n");
} // End of myIoctl

#endif

/*
	Experiment : To print number of inodes belonging to each filesystem
*/

#if 0
int myIoctl (struct inode *in, struct file *fp, unsigned int pid, 
             unsigned long A)
{
	struct super_block *sb;
	struct list_head *sbh;
	struct list_head *inh;
	unsigned int i_count = 0;

	sbh = get_sb_head(sbh);
	list_for_each_entry(sb, sbh, s_list)
	{
		i_count = 0;
		if (sb->s_inodes.next)
		{
			list_for_each(inh, &(sb->s_inodes))
			{
				i_count++;
			}
		}
		printk (KERN_INFO "Number of inodes on device %s is = %u \n",
							sb->s_id, i_count);
			
	}

	printk (KERN_INFO "\n");
	return 0;

} // End of myIoctl
#endif

/*
	Experiment : To figure out the relation between a directory and dentry
	           : To figure out the relation between the directory and the 
			   : file contents of the directory
*/

#if 0
// Local function declarations
void listdentries(struct dentry *dent);

void listdentries(struct dentry *dent)
{
	struct dentry *ldent, *pdent, *sdent;
	struct list_head *denhd;
	int dchild, dsubdirs;

	printk (KERN_INFO "\n");
	// To get the no. of dir entries in the same directory
	dchild = 0;
	list_for_each_entry(ldent, &(dent->d_u.d_child), d_u.d_child)
	{
		dchild++;
		if (ldent->d_inode && ldent->d_inode->i_ino != 0)
		{
			printk (KERN_INFO "Name of the dentry  and inode number = %s %u  ",ldent->d_name.name, ldent->d_inode->i_ino);

			if (ldent->d_subdirs.next != &ldent->d_subdirs)
			{
				printk ("---> SUBDIR\n");
				list_for_each_entry(sdent, &ldent->d_subdirs, d_u.d_child)
				{
					printk ("%s ", sdent->d_name.name);
				}
				printk ("\n");
			}
		printk ("\n");
		}

		if (!ldent->d_inode)
			printk (KERN_INFO "Name of the dentry  and inode number = %s %s \n",ldent->d_name.name, ldent->d_parent->d_name.name);

	} // End of list_for_each_entry
	printk (KERN_INFO "Name of the dentry  and inode number = %s %u 0x%08x \n",ldent->d_name.name, ldent->d_inode->i_ino, ldent->d_parent);

	printk ("\n");
	printk (KERN_INFO "Name of the parent directory = %s 0x%08x\n", 
						dent->d_parent->d_name.name, dent->d_parent);
	printk ("No. of dir dentries in the same parent dir = %d \n", dchild);

	// To get the no. of heads of the list of subdir entries.
	dsubdirs= 0;
	ldent = NULL;
	list_for_each_entry(ldent, &(dent->d_subdirs), d_subdirs)
	{
		dsubdirs++;
		if (ldent->d_parent)
			printk (KERN_INFO "Name of the dentry  and inode number = 0x%08x \n",ldent->d_parent);

#if 0
		if (!ldent->d_inode)
			printk (KERN_INFO "Name of the dentry  and inode number = %s %s \n",ldent->d_name.name, ldent->d_parent->d_name.name);
#endif
	} // End of list_for_each_entry

	printk ("No. of dir dentries in the same parent dir associated with subdirs = %d \n", dsubdirs);

	printk (KERN_INFO "Name of the file = %s \n", dent->d_name.name);

	return ;

} // End of listdentries

int myIoctl (struct inode *in, struct file *fp, unsigned int pid, 
             unsigned long fd)
{
	struct task_struct *myTask;
	struct dentry *dent;
	struct file *ifp;
	struct files_struct *myFiles;

	printk (KERN_INFO "Entering ioctl \n");
	myTask = find_task_by_pid(pid);
	myFiles = myTask->files;
	ifp = myFiles->fdt->fd[fd];
	dent = ifp->f_dentry;

	listdentries(dent);
	//listdentries(dent->d_parent);
	printk (KERN_INFO "\n");
	return 0;

} // End of myIoctl
#endif

/*
	Experiment : To compare the dentry associated with the root directory 
				of the native file system from super_block's member 
				(s_root) and task_struct->fs_struct->dentry (root)
*/
#if 0
int myIoctl (struct inode *in, struct file *fp, unsigned int pid, 
             unsigned long fd)
{
	struct task_struct *myTask;
	struct super_block *sb;
	struct list_head *sbh;
	struct dentry *rdent;

	printk ("\n");
	sbh = get_sb_head(sbh);
	list_for_each_entry(sb, sbh, s_list)
	{
		if (!strcmp(sb->s_id, "sda8"))
			printk (KERN_INFO "dentry s_root on /dev/sda8 = 0x%08x %s \n", 
							sb->s_root, sb->s_root->d_name.name);

	} // End of list_for_each_entry

	myTask = find_task_by_pid(pid);
	rdent = myTask->fs->root;
	printk (KERN_INFO "dentry myTask->fs->root = 0x%08x %s \n",
						rdent, rdent->d_name.name);

	printk ("\n");
	return 0;

} // End of myIoctl
#endif

/*
	Experiment : Relation between vfsmount and dentry of native file system

*/

#if 0
int myIoctl (struct inode *in, struct file *fp, unsigned int pid, 
             unsigned long A)
{
	struct task_struct *myTask;
	struct vfsmount *mntHead;
	struct vfsmount *rootmnt;
	struct vfsmount *mnt_parent;

	myTask = find_task_by_pid(pid);
	rootmnt = myTask->fs->rootmnt;
	mnt_parent = rootmnt->mnt_parent;


	printk (KERN_INFO "\n");
	printk (KERN_INFO "Dev filename of the mounted filesystem descriptor of the root directory = %s \n", rootmnt->mnt_devname);

	printk (KERN_INFO "Name of the mount point directory where this file system is mounted = %s \n", rootmnt->mnt_mountpoint->d_name.name);

	printk (KERN_INFO "Name of the root directory of this file system is = %s \n", rootmnt->mnt_root->d_name.name);

	printk (KERN_INFO "\n");

	printk (KERN_INFO "Dev filename of parent filesystem on which this filesystem is mounted = %s \n", mnt_parent->mnt_devname);
	
	printk (KERN_INFO "Name of the mount point directory where the parent file system is mounted = %s \n", mnt_parent->mnt_mountpoint->d_name.name);

	printk (KERN_INFO "Name of the root directory of the parent file system = %s \n", mnt_parent->mnt_mountpoint->d_name.name);


	// List of dev filename, mnt point directory and root directory of 
	// the filesystem descriptors mounted on this filesystem
	printk (KERN_INFO "\nList of dev filename, mount point directory and root directory of the filesystem descriptors mounted on the native filesystem \n\n");

	printk (KERN_INFO "Dev name = Device file name\n");
	printk (KERN_INFO "Mnt pt. = Name of the mount point directory \
						\n \t\twhere the filesystem is mounted \n");
	printk (KERN_INFO "mnt_mountpoint = Addr of the dentry of the mount \
			\n\t\t point directory where the filesystem is mounted \n");
	printk (KERN_INFO "mnt_root = Addr of the dentry of the root \
					\n \t\tdirectory of the mounted filesystem \n");
	printk ("\n");

	printk (KERN_INFO " Dev name | Mnt pt. | mnt_mountpoint |   mnt_root \n\n");
	list_for_each_entry(mntHead, &(rootmnt->mnt_mounts), mnt_child)
	{
		printk (KERN_INFO "%-9s | %-7s | ", mntHead->mnt_devname, 
						mntHead->mnt_mountpoint->d_name.name);
		printk (" 0x%08x    |  0x%08x \n", mntHead->mnt_mountpoint, mntHead->mnt_root);	
	} // End of list_for_each_entry

	printk ("\n");
	printk (KERN_INFO "Address of the dentry of the mount point directory of the rootfs filesystem = 0x%08x \n", rootmnt->mnt_mountpoint);
	printk (KERN_INFO "Address of the dentry of the root directory of the tnative filesystem = 0x%08x \n", rootmnt->mnt_root );
	printk (KERN_INFO "Address of the dentry of the root directory of the tnative filesystem = 0x%08x \n", myTask->fs->root);

	printk (KERN_INFO "Mount point dev name = %s \n", rootmnt->mnt_mountpoint->d_name.name);

	printk ("\n");

	return 0;
} // End of myIoctl

#endif

