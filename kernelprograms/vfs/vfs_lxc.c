
#include <linux/init.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/mount.h>
#include <linux/namespace.h>
#include <linux/file.h>

MODULE_LICENSE("Dual BSD/GPL");

char *devname;
int majNo;

module_param(devname, charp, 0000);
// Function Declarations for syscall definitions
int myOpen (struct inode *inode, struct file *filep);
int myRelease (struct inode *in, struct file *fp);
int myIoctl (struct inode *in, struct file *fp, unsigned int, unsigned long);

// Initialization routines
static int myInit (void);
static void myExit (void);

struct file_operations fops = {
	open:myOpen,
	release:myRelease,
	unlocked_ioctl:myIoctl
};

static int myInit (void)
{
   printk(KERN_INFO "Initializing the World \n");

   majNo = register_chrdev(0,devname,&fops);
   if (majNo < 0)
   {
      printk(KERN_INFO "register_chrdev failed \n");
   } else {
      printk(KERN_INFO "Major Number of the device = %d \n", majNo);
   }
   
   return 0;
}

int myOpen (struct inode *inode, struct file *filep)
{
        printk(KERN_INFO "Open successful\n");
        return 0;
}

int myRelease (struct inode *in, struct file *fp)
{
   printk(KERN_INFO "File released \n");
   return 0;
}


static void myExit (void)
{
   printk (KERN_INFO "Exiting the World \n");
   unregister_chrdev(majNo, devname);
   return;
}

module_init(myInit);
module_exit(myExit);

/*
	Experiment : To look into the vfsmount from task_struct
*/

#if 1
int myIoctl (struct inode *in, struct file *fp, unsigned int pid, 
             unsigned long A)
{
	struct task_struct *myTask;
	struct vfsmount *mntHead;
	struct vfsmount *rootmnt;
	struct vfsmount *pwdmnt;
	struct vfsmount *mnt_parent;

	myTask = find_task_by_pid(pid);
	rootmnt = myTask->fs->rootmnt;
	pwdmnt = myTask->fs->pwdmnt;
	mnt_parent = rootmnt->mnt_parent;


	printk (KERN_INFO "\n");
	printk (KERN_INFO "Dev filename of the mounted filesystem descriptor of the root directory = %s \n", rootmnt->mnt_devname);

	printk (KERN_INFO "Name of the mount point directory where this file system is mounted = %s \n", rootmnt->mnt_mountpoint->d_name.name);

	printk (KERN_INFO "Name of the root directory of this file system is = %s \n", rootmnt->mnt_root->d_name.name);

//	printk (KERN_INFO "Dev filename of the mounted filesystem descriptor of the cwd = %s \n", pwdmnt->mnt_devname);

	//printk (KERN_INFO "Name of the mount point directory where the file system is mounted = %s \n", mnt_mntpoint->d_name.name);

	printk (KERN_INFO "\n");

	printk (KERN_INFO "Dev filename of parent filesystem on which this filesystem is mounted = %s \n", mnt_parent->mnt_devname);
	
	printk (KERN_INFO "Name of the mount point directory where this file system is mounted = %s \n", mnt_parent->mnt_mountpoint->d_name.name);

	printk (KERN_INFO "Name of the root directory of the parent file system = %s \n", mnt_parent->mnt_mountpoint->d_name.name);


	// List of dev filename, mnt point directory and root directory of 
	// the filesystem descriptors mounted on this filesystem
	printk (KERN_INFO "\nList of dev filename, mount point directory and root directory of the filesystem descriptors mounted on the native filesystem \n\n");

	list_for_each_entry(mntHead, &(rootmnt->mnt_mounts), mnt_child)
	{
		printk (KERN_INFO "Dev name = %-9s | ", mntHead->mnt_devname);
		printk ("Mnt pt. dir = %-7s | ", mntHead->mnt_mountpoint->d_name.name);
		printk ("Root dir = %s \n", mntHead->mnt_root->d_name.name);
		
	} // End of list_for_each_entry

	// List of dev filename, mnt point directory and root directory of 
	// the filesystem descriptors mounted on rootfs filesystem
	printk (KERN_INFO "\nList of dev filename, mount point directory and root directory of the filesystem descriptors mounted on rootfs filesystem \n\n");
	
	list_for_each_entry(mntHead, &(mnt_parent->mnt_mounts), mnt_child)
	{
		printk (KERN_INFO "Dev name = %s \t", mntHead->mnt_devname);
		printk ("Mnt pt. dir = %s \t", mntHead->mnt_mountpoint->d_name.name);
		printk ("Root dir = %s \n", mntHead->mnt_root->d_name.name);
		
	} // End of list_for_each_entry

	// List of dev filename, mnt point directory and root directory of 
	// the mounted filesystem descriptors belonging to the namespace.

	printk (KERN_INFO "\nList of dev filename, mount point directory and root directory of the mounted filesystem descriptors belonging to the namespace \n\n");
	
	list_for_each_entry(mntHead, &(myTask->namespace->list), mnt_list)
	{
		printk (KERN_INFO "Dev name = %-13s | ", mntHead->mnt_devname);
		printk ("Mnt pt. dir = %-11s | ", mntHead->mnt_mountpoint->d_name.name);
		printk ("Root dir = %s \n", mntHead->mnt_root->d_name.name);
		
	} // End of list_for_each_entry

	printk (KERN_INFO "\n");
	return 0;
} // End of myIoctl

#endif
