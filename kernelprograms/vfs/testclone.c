
#include <stdio.h>
#include <errno.h>
#include <linux/sched.h>
#include <fcntl.h>
#include <stdlib.h>


/*
	Experiment : To understand mnt namespace. 
	Definition : mnt_namespace is the list of the mounted filesystems visible
				 associated to the task. If a particular task is cloned, then
				 the new tasks inherits the mnt_namespace of the parent task.
				 If the new task is cloned with CLONE_NEWNS, then a copy of
				 the mounted filesystem list is created for the new task,
				 though they will be accessing the same superblocks accessed
				 thru the parent's task, with only difference that if the
				 new task mounts any new filesystem, then it is only visible
				 to the new task and the tasks created by this new task but
				 not to the parent task.

	1. Created a new task using clone system call with CLONE_NEWNS flag  
	   and myfunc function.

	2. Using ioctl in myfunc function, found that there was a new address
	   for the namespace in the task_struct of the newly created task and
	   also new addresses for the vfsmount lists headed by the namespace.
	   But the superblock pointed to by the vfsmount was same as that of the
	   parent. Thus infering that a copy of the newspace with the duplicated 
	   vfsmount lists has been created for the new task.

	3. Thus, any file created/modified/deleted by myfunc in the filesystem 
	   would be reflected on the native filesystem and thus visible in the 
	   parent process filesystem view also. 

 	4. But any filesystem mounted on to the child task (myfunc) would be 
	   explicitly visible to the child task only and not visible to the 
	   parent task or any other task which has a filesystem view other 
	   than the child task.

*/
int myfunc (void *);

int myfunc(void *A)
{
	int fd;
	int ret;
	int data;

	printf ("I am in myfunc %d \n", getpid());

    fd = open("/dev/myChar", O_RDWR);
    if (fd < 0)
		perror("Unable to open the device");
    else
	    printf("File opened in myfunc() Successfully %d\n", fd);


	ret = mount ("/dev/uba1", "/mnt", "vfat", 0, "posix");
	if (ret < 0)
		perror("Mount failed");
	else
		printf ("Mount success \n");

    ioctl (fd, getpid(), 0);
    close(fd);

	system ("touch /mnt/myfile");
	system ("ls /mnt");
	system ("rm -f /mnt/myfile");


	getchar();
	sleep(5);
	if (!umount ("/mnt"))
		printf ("Umount successfull \n");
	else
		perror("Unmount failed:");
	return 0;

} // End of myfunc

int B = 3;
void *cstack;
int main()
{
	int cret;
	int fd;
	
	cstack = malloc(4096 * 2);


	printf ("Main func %d \n", getpid());

	// clone sys call with the flag CLONE_NEWNS
	cret = clone(myfunc, cstack + 8191, CLONE_NEWNS, (void *)&B );
//	cret = clone(myfunc, cstack + 8191, (void *)&B );

	if (cret > 0)
	{
		printf ("Pid of the child = %d \n", getpid());
    	fd = open("/dev/myChar", O_RDWR);

    	if (fd < 0)
			perror("Unable to open the device");
	    else
		    printf("File opened Successfully %d\n", fd);

 	   	//ioctl (fd, getpid(), 0);
   		close(fd);
		wait();

	}
	else if (cret == 0)
		printf ("Pid of the parent = %d \n", cret);
	else
		perror("clone failed");

	return 0;

}
