#include<linux/init.h>
#include<linux/module.h>
#include<linux/config.h>
#include<linux/kernel.h>
#include<linux/slab.h>
#include<linux/fs.h>
#include<linux/errno.h>
#include<linux/types.h>
#include<linux/proc_fs.h>
#include<linux/fcntl.h>
#include<linux/interrupt.h>
#include<asm/system.h>
#include<asm/uaccess.h>
#define MEMORY_IOCTL_BASE 0xbb
#define MEMORY_IOCTL_READ _IOR(MEMORY_IOCTL_BASE,0,char)
#define MEMORY_IOCTL_WRITE _IOW(MEMORY_IOCTL_BASE,1,char)

int memory_open(struct inode *inode,struct file *flip);
int memory_release(struct inode *inode,struct file *flip);
int memory_ioctl(struct inode *inode,struct file *flip,unsigned int cmd,unsigned int *arg);

void memory_exit(void);
int memory_init(void);



extern struct file_operations memory_fops ;


module_init(memory_init);
module_exit(memory_exit);

extern int memory_major;
extern char *memory_buffer;
