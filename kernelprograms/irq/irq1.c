#include<linux/module.h>
#include<linux/kernel.h>
#include<linux/init.h>
#include<linux/fs.h>
#include<linux/mm.h>
#include<linux/interrupt.h>
//#include<asm/irq.h>

MODULE_LICENSE("Dual BSD/GPL");
	
int majno;
int numInt; // Total number of interrupts
int irqNo=0; 

// PRototypes
static int __init my_init(void);
static void __exit my_exit(void);

//void irqHandle (void);
irq_handler_t irqHandle(int, void *,struct pt_regs *);

int *my_dev_id;


irq_handler_t irqHandle(int a, void *p, struct pt_regs *xxx)
{
   numInt++;
   return 0;
}

/*
void irqHandle (void)
{
   numInt++;
}    
*/
static int __init my_init()
{

   int retIrq;
   
   *my_dev_id = 0;
   while (irqNo < 14)
   {
      retIrq = request_irq(irqNo, irqHandle, IRQF_SHARED, "mydvice", my_dev_id);
      if (retIrq < 0)
      {
         printk ("Request_IRQ Failed %d \n", irqNo);
      }
      else {
            printk ("Request_IRQ Passed %d\n", irqNo);
            break;
      }
   irqNo++;
   }   
	return 0;
	
}
static void __exit my_exit()
{
	printk("Module exit called\n");
        printk("<0> Number of INterrupts %d\n", numInt);
        free_irq(irqNo, my_dev_id);
}

module_init(my_init);
module_exit(my_exit);
