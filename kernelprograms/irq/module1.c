#include<linux/module.h>
#include<linux/kernel.h>
#include<linux/init.h>
#include<linux/fs.h>
#include<linux/mm.h>
MODULE_LICENSE("Dual BSD/GPL");
	
int majno;
// PRototypes
int my_open (struct inode *in, struct file *fp);
int my_release (struct inode *in, struct file *fp);
int my_mmap(struct file *fp, struct vm_area_struct *vmstruct);
static int __init my_init(void);
static void __exit my_exit(void);

struct file_operations fops = {
	.open = my_open,
	.release = my_release,
	.mmap = my_mmap
};

int my_open (struct inode *in, struct file *fp)
{
	printk("File opened \n");
	return 0;
}
int my_release (struct inode *in, struct file *fp)
{
	printk("File released \n");
	return 0;
}
int my_mmap(struct file *fp, struct vm_area_struct *vmstruct)
{
	int ret;
	int *ptr = (int *)0xC00a0000;
	ret = remap_pfn_range(vmstruct,vmstruct->vm_start,0xa0,(vmstruct->vm_end)-(vmstruct->vm_start),vmstruct->vm_page_prot);
	printk("Remap done with return value %d \n",ret);
	*ptr = 'A';
	return ret;
}

static int __init my_init()
{
	majno=register_chrdev(0,"mydevice",&fops);
	if(majno<0)
		printk("Module registeration failed\n");
	else
		printk("Module 1 registered\n");
	return 0;
	
}

static void __exit my_exit()
{
	printk("Module exit called\n");
	unregister_chrdev(majno,"mydevice");	
}
module_init(my_init);
module_exit(my_exit);
