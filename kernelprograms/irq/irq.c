#include<linux/module.h>
#include<linux/kernel.h>
#include<linux/init.h>
#include<linux/fs.h>
#include<linux/mm.h>
#include<linux/interrupt.h>
#include<linux/irqreturn.h>
//#include<asm/irq.h>

MODULE_LICENSE("Dual BSD/GPL");
	
int majno;
int numInt; // Total number of interrupts
int irqNo=16; 

// Prototypes
static int __init my_init(void);
static void __exit my_exit(void);

//void irqHandle (void);
irqreturn_t irqHandle(int, void *,struct pt_regs *);

int my_dev_id;



irqreturn_t irqHandle(int a, void *p, struct pt_regs *xxx)
{
   numInt++;
   return 0;
}

static int __init my_init()
{

   int retIrq;
      
      my_dev_id = 0;
      printk(KERN_INFO "Initializing irq routine \n");
      retIrq = request_irq(irqNo, irqHandle, IRQF_SHARED, "mydvice", &my_dev_id);
      if (retIrq < 0)
      {
         printk ("Request_IRQ Failed %d \n", irqNo);
      }
      else {
            printk ("Request_IRQ Passed %d\n", irqNo);
      }
	return 0;
	
}
static void __exit my_exit()
{
	printk("Exiting IRQ module \n");
        printk(KERN_INFO "Number of INterrupts %d\n", numInt);
        free_irq(irqNo, &my_dev_id);
}

module_init(my_init);
module_exit(my_exit);
