#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/types.h>
#include <linux/fs.h>
#include <linux/mm.h>
#include <asm-arm/arch-pxa/hardware.h>
#include <asm-arm/arch-pxa/memory.h>
#include <asm-arm/delay.h>

#include "pxa.h"

MODULE_LICENSE("Dual BSD/GPL");
int majno;
int write(int);
void write_val_to_addr(unsigned long *addr, unsigned long val)
{
    (*addr) = val;
}



struct file_operations pxa_serial_fops = {
	
//	.read = pxa_serial_read,
//	.write = write,
	
};
int write(int data)
{

	write_val_to_addr((unsigned long *)FFTHR, 0x41);
 
 	udelay(1);
	   
	write_val_to_addr((unsigned long *)FFTHR, 0x41);
    
	return 1;
}

int serial_init(void)
{
    int majno;
	majno=register_chrdev(0,"pxadev",&pxa_serial_fops);
	
	write_val_to_addr((unsigned long *)FFLCR, 0x83);
	write_val_to_addr((unsigned long *)FFTHR, 0x08);
	write_val_to_addr((unsigned long *)FFFCR, 0x00);
	write_val_to_addr((unsigned long *)FFLCR, 0x03);
	write_val_to_addr((unsigned long *)FFIER, 0x40);
	write(0x41);
	
	
	/*
	FFLCR = 0x83;
	FFTHR = 0x08;
	FFFCR = 0x00;
	FFLCR = 0x03;
	FFIER = 0x40;
	*/
	//Disable Tx and Rx fifo FFFCR = 0x00;
	return 0;
}

void say_good_bye(void)
{
	printk ("Good Bye from printing useless junk\n");
	
}
module_init(serial_init);
module_exit(say_good_bye);
