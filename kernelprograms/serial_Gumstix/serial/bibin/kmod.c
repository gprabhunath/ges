#include <linux/init.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/sched.h>
#include <linux/proc_fs.h>
#include <linux/kernel.h>
#include<asm-arm/arch-pxa/hardware.h>

#define BASE_ADDRESS io_p2v(0x401000000)
#define UAR_DLH io_p2v(0x40100004)
#define UAR_LCR io_p2v(0x4010000c)
#define UAR_FCR io_p2v(0x40100008)
//#define UAR_ABR io_p2v(0x40100028)
#define UAR_IER io_p2v(0x40100004)
#define UAR_LSR io_p2v(0x40100014)
  
MODULE_LICENSE("Dual BSD/GPL");

static unsigned int *base;
static unsigned int *uar_dlh;
static unsigned int *uar_lcr;
static unsigned int *uar_fcr;
//static unsigned int *uar_abr;
static unsigned int *uar_ier;
static unsigned int *uar_lsr;

char *devname;
int majNo;
//static int IntCount = 0;
//module_param(devname, charp, 0000);
// Function Declarations for syscall definitions
int myOpen (struct inode *inode, struct file *filep);
int myRelease (struct inode *in, struct file *fp);
int write_to_base(int data );
//int myIoctl (struct inode *inode, struct file *fp,unsigned int pid,unsigned long vadd);
// Initialization routines
static int myInit (void);
static void myExit (void);

struct file_operations fops = {open:myOpen,release:myRelease/*,ioctl:myIoctl*/};

//int myprocdata(char *page, char **start, off_t off,int count, int *eof, void *data);
//extern int request_irq( unsigned int irq,irq_handler_t handler,devname ) 

static int myInit (void)
{
//   printk(KERN_INFO "Initializing the World \n");
	
//   create_proc_read_entry("pageWalk",0,NULL,myprocdata,NULL);

   majNo = register_chrdev(0,"myChar",&fops);
   if (majNo < 0)
   {
      printk(KERN_INFO "register_chrdev failed \n");
   } 
   else
   {
      printk(KERN_INFO "Major Number of the device = %d \n", majNo);
   }

   return 0;
}

int myOpen (struct inode *inode, struct file *filep)
{
        printk( KERN_INFO "Open successful\n"  );
		base = BASE_ADDRESS;
		uar_dlh = UAR_DLH;
		uar_lcr = UAR_LCR;
		uar_fcr = UAR_FCR;
		uar_ier = UAR_IER;
		uar_lsr = UAR_LSR;
		
		*uar_lcr = 0x00000083;
		*uar_fcr = 0x00000000;
		*uar_dlh = 0x00000000;
		*base = 0x00000008;
		*uar_lcr = 0x00000003;
		*uar_ier = 0x00000040;	

		
				write_to_base( 0x42 );
				write_to_base( 0x49 );
				write_to_base( 0x42 );
				write_to_base( 0x49 );
				write_to_base( 0x4e );

		
		*uar_ier = 0x55;
		*uar_fcr = 0xc1;
		*uar_lcr = 0x13;
        return 0;
}


int myRelease (struct inode *in, struct file *fp)
{
   		printk(KERN_INFO "File released \n");
   		return 0;
}


int write_to_base( int data )
{
		while( !(*uar_lsr &  0x20) );
		*base = data;
		return 0;
}
/*int myprocdata(char *page, char **start, off_t off,int count, int *eof, void *data)
{
		int ret;
		static int i;

		i = i + 1;
		ret = sprintf(page,"Test data %d \n",i);
		*eof = 1;
		return ret;
}*/

/*int myIoctl (struct inode *inode, struct file *fp,unsigned int vadd,unsigned long pid)
{
		unsigned int offset1 = vadd>>22;
		unsigned int offset2 = (vadd<<10)>>22;
		unsigned int offset3 = vadd & 0xfff;
		unsigned int flags =0;
	   	unsigned int pfn = 0;

		struct page *page = NULL;
		
		flags  = *((unsigned int *)((((current->mm->pgd) + offset1)->pgd + 0xc0000000) & 0xfffff000) + offset2) & 0xfff;
		pfn = (*((unsigned int *)((((current->mm->pgd) + offset1)->pgd + 0xc0000000) & 0xfffff000) + offset2) & 0xfffff000)>>12;
		*((unsigned int *)((pfn<<12) + 0xc0000000 + offset3)) = 12345;
		page = pfn_to_page(pfn);
   		printk(KERN_INFO "Page flag info - 0x%08x \n",(unsigned int)(page->flags) );
   		printk(KERN_INFO "Page table flags info - 0x%08x \n",flags);
		return 0;
}*/

static void myExit (void)
{
		
   printk (KERN_INFO "Exiting the World \n");
   unregister_chrdev(majNo, devname);
   return;
}

module_init(myInit);
module_exit(myExit);
