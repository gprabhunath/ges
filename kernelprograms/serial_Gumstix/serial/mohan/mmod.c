#include <linux/init.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/proc_fs.h>


#define io_p2v(x) (0xf2000000 + ((x) & 0x01ffffff) + (((x) & 0x1c000000) >> 1))


#define FCR io_p2v(0x40100008)
#define LCR io_p2v(0x4010000C)
#define IER io_p2v(0x40100004)
#define ABR io_p2v(0x40100028)
#define DLL io_p2v(0x40100000)
#define DLH io_p2v(0x40100004)
#define MCR io_p2v(0x40100010)
#define LSR io_p2v(0x40100014)
#define THR io_p2v(0x40100000)

MODULE_LICENSE("Dual BSD/GPL");

char *devname;
int majNo,i = 0;
unsigned long LCR_val,FCR_val,DLL_val,IER_val,ABR_val,val;

module_param(devname, charp, 0000);


// Function Declarations for syscall definitions
int myOpen (struct inode *inode, struct file *filep);
int myRelease (struct inode *in, struct file *fp);

// Initialization routines
static int myInit (void);
static void myExit (void);
struct file_operations fops = {open:myOpen,release:myRelease};

static int myInit (void)
{
   printk(KERN_INFO "Initializing the World \n");
   majNo =register_chrdev(0,"myChar",&fops);
 
   printk(KERN_INFO "\nThe major number of the device is = %d",majNo);   

   return 0;
}
int myOpen (struct inode *inode, struct file *filep)
{
   printk(KERN_INFO "Open successful\n");

   LCR_val = *((unsigned long *)LCR);
   FCR_val = *((unsigned long *)FCR);
   DLL_val = *((unsigned long *)DLL);
   IER_val = *((unsigned long *)IER);
   ABR_val = *((unsigned long *)ABR);

   *((unsigned long *)FCR) = 0x00000000;
   *((unsigned long *)LCR) = 0x00000083;
   *((unsigned long *)IER) = 0x00000040;
   *((unsigned long *)ABR) = 0x00000000;
   *((unsigned long *)DLL) = 0x00000008;
   *((unsigned long *)DLH) = 0x00000000;
   *((unsigned long *)LCR) = 0x00000003;
   *((unsigned long *)MCR) = 0x00000000;

   *((unsigned long *)THR) = 65;
   *((unsigned long *)THR) = 66;
   *((unsigned long *)THR) = 67;
   *((unsigned long *)THR) = 68;
   
  while(i <= 5)
  {
   if(0x00000001 & *((unsigned long *)LSR))
   {
     val =  *((unsigned long *)THR);
     *((unsigned long *)THR) =  val;
      i++;
   }

   *((unsigned long *)LCR) = LCR_val;
   *((unsigned long *)FCR) = FCR_val;
   *((unsigned long *)DLL) = DLL_val;
   *((unsigned long *)IER) = IER_val;
   *((unsigned long *)ABR) = ABR_val;
 }

   return 0;
}

int myRelease (struct inode *in, struct file *fp)
{
      printk(KERN_INFO "File released \n");
      unregister_chrdev(majNo,"myChar");
	return 0;
}


static void myExit (void)
{
   printk (KERN_INFO "Exiting the World \n");
}

module_init(myInit);
module_exit(myExit);
