#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>


int main()
{
		int fd, rret, wret;
		char buf[30] = "Hello, How are you?";
		fd =open("/dev/myChar",O_RDWR);
		if (fd < 0)
			perror ("Open failed: ");

		wret = write(fd, buf, (size_t)sizeof(buf) );
		if (wret < 0)
			perror ("Write failed: ");

		rret = read(fd, buf, (size_t)20);
		if (rret < 0)
			perror ("Read failed: ");

		close(fd);
		printf("The string is : %s",buf);
		return 0;
}
