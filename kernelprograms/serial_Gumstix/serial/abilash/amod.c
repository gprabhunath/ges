#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>


int main()
{
		int fd;
		char buf[30] = "Hello, How are you?";
		fd =open("/dev/myChar",O_RDWR);

		write(fd, buf, (size_t)sizeof(buf) );

		read(fd, buf, (size_t)20);

		close(fd);
		printf("The string is : %s",buf);
		return 0;
}
