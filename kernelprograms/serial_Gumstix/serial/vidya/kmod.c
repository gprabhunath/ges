// ########################### UART###################



#include <linux/init.h>
#include <linux/module.h>
#include <linux/fs.h>

#include<linux/proc_fs.h>
#include<linux/interrupt.h>
#include<asm-arm/arch-pxa/hardware.h>


MODULE_LICENSE("Dual BSD/GPL");


#define LCR io_p2v(0x4010000C)
#define DLH io_p2v(0x40100004)
#define DLL io_p2v(0x40100000)
#define FCR io_p2v(0x40100008)
#define ABR io_p2v(0x40100028)
#define IER io_p2v(0x40100004)
#define LSR io_p2v(0x40100014)
#define THR io_p2v(0x40100000)
#define RBR io_p2v(0x40100000)






char *devname;
int majNo;

module_param(devname, charp, 0000);
// Function Declarations for syscall definitions
int myOpen (struct inode *inode, struct file *filep);
int myRelease (struct inode *in, struct file *fp);

// Initialization routines
static int myInit (void);
static void myExit (void);


void write(int data);
void read(void);

struct file_operations fops = {open:myOpen,release:myRelease};

static int myInit (void)
{
   printk(KERN_INFO "Initializing the World \n");

   majNo = register_chrdev(0,devname,&fops);
   if (majNo < 0)
   {
      printk(KERN_INFO "register_chrdev failed \n");
   } else {
      printk(KERN_INFO "Major Number of the device = %d \n", majNo);
   }
   
   return 0;
}

int myOpen (struct inode *inode, struct file *filep)
{	


	unsigned long FFLCR;
	unsigned long FFDLH; 	
	unsigned long FFDLL;
	unsigned long FFFCR; 
	unsigned long FFABR; 
	unsigned long FFIER; 
	int i;
	
        printk(KERN_INFO "Open successful\n");

// save current config
  	FFLCR=*((unsigned long*)LCR);
	FFDLH=*((unsigned long*)DLH);
	FFDLL=*((unsigned long*)DLL);
	FFFCR=*((unsigned long*)FCR);
	FFABR=*((unsigned long*)ABR);
	FFIER=*((unsigned long*)IER);

 // INITIALIZE REGISTERS
 
 	*((unsigned long*)LCR) =0x00000083;
	*((unsigned long*)DLH) =0x00000000;
	*((unsigned long*)DLL) =0x00000008;
	*((unsigned long*)FCR) =0x00000000;
	*((unsigned long*)ABR) =0x00000000;
	*((unsigned long*)LCR) =0x00000003;
	*((unsigned long*)IER) =0x00000040;

	// write
	
	write(86);
	write(74);
	write(86);

	
	for(i=0;i<3;i++)
	{
		read();
	}

	while(!((*(unsigned long*)LSR)& 0x20));
		
	// restore reg values
	*((unsigned long*)LCR)=FFLCR;
	*((unsigned long*)DLH)=FFDLH;
	*((unsigned long*)DLL)=FFDLL;
	*((unsigned long*)FCR)=FFFCR;
	*((unsigned long*)ABR)=FFABR;
	*((unsigned long*)IER)=FFIER;


        return 0;
}

int myRelease (struct inode *in, struct file *fp)
{
   printk(KERN_INFO "File released \n");

	
	
   return 0;
}

void write(int data)
{
	while(!((*(unsigned long*) LSR)& 0x20))
	{
	}
	*((unsigned long*)THR)=data;
	return ;
}
	


void read(void)
{
	
	while(!((*(unsigned long*) LSR)& 0x01));
	
		write(*((unsigned long*)RBR));

	return;
}

static void myExit (void)
{
   printk (KERN_INFO "Exiting the World \n");
   unregister_chrdev(majNo, devname);
   return;
}

module_init(myInit);
module_exit(myExit);
