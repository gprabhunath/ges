#include <linux/init.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/proc_fs.h>

#define FCR io_p2v(0x40100008)
#define LCR io_p2v(0x4010000C)
#define IER io_p2v(0x40100004)
#define ABR io_p2v(0x40100028)
#define DLL io_p2v(0x40100000)
#define DLH io_p2v(0x40100004)
#define MCR io_p2v(0x40100010)
#define LSR io_p2v(0x40100014)
#define THR io_p2v(0x40100000)
#define RBR io_p2v(0x40100000)

#define io_p2v(x) (0xf2000000 + ((x) & 0x01ffffff) + (((x) & 0x1c000000) >> 1))
MODULE_LICENSE("Dual BSD/GPL");

char *devname;
int majNo;
module_param(devname, charp, 0000);
// Function Declarations for syscall definitions
int myOpen (struct inode *inode, struct file *filep);
int myRelease (struct inode *in, struct file *fp);

// Initialization routines
static int myInit (void);
static void myExit (void);
void write(void);
void read(void);
unsigned long fcr_val,lcr_val,ier_val,abr_val,dll_val;
struct file_operations fops = {open:myOpen,release:myRelease};

static int myInit (void)
{
 
   printk(KERN_INFO "Initializing the World \n");
   majNo = register_chrdev(0,"myChar",&fops);
   printk(KERN_INFO "\n the maj no of the device is =%d\n",majNo);
  
  
   return 0;
}
int myOpen (struct inode *inode, struct file *filep)
{
        printk(KERN_INFO "Open successful\n");

	fcr_val = *((unsigned long*)(FCR));
	lcr_val = *((unsigned long*)(LCR));
	ier_val = *((unsigned long *)(IER));
	abr_val = *((unsigned long *)(ABR));
	dll_val = *((unsigned long*)(DLL));
	
   	*((unsigned long*)(LCR)) = 0x00000083;
   	*((unsigned long*)(DLL)) = 0x00000008;
   	*((unsigned long*)(DLH)) = 0x00000000;
	*((unsigned long*)(LCR)) = 0x00000003;
   	*((unsigned long*)(FCR)) = 0x00000000;
   	*((unsigned long*)(IER)) = 0x00000040;
   	*((unsigned long*)(ABR)) = 0x00000000;
   	*((unsigned long*)(MCR)) = 0x00000000;
	write();
	read();

	*((unsigned long*)(FCR)) = fcr_val;
	*((unsigned long*)(LCR)) = lcr_val;
	*((unsigned long*)(IER)) = ier_val;
	*((unsigned long*)(ABR)) = abr_val;
	*((unsigned long*)(DLL)) = dll_val;
	return 0;
}
void write()
{
	int i = 5;
	while(i-- > 0)
	{
   	 	while(!(*((unsigned long *)LSR) & 0x00000020))
      		{}
   			*((unsigned long*)(THR)) = 0x65;
      		
	
	}
	
}
void read()
{
	int i =5;
	while(i-- > 0)
	{
	while(!(*((unsigned long*)LSR) & 0x00000001))
	{}
		*((unsigned long*)(THR)) = *((unsigned long*)(RBR));
	
        }
}
int myRelease (struct inode *in, struct file *fp)
{
      printk(KERN_INFO "File released \n");
      unregister_chrdev(majNo,"Mychar");
      return 0;
}


static void myExit (void)
{
   printk (KERN_INFO "Exiting the World \n");
   unregister_chrdev(majNo,"myChar");
   return; 
}

module_init(myInit);
module_exit(myExit);
