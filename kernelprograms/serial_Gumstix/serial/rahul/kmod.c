
#include <linux/init.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <asm-arm/arch-pxa/hardware.h>


MODULE_LICENSE("Dual BSD/GPL");

char *devname;
int majNo;
#define LCR io_p2v(0x4010000C)
#define DLH io_p2v(0x40100004)
#define DLL io_p2v(0x40100000)
#define FCR io_p2v(0x40100008)
#define ABR io_p2v(0x40100028)
#define IER io_p2v(0x40100004)
#define THR io_p2v(0x40100000)
#define LSR io_p2v(0x40100014)
#define RBR io_p2v(0x40100000)

module_param(devname, charp, 0000);
// Function Declarations for syscall definitions
int myOpen (struct inode *inode, struct file *filep);
int myRelease (struct inode *in, struct file *fp);
void write(int);
void read(void);

// Initialization routines
static int myInit (void);
static void myExit (void);

struct file_operations fops = {open:myOpen,release:myRelease};

static int myInit (void)
{
   printk(KERN_INFO "Initializing the World \n");

   majNo = register_chrdev(0,devname,&fops);
   if (majNo < 0)
   {
      printk(KERN_INFO "register_chrdev failed \n");
   } else {
      printk(KERN_INFO "Major Number of the device = %d \n", majNo);
   }
   
   return 0;
}

int myOpen (struct inode *inode, struct file *filep)
{
		unsigned long fflcr;
		unsigned long ffdlh;
		unsigned long ffdll;
		unsigned long fffcr;
		unsigned long ffabr;
		unsigned long ffier;
		int i;	

        printk(KERN_INFO "Open successful\n");

		//storing prev values
		fflcr = *((unsigned long*)LCR);
		ffdlh = *((unsigned long*)DLH);
		ffdll = *((unsigned long*)DLL);
		fffcr = *((unsigned long*)FCR);
		ffabr = *((unsigned long*)ABR);
		ffier = *((unsigned long*)IER);
		
		//setting reg values
		*((unsigned long *)LCR) = 0x00000083;
		*((unsigned long *)DLH) = 0x00000000;
		*((unsigned long *)DLL) = 0x00000008;
		*((unsigned long *)FCR) = 0x00000000;
//		*((unsigned long *)ABR) = 0x00000000;
		*((unsigned long *)IER) = 0x00000040;
		*((unsigned long *)LCR) = 0x00000003;

		//writing values to transmit
		write(70);
		write(71);
		write(72);
		
		//reading 
		i = 0;
		while(i<3)
		{
				read();
				i++;
		}

		//restorin values
		*((unsigned long*)LCR) = fflcr;
		*((unsigned long*)DLH) = ffdlh;
		*((unsigned long*)DLL) = ffdll;
		*((unsigned long*)FCR) = fflcr;
		*((unsigned long*)ABR) = ffabr;
		*((unsigned long*)IER) = ffier;
		
		
        return 0;
}
void write(int data)
{
		while( !(*(unsigned long *)LSR & 0x20) );
		*(unsigned long*)THR = data;
}
void read()
{
		while(!(*(unsigned long *)LSR & 0x01));
		write(*(unsigned long *)RBR);
}

int myRelease (struct inode *in, struct file *fp)
{
   printk(KERN_INFO "File released \n");
   return 0;
}


static void myExit (void)
{
   printk (KERN_INFO "Exiting the World \n");
   unregister_chrdev(majNo, devname);
   return;
}

module_init(myInit);
module_exit(myExit);
