#include <linux/init.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/proc_fs.h>
#include<asm-arm/arch-pxa/hardware.h>

#define FCR io_p2v(0x40100008)
#define LCR io_p2v(0x4010000C)
#define IER io_p2v(0x40100004)
#define ABR io_p2v(0x40100028)
#define DLL io_p2v(0x40100000)
#define DLH io_p2v(0x40100004)
#define LSR io_p2v(0x40100014)
#define THR io_p2v(0x40100000)
#define RBR io_p2v(0x40100000)

MODULE_LICENSE("Dual BSD/GPL");

char *devname;
int majNo;
unsigned long lcr,fcr,ier,abr,dll,dlh;
module_param(devname, charp, 0000);
// Function Declarations for syscall definitions
int myOpen (struct inode *inode, struct file *filep);
int myRelease (struct inode *in, struct file *fp);

// Initialization routines
static int myInit (void);
static void myExit (void);
void write(int);
void read(void);
struct file_operations fops = {open:myOpen,release:myRelease};

static int myInit (void)
{
   printk(KERN_INFO "Initializing the World \n");
   majNo = register_chrdev(0,"myChar",&fops);   
   return 0;
}
int myOpen (struct inode *inode, struct file *filep)
{
	int i = 5;
        printk(KERN_INFO "Open successful\n");
	
	lcr = *((unsigned long*)LCR);
	fcr = *((unsigned long*)FCR);
	ier = *((unsigned long*)IER);
	abr = *((unsigned long*)ABR);
	dll = *((unsigned long*)DLL);
	dlh = *((unsigned long*)DLH);

	*((unsigned long*)LCR) = 0x00000083;
   	*((unsigned long*)DLL) = 0x00000008;
   	*((unsigned long*)DLH) = 0x00000000;
   	*((unsigned long*)LCR) = 0x00000003;
   	*((unsigned long*)FCR) = 0x00000000;
	*((unsigned long*)IER) = 0x00000040;
   	*((unsigned long*)ABR) = 0x00000000;

	write(65);
	write(66);
	write(67);
	write(68);
	while(i-- > 0)
	{
		read();
	}

	while(!(*((unsigned long*)LSR)&0x20));
      *((unsigned long*)LCR) = lcr;
      *((unsigned long*)FCR) = fcr;
      *((unsigned long*)IER) = ier;
      *((unsigned long*)ABR) = abr;
      *((unsigned long*)DLL) = dll;
      *((unsigned long*)DLH) = dlh;
	return 0;
}
void write(int data)
{
	while(!(*(unsigned long*)LSR & 0x20))
   	{}
   	*((unsigned long*)THR) = data;
}
void read(void)
{
	while( !((*(unsigned long*)LSR) & 0x01) )
	{}
	write(*((unsigned long*)RBR));
}
int myRelease (struct inode *in, struct file *fp)
{
      printk(KERN_INFO "File released \n");
      return 0;
}


static void myExit (void)
{
   printk (KERN_INFO "Exiting the World \n");
   unregister_chrdev(majNo,"myChar");
}

module_init(myInit);
module_exit(myExit);
