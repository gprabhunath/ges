#include <linux/init.h>
#include <linux/module.h>  
#include <linux/fs.h>
#include <linux/proc_fs.h>
#include <linux/sched.h>
#include <asm-arm/arch-pxa/hardware.h>

MODULE_LICENSE("Dual BSD/GPL"); 
        

#define LCR io_p2v ( 0x4010000C ) 
#define DLH io_p2v ( 0x40100004 )
#define DLL io_p2v ( 0x40100000 )
#define FCR io_p2v ( 0x40100008 )
#define ABR io_p2v ( 0x40100028 )
#define IER io_p2v ( 0x40100004 ) 
#define THR io_p2v ( 0x40100000 )
#define FFLSR io_p2v ( 0x40100014 )
#define FFRBR io_p2v ( 0x40100000 )

char *devname;
int majNo;
unsigned long FFLCR;
unsigned long FFTHR;
//unsigned long FFLSR;
int i=0;

module_param(devname, charp, 0000);
// Function Declarations for syscall definitions
int myOpen (struct inode *inode, struct file *filep);
int myRelease (struct inode *in, struct file *fp);

// Initialization routines
static int myInit (void);
static void myExit (void);

struct file_operations fops = {open:myOpen,release:myRelease};

static int myInit (void)
{
   printk(KERN_INFO "Initializing the World \n");

   majNo = register_chrdev(0,devname,&fops);
   if (majNo < 0)
   {
      printk(KERN_INFO "register_chrdev failed \n");
   } else {
      printk(KERN_INFO "Major Number of the device = %d \n", majNo);
      
      }

   return 0;
}

void write( unsigned long * add, unsigned long data)
{
	*add = data;
}

void write_ttr(unsigned long);

void read( )
{
	while( !(*(unsigned int *)FFLSR & 0x01) );
	
	write_ttr( *(unsigned int *)FFRBR);
}
	 
void write_ttr(unsigned long data)
{
	 while( !(*((unsigned long *)FFLCR) & 0x20)) { }
	 * (unsigned long *)FFTHR = data; 
}


int myOpen (struct inode *inode, struct file *filep)
{
        unsigned long FFLCR;
	unsigned long FFDLH;
	unsigned long FFDLL;
	unsigned long FFFCR;
	unsigned long FFABR;
	unsigned long FFIER;
	
	FFLCR = *( ( unsigned long * ) LCR );
	FFDLH = *( ( unsigned long * ) DLH );
	FFDLL = *( ( unsigned long * ) DLL );
	FFFCR = *( ( unsigned long * ) FCR );
	FFABR = *( ( unsigned long * ) ABR );
	FFIER = *( ( unsigned long * ) IER );
	FFLCR = *( ( unsigned long * ) LCR );

	
	*( ( unsigned long * ) LCR) = 0x00000083;
	*( ( unsigned long * ) DLH) = 0x00000000;
	*( ( unsigned long * ) DLL) = 0x00000008;
	*( ( unsigned long * ) FCR) = 0x00000000;
	*( ( unsigned long * ) ABR) = 0x00000000;
	*( ( unsigned long * ) IER) = 0x00000040;
	*( ( unsigned long * ) LCR) = 0x00000003;
	
 	write_ttr('K');
	write_ttr('I');
	write_ttr('R');
	write_ttr('A');
	write_ttr('N');
	write_ttr('\n');

	while(i<6)
	{
		read();
		i++;
	}

	write( ( unsigned long * ) LCR, FFLCR);
	write( ( unsigned long * ) DLH, FFDLH);
	write( ( unsigned long * ) DLL, FFDLL);
	write( ( unsigned long * ) FCR, FFFCR);
	write( ( unsigned long * ) ABR, FFABR);
	write( ( unsigned long * ) IER, FFIER);


	
	printk(KERN_INFO "Open successful\n");
        
	return 0;

}

int myRelease (struct inode *in, struct file *fp)
{
   printk(KERN_INFO "File released \n");
   return 0;
}


                                                           
static void myExit (void)
{
   printk (KERN_INFO "Exiting the World \n");
   unregister_chrdev(majNo, devname);
   return;
}

module_init(myInit);
module_exit(myExit);


                                                        
                                                           






