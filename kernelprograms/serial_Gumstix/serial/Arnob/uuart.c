#include<linux/init.h>
#include<linux/module.h>
#include<linux/fs.h>
#include<linux/moduleparam.h>
#include<asm-arm/arch-pxa/hardware.h>
#include<asm-arm/arch-pxa/memory.h>

//#define FFRBR 0x40100000
//#define FFTHR 0x40100000
#define FFDLL 0x40100000
#define FFIER 0x40100004
#define FFDLH 0x40100004
//#define FFIIR 0x40100008
#define FFFCR 0x40100008
#define FFLCR 0x4010000c

MODULE_LICENSE("Dual BSD/GPL");

static int Major_Num = 0;
static char *dev_name = NULL;
static int number = 0;

int myOpen(struct inode *,struct file *);
int myIoctl(struct inode *,struct file *,unsigned int,unsigned long);
ssize_t write_into_tx_reg(struct file *,char __user *,size_t,loff_t *);
ssize_t read_from_rx_reg(struct file *,char __user *,size_t,loff_t *);

struct file_operations fops = {
	.ioctl = myIoctl,
	.open  = myOpen,
	.read = read_from_rx_reg,
	.write = write_into_tx_reg
	};

//module_param(dev_name,charp,00000);
//module_param(number,int,00000);

static int init_ffuart (void)
{
	
	//unsigned int *ffrbr;
	//unsigned int *ffthr;
	unsigned int *ffdll;
	unsigned int *ffier;
	unsigned int *ffdlh;
	unsigned int *ffiir;
	unsigned int *fffcr;
	unsigned int *fflcr, *ffabr, *ffthr;


	fflcr=(unsigned int *)io_p2v(FFLCR);
	*fflcr=0x83;

	/*ffdll=(unsigned int *)io_p2v(FFDLL);
	*ffdll=0x08;

	ffdlh=(unsigned int *)io_p2v(FFDLH);
	*ffdlh=0x00;
*/
	fflcr=(unsigned int *)io_p2v(FFLCR);
	*fflcr=0x03;
	 
	ffabr=(unsigned int *)io_p2v(0x40100028);
	*ffabr=0x00;
	
	fffcr=(unsigned int *)io_p2v(FFFCR);
	*fffcr=0x00;

    ffier = (unsigned int *) io_p2v (FFIER);
	*ffier=0x40;

	static unsigned int i=0;

    ffthr = (unsigned int *)io_p2v (0x40100000);
    for (i=0; i<5; i++)
		*ffthr = 0x41;
	
	
	/*Major_Num = register_chrdev(0,"/dev/pseudo",&fops);
	if(Major_Num<0)
	{
		printk(KERN_INFO "Error in registration\n");
		return -1;
	}
	//for(i=0;i<number;i++)
	printk(KERN_INFO "I am in Initialization routine \n");
	//printk("<0> the major number is %u \n",  Major_Num); 
	return 0;*/


} /* end of the module_initialization() */

int myOpen(struct inode *inode,struct file *file)
{
	printk("<0> Open Successful\n");
	return 0;
} /*end of the module myOpen() */

int myIoctl(struct inode *inode,struct file *file,unsigned int command,unsigned long arg)
{
	printk("<0> I am in IOCTL function\n");
	return 0;
} /*end of myIoctl() */

ssize_t write_into_tx_reg(struct file *file,char __user *user,size_t size,loff_t *t)
{
	unsigned int *lcr_reg;
	unsigned int *rx_reg;
	unsigned int *tx_reg;
	//unsigned int *lsr_reg;
    int i;

	lcr_reg=(unsigned int *)io_p2v(0x40100000);
	rx_reg=(unsigned int *)io_p2v(0x40100000);
	tx_reg=(unsigned int *)io_p2v(0x40100000);


	printk("<0> I am in write function \n");
	printk("<0> Data sent is - %c",(char ) *rx_reg);
    
	for (i = 0; i < 5; i++)
	{
		*tx_reg=0x41;
	}
	return 0;
} /*end of myRead() */


ssize_t read_from_rx_reg(struct file *file,char __user *user,size_t size,loff_t *t)
{
	
	unsigned int *lcr_reg;
	unsigned int *rx_reg;
	unsigned int *tx_reg;
	unsigned int *lsr_reg;

	lcr_reg=(unsigned int *)io_p2v(0x40100000);
	rx_reg=(unsigned int *)io_p2v(0x40100000);
	tx_reg=(unsigned int *)io_p2v(0x40100000);

	*lcr_reg = 0x00000003;

	printk("<0> I am in read function \n");
	printk("<0> Data sent is - %c",(char ) *rx_reg);

	printk("\n<0> data from lsr element = %d \n", (int  )*lsr_reg);

	*rx_reg=*tx_reg;

	return 0;

}


void module_cleanup(void)
{
//unlinking the kernel module from the base kernel

unregister_chrdev(Major_Num , "/dev/pseudo");
printk(KERN_INFO "I am in clean UP routine\n");
} /*end of the module_cleanup() */

module_init(init_ffuart);
module_exit(module_cleanup);

























	
