#include <linux/init.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/sched.h>
#include <linux/proc_fs.h>
#include <linux/kernel.h>
#include<asm-arm/arch-pxa/hardware.h>

#define BASE_ADDRESS io_p2v(0x40100000)
#define UAR_DLH io_p2v(0x40100004)
#define UAR_LCR io_p2v(0x4010000c)
#define UAR_FCR io_p2v(0x40100008)
//#define UAR_ABR io_p2v(0x40100028)
#define UAR_IER io_p2v(0x40100004)
#define UAR_LSR io_p2v(0x40100014)
  
MODULE_LICENSE("Dual BSD/GPL");

static unsigned int *base;
static unsigned int *uar_dlh;
static unsigned int *uar_lcr;
static unsigned int *uar_fcr;
//static unsigned int *uar_abr;
static unsigned int *uar_ier;
static unsigned int *uar_lsr;

char *devname;
int majNo;
//static int IntCount = 0;
//module_param(devname, charp, 0000);
// Function Declarations for syscall definitions
int myOpen (struct inode *inode, struct file *filep);
int myRelease (struct inode *in, struct file *fp);
int write_to_base(long data );
void read(void);
//int myIoctl (struct inode *inode, struct file *fp,unsigned int pid,unsigned long vadd);
// Initialization routines
static int myInit (void);
static void myExit (void);

struct file_operations fops = {open:myOpen,release:myRelease/*,ioctl:myIoctl*/};

//int myprocdata(char *page, char **start, off_t off,int count, int *eof, void *data);
//extern int request_irq( unsigned int irq,irq_handler_t handler,devname ) 

static int myInit (void)
{
   printk(KERN_INFO "Initializing the World \n");

   majNo = register_chrdev(0,"myChar",&fops);
   if (majNo < 0)
   {
      printk(KERN_INFO "register_chrdev failed \n");
   } 
   else
   {
      printk(KERN_INFO "Major Number of the device = %d \n", majNo);
   }

   return 0;
}

int myOpen (struct inode *inode, struct file *filep)
{
		static int i;
		unsigned int *temp_base;
		unsigned int *temp_dlh;
		unsigned int *temp_lcr;
		unsigned int *temp_fcr;
		unsigned int *temp_ier;
		unsigned int *temp_lsr;
        printk( KERN_INFO "Open successful\n" );

		base = (unsigned int*)BASE_ADDRESS;
		uar_dlh = (unsigned int*)UAR_DLH;
		uar_lcr = (unsigned int*)UAR_LCR;
		uar_fcr = (unsigned int*)UAR_FCR;
		uar_ier = (unsigned int*)UAR_IER;
		uar_lsr = (unsigned int*)UAR_LSR;

		*temp_ier = *uar_ier;
		*temp_lcr = *uar_lcr;
		*temp_fcr = *uar_fcr;
		
		*uar_lcr = 0x00000083;
		*uar_fcr = 0x00000000;
		*uar_dlh = 0x00000000;
		*base =    0x00000008;
		*uar_lcr = 0x00000003;
		*uar_ier = 0x00000040;	

		
				write_to_base( 0x42 );
				write_to_base( 0x49 );
				write_to_base( 0x42 );
				write_to_base( 0x49 );
				write_to_base( 0x4e );
				write_to_base( 0x62 );
				write_to_base( 0x52 );
				write_to_base( 0x41 );
				write_to_base( 0x48 );
				write_to_base( 0x55 );
				write_to_base( 0x4c );

		for( i = 0 ; i < 4 ; i++ )
		{
				read();
		}

		*uar_ier = *temp_ier;
		*uar_fcr = *temp_fcr;
		*uar_lcr = *temp_lcr;
        return 0;
}


int myRelease (struct inode *in, struct file *fp)
{
   		printk(KERN_INFO "File released \n");
   		return 0;
}


int write_to_base( long data )
{
		while( (*uar_lsr &  0x20) == 0 );
		*base = data;
		return 0;
}

void read ()
{
		while( (*uar_lsr & 0x01 ) == 0 );
		long data = *base;
		write_to_base( data );
}
static void myExit (void)
{
		
   printk (KERN_INFO "Exiting the World \n");
   unregister_chrdev(majNo, "myChar");
   return;
}

module_init(myInit);
module_exit(myExit);
