#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/types.h>
#include <linux/fs.h>
#include <linux/mm.h>
#include <asm-arm/arch-pxa/hardware.h>
#include <asm-arm/arch-pxa/memory.h>
#include <asm-arm/delay.h>

#include "pxa.h"

MODULE_LICENSE("Dual BSD/GPL");
int majno;
int write(int);


// Register values to be saved
unsigned int FFIER_val;
unsigned int FFFCR_val;
unsigned int FFLCR_val;

int myOpen (struct inode *inode, struct file *filep);
int myRelease (struct inode *in, struct file *fp);

#if 0
irq_handler_t serrecv (int, void *);
irq_handler_t serrecv (int irq, void *dev_id)
{
	
}
#endif

void read_from_device (void)
{
	unsigned int r_char;

		while ( !(*(unsigned int *)FFLSR & 0x01) ) {}
			r_char = *(unsigned int *)FFRBR;
			if ( r_char == 0x2A) return;
	 			write(r_char);

	return;

}
void write_val_to_addr(unsigned long *addr, unsigned long val)
{

	while (!(*(unsigned int *)FFLSR & 0x20)) {}
	    (*addr) = val;
	
	return;

}


struct file_operations pxa_serial_fops = {

	.open = myOpen,
	
};
int write(int data)
{

	write_val_to_addr((unsigned long *)FFTHR, data);
 
// 	udelay(1);
	   
	return 1;
}

int serial_init(void)
{
    int majNo;
	majNo = register_chrdev(0,"pxadev",&pxa_serial_fops);

   if (majNo < 0)
   {
	   printk(KERN_INFO "register_chrdev failed \n");
   } else {
       printk(KERN_INFO "Major Number of the device = %d \n", majNo);
   }

   //ret = request_irq(22, serrecv, IRQ_SHARED, myFF, 100);

	return 0;
}

int myOpen (struct inode *inode, struct file *filep)
{
	int rd = 0;

	printk(KERN_INFO "Open successful\n");

 	// Existing Configuration 
	printk (KERN_INFO "Existing Configuration \n");
	printk (KERN_INFO "Value of FFIER = 0x%08x \n", *(unsigned int *)FFIER );
	printk (KERN_INFO "Value of FFFCR = 0x%08x \n", *(unsigned int *)FFFCR );
	printk (KERN_INFO "Value of FFLCR = 0x%08x \n", *(unsigned int *)FFLCR );

	// Saving the existing configuration; 
	FFIER_val = *(unsigned int *)FFIER;
	FFFCR_val = *(unsigned int *)FFFCR;
	FFLCR_val = *(unsigned int *)FFLCR;

	// Initialization of FFUART
	write_val_to_addr((unsigned long *)FFLCR, 0x83);
	write_val_to_addr((unsigned long *)FFDLL, 0x08);
	write_val_to_addr((unsigned long *)FFFCR, 0x00);
	write_val_to_addr((unsigned long *)FFLCR, 0x03);
	write_val_to_addr((unsigned long *)FFIER, 0x40);

	// Writing values to Transmit Hold Register

	write(0x50);
	write(0x52);
	write(0x41);
	write(0x42);
	write(0x48);
	write(0x55);
	write(0xa);
	write(0xd);
	
	// while (!(*(unsigned int *)FFLSR & 0x20)) {};

	//while (rd++ < 5)
		read_from_device();
	// Restoring original initialization
	*(unsigned int *)FFIER = FFIER_val;
	*(unsigned int *)FFFCR = FFFCR_val;
	*(unsigned int *)FFLCR = FFLCR_val;

	printk (KERN_INFO "End of open\n");

	return 0;
}

int myRelease (struct inode *in, struct file *fp)
{
	printk ("Restoring original initialization\n");

	return 0;

}
void say_good_bye(void)
{
	printk ("Good Bye from printing useless junk\n");
	
}
module_init(serial_init);
module_exit(say_good_bye);
