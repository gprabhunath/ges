#include <linux/init.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/sched.h>
#include <linux/proc_fs.h>
#include <asm-arm/arch-pxa/hardware.h>


MODULE_LICENSE("Dual BSD/GPL");

int majNo;
#define FFLCR io_p2v(0x4010000C)
#define FFLSR io_p2v(0x40100014)
#define FFTHR io_p2v(0x40100000)
#define FFRBR io_p2v(0x40100000)
#define FFFCR io_p2v(0x40100008)
#define FFIER io_p2v(0x40100004)
#define FFABR io_p2v(0x40100028)
#define FFMCR io_p2v(0x40100010)
#define FFDLL io_p2v(0x40100000)
#define FFDLH io_p2v(0x40100004)

unsigned int fflcr;
unsigned int fffcr;
unsigned int ffier;
unsigned int ffabr;
unsigned int ffmcr;
unsigned int ffdll;

//module_param(devname, charp, 0000);

// Function Declarations for syscall definitions
int myOpen (struct inode *inode, struct file *filep);
int myRelease (struct inode *in, struct file *fp);
ssize_t myWrite (struct file *, const char *, size_t, loff_t *);
void write_data (int data);
void write(unsigned int * add, int data);

// Initialization routines
static int myInit (void);
static void myExit (void);

struct file_operations fops = {
	.open = myOpen,
	.release = myRelease,
	.write = myWrite
};

//struct file_operations fops = {open:myOpen,release:myRelease};


static int myInit (void)
{
   printk(KERN_INFO "Initializing the World \n");
   majNo = register_chrdev(0,"myChar",&fops);
   if (majNo < 0)
   {
      printk(KERN_INFO "register_chrdev failed \n");
   } else {
      printk(KERN_INFO "Major Number of the device = %d \n", majNo);
   }
  
   return 0;
}

void write_data (int data)
{
		while( !(*((unsigned int *)FFLSR) & 0x20));
	   * (unsigned int *)FFTHR = data;	
}

void write(unsigned int * add, int data)
{
		
		*add = data;
}

int myOpen (struct inode *inode, struct file *filep)
{
//		int count =0;
		/*Saving register values*/
	fflcr = *( (unsigned int *)FFLCR );
	fffcr = *( (unsigned int *)FFFCR );
	ffier = *( (unsigned int *)FFIER );
	ffabr = *( (unsigned int *)FFABR );
	ffmcr = *( (unsigned int *)FFMCR );
	ffdll = *( (unsigned int *)FFDLL );


   printk(KERN_INFO "Value of FFLCR = %08x \n",fflcr);
   printk(KERN_INFO "Value of FFFCR = %08x \n",fffcr);
   printk(KERN_INFO "Value of FFIER = %08x \n",ffier);
   printk(KERN_INFO "Value of FFABR = %08x \n",ffabr);
   printk(KERN_INFO "Value of FFMCR = %08x \n",ffmcr);
   printk(KERN_INFO "Value of FFDLL = %08x \n",ffdll);




   /*Initializing registers*/
   write ( (unsigned int *)FFLCR , 0x83);	// Setting DLAB bit to write into DLL/DLH, and Setting 8 bit data
   write ( (unsigned int *)FFDLL , 0x08);	// Setting baudrate to 115200
   write ( (unsigned int *)FFDLH , 0x00);	// Setting baudrate to 115200
   write ( (unsigned int *)FFLCR , 0x03);	// Clearing DLAB bit for normal operation
   write ( (unsigned int *)FFFCR , 0x00);	// Working in non-FIFO mode
   write ( (unsigned int *)FFABR , 0x00);	// Disabling auto baud rate set
   write ( (unsigned int *)FFIER , 0x40);	// Enabling UART, UUE bit set

        return 0;

} // End of myOpen()




ssize_t myWrite (struct file *fp, const char *buffer, size_t size, loff_t *lf)
{
		int count =0;

		write_data('\n');
		write_data('\r');
		
		for( count =0; count < size; count++ )
		{
   				write_data( buffer[count] );	// Writing  into transmit register
		}
		write_data('\n');
		write_data('\r');
		return count;
}

int myRelease (struct inode *in, struct file *fp)
{
   /*Restoring register values*/
   
   //printk(KERN_INFO "Entering myRelase \n");
   write ( (unsigned int *)FFLCR , fflcr);	
   write ( (unsigned int *)FFFCR , fffcr);
   write ( (unsigned int *)FFABR , ffabr);
   write ( (unsigned int *)FFMCR , ffmcr);
   write ( (unsigned int *)FFIER , ffier);	
   printk(KERN_INFO "File released \n");

   return 0;
}

static void myExit (void)
{
   printk (KERN_INFO "Exiting the World \n");
   unregister_chrdev(majNo, "myChar");
   return;
}

module_init(myInit);
module_exit(myExit);
