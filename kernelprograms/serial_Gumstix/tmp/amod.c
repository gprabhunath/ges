#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>


int main()
{
		int fd;
		char buf[] = "Hello, How are you?";
		fd =open("/dev/myChar",O_RDWR);

		write(fd, buf, (size_t)sizeof(buf) );

		close(fd);

		return 0;
}
