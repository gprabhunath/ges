
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/license.h>
#include <linux/vmalloc.h>
#include <linux/slab1.h>


MODULE_LICENSE("Dual BSD/GPL");

extern struct list_head *get_cache_chain(struct list_head *);

/*
 * print_kmem_list3_addr :
 *	Print the address of kmem_list3 instance nodelists
 */
void print_kmem_list3_addr(void)
{
	struct kmem_cache *kcparse;
	struct list_head *cchain = NULL;
	int namelen;
	char kcname[60];
	struct kmem_list3 *nodelist;

    // Get the address of head of the kmem_cache list (cache_chain)
	cchain = get_cache_chain(cchain);


	list_for_each_entry(kcparse, cchain, next)
	{
 		namelen = strlen(kcparse->name);
   		memcpy(kcname, kcparse->name, namelen);
   		kcname[namelen] = '\0';

		nodelist = kcparse->nodelists[0];
		printk (KERN_INFO "%s : 0x%08x : 0x%08x \n", 
					kcname, kcparse, kcparse->nodelists[0]); 
	}
	
	return;

} // End of print_kmem_list3_addr

void print_slab_addr(void)
{

	struct kmem_cache *kcparse;
	struct slab *pasparse;
	struct slab *fusparse;
	struct slab *frsparse;
	struct list_head *paslab = NULL;
	struct list_head *fuslab = NULL;
	struct list_head *frslab = NULL;
    struct list_head *cchain = NULL;
    int namelen;
    char kcname[60];
    struct kmem_list3 *nodelist;

    // Get the address of head of the kmem_cache list (cache_chain)
	cchain = get_cache_chain(cchain);

	list_for_each_entry(kcparse, cchain, next)
	{
 		namelen = strlen(kcparse->name);
   		memcpy(kcname, kcparse->name, namelen);
   		kcname[namelen] = '\0';
		printk (KERN_INFO "%s ",	kcname); 

		nodelist = kcparse->nodelists[0];

		// Parse the Partial slab list
		printk ("Partial: ");
		paslab = &(nodelist->slabs_partial);
		list_for_each_entry(pasparse, paslab, list)
		{
			printk ("0x%08x : %d ", 
					 pasparse, pasparse->inuse); 
		} // End of list_for_each_entry(pasparse, ...
		printk ("\n\n");

	} // End of list_for_each_entry

	return;
} // End of print_slab_addr

