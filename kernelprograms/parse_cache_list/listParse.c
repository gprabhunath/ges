
// Parse list of struct module 
// Status : Executed 
// Exported Data Structure modules 

// Functions Implemented
/*
  * Parse the list of struct module and do the following
  * Print the name of the module
  * Print the name of modules which uses the current module
  * Print the ELF Symbols shown in /proc/kallsyms
*/


#include <linux/module.h>
#include <linux/moduleloader.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/license.h>
#include <linux/vmalloc.h>
//#include <linux/slab.h>
#include <linux/slab1.h>
#include <linux/numa.h>


MODULE_LICENSE("Dual BSD/GPL");


// Function Prototypes
static int __init mod_init(void);
static void __exit mod_exit(void);

extern struct list_head *get_cache_chain(struct list_head *);
extern void print_kmem_list3_addr(void);
extern void print_slab_addr(void);

// Data Structures Declaration
struct cache_sizes *csizes;

// Function Definitions
static int __init mod_init()
{

	struct kmem_cache *kcparse;
	//kmem_cache_t *kcparse;
	struct list_head *cchain = NULL;
	int namelen; 
	char kcname[60];
	struct kmem_list3 *nodelist;
	

	printk("kmem_cache_list: Module Registered \n");

	printk (KERN_INFO "sizeof kmem_cache_create = %d\n", 
				sizeof (struct kmem_cache));

	printk (KERN_INFO "KMEM Cache Names\n"); 
	// Print the address of nodelist (instance of kmem_list3)
#if 0
	print_kmem_list3_addr();
#endif

	// Print address of the slabs
	print_slab_addr();


#if 0
	printk (KERN_INFO "Address of malloc_sizes = 0x%08x \n", malloc_sizes);
	printk (KERN_INFO "#########################\n");
	printk (KERN_INFO "KMEM Cache Names from mallox_sizes \n"); 

	// Print the names of the general caches from malloc_sizes array
	csizes = malloc_sizes;

	while (csizes->cs_size != ULONG_MAX)
	{
		printk (KERN_INFO "Gen Cache name = %s --> 0x%08x \n", 
					csizes->cs_cachep->name, csizes->cs_cachep);

		printk (KERN_INFO "Gen Cache DMA name = %s --> 0x%08x \n", 
					csizes->cs_dmacachep->name, csizes->cs_dmacachep);
		csizes++;
	}

	// Print addresses of kmem_cache_t instances
	printk (KERN_INFO "Addresses of kmem_cache_t instances \n");
	list_for_each_entry(kcparse, cchain, next) 
	{
		printk (KERN_INFO "0x%08x \n", kcparse); 

	} // End of list_for_each_entry

	printk (KERN_INFO "KMEM Cache Names\n"); 
	list_for_each_entry(kcparse, cchain, next) 
	{
	 	namelen = strlen(kcparse->name);
	   	memcpy(kcname, kcparse->name, namelen);
	   	kcname[namelen] = '\0';
		printk (KERN_INFO "%s \n", kcname); 

	} // End of list_for_each_entry

#endif

	return 0;

} // End of mod_init

static void __exit mod_exit()
{
	printk(KERN_INFO "kem_cache_list: Module Exited \n");
}

module_init(mod_init);
module_exit(mod_exit);

