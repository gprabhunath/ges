#include <stdio.h>
#include <stdlib.h>

struct date
{
	int d;	
	int m;
	int y;
};

struct student
{
	char *name;
	int age;
	struct date birthdate;
};

struct date *birthdate;
struct student *student,*student1;

int main()
{
	student = (struct student *)malloc(sizeof(struct student));
	birthdate = (struct date *)malloc(sizeof(struct date));

	student->name = "DEEPAK";
	student->age  = 21;
	student->birthdate.d = 13; 
	student->birthdate.m = 11; 
	student->birthdate.y = 1986; 

	printf("\n student       = %08x ----> %08x ----> %s \n",& student,student,student->name);
	printf("                                %08x ----> %d \n",& student->age,student->age);
	printf("                                %08x ----> %d.%d.%d \n\n",& student->birthdate,student->birthdate.d,student->birthdate.m,student->birthdate.y);

	birthdate = &student->birthdate;
	printf(" birthdate     = %08x ----> %08x ----> %d.%d.%d \n\n",& birthdate,birthdate,*birthdate,*(birthdate + 1),*(birthdate + 2));
	
	student1 = (struct student *)((unsigned int)birthdate - 8);

	printf(" birthdate - 8 =                %08x \n\n",(unsigned int)birthdate - 8);
	printf(" student1      = %08x ----> %08x ----> %s \n",& student1,student1,student1->name);
	printf("                                %08x ----> %d \n",& student1->age,student1->age);
	printf("                                %08x ----> %d.%d.%d \n\n",& student1->birthdate,student1->birthdate.d,student1->birthdate.m,student1->birthdate.y);
}	

