#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <sys/ioctl.h>

int A=100;

int main()
{

	int ret;
	char *str = "LINUX";
	int fd;

        fd = open("/dev/mydev",O_RDWR);

        if(fd < 0)
                perror("Unable to open the Device");
        else
                printf("File open Successful %d\n",fd);

	printf("\n Fork into 2 tasks \n");

	ret = fork();

	if(ret > 0)
	{
		sleep(2);
		A = A + 1;
		printf(" I am in Parent context \n");
		printf(" The value of ret = %d %d   ",ret,getpid());
		printf(" A = %d    str = %s \n",A,str);
		
	        ioctl(fd,1,&A);
	}

	else if(ret == 0)
	{
		str = "FEDORA";
		printf(" I am in Child context \n");
		printf(" The value of ret = %d    %d   ",ret,getpid());
		printf(" A = %d    str = %s \n",A,str);

	        ioctl(fd,0,&A);
	}
	
	close(fd);
	return 0;
}
