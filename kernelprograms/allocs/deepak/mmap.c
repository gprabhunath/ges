#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <sys/mman.h>

unsigned int *va;
unsigned int *pa;

int main()
{
	int fd;
	unsigned int vidArea = 4 * 1024 *1024;
	int i;
    fd = open("/dev/mydev",O_RDWR);
    if(fd < 0)
		perror("\nUnable to open the Device\n");
	else
	printf("\nFile open Successful %d\n\n",fd);

	va = mmap(0,vidArea,PROT_READ|PROT_WRITE,MAP_SHARED,fd,0);
	
	printf("VIR_ADDR = %08x\n\n",(unsigned int)va);

	if(va == (void *)-1)
	{
		printf("MAPPING FAILED\n");
		exit(0);
	}
	else
		printf("MAPPING SUCCESSFUL\n\n");

	pa = (unsigned int *)va;
	for(i=0;i<1024*1024;i++)
	{
		printf("%x ",i);
		*(pa+i) = 0xa0000fff;
	}
		
        close(fd);
        return 0;
}


