#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <sys/mman.h>

int a = 5;
unsigned int *ptr1;
unsigned int *p2;


int main()
{
        int fd;
	
	printf("I am in App 1 \n");
	
	ptr1 = (unsigned int *)malloc(4096 * 2);

//	*ptr1 = 1234;

	printf("Allocated 2 pages; viradd1 = %08x \n",(unsigned int)ptr1);

        fd = open("/dev/mydev",O_RDWR);

        if(fd < 0)
                perror("Unable to open the Device\n");
        else
                printf("File open Successful %d\n",fd);
	
	p2= (unsigned int *)malloc(4096 * 4);

//	*p2 = 1345;
	
	printf("Allocated 2 pages; viradd2 = %08x \n",(unsigned int)p2);
        
	ioctl(fd,a,&a);

        printf("VMA Dumped for App 1; PID = %d\n", getpid());

        while (1) { };
        close(fd);
        return 0;
}


