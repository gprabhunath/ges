#include <linux/init.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/sched.h>
#include <linux/mm.h>
//#include <linux/slab_def.h>

MODULE_LICENSE("Dual BSD/GPL");

struct task_struct *p;

char *devname;

int majNo;

module_param(devname,charp,0000);

int myOpen(struct inode *inode,struct file *filep);

int myRelease(struct inode *in,struct file *fp);

int myIoctl (struct inode *inode, struct file *f,unsigned int,unsigned long);

void pgtdump(void);

static int myInit(void);
static void myExit(void);

struct file_operations fops = {open:myOpen,release:myRelease,ioctl:myIoctl};

static int myInit(void)
{
	printk(KERN_INFO "\nInitializing the World\n");
	majNo = register_chrdev(0,devname,&fops);
	if(majNo < 0)
	{
		printk(KERN_INFO "register_chrdev failed\n");
	}
	else
	{
		printk(KERN_INFO "Major Number of the Device = %d\n",majNo);
	}

	return 0;
}

int myOpen(struct inode *inode,struct file *filep)
{
	printk(KERN_INFO "Open Successful\n");
	return 0;
}

int myRelease(struct inode *in,struct file *fp)
{
	printk(KERN_INFO "File Released\n\n");
	return 0;
}


int myIoctl (struct inode *inode, struct file *f,unsigned int pid,unsigned long addr)
{
	for(p = current;(p = list_entry( (p)->tasks.next, struct task_struct, tasks )) != current;)
	{		
		printk(KERN_INFO "PID  = %d \n",p->pid);
		printk(KERN_INFO "TGID = %d \n\n",p->tgid);
	}
	return 0;
}


static void myExit(void)
{
	printk(KERN_INFO "\nExiting the World\n");
	unregister_chrdev(majNo,devname);
	return;
}

module_init(myInit);
module_exit(myExit);

		


