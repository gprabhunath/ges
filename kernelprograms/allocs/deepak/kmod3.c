#include <linux/init.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/sched.h>
#include <linux/mm_types.h>
#include <linux/highmem.h>

MODULE_LICENSE("Dual BSD/GPL");

char *devname;

int majNo;

module_param(devname,charp,0000);

int myOpen(struct inode *inode,struct file *filep);

int myRelease(struct inode *in,struct file *fp);

int myIoctl (struct inode *inode, struct file *f, unsigned int a, unsigned long addr);

static int myInit(void);
static void myExit(void);

struct file_operations fops = {open:myOpen,release:myRelease,ioctl:myIoctl};

static int myInit(void)
{
	printk(KERN_INFO "\nInitializing the World\n");
	majNo = register_chrdev(0,devname,&fops);
	if(majNo < 0)
	{
		printk(KERN_INFO "register_chrdev failed\n");
	}
	else
	{
		printk(KERN_INFO "Major Number of the Device = %d\n",majNo);
	}

	return 0;
}

int myOpen(struct inode *inode,struct file *filep)
{
	printk(KERN_INFO "Open Successful\n");
	return 0;
}

int myRelease(struct inode *in,struct file *fp)
{
	printk(KERN_INFO "File Released\n\n");
	return 0;
}

int myIoctl (struct inode *inode, struct file *f, unsigned int a, unsigned long addr)
{	
	unsigned int *add;
	
	unsigned int *pgd_base;
	unsigned int pt_base;
	unsigned int p_base;

	unsigned int pfn;
	
	unsigned int *vir_pgd;
	unsigned int *vir_pt;
	unsigned int *vir_p;

	int pgd_index;
	int pt_index;
	int p_index;

	struct task_struct *task_ptr;
	struct page *page;	

	add       = (unsigned int *)addr;
	pgd_index = (unsigned int)add >> 22;		
	pt_index  = ((unsigned int)add << 10) >> 22;	
	p_index   = (unsigned int)add & 0x00000fff;

	printk(KERN_INFO "\nAdd of a  = 0x%08x\n",(unsigned int)add);

	printk(KERN_INFO "Pgd_Index = 0x%08x\n",(unsigned int)pgd_index);
	printk(KERN_INFO "Pt_Index  = 0x%08x\n",(unsigned int)pt_index);
	printk(KERN_INFO "P_Index   = 0x%08x\n",(unsigned int)p_index);

	task_ptr = current;
	
	pgd_base = (unsigned int *)task_ptr->mm->pgd;
	printk(KERN_INFO "\npgd                       = 0x%08x\n",(unsigned int)pgd_base);
	vir_pgd = pgd_index + pgd_base;	
	printk(KERN_INFO "PGD after adding index    = 0x%08x\n",(unsigned int)vir_pgd);
	
	pt_base = *vir_pgd;	
	printk(KERN_INFO "PGD physical entry        = 0x%08x\n",(unsigned int)pt_base);
	pt_base = pt_base & 0xfffff000;	
	printk(KERN_INFO "\nPage Table Base           = 0x%08x\n",(unsigned int)pt_base);
	pt_base = pt_base + 0xc0000000;	
	printk(KERN_INFO "Page Table Vitual Address = 0x%08x\n",(unsigned int)pt_base);	
	vir_pt = (unsigned int *)pt_base;	
	vir_pt = pt_index + vir_pt;
	printk(KERN_INFO "PT after adding index     = 0x%08x\n",(unsigned int)vir_pt);
	
	p_base = *vir_pt;
	printk(KERN_INFO "Page Table Physical Entry = 0x%08x\n",(unsigned int)p_base);
	p_base = p_base & 0xfffff000;
	printk(KERN_INFO "\nPage Base                 = 0x%08x\n",(unsigned int)p_base);
	
	pfn = p_base;
	pfn = pfn >> 12;

	page = pfn_to_page(pfn);

	vir_p = kmap(page);

	p_base = (unsigned int)vir_p;	

 	printk(KERN_INFO "Page virtual base         = 0x%08x\n",(unsigned int)p_base);

	p_base = p_base | p_index;
	
	printk(KERN_INFO "Page after adding index   = 0x%08x\n",(unsigned int)p_base);

	vir_p = (unsigned int *)p_base;
	
	printk(KERN_INFO "\nValue of a = %d\n",*vir_p);	

	*vir_p = 66;

	printk(KERN_INFO "\nValue of a (changed) = %d\n\n",*vir_p);	
	
	return 0;
}

static void myExit(void)
{
	printk(KERN_INFO "\nExiting the World\n");
	unregister_chrdev(majNo,devname);
	return;
}

module_init(myInit);
module_exit(myExit);

		


