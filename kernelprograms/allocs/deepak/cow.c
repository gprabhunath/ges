#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <sys/ioctl.h>

int A;

int main()
{

	int ret;
	int fd;

        fd = open("/dev/mydev",O_RDWR);
        if(fd < 0)
                perror("Unable to open the Device");
        else
                printf("File open Successful %d\n\n",fd);

	ioctl(fd,2,&A);

	ret = fork();

	if(ret > 0)
	{
	//	sleep(1);
		A = A+1;
		printf("I am in Parent context \n");
		printf("The value of ret = %d %d   \n",ret,getpid());
		
	        ioctl(fd,1,&A);
	}

	else if(ret == 0)
	{
	//	sleep(1);
		A = A+1;
		printf("I am in Child context \n");
		printf("The value of ret = %d    %d   \n",ret,getpid());

	        ioctl(fd,0,&A);
	}
	
	close(fd);
	return 0;
}
