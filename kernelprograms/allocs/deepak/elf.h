#include <stdio.h>
#include <stdlib.h>

typedef unsigned int Elf32_Word; 
typedef unsigned int Elf32_Addr;
typedef unsigned int Elf32_Off;
typedef signed int Elf32_Sword;
typedef unsigned short int Elf32_Half;

#define EI_NIDENT 16

#define ELF32_ST_BIND(i)   ((i)>>4)
#define ELF32_ST_TYPE(i)   ((i)&0xf)
#define ELF32_ST_INFO(b,t) (((b)<<4)+((t)&0xf))

#define ELF32_R_SYM(i)    ((i)>>8)
#define ELF32_R_TYPE(i)   ((unsigned char)(i))
#define ELF32_R_INFO(s,t) (((s)<<8)+(unsigned char)(t))

typedef struct Elf32_Ehdr{
          unsigned char e_ident[EI_NIDENT];
          Elf32_Half    e_type;
          Elf32_Half    e_machine;
          Elf32_Word    e_version;
          Elf32_Addr    e_entry;
          Elf32_Off     e_phoff;
          Elf32_Off     e_shoff;
          Elf32_Word    e_flags;
          Elf32_Half    e_ehsize;
          Elf32_Half    e_phentsize;
          Elf32_Half    e_phnum;
          Elf32_Half    e_shentsize;
          Elf32_Half    e_shnum;
          Elf32_Half    e_shstrndx;
}elfheader;

typedef struct Elf32_Shdr{
          Elf32_Word sh_name;
          Elf32_Word sh_type;
          Elf32_Word sh_flags;
          Elf32_Addr sh_addr;
          Elf32_Off  sh_offset;
          Elf32_Word sh_size;
          Elf32_Word sh_link;
          Elf32_Word sh_info;
          Elf32_Word sh_addralign;
          Elf32_Word sh_entsize;
}elfsecheader;

typedef struct Elf32_Sym{
          Elf32_Word    st_name;
          Elf32_Addr    st_value;
          Elf32_Word    st_size;
          unsigned char st_info;
          unsigned char st_other;
          Elf32_Half    st_shndx;
}elfsymtab;

typedef struct ELF32_Rel{
        Elf32_Addr  r_offset;
        Elf32_Word  r_info;
}elfrel;

typedef struct Elf32_Rela{
        Elf32_Addr  r_offset;
        Elf32_Word  r_info;
        Elf32_Sword r_addend;
}elfrela;

typedef struct Elf32_Phdr{
          Elf32_Word p_type;
          Elf32_Off  p_offset;
          Elf32_Addr p_vaddr;
          Elf32_Addr p_paddr;
          Elf32_Word p_filesz;
          Elf32_Word p_memsz;
          Elf32_Word p_flags;
          Elf32_Word p_align;
}elfprogheader;


typedef struct Elf32_Dyn{
          Elf32_Sword   d_tag;
          union {
                  Elf32_Word   d_val;
                  Elf32_Addr   d_ptr;
          } d_un;
}elfdynamic;


