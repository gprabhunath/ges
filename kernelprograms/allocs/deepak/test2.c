#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <sys/mman.h>

int a = 5;

int main()
{
        int fd;
        fd = open("/dev/mydev",O_RDWR);

        if(fd < 0)
                perror("Unable to open the Device\n");
        else
                printf("File open Successful %d\n",fd);

        ioctl(fd,a,&a);

		printf("VMA Dumped %d\n", getpid());  

		while (1) { };
        close(fd);
        return 0;
}        

