#include <stdio.h>
#include "common.h"

int main()
{
	int shmid;
	int semid1;
	int semid2;
	char name[50];
	unsigned int *map_add;
	struct sembuf semaphore1,semaphore2;
	
	shmid = shmget(MY_SHM_ID,0,0);
	if(shmid == -1)
	{
		printf("Shared Page cannot be created \n");
		exit(0);
	}

	map_add = shmat(shmid,(const void*)0,0);

	printf("Mapped address = %08x shmid = %d \n",(unsigned int)map_add,shmid);

	semid1 = semget(MY_SEM_ID1,1,0);
	if(semid1 == -1)
	{
		printf("Semaphore1 create failure \n");
		exit(0);
	}

	semid2 = semget(MY_SEM_ID2,1,0666 | IPC_CREAT);
	if(semid2 == -1)
	{
		printf("Semaphore2 create failure \n");
		exit(0);
	}
	semctl(semid2,0,SETVAL,0);	
	
	printf("Enter the Name: ");
	scanf("%s",name);
	strcpy((char *)map_add,name);
	printf("\n Name      = %s \n",name);

	semaphore1.sem_num = 0;	
	semaphore1.sem_op  = 1;
	semaphore1.sem_flg = 0;
	semop(semid1,&semaphore1,1); 
	
	semaphore2.sem_num = 0;
	semaphore2.sem_op  = -1;
	semaphore2.sem_flg = 0;
	semop(semid2,&semaphore2,1);

	printf(" Number is = %s \n\n",(char *)map_add);	

	exit(0);
}
