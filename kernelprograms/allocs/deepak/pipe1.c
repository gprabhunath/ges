#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>

int main()
{

	int ret,rel;
	char *str = "LINUX";
	int myPipe[2];
	char buffer[10];
	
	ret = pipe(myPipe);
	
	if(ret == 0)
		printf("Pipe Created Successfully \n");
	else
	{
		printf("Pipe Failure \n");
		exit(0);
	}

	rel = fork();

	if(rel > 0)
	{
		sleep(1);
		printf(" I am in Parent context \n");
		write(myPipe[1],str,strlen(str));
		printf(" The value of rel = %d %d   ",rel,getpid());
		printf(" String Written = %s \n",str);
	}

	else if(rel == 0)
	{
		printf(" I am in Child context \n");
		read(myPipe[0],buffer,10);
		printf(" The value of rel = %d    %d   ",rel,getpid());
		printf(" String Read    = %s \n",buffer);
	}
	
	close(myPipe);	
	return 0;
}
