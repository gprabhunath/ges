#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <sys/ioctl.h>

int main()
{
        int fd;
	unsigned int pid;
	
	printf("I am in App 2 \n");
        
        fd = open("/dev/mydev",O_RDWR);

        if(fd < 0)
                perror("Unable to open the Device\n");
        else
                printf("File open Successful %d\n",fd);

	pid = 6280;

        ioctl(fd,pid,0);

        close(fd);
        return 0;
}

