#include <linux/init.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/sched.h>
#include <linux/mm.h>
#include <linux/interrupt.h>

MODULE_LICENSE("Dual BSD/GPL");

struct task_struct *p;

char *devname;

int majNo;

module_param(devname,charp,0000);

int myOpen(struct inode *inode,struct file *filep);

int myRelease(struct inode *in,struct file *fp);

void myIsr (void);

static int myInit(void);
static void myExit(void);

struct file_operations fops = {open:myOpen,release:myRelease};

static int myInit(void)
{
	int ret;
	printk(KERN_INFO "\nInitializing the World\n");
	majNo = register_chrdev(0,devname,&fops);
	if(majNo < 0)
	{
		printk(KERN_INFO "register_chrdev failed\n");
	}
	else
	{
		printk(KERN_INFO "Major Number of the Device = %d\n",majNo);
	}

	ret = request_irq(17,&myIsr,IRQF_SHARED,devname,2);
    if(ret < 0)
        printk(KERN_INFO "Device Hook Failed \n");
    else 
        printk(KERN_INFO "Device Hooked Succesfully \n");


	return 0;
}

void myIsr(void)
{
	printk(KERN_INFO "\n THIS IS MY DEVICE \n");
}

int myOpen(struct inode *inode,struct file *filep)
{
	printk(KERN_INFO "Open Successful \n");
	return 0;
}

int myRelease(struct inode *in,struct file *fp)
{
	printk(KERN_INFO "File Released\n\n");
	return 0;
}

static void myExit(void)
{
	printk(KERN_INFO "\nExiting the World\n");
	unregister_chrdev(majNo,devname);
	return;
}

module_init(myInit);
module_exit(myExit);

		


