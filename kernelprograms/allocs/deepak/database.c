#include <stdio.h>
#include <string.h>
#include "common.h"

int main()
{
	FILE *fp;
	int i;
	database *mydatabase;	

	fp = fopen("database.txt","r+");
	
	mydatabase = (database *)malloc(sizeof(database));

	for(i=0;i<MAX_SIZE;i++)
	{
		printf("\n Entry %d",i+1);
		printf("\n Enter Name  :   ");
		scanf("%s",mydatabase->name);
		printf(" Enter Number:   ");
		scanf("%s",mydatabase->number);
		fwrite(mydatabase,sizeof(database),1,fp);
	}

	rewind(fp);	

	for(i=0;i<MAX_SIZE;i++)
	{
		fread(mydatabase,sizeof(database),1,fp);
		printf("\n Name  : %s",mydatabase->name);
		printf("\n Number: %s \n",mydatabase->number);
	}
	fclose(fp);
}
