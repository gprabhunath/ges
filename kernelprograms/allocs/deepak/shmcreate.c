#include <stdio.h>
#include "common.h"

int main()
{
	int shmid;
	
	shmid = shmget(MY_SHM_ID,4096,0666 | IPC_CREAT);
	if(shmid == -1)
		printf("Shared Page cannot be created \n");

	return 0;
}
