#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <sys/ioctl.h>

int a = 55;

int main()
{
        int fd;

        fd = open("/dev/mydev",O_RDWR);

        if(fd < 0)
                perror("Unable to open the Device");
        else
                printf("File open Successful %d\n",fd);

	printf("\nOriginal value of a = %d\n",a);

        int rel;
        rel = ioctl(fd,a,&a);

	if(rel < 0)
	{
		perror("Unable to print");
		exit(0);
	}
	
	printf("\nChanged value of a  = %d\n\n",a);

	printf("\n");
        close(fd);
        return 0;
}


