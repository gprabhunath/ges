#!/bin/sh

killall /opt/packages/binutils-2.18/binutils/readelf
rmmod kmod5
rm -f /dev/mydev
make clean

make
insmod kmod5.ko devname=mydev
DEV=`cat /proc/devices | grep 'mydev' | cut -b 1,2,3`
mknod /dev/mydev c $DEV 0
dmesg -c > /dev/null
 
/opt/packages/binutils-2.18/binutils/readelf -a a.out > /dev/null &

dmesg > dump
head -n 96 dump > vmadump
objdump -D a.out > dumpobj

