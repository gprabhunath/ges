#include <stdio.h>
#include <fcntl.h>

int main()
{
	int fd;
	
	fd = open("/dev/myChar",O_RDWR);

	if(fd < 0)
		perror("Unable to open the Device");
	else 
		printf("File open Successful %d\n",fd);

	close(fd);
	return 0;
}

