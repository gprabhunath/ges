
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "elf.h"

FILE *fp,*f;
elfheader *header;
elfsecheader *secheader[];
elfsecheader *strtab;
elfsymtab *symtab;
elfrel *reltab;
elfprogheader *progheader;
unsigned int stroff,strsize;


void getelfheader()
{
	int i;

	header = (elfheader*)malloc(sizeof(elfheader));

	fread(header,sizeof(elfheader),1,fp);

	if((header->e_ident[0] != 0x7f) && (header->e_ident[1] != 0x45)	&& 
				(header->e_ident[2] != 0x4c) && (header->e_ident[3] != 0x46))
	{
		printf("\nreadelf: Error: Not an ELF file - it has the wrong magic bytes at the start\n\n");
		exit(0);
	}
	
	fprintf(f,"\nElf Header:");
	fprintf(f,"\n  Magic:   ");
	for(i=0;i<16;i++)
	{
		fprintf(f,"%02x ",header->e_ident[i]);
	}
	fprintf(f,"\n  Class: ");

	switch(header->e_ident[4])
	{
		case 0x00:fprintf(f,"\t\t\t\tInvalid class");break;
	 	case 0x01:fprintf(f,"\t\t\t\tELF32");break;
	 	case 0x02:fprintf(f,"\t\t\t\tELF64");break;
	}

	fprintf(f,"\n  Data: ");

	switch(header->e_ident[5])
	{
		case 0x00:fprintf(f,"\t\t\t\tInvalid data encoding");break;
		case 0x01:fprintf(f,"\t\t\t\t2's complement, little endian");break;
		case 0x02:fprintf(f,"\t\t\t\t2's complement, big endian");break;
	}

	fprintf(f,"\n  Version: ");
	
	switch(header->e_ident[5])
	{
		case 0x00:fprintf(f,"\t\t\t\tInvalid version");break;
		case 0x01:fprintf(f,"\t\t\t\t1 (current)");break;
	}
	fprintf(f,"\n  OS/ABI: ");
	fprintf(f,"\t\t\t\tUNIX - System V");
	
	fprintf(f,"\n  ABI Version: ");
	fprintf(f,"\t\t\t\t0");
	
	fprintf(f,"\n  Type: ");
	 	
	switch(header->e_type)
	{
		case 0x0000:fprintf(f,"\t\t\t\tNONE (No file type)");break;
		case 0x0001:fprintf(f,"\t\t\t\tREL (Relocatable file)");break;
		case 0x0002:fprintf(f,"\t\t\t\tEXEC (Executable file)");break;
		case 0x0003:fprintf(f,"\t\t\t\tDYN (Shared object file)");break;
		case 0x0004:fprintf(f,"\t\t\t\tCORE (Core file)");break;
		case 0xff00:fprintf(f,"\t\t\t\tLOPROC (Processor specific)");break;
		case 0xffff:fprintf(f,"\t\t\t\tHIPROC (Processor specific)");break;
	}

	fprintf(f,"\n  Machine: ");
	 	
	switch(header->e_machine)
	{
		case 0x0000:fprintf(f,"\t\t\t\tNONE (No machine)");break;
		case 0x0001:fprintf(f,"\t\t\t\tAT&T WE 32100");break;
		case 0x0002:fprintf(f,"\t\t\t\tSPARC");break;
		case 0x0003:fprintf(f,"\t\t\t\tIntel 80386");break;
		case 0x0004:fprintf(f,"\t\t\t\tMotorola 68000");break;
		case 0x0005:fprintf(f,"\t\t\t\tMotorola 88000");break;
		case 0x0007:fprintf(f,"\t\t\t\tIntel 80860");break;
		case 0x0008:fprintf(f,"\t\t\t\tMIPS RS3000 Big-Endian");break;
		case 0x0010:fprintf(f,"\t\t\t\tMIPS RS4000 Big-Endian");break;
		default:    if((header->e_machine >= 0x0011) && (header->e_machine <= 0x0016))
			 	fprintf(f,"\t\t\t\tRESERVED");
	}

	fprintf(f,"\n  Version: ");

	switch(header->e_version)
	{
		case 0x00000000:fprintf(f,"\t\t\t\t0x0");break;
		case 0x00000001:fprintf(f,"\t\t\t\t0x1");break;
		default:fprintf(f,"\t\t\t\t0x%08x",header->e_version);
	}

	fprintf(f,"\n  Entry point address: ");

	switch(header->e_entry)
	{
		case 0x00000000:fprintf(f,"\t\t\t0x0");break;
		case 0x00000001:fprintf(f,"\t\t\t0x1");break;
		default:fprintf(f,"\t\t\t0x%08x",header->e_entry);
	}

  	fprintf(f,"\n  Start of program headers: ");  
	fprintf(f,"\t\t%x (bytes into file)",header->e_phoff);  
      
  	fprintf(f,"\n  Start of section headers: "); 
	fprintf(f,"\t\t%d (bytes into file)",header->e_shoff);     
   
  	fprintf(f,"\n  Flags: ");      
	fprintf(f,"\t\t\t\t0x%x",header->e_flags);  

  	fprintf(f,"\n  Size of this header: "); 
	fprintf(f,"\t\t\t%d (bytes)",header->e_ehsize);   
            
  	fprintf(f,"\n  Size of program headers: ");  
	fprintf(f,"\t\t%d (bytes)",header->e_phentsize); 
        
  	fprintf(f,"\n  Number of program headers: ");     
	fprintf(f,"\t\t%d",header->e_phnum);  
 
  	fprintf(f,"\n  Size of section headers: "); 
	fprintf(f,"\t\t%d (bytes)",header->e_shentsize);  
       
  	fprintf(f,"\n  Number of section headers: ");  
	fprintf(f,"\t\t%d",header->e_shnum);    
   
  	fprintf(f,"\n  Section header string table index: "); 
	fprintf(f,"\t%d",header->e_shstrndx);

}


void getelfsecheader()
{
	if(header->e_shnum == 0)
	{
		fprintf(f,"\n\nThere are no section headers in this file.");
		return;
	}
	int i=0,flag=1;
	char *str,*str1;
		
	strtab = (elfsecheader*)malloc(sizeof(elfsecheader));

	fprintf(f,"\n\nSection Headers:");
	fprintf(f,"\n  [Nr]  Name               Type            Addr     Off    Size   ES Flg Lk Inf Al");
	fprintf(f,"\n");

	fseek(fp,header->e_shoff + (header->e_shstrndx * 40),0);
	
	fread(strtab,sizeof(elfsecheader),1,fp);
	str = (char*)malloc(strtab->sh_size);
	fseek(fp,strtab->sh_offset,0);
	fread(str,strtab->sh_size,1,fp);
	fseek(fp,header->e_shoff,0);
						
	while(i<header->e_shnum)
	{
		secheader[i] = (elfsecheader*)malloc(sizeof(elfsecheader));
 		fread(secheader[i],sizeof(elfsecheader),1,fp);
		str1 = str;
		str1 = str1 + secheader[i]->sh_name;
	
		flag = strcmp(str1,".strtab");
		if(flag == 0)
		{
			stroff = secheader[i]->sh_offset;		
			strsize = secheader[i]->sh_size;
			flag = 1;
		}

		fprintf(f,"  [%2d]",i);

		fprintf(f,"  %-19s",str1);

		switch(secheader[i]->sh_type)
		{	
			case 0x00000000:fprintf(f,"NULL           ");break;
			case 0x00000001:fprintf(f,"PROGBITS       ");break;
			case 0x00000002:fprintf(f,"SYMTAB         ");break;
			case 0x00000003:fprintf(f,"STRTAB         ");break;
			case 0x00000004:fprintf(f,"RELA           ");break;
			case 0x00000005:fprintf(f,"HASH           ");break;
			case 0x00000006:fprintf(f,"DYNAMIC        ");break;
			case 0x00000007:fprintf(f,"NOTE           ");break;
			case 0x00000008:fprintf(f,"NOBITS         ");break;
			case 0x00000009:fprintf(f,"REL            ");break;
			case 0x00000010:fprintf(f,"SHLIB          ");break;
			case 0x00000011:fprintf(f,"DYNSYM         ");break;
			case 0x70000000:fprintf(f,"LOPROC         ");break;
			case 0x7fffffff:fprintf(f,"HIPROC         ");break;
			case 0x80000000:fprintf(f,"LOUSER         ");break;
			case 0xffffffff:fprintf(f,"HIUSER         ");break;
			default:fprintf(f,"               ");
		}
	
	
        	fprintf(f," %08x",secheader[i]->sh_addr);
        	fprintf(f," %06x",secheader[i]->sh_offset);
        	fprintf(f," %06x",secheader[i]->sh_size);
		fprintf(f," %02x",secheader[i]->sh_entsize);
		switch(secheader[i]->sh_flags)
		{
			case 0x00000001:fprintf(f,"   W ");break;
			case 0x00000002:fprintf(f,"   A ");break;
			case 0x00000004:fprintf(f,"   X ");break;
			case 0xf0000000:fprintf(f,"   M ");break;
			case 0x00000003:fprintf(f,"  WA ");break;
			case 0x00000005:fprintf(f,"  WX ");break;
			case 0x00000006:fprintf(f,"  AX ");break;
			default:fprintf(f,"     ");
		}
        	fprintf(f," %-2d",secheader[i]->sh_link);
        	fprintf(f,"  %-2d",secheader[i]->sh_info);
        	fprintf(f," %d\n",secheader[i]->sh_addralign);
		i++;
	}
	fprintf(f,"Key to Flags:");
	fprintf(f,"\nW (write), A (alloc), X (execute), M (merge), S (strings)");
  	fprintf(f,"\nI (info), L (link order), G (group), x (unknown)");
  	fprintf(f,"\nO (extra OS processing required) o (OS specific), p (processor specific)");
}

void getsecgroups()
{

}

void getprogheader()
{
	if(header->e_phnum == 0)
	{
		fprintf(f,"\n\nThere are no program headers in this file.");
		return;
	}
	int i=0;
	progheader = (elfprogheader*)malloc(sizeof(elfprogheader));
	fprintf(f,"\n\nProgram Headers:");
	fprintf(f,"\n Type           Offset   VirtAddr   PhysAddr   FileSiz MemSiz  Flg Align\n");

	fseek(fp,header->e_phoff,0);
	while(i<header->e_phnum)
	{
		fread(progheader,sizeof(elfprogheader),1,fp);
		switch(progheader->p_type)
		{
			case 0:fprintf(f," NULL           ");break;
			case 1:fprintf(f," LOAD           ");break;
			case 2:fprintf(f," DYNAMIC        ");break;
			case 3:fprintf(f," INTERP         ");break;
			case 4:fprintf(f," NOTE           ");break;
			case 5:fprintf(f," SHLIB          ");break;
			case 6:fprintf(f," PHDR           ");break;
			case 0x70000000:fprintf(f," LOPROC         ");break;
			case 0x7fffffff:fprintf(f," HIPROC         ");break;
			default:fprintf(f," DEFAULT        ");
		}
		
		fprintf(f,"0x%06x",progheader->p_offset);
		fprintf(f," 0x%08x",progheader->p_vaddr);
		fprintf(f," 0x%08x",progheader->p_paddr);
		fprintf(f," 0x%05x",progheader->p_filesz);
		fprintf(f," 0x%05x",progheader->p_memsz);
		fprintf(f,"     0x%x",progheader->p_align);
		fprintf(f,"\n");	
		i++;

	}
}

void getdynsec()
{

}

void getelfreloc()
{	
	int j=0,flag=1;
	int entries,i=0;
	unsigned int info;
	reltab = (elfrel*)malloc(sizeof(elfrel));
	char *name = NULL;
	
	while(j<header->e_shnum)
	{
		name = name + secheader[j]->sh_name;
		flag = strncmp(name,".rel",4);
		if(flag == 0)
		{
			entries = secheader[j]->sh_size/8;
			fprintf(f,"\n\nRelocation section '%s' at offset 0x%x contains %d entries:",name,secheader[j]->sh_offset,entries);
			fprintf(f,"\n Offset     Info    Type            Sym.Value  Sym. Name\n");
			fseek(fp,secheader[j]->sh_offset,0);
			while(i<entries)
			{
				fread(reltab,sizeof(elfrel),1,fp);
				fprintf(f,"%08x",reltab->r_offset);
				fprintf(f,"  %08x",reltab->r_info);
				info = secheader[j]->sh_offset + reltab->r_info;
       				fprintf(f,"  %08x\n",ELF32_R_SYM(info));
				i++;	
			}
			i=0;
			flag = 1;
		}
		j++;
	}	
}

void getunwindsec()
{

}

void getelfsymtab()
{
	int entries,i=0;
	unsigned char info;
	char *str,*str1;
	int j=0,flag1=1,flag2=1;
	char *name = NULL;	

	symtab = (elfsymtab*)malloc(sizeof(elfsymtab));

	fseek(fp,stroff,0);
	str = (char*)malloc(strsize);
	fread(str,strsize,1,fp);

	while(j<header->e_shnum)
	{
		name = name + secheader[j]->sh_name;
		flag1 = strcmp(name,".symtab");
		flag2 = strcmp(name,".dynsym");
			
		if(flag1 == 0 || flag2 == 0)
		{	
			entries = secheader[j]->sh_size/16;
			fprintf(f,"\n\nSymbol table '%s' contains %d entries:",name,entries);
			fprintf(f,"\n  Num:    Value  Size Type    Bind   Vis      Ndx Name\n");
			fseek(fp,secheader[j]->sh_offset,0);
			while(i<entries)
			{
				fread(symtab,sizeof(elfsymtab),1,fp);
				fprintf(f," %4d:",i);
				fprintf(f," %08x",symtab->st_value);
				fprintf(f,"  %4d",symtab->st_size);

				info = symtab->st_info;
				switch(ELF32_ST_TYPE(info))
				{
					case 0:fprintf(f," NOTYPE ");break;
					case 1:fprintf(f," OBJECT ");break;
					case 2:fprintf(f," FUNC   ");break;
					case 3:fprintf(f," SECTION");break;
					case 4:fprintf(f," FILE   ");break;
					case 13:fprintf(f," LOPROC");break;
					case 15:fprintf(f," HIPROC");break;
				}

				info = symtab->st_info;
				switch(ELF32_ST_BIND(info))
				{
					case 0:fprintf(f," LOCAl  ");break;
					case 1:fprintf(f," GLOBAl ");break;
					case 2:fprintf(f," WEAK   ");break;
					case 13:fprintf(f," LOPROC");break;
					case 15:fprintf(f," HIPROC");break;
				}

				fprintf(f,"DEFAULT");
				switch(symtab->st_shndx)
				{
					case 0:fprintf(f,"  UND");break;
					case 0xfff1:fprintf(f,"  ABS");break;
					case 0xfff2:fprintf(f,"  COM");break;
					default:fprintf(f,"   %2d",symtab->st_shndx);
				}
		
				str1 = str;
				str1 = str1 + symtab->st_name;
				fprintf(f," %s",str1);
		
				fprintf(f,"\n");
				i++;
			}
			if(flag1 == 0)
				flag1 = 1;
			if(flag2 == 0)
				flag2 = 1;
			i=0;
		}
		j++;
	}
}
void getversioninfo()
{

}

int main(int argc, char *argv[])
{	
	fp = fopen(argv[1],"r");
	if(fp == NULL)
		fprintf(f,"Error in opening the file");
	
	f = fopen(argv[2],"w");

printf("\n\nHELLO");
	getelfheader();

	getelfsecheader();

	getsecgroups();

	getprogheader();

	getdynsec();

	getelfreloc();

	getunwindsec();

	getelfsymtab();

	getversioninfo();

	fprintf(f,"\n\n");
	fclose(fp);
	fclose(f);
}
	
	
	



