#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <sys/mman.h>

int a = 5;
unsigned int *ptr2;

int main()
{
        int fd;

	printf("I am in App 2 \n");
        
        ptr2 = (unsigned int *)malloc(4096 * 4);

        printf("Allocated 4 pages; viradd = %08x \n",(unsigned int)ptr2);

        fd = open("/dev/mydev",O_RDWR);

        if(fd < 0)
                perror("Unable to open the Device\n");
        else
                printf("File open Successful %d\n",fd);

        ioctl(fd,a,&a);

        printf("VMA Dumped for App 2; PID = %d\n", getpid());

        while (1) { };
        close(fd);
        return 0;
}

