
/*THIS PROGRAM DOES OPERATION OF "readelf" UTILITY IN LINUX*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "elf.h"

/*****************************Global variable declarations**********************************/

FILE *fp,*f;
elfheader *header;
elfsecheader *secheader;
elfsecheader *strtab;
elfsymtab *symtab;
elfrel *reltab;
elfprogheader *progheader;
unsigned int reloff,symoff,relsize,symsize,stroff,strsize;
unsigned int dynsmoff,dynsmsize,dynstroff,dynstrsize,rdynoff,rdynsize,pltoff,pltsize;
int flg = 0,fl = 0;

/*****************************Function to get elf header***********************************/

void getelfheader()
{
	int i;

	header = (elfheader*)malloc(sizeof(elfheader));

	fread(header,sizeof(elfheader),1,fp);

	if((header->e_ident[0] != 0x7f) && (header->e_ident[1] != 0x45)	&& 
				(header->e_ident[2] != 0x4c) && (header->e_ident[3] != 0x46))
	{
		printf("\nreadelf: Error: Not an ELF file - it has the wrong magic bytes at the start\n\n");
		exit(0);
	}
	
	fprintf(f,"\nElf Header:");
	fprintf(f,"\n  Magic:   ");
	for(i=0;i<16;i++)
	{
		fprintf(f,"%02x ",header->e_ident[i]);
	}
	fprintf(f,"\n  Class: ");

	switch(header->e_ident[4])
	{
		case 0x00:fprintf(f,"\t\t\t\tInvalid class");break;
	 	case 0x01:fprintf(f,"\t\t\t\tELF32");break;
	 	case 0x02:fprintf(f,"\t\t\t\tELF64");break;
	}

	fprintf(f,"\n  Data: ");

	switch(header->e_ident[5])
	{
		case 0x00:fprintf(f,"\t\t\t\tInvalid data encoding");break;
		case 0x01:fprintf(f,"\t\t\t\t2's complement, little endian");break;
		case 0x02:fprintf(f,"\t\t\t\t2's complement, big endian");break;
	}

	fprintf(f,"\n  Version: ");
	
	switch(header->e_ident[5])
	{
		case 0x00:fprintf(f,"\t\t\t\tInvalid version");break;
		case 0x01:fprintf(f,"\t\t\t\t1 (current)");break;
	}
	fprintf(f,"\n  OS/ABI: ");
	fprintf(f,"\t\t\t\tUNIX - System V");
	
	fprintf(f,"\n  ABI Version: ");
	fprintf(f,"\t\t\t\t0");
	
	fprintf(f,"\n  Type: ");
	 	
	switch(header->e_type)
	{
		case 0x0000:fprintf(f,"\t\t\t\tNONE (No file type)");break;
		case 0x0001:fprintf(f,"\t\t\t\tREL (Relocatable file)");break;
		case 0x0002:fprintf(f,"\t\t\t\tEXEC (Executable file)");break;
		case 0x0003:fprintf(f,"\t\t\t\tDYN (Shared object file)");break;
		case 0x0004:fprintf(f,"\t\t\t\tCORE (Core file)");break;
		case 0xff00:fprintf(f,"\t\t\t\tLOPROC (Processor specific)");break;
		case 0xffff:fprintf(f,"\t\t\t\tHIPROC (Processor specific)");break;
	}

	fprintf(f,"\n  Machine: ");
	 	
	switch(header->e_machine)
	{
		case 0x0000:fprintf(f,"\t\t\t\tNONE (No machine)");break;
		case 0x0001:fprintf(f,"\t\t\t\tAT&T WE 32100");break;
		case 0x0002:fprintf(f,"\t\t\t\tSPARC");break;
		case 0x0003:fprintf(f,"\t\t\t\tIntel 80386");break;
		case 0x0004:fprintf(f,"\t\t\t\tMotorola 68000");break;
		case 0x0005:fprintf(f,"\t\t\t\tMotorola 88000");break;
		case 0x0007:fprintf(f,"\t\t\t\tIntel 80860");break;
		case 0x0008:fprintf(f,"\t\t\t\tMIPS RS3000 Big-Endian");break;
		case 0x0010:fprintf(f,"\t\t\t\tMIPS RS4000 Big-Endian");break;
		default:    if((header->e_machine >= 0x0011) && (header->e_machine <= 0x0016))
			 	fprintf(f,"\t\t\t\tRESERVED");
	}

	fprintf(f,"\n  Version: ");

	switch(header->e_version)
	{
		case 0x00000000:fprintf(f,"\t\t\t\t0x0");break;
		case 0x00000001:fprintf(f,"\t\t\t\t0x1");break;
		default:fprintf(f,"\t\t\t\t0x%08x",header->e_version);
	}

	fprintf(f,"\n  Entry point address: ");

	switch(header->e_entry)
	{
		case 0x00000000:fprintf(f,"\t\t\t0x0");break;
		case 0x00000001:fprintf(f,"\t\t\t0x1");break;
		default:fprintf(f,"\t\t\t0x%08x",header->e_entry);
	}

  	fprintf(f,"\n  Start of program headers: ");  
	fprintf(f,"\t\t%x (bytes into file)",header->e_phoff);  
      
  	fprintf(f,"\n  Start of section headers: "); 
	fprintf(f,"\t\t%d (bytes into file)",header->e_shoff);     
   
  	fprintf(f,"\n  Flags: ");      
	fprintf(f,"\t\t\t\t0x%x",header->e_flags);  

  	fprintf(f,"\n  Size of this header: "); 
	fprintf(f,"\t\t\t%d (bytes)",header->e_ehsize);   
            
  	fprintf(f,"\n  Size of program headers: ");  
	fprintf(f,"\t\t%d (bytes)",header->e_phentsize); 
        
  	fprintf(f,"\n  Number of program headers: ");     
	fprintf(f,"\t\t%d",header->e_phnum);  
 
  	fprintf(f,"\n  Size of section headers: "); 
	fprintf(f,"\t\t%d (bytes)",header->e_shentsize);  
       
  	fprintf(f,"\n  Number of section headers: ");  
	fprintf(f,"\t\t%d",header->e_shnum);    
   
  	fprintf(f,"\n  Section header string table index: "); 
	fprintf(f,"\t%d",header->e_shstrndx);

}

/*****************************Function to get elf section headers****************************/

void getelfsecheader()
{
	if(header->e_shnum == 0)
	{
		fprintf(f,"\n\nThere are no section headers in this file.");
		return;
	}
	int i=0;
	int flag1,flag2,flag3,flag4,flag5,flag6,flag7;
	char *str,*str1;
		
	secheader = (elfsecheader*)malloc(sizeof(elfsecheader));
	strtab = (elfsecheader*)malloc(sizeof(elfsecheader));

	fprintf(f,"\n\nSection Headers:");
	fprintf(f,"\n  [Nr]  Name               Type            Addr     Off    Size   ES Flg Lk Inf Al");
	fprintf(f,"\n");

	fseek(fp,header->e_shoff + (header->e_shstrndx * 40),0);
	
	fread(strtab,sizeof(elfsecheader),1,fp);
	str = (char*)malloc(strtab->sh_size);
	fseek(fp,strtab->sh_offset,0);
	fread(str,strtab->sh_size,1,fp);
	fseek(fp,header->e_shoff,0);
						
	while(i<header->e_shnum)
	{
		fread(secheader,sizeof(elfsecheader),1,fp);
		str1 = str;
		str1 = str1 + secheader->sh_name;
		fprintf(f,"  [%2d]",i);

		fprintf(f,"  %-19s",str1);

		flag1 = strcmp(str1,".symtab");
		flag2 = strcmp(str1,".rel.text");
		flag3 = strcmp(str1,".strtab");
		flag4 = strcmp(str1,".dynsym");
		flag5 = strcmp(str1,".dynstr");
		flag6 = strcmp(str1,".rel.dyn");
		flag7 = strcmp(str1,".rel.plt");

		if(flag1 == 0)
		{
			symoff = secheader->sh_offset;
			symsize = secheader->sh_size;
			flag1 = 1;
		}
		if(flag2 == 0)
		{
			reloff = secheader->sh_offset;
			relsize = secheader->sh_size;
			flag2 = 1;
			flg = 1;
		}
		if(flag3 == 0)
		{
			stroff = secheader->sh_offset;
			strsize = secheader->sh_size;
			flag3 = 1;
		}	
		if(flag4 == 0)
		{
			dynsmoff = secheader->sh_offset;
			dynsmsize = secheader->sh_size;
			flag4 = 1;
			fl = 1;
		}
		if(flag5 == 0)
		{
			dynstroff = secheader->sh_offset;
			dynstrsize = secheader->sh_size;
			flag5 = 1;
		}
		if(flag6 == 0)
		{
			rdynoff = secheader->sh_offset;
			rdynsize = secheader->sh_size;
			flag6 = 1;
			flg = 0;
		}
		if(flag7 == 0)
		{
			pltoff = secheader->sh_offset;
			pltsize = secheader->sh_size;
			flag7 = 1;
			flg = 0;
		}
	
		switch(secheader->sh_type)
		{	
			case 0x00000000:fprintf(f,"NULL           ");break;
			case 0x00000001:fprintf(f,"PROGBITS       ");break;
			case 0x00000002:fprintf(f,"SYMTAB         ");break;
			case 0x00000003:fprintf(f,"STRTAB         ");break;
			case 0x00000004:fprintf(f,"RELA           ");break;
			case 0x00000005:fprintf(f,"HASH           ");break;
			case 0x00000006:fprintf(f,"DYNAMIC        ");break;
			case 0x00000007:fprintf(f,"NOTE           ");break;
			case 0x00000008:fprintf(f,"NOBITS         ");break;
			case 0x00000009:fprintf(f,"REL            ");break;
			case 0x00000010:fprintf(f,"SHLIB          ");break;
			case 0x00000011:fprintf(f,"DYNSYM         ");break;
			case 0x70000000:fprintf(f,"LOPROC         ");break;
			case 0x7fffffff:fprintf(f,"HIPROC         ");break;
			case 0x80000000:fprintf(f,"LOUSER         ");break;
			case 0xffffffff:fprintf(f,"HIUSER         ");break;
			default:fprintf(f,"               ");
		}
	
	
        	fprintf(f," %08x",secheader->sh_addr);
        	fprintf(f," %06x",secheader->sh_offset);
        	fprintf(f," %06x",secheader->sh_size);
		fprintf(f," %02x",secheader->sh_entsize);
		switch(secheader->sh_flags)
		{
			case 0x00000001:fprintf(f,"   W ");break;
			case 0x00000002:fprintf(f,"   A ");break;
			case 0x00000004:fprintf(f,"   X ");break;
			case 0xf0000000:fprintf(f,"   M ");break;
			case 0x00000003:fprintf(f,"  WA ");break;
			case 0x00000005:fprintf(f,"  WX ");break;
			case 0x00000006:fprintf(f,"  AX ");break;
			default:fprintf(f,"     ");
		}
        	fprintf(f," %-2d",secheader->sh_link);
        	fprintf(f,"  %-2d",secheader->sh_info);
        	fprintf(f," %d\n",secheader->sh_addralign);
		i++;
	}
	fprintf(f,"Key to Flags:");
	fprintf(f,"\nW (write), A (alloc), X (execute), M (merge), S (strings)");
  	fprintf(f,"\nI (info), L (link order), G (group), x (unknown)");
  	fprintf(f,"\nO (extra OS processing required) o (OS specific), p (processor specific)");
}

void getsecgroups()
{

}

/*****************************Function to get elf program headers**************************/

void getprogheader()
{
	if(header->e_phnum == 0)
	{
		fprintf(f,"\n\nThere are no program headers in this file.");
		return;
	}
	int i=0;
	progheader = (elfprogheader*)malloc(sizeof(elfprogheader));
	fprintf(f,"\n\nProgram Headers:");
	fprintf(f,"\n Type           Offset   VirtAddr   PhysAddr   FileSiz MemSiz  Flg Align\n");

	fseek(fp,header->e_phoff,0);
	while(i<header->e_phnum)
	{
		fread(progheader,sizeof(elfprogheader),1,fp);
		switch(progheader->p_type)
		{
			case 0:fprintf(f," NULL           ");break;
			case 1:fprintf(f," LOAD           ");break;
			case 2:fprintf(f," DYNAMIC        ");break;
			case 3:fprintf(f," INTERP         ");break;
			case 4:fprintf(f," NOTE           ");break;
			case 5:fprintf(f," SHLIB          ");break;
			case 6:fprintf(f," PHDR           ");break;
			case 0x70000000:fprintf(f," LOPROC         ");break;
			case 0x7fffffff:fprintf(f," HIPROC         ");break;
			default:fprintf(f," DEFAULT        ");
		}
		
		fprintf(f,"0x%06x",progheader->p_offset);
		fprintf(f," 0x%08x",progheader->p_vaddr);
		fprintf(f," 0x%08x",progheader->p_paddr);
		fprintf(f," 0x%05x",progheader->p_filesz);
		fprintf(f," 0x%05x",progheader->p_memsz);

		switch(progheader->p_flags)
		{
                	case 0:fprintf(f,"    ");break;
             		case 1:fprintf(f,"   E");break;
             		case 2:fprintf(f,"  W ");break;
         		case 3:fprintf(f,"  WE");break;
               		case 4:fprintf(f," R  ");break;
         		case 5:fprintf(f," R E");break;
			case 6:fprintf(f," RW ");break;
			case 7:fprintf(f," RWE");break;
		}

		fprintf(f," 0x%x",progheader->p_align);
		fprintf(f,"\n");	
		i++;

	}
}

void getdynsec()
{

}

/*****************************Function to print relocation table**********************************/

void printreltab(int entries,int flag,char *str,unsigned int offset)
{
	int i=0,n;
	unsigned int info;
	char *str1;

	switch(flag)
	{
		case 0:	fprintf(f,"\n\nRelocation section '.rel.dyn' at offset 0x%x contains %d entries:",rdynoff,entries);
			fprintf(f,"\n Offset     Info    Type                 Sym.Value  Sym. Name\n");break;
		case 1:	fprintf(f,"\n\nRelocation section '.rel.plt' at offset 0x%x contains %d entries:",pltoff,entries);
			fprintf(f,"\n Offset     Info    Type                 Sym.Value  Sym. Name\n");break;
		case 2:	fprintf(f,"\n\nRelocation section '.rel.text' at offset 0x%x contains %d entries:",reloff,entries);
			fprintf(f,"\n Offset     Info    Type                 Sym.Value  Sym. Name\n");break;
	}
	
	symtab = (elfsymtab*)malloc(sizeof(elfsymtab));
	reltab = (elfrel*)malloc(sizeof(elfrel));	

	while(i<entries)
	{
		fread(reltab,sizeof(elfrel),1,fp);
		fprintf(f,"%08x",reltab->r_offset);
		fprintf(f,"  %08x",reltab->r_info);
		info = reltab->r_info;
		switch(ELF32_R_TYPE(info))
		{
			case 0:fprintf(f,"  R_386_NONE     ");break;
			case 1:fprintf(f,"  R_386_32       ");break;
			case 2:fprintf(f,"  R_386_PC32     ");break;
			case 3:fprintf(f,"  R_386_GOT32    ");break;
			case 4:fprintf(f,"  R_386_PLT32    ");break;
			case 5:fprintf(f,"  R_386_COPY     ");break;
			case 6:fprintf(f,"  R_386_GLOB_DAT ");break;
			case 7:fprintf(f,"  R_386_JMP_SLOT ");break;
			case 8:fprintf(f,"  R_386_RELATIVE ");break;
			case 9:fprintf(f,"  R_386_GOTOFF   ");break;
			case 10:fprintf(f,"  R_386_GOTPC    ");break;
  		}
 
		info = reltab->r_info;
		n = ftell(fp);

		fseek(fp,(offset + (16 * ELF32_R_SYM(info))),0);
		fread(symtab,sizeof(elfsymtab),1,fp);
		
       		fprintf(f,"      %08x",symtab->st_value);
		str1 = str;
		str1 = str1 + symtab->st_name;		
		fprintf(f,"   %s",str1);
		
		fseek(fp,n,0);
		i++;
		fprintf(f,"\n");
	}
}

/*****************************Function to get relocation table**********************************/

void getelfreloc()
{	
	int entries;
	char *str;
	int flag;
	
	if (flg == 0)
	{
	fseek(fp,dynstroff,0);
	str = (char*)malloc(dynstrsize);
	fread(str,dynstrsize,1,fp);
	flag = 0;
	entries = rdynsize/8;
	fseek(fp,rdynoff,0);
	printreltab(entries,flag,str,dynsmoff);

	fseek(fp,dynstroff,0);
	str = (char*)malloc(dynstrsize);
	fread(str,dynstrsize,1,fp);
	flag = 1;
	entries = pltsize/8;
	fseek(fp,pltoff,0);
	printreltab(entries,flag,str,dynsmoff);
	}

	if(flg == 1)
	{
	fseek(fp,stroff,0);
	str = (char*)malloc(strsize);
	fread(str,strsize,1,fp);
	flag = 2;
	entries = relsize/8;
	fseek(fp,reloff,0);
	printreltab(entries,flag,str,symoff);
	}
}
void getunwindsec()
{

}

/*****************************Function to print symbol table**********************************/

void printsymdyn(int entries,int flag,char *str)
{
	int i =0;
	unsigned char info;
	char *str1;

	if(flag == 1)
		fprintf(f,"\n\nSymbol table '.symtab' contains %d entries:",entries);
	if(flag == 0)
		fprintf(f,"\n\nSymbol table '.dynsym' contains %d entries:",entries);

	fprintf(f,"\n  Num:    Value  Size Type    Bind   Vis      Ndx Name\n");

	symtab = (elfsymtab*)malloc(sizeof(elfsymtab));

	while(i<entries)
	{
		fread(symtab,sizeof(elfsymtab),1,fp);
		fprintf(f," %4d:",i);
		fprintf(f," %08x",symtab->st_value);
		fprintf(f,"  %4d",symtab->st_size);

		info = symtab->st_info;
		switch(ELF32_ST_TYPE(info))
		{
			case 0:fprintf(f," NOTYPE ");break;
			case 1:fprintf(f," OBJECT ");break;
			case 2:fprintf(f," FUNC   ");break;
			case 3:fprintf(f," SECTION");break;
			case 4:fprintf(f," FILE   ");break;
			case 13:fprintf(f," LOPROC");break;
			case 15:fprintf(f," HIPROC");break;
		}

		info = symtab->st_info;
		switch(ELF32_ST_BIND(info))
		{
			case 0:fprintf(f," LOCAl  ");break;
			case 1:fprintf(f," GLOBAl ");break;
			case 2:fprintf(f," WEAK   ");break;
			case 13:fprintf(f," LOPROC");break;
			case 15:fprintf(f," HIPROC");break;
		}

		switch(symtab->st_other)
		{
			case 0:fprintf(f,"DEFAULT");break;
			default:fprintf(f,"HIDDEN ");break;
		}

		switch(symtab->st_shndx)
		{
			case 0:fprintf(f,"  UND");break;
			case 0xff00:fprintf(f,"  LOPROC");break;
			case 0xff1f:fprintf(f,"  HIPROC");break;
			case 0xfff1:fprintf(f,"  ABS");break;
			case 0xfff2:fprintf(f,"  COM");break;
			default:fprintf(f,"   %2d",symtab->st_shndx);
		}
		
		str1 = str;
		str1 = str1 + symtab->st_name;
		fprintf(f," %s",str1);
		
		fprintf(f,"\n");
		i++;
	}
}

/*****************************Function to get symbol table**********************************/

void getelfsymtab()
{
	int entries;	
	int flag;
	char *str;
	
	if(fl == 1)
	{	
	fseek(fp,dynstroff,0);
	str = (char*)malloc(strsize);
	fread(str,dynstrsize,1,fp);
	
	fseek(fp,dynsmoff,0);
	entries = dynsmsize/16;
	flag = 0;
	
	printsymdyn(entries,flag,str);
	}

	fseek(fp,stroff,0);
	str = (char*)malloc(strsize);
	fread(str,strsize,1,fp);
	
	entries = symsize/16;
	fseek(fp,symoff,0);
	flag = 1;
	
	printsymdyn(entries,flag,str);
}

void getversioninfo()
{

}

/************************************Main Function********************************************/

int main(int argc, char *argv[])
{	
	fp = fopen(argv[1],"r");
	if(fp == NULL)
		fprintf(f,"Error in opening the file");
	
	f = fopen(argv[2],"w");

	getelfheader();

	getelfsecheader();

	getsecgroups();

	getprogheader();

	getdynsec();

	getelfreloc();

	getunwindsec();

	getelfsymtab();

	getversioninfo();

	fprintf(f,"\n\n");
	fclose(fp);
	fclose(f);
}
	
	
	



