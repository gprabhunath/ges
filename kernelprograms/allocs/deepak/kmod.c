#include <linux/init.h>
#include <linux/module.h>
#include <linux/fs.h>

MODULE_LICENSE("Dual BSD/GPL");

char *devname;

int majNo;

module_param(devname,charp,0000);

int myOpen(struct inode *inode,struct file *filep);

int myRelease(struct inode *in,struct file *fp);

static int myInit(void);
static void myExit(void);

struct file_operations fops = {open:myOpen,release:myRelease};

static int myInit(void)
{
	printk(KERN_INFO "Initializing the World\n");
	majNo = register_chrdev(0,devname,&fops);
	if(majNo < 0)
	{
		printk(KERN_INFO "register_chrdev failed\n");
	}
	else
	{
		printk(KERN_INFO "Major Number of the Device = %d\n",majNo);
	}

	return 0;
}

int myOpen(struct inode *inode,struct file *filep)
{
	printk(KERN_INFO "Open Successful\n");
	return 0;
}

int myRelease(struct inode *in,struct file *fp)
{
	printk(KERN_INFO "File Released\n");
	return 0;
}

static void myExit(void)
{
	printk(KERN_INFO "Exiting the World\n");
	unregister_chrdev(majNo,devname);
	return 0;
}

module_init(myInit);
module_exit(myExit);

		


