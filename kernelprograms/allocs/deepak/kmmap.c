#include <linux/init.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/sched.h>
#include <linux/mm.h>

MODULE_LICENSE("Dual BSD/GPL");

char *devname;

int majNo;

module_param(devname,charp,0000);

int myOpen(struct inode *inode,struct file *filep);

int myRelease(struct inode *in,struct file *fp);

int myMmap (struct file *f, struct vm_area_struct *vma);

static int myInit(void);
static void myExit(void);

struct file_operations fops = {open:myOpen,release:myRelease,mmap:myMmap};

static int myInit(void)
{
	printk(KERN_INFO "\nInitializing the World\n");
	majNo = register_chrdev(0,devname,&fops);
	if(majNo < 0)
	{
		printk(KERN_INFO "register_chrdev failed\n");
	}
	else
	{
		printk(KERN_INFO "Major Number of the Device = %d\n",majNo);
	}

	return 0;
}

int myOpen(struct inode *inode,struct file *filep)
{
	printk(KERN_INFO "Open Successful\n");
	return 0;
}

int myRelease(struct inode *in,struct file *fp)
{
	printk(KERN_INFO "File Released\n\n");
	return 0;
}

int myMmap (struct file *f, struct vm_area_struct *vma)
{
	int flag ;
	unsigned long size;

	size = vma->vm_end - vma->vm_start;
	
	printk(KERN_INFO "Size = %d\n\n",(unsigned int)size); 
	
	flag = remap_pfn_range(vma,vma->vm_start,0x20002,size,vma->vm_page_prot);
	printk ("Value of flag = %d \n", flag);


	if(flag == 0)
		printk(KERN_INFO "Mapping Successful\n\n");
	else
		printk(KERN_INFO "Mapping Failure\n\n");

	
	return 0;
}


static void myExit(void)
{
	printk(KERN_INFO "\nExiting the World\n");
	unregister_chrdev(majNo,devname);
	return;
}

module_init(myInit);
module_exit(myExit);

