#include <stdio.h>
#include "common.h"

int main()
{
	int shmid;
	int semid1;
	int semid2;
	int i;
	int flag = 1;
	unsigned int *map_add;
	database *mydatabase;
	struct sembuf semaphore1,semaphore2;
	FILE *fp;

	fp = fopen("database.txt","r");	

	mydatabase = (database *)malloc(sizeof(database));

	shmid = shmget(MY_SHM_ID,4096,0666 | IPC_CREAT);
	if(shmid == -1)
	{
		printf("Shared Page cannot be created \n");
		exit(0);
	}

	map_add = shmat(shmid,(const void*)0,0);

	printf("Mapped address = %08x shmid = %d \n",(unsigned int)map_add,shmid);

	semid1 = semget(MY_SEM_ID1,1,0666 | IPC_CREAT);
	if(semid1 == -1)
	{
		printf("Semaphore1 create failure \n");
		exit(0);
	}
	semctl(semid1,0,SETVAL,0);

	semaphore1.sem_num = 0;	
	semaphore1.sem_op  = -1;
	semaphore1.sem_flg = 0;
	semop(semid1,&semaphore1,1);

	semid2 = semget(MY_SEM_ID2,1,0);	
	if(semid2 == -1)
	{
		printf("Semaphore2 create failure \n");
		exit(0);
	}

	for(i=0;i<MAX_SIZE;i++)
	{	
                fread(mydatabase,sizeof(database),1,fp);
		flag = strcmp((char *)map_add,mydatabase->name);
	
		if(flag == 0)
		{
			semaphore2.sem_num = 0;
			semaphore2.sem_op  = 1;
			semaphore2.sem_flg = 0;
			strcpy((char *)map_add,mydatabase->number);
			printf("\n Number Sent \n\n");
			semop(semid2,&semaphore2,1);
			exit(0);
		}
	}

	printf("\n No such Entry \n\n");
	semaphore2.sem_num = 0;
	semaphore2.sem_op  = 1;
	semaphore2.sem_flg = 0;
	strcpy((char *)map_add,"NO ENTRY");
	semop(semid2,&semaphore2,1);
	fclose(fp);
	return 0;
}
