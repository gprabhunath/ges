//#include <linux/init.h>
//#include <linux/module.h>
//#include <linux/kernel.h>
//#include <linux/fs.h>
#include <linux/sched.h>
#include <linux/highmem.h>
#include <asm-i386/page.h>
#include <asm/processor.h>
#include <asm/fixmap.h>

//MODULE_LICENSE("Dual BSD/GPL");

#define VA(addr) addr+0xc0000000

//extern pgd_t swapper_pg_dir[1024];
void pgwalk(unsigned long);
void pgdDump(void);
void ptDump (unsigned int);

struct page *pgaddr;
struct task_struct *task;
int *kaddr;

void ptDump (unsigned int pgdEntry)
{
	unsigned int *pt_base;
	int j = 0;

	pgdEntry = pgdEntry & 0xfffff000;
	pt_base = (unsigned int *)(pgdEntry + 0xc0000000);
	while (j < 1024)
	{
		if (*pt_base != 0x0)
		{
			printk (KERN_INFO "\t\t %03x  %08x \n", j, *pt_base);
		}

		pt_base++;
		j++;
	} // End of While 

	return;

} // End of ptDump


void pgdDump(void)
{
	int i = 0x340;
    unsigned int *pgd_base;
    unsigned int *vir_pgd;

	task = current;
    pgd_base = (unsigned int *)task->mm->pgd;

    printk(KERN_INFO "-----------------------------------\n\n");
    printk(KERN_INFO "\nPGD BASE ADDRESS = 0x%08x\n\n",(unsigned int)pgd_base);

    vir_pgd = pgd_base + i;

    while(i < 0x350)
    {
		if(*vir_pgd == 0x00000000)
		{	
			i++;
			vir_pgd++;
			continue;
		}

        printk(KERN_INFO "%03x  %08x\n",i, *vir_pgd);
		ptDump(*vir_pgd);
		i++;
	    vir_pgd++;
	}
        
    printk(KERN_INFO "-----------------------------------\n\n");
	return;

} // End of pgdDump



void pgwalk (unsigned long arg)
{
 	pgd_t *pgdbase;
 	pgd_t *pgd_indexed_addr;
	pte_t *pt_base;
	unsigned long pfn;
	unsigned long virt_addr;
 	pte_t *pt_indexed_addr;
	int *addr;
	int pgd_offset;
	int pt_offset;
	int page_offset;
	int value;
		
	addr=(int *)arg;
//	*(addr) = 0x4501;
	pgd_offset=(unsigned int )addr >> 22;
	pt_offset=((unsigned int)addr<<10) >> 22;
	page_offset=((unsigned int)addr&0x00000fff);
	
	printk(KERN_INFO "pgd_offset=%x\t pt_offset=%x\t page_offset=%x \n",
					pgd_offset,pt_offset,page_offset);

//	printk(KERN_INFO "MK PGD entry value of pgd_offset = 0x%08x \n", swapper_pg_dir[pgd_offset]);
	printk(KERN_INFO "Virtual addr = %p \n",addr);
	printk(KERN_INFO "addr_val = 0x%08x \n",*addr);

	// Get the pointer of task_struct
    //task=find_task_by_pid(command);	
	task = current;
    pgdbase=task->mm->pgd; // Get the base address of PGD
	printk(KERN_INFO "pgdbase = %p \n",(unsigned long *)pgdbase);

	pgd_indexed_addr=pgdbase+pgd_offset; // Get the entry in the PGD
	printk(KERN_INFO "pgd_indexed_addr = %p \n",
				(unsigned long *)pgd_indexed_addr);

	// Get Page Table Base address
	pt_base= (pte_t *)((pgd_indexed_addr->pgd&0xFFFFF000)+0xC0000000);
	printk(KERN_INFO "pt_base = %p \n",(unsigned long *)pt_base);
	pt_indexed_addr=pt_base+pt_offset; // Get the entry in the Page Table
	printk(KERN_INFO "pt_indexed_addr = %p \n",
				(unsigned long *)pt_indexed_addr);
	pfn = pt_indexed_addr->pte_low&0xFFFFF000; // Get the page base address
	printk(KERN_INFO "pfn = %p \n",(unsigned long *)pfn);

	pgaddr=pfn_to_page(pfn>>12); // Get struct page address
	printk(KERN_INFO "pgaddr = %p \n",(unsigned long *)pgaddr);

	// Get the mapped Kernel Virtual Address of the page base
    if (NULL==(kaddr=(int *)kmap(pgaddr))) 
    	printk(KERN_INFO "kmap failed\n");	
	else 
		printk(KERN_INFO "kaddr = %p \n",(int *)kaddr);

	virt_addr=(unsigned long)kaddr|page_offset; // Index into the base
    value=*((int *)virt_addr); // Access the value of the memory
	printk(KERN_INFO "address = %p \n",(int *)virt_addr);
	printk(KERN_INFO "value = %0x \n",value);
	*((int *)virt_addr) = value + 0x20; // Change the value in the location
	printk(KERN_INFO "value = %0x \n",*((int *)virt_addr));
	return;

} // End of pgwalk

