#include <stdio.h>
#include <fcntl.h>

int A = 0x99;
int main()
{

   int fd;

   fd = open("/dev/myChar", O_RDWR);
   
   if (fd < 0)
      perror("Unable to open the device");
   else
      printf("File opened Successfully %d\n", fd);

   printf ("The value of A = %0x \n", A);
   close(fd);

   return 0;
}
