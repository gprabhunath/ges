
#include <linux/init.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <asm-i386/pgtable.h>
#include <linux/mm.h>


MODULE_LICENSE("Dual BSD/GPL");

// External Function declarations
extern void pgwalk(unsigned long);
extern void pgdDump(void);


// For kmalloc
char *myName;
int *myInt;
unsigned int *myNum;

// For KM initializations
char *devname;
int majNo;

module_param(devname, charp, 0000);

// Function Declarations for syscall definitions
int myOpen (struct inode *inode, struct file *filep);
int myRelease (struct inode *in, struct file *fp);

// Initialization routines
static int myInit (void);
static void myExit (void);

struct file_operations fops = {open:myOpen,release:myRelease};

static int myInit (void)
{
   printk(KERN_INFO "Initializing the World \n");

   majNo = register_chrdev(0,devname,&fops);
   if (majNo < 0)
   {
      printk(KERN_INFO "register_chrdev failed \n");
   } else {
      printk(KERN_INFO "Major Number of the device = %d \n", majNo);
   }
   
   return 0;
}

int myOpen (struct inode *inode, struct file *filep)
{
		int i = 0;

	    int pgd_offset;
	    int pt_offset;
	    int page_offset;
		unsigned int addr;


        printk(KERN_INFO "Open successful\n");

		//pgdDump();
		myName = (char *) kmalloc(5, GFP_USER);
		myInt = (int *) kmalloc (sizeof (int), GFP_USER);
		*myInt = 0x45;

		printk(KERN_INFO "KVA of myInt = %08x \n", (unsigned int)myInt);
		printk(KERN_INFO "Value of myInt = %08x \n", (unsigned int)*myInt);

		while (i < 5)
		{
			myName[i] = '4' + i;
			i++;
		}
		myName[4] = '\0';
		printk (KERN_INFO "################################ \n\n");
		printk(KERN_INFO "string is  %s \n", myName);
		printk(KERN_INFO "KVA of myname = %08x \n", (unsigned int)myName);
		myNum = (unsigned int *)myName;
		printk(KERN_INFO "KVA of myname = 0x%x \n", *myNum);
		printk (KERN_INFO "\n################################ \n\n");
		//pgwalk((unsigned long)0x080496ac);
		//pgwalk((unsigned long)myName);
		printk (KERN_INFO "\n################################ \n");

		addr = (unsigned int)myInt;
		pgd_offset=(unsigned int )addr >> 22;
	    pt_offset=((unsigned int)addr << 10) >> 22;
		page_offset=((unsigned int)addr & 0x00000fff);

	    printk(KERN_INFO "pgd_offset=%x\t pt_offset=%x\t page_offset=%x \n",
	                    pgd_offset,pt_offset,page_offset);

		
        return 0;
}

int myRelease (struct inode *in, struct file *fp)
{
	pgdDump();
	kfree(myName);
	printk (KERN_INFO "high_memory = 0x%08x \n", (unsigned int)high_memory);
	printk (KERN_INFO " = 0x%08x \n", (unsigned int)vmalloc_earlyreserve);
	printk (KERN_INFO "vmalloc start = 0x%08x \n", (unsigned int)VMALLOC_START);
	printk (KERN_INFO "vmalloc end = 0x%08x \n", (unsigned int)VMALLOC_END);
	printk(KERN_INFO "File released \n");
	return 0;
}


static void myExit (void)
{
   printk (KERN_INFO "Exiting the World \n");
   unregister_chrdev(majNo, devname);
   return;
}

module_init(myInit);
module_exit(myExit);
