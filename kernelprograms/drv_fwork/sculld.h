
#define SCULL_MAJOR	0
#define SCULL_QUANTUM 4096
#define SCULL_QSET 1024

//Function Declarations for syscall definitions
int scull_open (struct inode *inode, struct file *filep);
int scull_release (struct inode *in, struct file *fp);
loff_t scull_llseek (struct file *, loff_t, int);
ssize_t scull_read (struct file *, char __user *, size_t, loff_t *);
ssize_t scull_write (struct file *, const char __user *, size_t, loff_t *);
int scull_ioctl (struct inode *, struct file *, unsigned int, unsigned long);

struct scull_dev 
{
	struct scull_qset *data;	// Pointer to first quantum set
	int quantum;				// the current quantum size
	int qset;					// the current array size
	unsigned long size;			// amount of data stored here
	unsigned int access_key;	// used by sculluid and scullpriv
	struct semaphore sem;		// mutual exclusion semaphore
	struct cdev cdev;			// Char device structure
};

struct file_operations scull_fops = {
        .owner    =  THIS_MODULE,
        .llseek   =  scull_llseek,
        .read     =  scull_read,
        .write    =  scull_write,
        .ioctl    =  scull_ioctl,
        .open     =  scull_open,
        .release  =  scull_release
};

static void scull_setup_cdev (struct scull_dev *dev, int index);

