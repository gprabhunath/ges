
#include <linux/init.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/slab.h>
#include <asm/uaccess.h>

#include "sculld.h"

MODULE_LICENSE("Dual BSD/GPL");

#define NR_DEVS	1 // Number of device numbers


// Initialization routines
static int myInit (void);
static void myExit (void);

int majNo;

unsigned int scull_major = SCULL_MAJOR;
unsigned int scull_minor;
unsigned int scull_nr_devs = NR_DEVS;
int scull_quantum = SCULL_QUANTUM;
int scull_qset = SCULL_QSET;

static struct cdev *my_cdev;
static struct scull_dev *sdev;

static int myInit (void)
{
	int result;
	dev_t dev;
	struct scull_dev *s_dev;

	printk(KERN_INFO "Initializing SCULL Character Device \n");

	// Allocating Device Numbers
	if (scull_major)
	{
		dev = MKDEV(scull_major, scull_minor);
		result = register_chrdev_region(dev, scull_nr_devs, "scull");
	}
	else {
		result = alloc_chrdev_region (&dev, scull_minor, 
							scull_nr_devs, "scull");
	}
	if (result < 0)
	{
		printk (KERN_WARNING "scull: Can't get major %d \n", 
								scull_major);
		return result;
	}

	printk (KERN_INFO "Major number allocated = %d \n", MAJOR(dev));
	s_dev = kzalloc (sizeof (struct scull_dev), GFP_KERNEL);
	s_dev->quantum = scull_quantum;
	s_dev->qset = scull_qset;
	//init_MUTEX(&s_dev->sem);

	my_cdev = &(s_dev->cdev); // For unloading the module;
	sdev = s_dev; // For unloading the module;
	scull_setup_cdev(s_dev, 0);

	return 0;
}

static void scull_setup_cdev (struct scull_dev *dev, int index)
{
	int err;
	int devno = MKDEV(scull_major, scull_minor);

	cdev_init (&dev->cdev, &scull_fops);
	dev->cdev.owner = THIS_MODULE;
	//dev->cdev.ops = &scull_fops;
	err = cdev_add (&dev->cdev, devno, 1);

	// Fail gracefully if need be
	if (err)
		printk (KERN_NOTICE "Error %d adding scull %d \n", 
					err, index);
	
}

int scull_open (struct inode *inode, struct file *filp)
{

	printk(KERN_INFO "Entering scull_open \n");

	return 0; // Success
}

int scull_release (struct inode *inode, struct file *filp)
{
   printk(KERN_INFO "Entering scull_release \n");
   return 0;
}

static void myExit (void)
{
	printk (KERN_INFO "Exiting SCULL Character Driver \n");
	unregister_chrdev_region(scull_major, scull_nr_devs);	
	cdev_del(my_cdev);
	kfree (sdev);

	return;
}

loff_t scull_llseek (struct file *filp, loff_t f_pos, int A)
{
	printk (KERN_INFO "Entering scull_llseek \n");
	return 0;
}

int scull_ioctl (struct inode *inode, struct file *filp, 
				unsigned int cmd, unsigned long ifreq)
{
	printk (KERN_INFO "Entering scull_ioctl \n");
	return 0;
}

ssize_t scull_read (struct file *filp, char __user *buf, size_t count,
						 loff_t *f_pos)
{

	printk (KERN_INFO "Entering scull_read \n");
	return 0;

} // End of read()...

ssize_t scull_write (struct file *filp, const char __user *buf, 
						size_t count, loff_t *f_pos)
{
	
	printk (KERN_INFO "Entering scull_write \n");

	return 0;

} // End of scull_write()

module_init(myInit);
module_exit(myExit);
