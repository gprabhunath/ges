#include <linux/init.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/proc_fs.h>

MODULE_LICENSE("Dual BSD/GPL");

char *devname;

int majNo;

module_param(devname,charp,0000);

int myOpen(struct inode *inode,struct file *filep);

int myRelease(struct inode *in,struct file *fp);

int myIoctl (struct inode *inode, struct file *f,unsigned int,unsigned long);


static int myInit(void);
static void myExit(void);

static struct proc_dir_entry *procdir;

int myread_proc(char *page, char **start, off_t offset, int count,
                 int *eof, void *data)
{

	int ret;

	ret = sprintf (page, "prabhu\n");
//	ret += sprintf (page+ret, "\r\n");

	*eof = 1;
	return ret;

} // End of myread_proc


static int myInit(void)
{

	printk(KERN_INFO "\nProc entry /proc/pseudo created \n");

	procdir = create_proc_read_entry("pseudo", S_IFREG|0666, NULL, myread_proc, NULL);

	return 0;
}

int myOpen(struct inode *inode,struct file *filep)
{
	printk(KERN_INFO "Open Successful\n");
	return 0;
}

int myRelease(struct inode *in,struct file *fp)
{
	printk(KERN_INFO "File Released\n\n");
	return 0;
}

static void myExit(void)
{
	printk(KERN_INFO "Proc Entry /proc/pseudo removed \n");
	remove_proc_entry("pseudo", NULL);

	return;
}

module_init(myInit);
module_exit(myExit);

		


