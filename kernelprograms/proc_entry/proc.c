
#include <linux/init.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/proc_fs.h>
#include <asm/uaccess.h>  /*for copy_from_user*/

MODULE_LICENSE("Dual BSD/GPL");

int majNo;

// Function Declarations for syscall definitions
int myOpen (struct inode *inode, struct file *filep);
int myRelease (struct inode *in, struct file *fp);

// Initialization routines
static int myInit (void);
static void myExit (void);

static int proc_read( char *page, char **start, off_t offset, int count, int *eof, void *data)
{
	char buffer[15] = "kernel data";;
	//printk ("<0> proc_read invoked\n");
	//printk ("page address %p \n",page);
	//strcpy(page, "This is the message from the driver\n");
	//sprintf(*page,"Data %d", 100);
	if (offset > 0)
	{
		return 0;
	}
	else
	{
		return sprintf(page,"hi.......\n"); // or do memcpy
	}
}

static int proc_write( struct file *file, const char *user_buffer, ulong count, void *data)
{
	char mybuf[100],*buf;
	buf = mybuf;
	printk ("invoked proc write count = %d\n",count);
	if (copy_from_user(buf,user_buffer,20))
	{
		return -EFAULT;
	}
	
	printk ("Message sent from user ---- %s\n",buf);
	return 0;
}

static struct proc_dir_entry *my_proc = NULL, *my_proc_entry = NULL;

static int my_proc_install(void)
{
	my_proc = create_proc_entry("pseudo",S_IFDIR|0666,NULL);

	if (my_proc)
	{
		my_proc->nlink=1;
		my_proc->owner = THIS_MODULE;
	}
	else
	{
		printk("<0> Error creating proc dir \n");
		return -1;
	}
 //entry in my_proc directory

 	my_proc_entry =  create_proc_entry("myfile",S_IFREG|0666,my_proc);

	if (my_proc_entry)
	{
	    my_proc_entry->nlink=1;
	    my_proc_entry->owner = THIS_MODULE;
	    my_proc_entry->read_proc = proc_read;
	    my_proc_entry->write_proc = proc_write;
	}
	else
	{
	    printk("<0> Error creating proc dir \n");
	    return -1;
	}
	return 0;
}
	
struct file_operations fops = {}; 
static int myInit (void)
{
   printk(KERN_INFO "Initializing the World \n");

   majNo = register_chrdev(0,"/dev/pseudo",&fops);
   if (majNo < 0)
   {
      printk(KERN_INFO "register_chrdev failed \n");
   } else {
      printk(KERN_INFO "Major Number of the device = %d \n", majNo);
   }
   my_proc_install();
   return 0;
}

void my_proc_delete()
{
   remove_proc_entry("myfile",my_proc);
   remove_proc_entry("psuedo",NULL);
}

static void myExit (void)
{
   printk (KERN_INFO "Exiting the World \n");
   unregister_chrdev(majNo, "/dev/pseudo");
   my_proc_delete();
   return;
}

module_init(myInit);
module_exit(myExit);
