#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/types.h>

int main()
{
	int fd;
	int size =0;
	char Buffer[100];
	
	fd = open("/proc/pseudo/myfile",O_RDWR);
	printf ("fd %d\n",fd);
	
	size = read(fd, (void *)Buffer, 15);
	printf ("size %d\n",size);
	
	printf ("Data from proc %s\n",Buffer);
	strcpy(Buffer,"From user space");
	
	size = write(fd,Buffer,20);
	printf ("size %d\n", size);
	
	return 0;
}

	
