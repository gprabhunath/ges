
#include <linux/init.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/cdev.h>

MODULE_LICENSE("Dual BSD/GPL");

#define FIRST_MINOR 	0
#define NR_DEVS	1 // Number of device numbers


char *devname;
module_param(devname, charp, 0000);

// Function Declarations for syscall definitions
int myOpen (struct inode *inode, struct file *filep);
int myRelease (struct inode *in, struct file *fp);

// Initialization routines
static int myInit (void);
static void myExit (void);

struct file_operations fops = {
		.owner = THIS_MODULE,
		.open = myOpen,
		.release = myRelease
};

int majNo;
unsigned static int c_major;
unsigned static int c_minor;

static dev_t dev_id;
struct cdev *my_cdev;

static int __init myInit (void)
{
	int err;

	printk(KERN_INFO "Initializing Character Device \n");

	// Allocating Device Numbers
	if (c_major)
	{
		dev_id = MKDEV(c_major, c_minor);	
		register_chrdev_region (dev_id, NR_DEVS, devname);
	}
	else
	{
		err = alloc_chrdev_region (&dev_id, FIRST_MINOR, NR_DEVS, devname);
		if (err < 0)
			printk (KERN_NOTICE "Device numbers allocation failed: %d \n", 
								err);
	}

	printk (KERN_INFO "Major number allocated = %d \n", MAJOR(dev_id));
	my_cdev = cdev_alloc(); // Allocate memory for my_cdev
	cdev_init(my_cdev, &fops); // Initialize my_cdev with fops
	my_cdev->owner = THIS_MODULE;

	err = cdev_add (my_cdev, dev_id, NR_DEVS);// Add my_cdev to the list
	if (err)
		printk (KERN_NOTICE "Error adding my_cdev \n");
	else
		printk (KERN_NOTICE "Device Added \n");
	
   return 0;
}


static void myExit (void)
{
	printk (KERN_INFO "Exiting the Character Driver \n");
	unregister_chrdev_region(dev_id, NR_DEVS);	
	cdev_del(my_cdev);

	return;
}

int myOpen (struct inode *inode, struct file *filep)
{
        printk(KERN_INFO "Open successful\n");
		printk (KERN_INFO "size of module = %d \n", sizeof (struct module));
        return 0;
}

int myRelease (struct inode *in, struct file *fp)
{
   printk(KERN_INFO "File released \n");
   return 0;
}

module_init(myInit);
module_exit(myExit);
