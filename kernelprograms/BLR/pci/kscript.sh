#!/bin/sh

# Usage : sh kscript.sh myChar

MOD=kmod
rmmod $MOD 
rm -f /dev/$1
dmesg -c > /dev/null

make clean 
make

insmod $MOD.ko devname=$1
x=`cat /proc/devices | grep $1 | cut -b 1,2,3`
echo $x

mknod /dev/$1 c $x 0

gcc -o amod amod.c
./amod
dmesg

