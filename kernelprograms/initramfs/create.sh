#!/bin/sh

# create.sh: takes 1 argument, the target. It creates a new initrd at the
# target and appends .new to target.


cd initrd
find ./ | cpio -H newc -o > ../$1.new
cd ..
gzip $1.new
mv $1.new.gz $1.new

