
// Experiment : To demonstrate the use of EXPORT_SYMBOL and use of 
//              dependency modules : dep.c

#include <linux/init.h>
#include <linux/module.h>
#include <linux/fs.h>

MODULE_LICENSE("Dual BSD/GPL");

char *devname;
int majNo;
unsigned muxData;
EXPORT_SYMBOL(muxData);

module_param(devname, charp, 0000);
// Function Declarations for syscall definitions
int myOpen (struct inode *inode, struct file *filep);
int myRelease (struct inode *in, struct file *fp);

// Initialization routines
static int myInit (void);
static void myExit (void);

struct file_operations fops = {open:myOpen,release:myRelease};

static int myInit (void)
{
   printk(KERN_INFO "\nInitializing dep.ko \n");

   majNo = register_chrdev(0, "myChar", &fops);
   if (majNo < 0)
   {
      printk(KERN_INFO "register_chrdev failed \n");
   } else {
      printk(KERN_INFO "Major Number of the device for dep.ko = %d \n", majNo);
   }
   
   return 0;
}

int myOpen (struct inode *inode, struct file *filep)
{
        printk(KERN_INFO "\nOpen successful in dep.ko \n");
		muxData = 243;
		printk (KERN_INFO "muxData %u written in dep.ko \n", muxData);
        return 0;
}

int myRelease (struct inode *in, struct file *fp)
{
   printk(KERN_INFO "\nFile released in dep.ko \n");
   return 0;
}


static void myExit (void)
{
   printk (KERN_INFO "\nExiting the module dep.ko \n");
   unregister_chrdev(majNo, devname);
   return;
}

module_init(myInit);
module_exit(myExit);
