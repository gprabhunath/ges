
// Experiment : To demonstrate the use of EXPORT_SYMBOL and use of
//              dependency modules : dep1.c

#include <linux/init.h>
#include <linux/module.h>
#include <linux/fs.h>

MODULE_LICENSE("Dual BSD/GPL");

extern unsigned int muxData;

char *devname;
int majNo;

//module_param(devname, charp, 0000);

// Function Declarations for syscall definitions
int myOpen (struct inode *inode, struct file *filep);
int myRelease (struct inode *in, struct file *fp);

// Initialization routines
static int myInit (void);
static void myExit (void);

struct file_operations fops = {open:myOpen,release:myRelease};

static int myInit (void)
{
   printk(KERN_INFO "\nInitializing the module dep1.ko \n");

   majNo = register_chrdev(0,"myChar1",&fops);
   if (majNo < 0)
   {
      printk(KERN_INFO "register_chrdev failed \n");
   } else {
      printk(KERN_INFO "Major Number of the device in dep1.ko = %d \n", majNo);
   }
   
   return 0;
}

int myOpen (struct inode *inode, struct file *filep)
{
        printk(KERN_INFO "\nOpen successful in dep1.ko\n");
		printk(KERN_INFO "Value of muxData in dep1.ko = %d \n", muxData);
        return 0;
}

int myRelease (struct inode *in, struct file *fp)
{
   printk(KERN_INFO "\nFile released in dep1.ko \n");
   return 0;
}


static void myExit (void)
{
   printk (KERN_INFO "\nExiting the module dep1.ko  \n");
   unregister_chrdev(majNo, "myChar");
   return;
}

module_init(myInit);
module_exit(myExit);
