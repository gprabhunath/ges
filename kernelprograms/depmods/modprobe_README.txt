
# Usage of modprobe

dep.ko = dependency module
dep1.ko = dependent module which is dependent on dep.ko

# Build both the kernel modules dep.ko and dep1.ko
$ make

# Copy both the modules to /lib/modules/`uname -r`/kernel/<folder>
$ cp dep.ko dep1.ko /lib/modules/`uname -r`/kernel/depmods

# Execute depmod and modprobe
$ depmod -a 
 	# This will build the dependency database modules.dep in the folder
	# /lib/modules/`uname -r`/

$ modprobe dep1
	# This will look for the modules on which dep1.ko is dependent on. If 
	# so, modprobe will first insmod that module and once all the dependencies
	# are built, then insmod dep1.

$ lsmod | grep dep
	# This will show both dep and dep1 would be insmoded by modprobe


## If the dependency database has to be set up on boot then the modprobe conf
# file has to be updated with the following entry

alias km1 dep1
alias km  dep

	# The above entries will make depmod update modules.dep on system start.
