#!/bin/sh

find . -name '*.symvers' | xargs rm -f
find . -name '.tmp_versions' | xargs rm -rf
find . -name 'a.out' | xargs rm -f
find . -name '*.ko' | xargs rm -f
find . -name '*.mod.c' | xargs rm -f
find . -name '*.o' | xargs rm -f
find . -name '.*.cmd' | xargs rm -f
find . -name 'amod' | xargs rm -f
find . -name '*.markers' | xargs rm -f
find . -name '*.order' | xargs rm -f
find . -name '*[dD]ump' | xargs rm -f



