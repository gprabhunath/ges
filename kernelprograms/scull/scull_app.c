#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>

char rbuf[15];
int count;
int offset;

int main()
{

	int fd;
	
	fd = open("/dev/scull", O_RDWR);
	   
	if (fd < 0)
		perror("Unable to open the device:");
	else
	{
		printf("File opened Successfully %d\n", fd);
	
		count = write (fd, "Tata Elxsi", 10);
		if (count < 0)
			perror ("Unable to write:");
		printf ("%d bytes were written to the device\n", count);

		offset = lseek(fd, 4, SEEK_SET);	
		if (offset == (off_t)-1)
			perror("lseek failed:");

		count = read (fd, rbuf, 10);
		if (count < 0)
			perror ("Unable to read:");
		printf ("%d bytes were read from the device\n", count);
	
		if (count > 0)
			printf ("buffer read from the device = %s \n", rbuf);
	
		close(fd);
	}
	return 0;
	
}
