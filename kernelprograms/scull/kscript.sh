#!/bin/sh

# Usage : sh kscript.sh myChar
MOD=pgtable_dump
MOD=scull
APP=scull_app
DEV=scull

dmesg -c > /dev/null

insmod $MOD.ko
x=`cat /proc/devices | grep $DEV | cut -b 1,2,3`
echo $x

mknod /dev/$DEV c $x 0
./$APP
rmmod $MOD 
rm -f /dev/$DEV
dmesg

