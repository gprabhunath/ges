
#include <linux/init.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/slab.h>
#include <asm/uaccess.h>
#include <linux/semaphore.h>

#include "scull.h"

MODULE_LICENSE("Dual BSD/GPL");

#define NR_DEVS	1 // Number of device numbers




// Initialization routines
static int myInit (void);
static void myExit (void);

int majNo;

unsigned int scull_major = SCULL_MAJOR;
unsigned int scull_minor;
unsigned int scull_nr_devs = NR_DEVS;
int scull_quantum = SCULL_QUANTUM;
int scull_qset = SCULL_QSET;
static dev_t mydev;

static struct cdev *my_cdev;
static struct scull_dev *sdev;

static int myInit (void)
{
	int result;
	struct scull_dev *s_dev;

	printk(KERN_INFO "Initializing SCULL Character Device \n");

	// Allocating Device Numbers
	if (scull_major)
	{
		mydev = MKDEV(scull_major, scull_minor);
		result = register_chrdev_region(mydev, scull_nr_devs, "scull");
	}
	else {
		result = alloc_chrdev_region (&mydev, scull_minor, 
							scull_nr_devs, "scull");
	}
	if (result < 0)
	{
		printk (KERN_WARNING "scull: Can't get major %d \n", 
								scull_major);
		return result;
	}

	printk (KERN_INFO "Major number allocated = %d \n", MAJOR(mydev));
	s_dev = kzalloc (sizeof (struct scull_dev), GFP_KERNEL);
	s_dev->quantum = scull_quantum;
	s_dev->qset = scull_qset;
	sema_init(&s_dev->sem, 1);

	my_cdev = &(s_dev->cdev); // For unloading the module;
	sdev = s_dev; // For unloading the module;
	scull_setup_cdev(s_dev, 0);

	return 0;
}

static void scull_setup_cdev (struct scull_dev *dev, int index)
{
	int err;

	cdev_init (&dev->cdev, &scull_fops);
	dev->cdev.owner = THIS_MODULE;
	dev->cdev.ops = &scull_fops;
	err = cdev_add (&dev->cdev, mydev, NR_DEVS);

	// Fail gracefully if need be
	if (err < 0)
		printk (KERN_NOTICE "Error %d adding scull %d \n", 
					err, index);
	
}

static int scull_trim(struct scull_dev *dev)
{
	struct scull_qset *next, *dptr;
	int qset = dev->qset; // "dev" is not-null
	int i;
	
	for (dptr = dev->data; dptr; dptr = next)
	{
		if (dptr->data)
		{
			for (i = 0; i < qset; i++)
				kfree(dptr->data[i]);
			kfree(dptr->data);
			dptr->data = NULL;
		}
		next = dptr->next;
		kfree(dptr);
	}

	dev->size = 0;
	dev->quantum = scull_quantum;
	dev->qset = scull_qset;
	dev->data = NULL;
	
	return 0;

} // End of scull_trim();

static struct scull_qset *scull_follow(struct scull_dev *dev, int item)
{
	struct scull_qset *dptr;

	dptr = dev->data;
	
	while (item)
	{
		dptr = dptr->next;
		item--;
	}
	
	return dptr;
	
} // End of scull_follow()

int scull_open (struct inode *inode, struct file *filp)
{
	struct scull_dev *dev; // Device information

	printk(KERN_INFO "Entering scull_open \n");

	dev = container_of (inode->i_cdev, struct scull_dev, cdev);	
	filp->private_data = dev; // for other methods;

	// now trim to 0 the length of the device if open was write-only
	if ( (filp->f_flags & O_ACCMODE) == O_WRONLY)
	{
		scull_trim(dev); // ignore errors
	}
	
	return 0; // Success
}

int scull_release (struct inode *inode, struct file *filp)
{
   printk(KERN_INFO "Entering scull_release \n");
   return 0;
}

static void myExit (void)
{
	printk (KERN_INFO "Exiting SCULL Character Driver \n");
	unregister_chrdev_region(mydev, scull_nr_devs);	
	cdev_del(my_cdev);
	kfree (sdev);

	return;
}

loff_t scull_llseek (struct file *filp, loff_t f_pos, int off)
{
	printk (KERN_INFO "Entering scull_llseek \n");
	
	printk (KERN_INFO "Value of f_pos = %lld \n", f_pos);
	printk (KERN_INFO "Value of off = %d \n", off);
	printk (KERN_INFO "Value of filp->f_pos = %lld \n", filp->f_pos);
	
	filp->f_pos = f_pos;

	return 0;
}

long scull_ioctl (struct file *filp, unsigned int cmd, unsigned long ifreq)
{
	printk (KERN_INFO "Entering scull_ioctl \n");
	return 0;
}

ssize_t scull_read (struct file *filp, char __user *buf, size_t count,
						 loff_t *f_pos)
{
	struct scull_dev *dev = filp->private_data;
	struct scull_qset *dptr; // The first listitem
	int quantum = dev->quantum;
	int qset = dev->qset;
	int itemsize = quantum * qset; // How many bytes in the listitem
	int item, s_pos, q_pos, rest;
	ssize_t retval = 0;

	printk (KERN_INFO "Entering scull_read \n");
	printk (KERN_INFO "Value of f_pos = %lld \n", *f_pos);
	if (down_interruptible(&dev->sem))
		return -ERESTARTSYS;
	if (*f_pos >= dev->size)
		goto out;
	if (*f_pos + count > dev->size)
		count = dev->size - *f_pos;
	
	// find listitem, qset index, and offset in the quantum
	item = (long)*f_pos / itemsize; // find listitem;
	rest = (long)*f_pos % itemsize;
	s_pos = rest / quantum;  // Find qset index
	q_pos = rest % quantum; // find quantum offset

	// follow the list up to the right position (defined elsewhere)
	dptr = scull_follow(dev, item);

	if (dptr == NULL || !dptr->data || !dptr->data[s_pos])
		goto out; // don't fill holes

	// read only up to the end of this quantum
	if (count > quantum - q_pos)
		count = quantum - q_pos;

	if (copy_to_user(buf, dptr->data[s_pos] + q_pos, count))
	{
		retval = -EFAULT;
		goto out;
	}
	
	*f_pos += count;
	retval = count;

out:
	up(&dev->sem);
	return retval;

} // End of read()...

ssize_t scull_write (struct file *filp, const char __user *buf, 
						size_t count, loff_t *f_pos)
{
	
	struct scull_dev *dev = filp->private_data;
	struct scull_qset *dptr;
	int quantum = dev->quantum;
	int qset = dev->qset;
	int itemsize = quantum * qset;
	int item, s_pos, q_pos, rest;
	ssize_t retval = -ENOMEM; // Value used in "goto out" statements	

	printk (KERN_INFO "Entering scull_write \n");
	if (down_interruptible(&dev->sem))
		return -ERESTARTSYS;

	// find listitem, qset index and offset in the quantum
	item = (long)*f_pos / itemsize;
	rest = (long)*f_pos % itemsize;
	s_pos = rest / quantum;
	q_pos = rest % quantum;
	
	// follow the list up to the right position

	dptr = scull_follow(dev, item);
	if (dptr == NULL)
	{
		dptr = kzalloc(sizeof (struct scull_qset), GFP_KERNEL);
		if (!dptr)
			goto out;
		dev->data = dptr;
	}

	if (!dptr->data)
	{
		dptr->data = kmalloc(qset * sizeof(char *), GFP_KERNEL);
		if (!dptr->data)
			goto out;
		memset (dptr->data, 0, qset * sizeof (char *));
	}

	if (!dptr->data[s_pos])
	{
		dptr->data[s_pos] = kmalloc(quantum, GFP_KERNEL);
		if (!dptr->data[s_pos])
			goto out;
	}
		
	// Write only up to the end of this quantum
	if (count > quantum - q_pos)
		count = quantum - q_pos;
	
	if (copy_from_user(dptr->data[s_pos] + q_pos, buf, count))
	{
		retval = -EFAULT;
		goto out;
	}
	*f_pos += count;
	retval = count;
	printk (KERN_INFO "*fpos = %lld \n", *f_pos);

	// Update the size;
	if (dev->size < *f_pos)
		dev->size = *f_pos;

out:
	up(&dev->sem);
	return retval;

} // End of scull_write()

module_init(myInit);
module_exit(myExit);
