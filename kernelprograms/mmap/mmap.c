#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <sys/mman.h>

unsigned int *va;
unsigned int *pa;
unsigned int ioMemSize = 4 * 1024;

int A;
int main()
{
	int fd;
	int i;

    fd = open("/dev/myChar",O_RDWR);
    if(fd < 0)
		perror("\nUnable to open the Device\n");

	va = mmap(0, ioMemSize, PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0);
	
	printf("VIR_ADDR = %08x\n\n",(unsigned int)va);

	if(va == (void *)-1)
	{
		printf("MAPPING FAILED\n");
		exit(0);
	}
	else
		printf("MAPPING SUCCESSFUL\n\n");

	//pa = (unsigned int *)va;
	//*va = 345;
	printf ("Value of *va = %u \n", *va);
	
#if 0
	ioctl (fd, getpid(), va);
	ioctl (fd, getpid(), &A);

	for(i=0;i<1024*1024;i++)
	{
		printf("%x ",i);
		*(pa+i) = 0xa0000fff;
	}
#endif
	
	close(fd);
	
	return 0;
}


