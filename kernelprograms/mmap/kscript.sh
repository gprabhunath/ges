#!/bin/sh

# Remove the kernel module 
rmmod kmmap
rm -f /dev/myChar 
make clean
dmesg -c > /dev/null

# Build the kernel module and insmod
make
insmod kmmap.ko devname="myChar"
majNo=`grep 'myChar' /proc/devices | cut -b 1,2,3`
mknod /dev/myChar c $majNo 0

# Build the application and execute
gcc -o mmap mmap.c
./mmap 
echo "------------------"
echo "Dumping of dmesg"
echo "------------------"
dmesg



