#include <linux/init.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/sched.h>
#include <linux/mm.h>
#include <asm/io.h>

MODULE_LICENSE("Dual BSD/GPL");

//#define PADDR		0xd0000000 // VGA Controller 
//#define PFN		0xd0000

#define PADDR	0x10002000
#define PFN		0x10002

char *devname;

int majNo;

module_param(devname,charp,0000);

int myOpen(struct inode *inode,struct file *filep);

int myRelease(struct inode *in,struct file *fp);

int myMmap (struct file *f, struct vm_area_struct *vma);

int myIoctl (struct inode *inode, struct file *filep,
        unsigned int pid, unsigned long addr);

void pgTableWalk(int pid, unsigned long addr);
static int myInit(void);
static void myExit(void);

struct file_operations fops = {open:myOpen,release:myRelease,mmap:myMmap, 
                              ioctl:myIoctl};

static int myInit(void)
{
	printk(KERN_INFO "\nInitializing the World\n");
	majNo = register_chrdev(0,devname,&fops);
	if(majNo < 0)
	{
		printk(KERN_INFO "register_chrdev failed\n");
	}
	else
	{
		printk(KERN_INFO "Major Number of the Device = %d\n",majNo);
	}

	return 0;
}

int myOpen(struct inode *inode,struct file *filep)
{
	printk(KERN_INFO "Open Successful\n");
	printk(KERN_INFO "_PAGE_CACHE_WB = %08x \n", _PAGE_CACHE_WB);
	printk(KERN_INFO "_PAGE_CACHE_WC = %08x \n", _PAGE_CACHE_WC);
	printk(KERN_INFO "_PAGE_CACHE_UC_MINUS = %08x \n", _PAGE_CACHE_UC_MINUS);
	printk(KERN_INFO "_PAGE_CACHE_UC = %08x \n", _PAGE_CACHE_UC);
	return 0;
}

int myRelease(struct inode *in,struct file *fp)
{
	printk(KERN_INFO "File Released\n\n");
	return 0;
}

static void myExit(void)
{
	printk(KERN_INFO "\nExiting the World\n");
	unregister_chrdev(majNo,devname);
	iounmap(PADDR);
	return;
}

int myIoctl (struct inode *inode, struct file *filep,
        unsigned int pid, unsigned long addr)

{
	unsigned long *val = (unsigned long *)addr;
#if 0
	printk (KERN_INFO "Value of addr = 0x%08x \n", addr);
	printk (KERN_INFO "Value of *addr = %lu \n", *val);
#endif
	pgTableWalk(pid, addr);
	return 0;
}

void pgTableWalk(int pid, unsigned long addr)
{

	struct task_struct *task;

 	pgd_t *pgdbase;
	pte_t *pt_base;
	unsigned long page_base;

 	pgd_t *pgd_indexed_addr;
 	pte_t *pt_indexed_addr;

	int pgd_index;
	int pt_index;
	int page_index;


	pgd_index = (unsigned int )addr >> 22;
	pt_index = ((unsigned int)addr << 10) >> 22;
	page_index = ((unsigned int)addr & 0x00000fff);
	
//	printk(KERN_INFO "pgd_index=%x\t pt_index=%x\t page_index=%x \n",
//					pgd_index,pt_index,page_index);

	// Get the pointer of task_struct
    task = find_task_by_vpid(pid);	

	// Get the PGD Base address
    pgdbase = task->mm->pgd; // Get the base address of PGD
	pgd_indexed_addr=pgdbase+pgd_index; // Get the entry in the PGD
	printk(KERN_INFO "pgd_indexed_addr = %p \n",
				(unsigned long *)pgd_indexed_addr);

	// Get Page Table Base address
	pt_base = (pte_t *)((pgd_indexed_addr->pgd & 0xFFFFF000) + PAGE_OFFSET);
	printk(KERN_INFO "pt_base = %p \n",(unsigned long *)pt_base);
	pt_indexed_addr = pt_base + pt_index; // Get the entry in the Page Table
	printk(KERN_INFO "pt_indexed_addr = %p \n",
				(unsigned long *)pt_indexed_addr);

	// Get the page base physical address
	//page_base = pt_indexed_addr->pte_low & 0xFFFFF000; 
	page_base = pt_indexed_addr->pte_low ; 
	printk(KERN_INFO "page_base = %p \n",(unsigned long *)page_base);

	printk ("\n----------- End of PgTableWalk ----------------\n");
	return;

} // End of pgTableWalk

int myMmap (struct file *f, struct vm_area_struct *vma)
{
	int ret ;
	unsigned long size;
	unsigned int *kva;

	size = vma->vm_end - vma->vm_start;
	
	//printk(KERN_INFO "Size = %d\n\n",(unsigned int)size); 
	
	ret = remap_pfn_range(vma,vma->vm_start,PFN,size,vma->vm_page_prot);
	//printk ("Value of ret = %d \n", ret);
	if(ret == 0)
		printk(KERN_INFO "Mapping Successful\n\n");
	else
		printk(KERN_INFO "Mapping Failure\n\n");
#if 0
	kva = 0xc00a0000;
	*kva = 345;
#endif

#if 1	
	kva = (unsigned int *) ioremap_nocache(PADDR, 4*1024);
	if (!kva)
		printk (KERN_INFO "ioremap failed \n");
	else
		printk (KERN_INFO "kva = 0x%08x \n", kva);

	*kva = 456;
//	printk ("Value of *kva = %u \n", *kva);
//	myIoctl (NULL, NULL, current->pid, kva);
#endif


	
	return 0;
}

module_init(myInit);
module_exit(myExit);
