
// List of experiments to parse the data structures related to 
// tasks and scheduler

#include <linux/init.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/mm.h>
#include <linux/sched.h>
#include "mysched.h"

MODULE_LICENSE("Dual BSD/GPL");

char *devname;
int majNo;

module_param(devname, charp, 0000);
// Function Declarations for syscall definitions
int myOpen (struct inode *inode, struct file *filep);
int myRelease (struct inode *in, struct file *fp);
int myIoctl (struct inode *inode, struct file *filep, unsigned int pid,
             unsigned long A);
//struct task_struct * (*findtask_by_vpid)(pid_t nr);

// Initialization routines
static int myInit (void);
static void myExit (void);

struct file_operations fops = {open:myOpen,release:myRelease,ioctl:myIoctl};

static int myInit (void)
{
   printk(KERN_INFO "Initializing task list parse module \n");

   majNo = register_chrdev(0,devname,&fops);
   if (majNo < 0)
   {
      printk(KERN_INFO "register_chrdev failed \n");
   } else {
      printk(KERN_INFO "Major Number of the device = %d \n", majNo);
   }
   
   return 0;
}

int myOpen (struct inode *inode, struct file *filep)
{
        printk(KERN_INFO "Opened task list parse module \n");
        return 0;
}

int myRelease (struct inode *in, struct file *fp)
{
   printk(KERN_INFO "\nReleased task list parse module \n");
   return 0;
}

static void myExit (void)
{
   printk (KERN_INFO "Removing task list parse module\n");
   unregister_chrdev(majNo, devname);
   return;
}

module_init(myInit);
module_exit(myExit);


// Experiment : KernelModule to print the sched policy, state and priorities
//				of all the tasks.
#if 1
// Local Function declarations
char *findPolicy (unsigned long policy);
char *findState (unsigned long state);
char *findExecName (int pid);

// findExecName : This function is not used, though this can be kept to 
//				  find out the task_struct whose mm is NULL

char *findExecName (int pid)
{
	struct task_struct *myTask;
	struct vm_area_struct *vma;
	struct mm_struct *mms;
	const char *execName;
	int i = 0;

	//myTask = find_task_by_pid(pid);
	myTask = current;
	if (!myTask)	
	{
		printk (KERN_INFO "task_struct");
		return NULL;
	}
	printk (KERN_INFO "%s", myTask->comm);

	if (!myTask->mm) 
		if (!myTask->active_mm)
		{
			printk (KERN_INFO "mm_struct");
			return NULL;
		}
		else
			mms = myTask->active_mm;
	else
		mms = myTask->mm;

	if (!mms->mmap)
	{
		printk (KERN_INFO "vm_area_struct");
		return NULL;
	}
	vma = mms->mmap;

	printk (KERN_INFO "Start code = 0x%08x ", mms->start_code);

	while (i++ < mms->map_count)
	{
		if (vma->vm_start == mms->start_code)
			break;
		vma = vma->vm_next;
	}

	execName = vma->vm_file->f_dentry->d_name.name;
	//printk (KERN_INFO "Start code = 0x%08x \n", vma->vm_start);
	//printk (KERN_INFO "Name of the executable = %s \n", execName);

	return execName;
	
} // End of function findExecName()

char *findPolicy (unsigned long policy)
{
	switch (policy)
	{
		case 0:
			return "NORMAL";
		case 1:
			return "FIFO";
		case 2:
			return "RR";
		case 3:
			return "SCHED_BATCH";
		default:
			return "UNKNOWN";
	}

} // End of findPolicy()

char *findState (unsigned long state)
{
	switch (state)
	{
		case 0:
			return "TASK_RUNNING";
		case 1:
			return "TASK_INT";
		case 2:
			return "TASK_UNINT";
		case 4:
			return "TASK_STOPPED";
		case 8:
			return "TASK_TRACED";
		case 16:
			return "EXIT_ZOMBIE";
		case 32:
			return "EXIT_DEAD";
		case 64:
			return "TASK_NONINTERACTIVE";
		default:
			return "UNKNOWN";
	}


} // End of findState(...
int myIoctl (struct inode *inode, struct file *filep, unsigned int pid,
             unsigned long A)
{
	struct task_struct *tskParse, *curTask, *thdParse = NULL;

	//findtask_by_vpid = 0xc04528bb;
	curTask = find_task_by_vpid((pid_t)pid);
	//curTask = current;
	printk (KERN_INFO "%s \t %d\n", findExecName(curTask->pid),
								curTask->pid);

	printk (KERN_INFO "    Name\t  pid \tppid\tPolicy\t  State\t\tS.prio\tD.prio\tRt.prio\n");
	printk (KERN_INFO "    ----\t  --- \t----\t------\t  -----\t\t------\t------\n");
	list_for_each_entry_reverse(tskParse, &(curTask->tasks), tasks)
	{
		printk(KERN_INFO "%11s\t%5d\t%5d\t%s\t%s\t%d\t%d\t%08x\n", 
			         tskParse->comm,tskParse->pid,tskParse->parent->pid,
					 findPolicy(tskParse->policy),
					 findState(tskParse->state),
					 tskParse->static_prio, tskParse->prio, 
					// tskParse->rt_priority);
					 tskParse->blocked.sig[0]);

		if (tskParse->pid > 1)
		{
		list_for_each_entry(thdParse, &(tskParse->thread_group), tasks)
		{
			#if 0
			printk(KERN_INFO "%11s\t%5d\t%5d\t%s\t%s\t%d\t%d\t%u\n", 
			         thdParse->comm,thdParse->pid,thdParse->parent->pid,
					 findPolicy(thdParse->policy),
					 findState(thdParse->state),
					 thdParse->static_prio, thdParse->prio, 
					 thdParse->rt_priority);
			#endif

		}
		}


	}
#if 0
		printk(KERN_INFO "%11s\t%5d\t%5d\t%s\t%s\t%d\t%d\n", 
			         tskParse->comm,tskParse->pid,tskParse->parent->pid,
					 findPolicy(tskParse->policy),
					 findState(tskParse->state),
					 tskParse->static_prio,tskParse->prio);
#endif

	printk (KERN_INFO "SIGNAL_UNKILLABLE = 0x%08x \n", SIGNAL_UNKILLABLE);
	printk (KERN_INFO "Size of sigset_t = %d \n", sizeof(sigset_t));
	return 0;
}

#endif

// Experiment : KM to print the members of runqueue structures

#if 0
// Function declarations
void printBitMap(unsigned long *bits);
void testActiveRunQ(struct rq *arqs);
void printPidRqs(struct prio_array *rqs);
static inline unsigned first_set_bit(const unsigned long *, unsigned );


static inline unsigned first_set_bit(const unsigned long *addr, unsigned size)
{
	unsigned long byte = 0;
	unsigned long index = 0;

    while (byte++ < 5) 
	{
		unsigned long val = *addr++;
		//printk (KERN_INFO "Value of val = 0x%08x \n", val);
		if (val)
		{
			index = __ffs(val) + index;
			//printk (KERN_INFO "Value of index = %u \n", index);
			return index;
		}
		index += (sizeof(*addr)<<3);

	} // End of while (byte++ ...

	return index;


} // End of first_set_bit()

void printPidRqs(struct prio_array *rqs)
{
	struct task_struct *tskParse;
	unsigned long bitmap[5];
	unsigned index;
	unsigned long setbit = 0;

	printk (KERN_INFO "\n");
	memcpy(bitmap, rqs->bitmap, 20);
	while (1)
	{
		index = first_set_bit(bitmap, 5);
		set_bit (index % 32, &setbit);
		//printk (KERN_INFO "first bit set = %u 0x%08x \n", index, setbit);

		if (index > 139)
			break;

		list_for_each_entry(tskParse, &(rqs->queue[index]), run_list)
		{

			printk (KERN_INFO "Pids and prios for index %u = ", index);
			printk (" %d %d %d %d\n", tskParse->pid, tskParse->prio,
			           tskParse->static_prio, tskParse->normal_prio);
		}


		clear_bit (index % 32, &setbit);
		clear_bit (index % 32, (bitmap + index / 32));

    } // End of while(1)

	return;

} // End of printPidRqs()

void printBitMap(unsigned long *bits)
{
	int i;

	printk (KERN_INFO "\n");
	//bits = (unsigned long *)bts->active->bitmap;
	for (i=0; i<=4; i++)	
	{
		if (*(bits + i))
			printk (KERN_INFO "Value of bitmap[%d] = 0x%08x \n", i, *(bits + i));
	}

	return;
	
} // End of printBitMap()

void testActiveRunQ(struct rq *arqs)
{
	int i;
	unsigned int *list_head, *next, *prev;

	printk (KERN_INFO "Addresses of list_head : next : prev \n");
	for (i=101; i<141; i++)
	{
		list_head = (unsigned int *)&(arqs->active->queue[i]);
		next = (unsigned int *)arqs->active->queue[i].next;
		prev = (unsigned int *)arqs->active->queue[i].prev;

		printk (KERN_INFO "%d  0x%08x  0x%08x  0x%08x ", i, list_head,
		        next, prev);
		if ( list_head == next && list_head == prev )
			printk ("Empty List \n");
		else
			printk (" \n");
	}
} // End of testActiveRunQ()

int myIoctl (struct inode *inode, struct file *filep, unsigned int pid, 
             unsigned long A)
{
	//struct task_struct *tskParse;
	struct rq *rqs = NULL;

	printk (KERN_INFO "\nEntering myIoctl \n\n");

	rqs = getrq(rqs);
	printk (KERN_INFO "Number of running processes = %lu \n", rqs->nr_running);
	//testActiveRunQ(rqs);

	printk (KERN_INFO "\nActive queues bitmap \n");
	printBitMap(rqs->active->bitmap);
	printk (KERN_INFO "\nActive queues pids and their prios (prio, static, normal) \n");
	printPidRqs(rqs->active);

	printk (KERN_INFO "\nExpired queues bitmap \n");
	printBitMap(rqs->expired->bitmap);
	printk (KERN_INFO "\nExpired queues pids and their prios (prio, static, normal) \n");
	printPidRqs(rqs->expired);
	//printk (KERN_INFO "Value of bitmap[%d] = 0x%08x \n", 4, rqs->active->bitmap[4]);

	return 0;

} // End of myIoctl

#endif

// Experiment : To print the names of the applications
#if 0
int myIoctl (struct inode *inode, struct file *filep, unsigned int pid,
             unsigned long A)
{
	struct task_struct *myTask;
	struct vm_area_struct *vma;
	struct mm_struct *mms;
	const char *execName;
	int i = 0;

	myTask = current;
    mms = myTask->mm;
	vma = mms->mmap;

	printk (KERN_INFO "Start code = 0x%08x \n", mms->start_code);

	while (i++ < mms->map_count)
	{
		if (vma->vm_start == mms->start_code)
			break;
		vma = vma->vm_next;
	}

	execName = vma->vm_file->f_dentry->d_name.name;
	//printk (KERN_INFO "Start code = 0x%08x \n", vma->vm_start);
	printk (KERN_INFO "Name of the executable = %s \n", execName);

	return 0;
} // End of myIoctl

#endif
