#include<stdio.h>
#include<pthread.h>
#include<fcntl.h>
#include <errno.h>
#include <stdlib.h>

#define PARENT 0x10000
#define THREAD 0x20000

void *func(void *);
int fd,a;

int main()
{

   pthread_t t1;
   fd = open("/dev/myChar", O_RDWR);
	if (fd < 0)
	{	
		perror("Open failed:");
		exit (-1);
	}
   	
   printf("prociess id is %d\n",getpid());
   pthread_create(&t1,NULL,func,NULL);  
   pthread_join(t1,NULL);
   //ioctl(fd,PARENT,0);
   close(fd);
   return 0;
}


void *func (void *a)
{
	FILE *fp;
	char str[10];

	printf(" Im a thread of execution\n");
	while (1)
	{
		fp = fopen("test.txt", "rw");
		fscanf (fp, "%s", str);
		if (!strcmp("prabhu", str))
			break;
		fclose(fp);	
	}
	printf(" End of thread execution\n");

   //ioctl(fd,THREAD,0);
   //printf("value of pid = %d \n",getpid());
}

