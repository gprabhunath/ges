#include <stdio.h>
#include <fcntl.h>

int A = 1;
int main()
{

	int fd;

	fd = open("/dev/myChar", O_RDWR);
	
	if (fd < 0)
	   perror("Unable to open the device");
	else
	   printf("File opened Successfully %d\n", fd);

	ioctl (fd, 1, &A);
	printf ("PID of current process is %d \n", getpid());

	close(fd);

	return 0;
}
