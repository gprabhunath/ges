
// List of experiments to parse the data structures related to 
// tasks and scheduler

#include <linux/init.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/sched.h>
#include <asm-generic/percpu.h>

MODULE_LICENSE("Dual BSD/GPL");

char *devname;
int majNo;
	#define PROCESS 1
	#define THREAD  2
	#define SIGHAND 4

module_param(devname, charp, 0000);
// Function Declarations for syscall definitions
int myOpen (struct inode *inode, struct file *filep);
int myRelease (struct inode *in, struct file *fp);
int myIoctl (struct file *filep, unsigned int pid, unsigned long A);

// Local Function declarations
char *findPolicy (unsigned long policy);

// Initialization routines
static int myInit (void);
static void myExit (void);

struct file_operations fops = {open:myOpen,release:myRelease,compat_ioctl:myIoctl};

static int myInit (void)
{
 //  printk(KERN_INFO "Initializing task list parse module \n");

   majNo = register_chrdev(0,devname,&fops);
   if (majNo < 0)
   {
      printk(KERN_INFO "register_chrdev failed \n");
   } else {
 //     printk(KERN_INFO "Major Number of the device = %d \n", majNo);
   }
   
   return 0;
}

int myOpen (struct inode *inode, struct file *filep)
{
	struct task_struct *myTask;
	myTask = current;

	//printk(KERN_INFO "Opened task list parse module \n");
	//printk ("block of %d = 0x%08x \n", myTask->pid, myTask->blocked);
		
    return 0;
}

int myRelease (struct inode *in, struct file *fp)
{
//   printk(KERN_INFO "Released task list parse module \n");
   return 0;
}

static void myExit (void)
{
   printk (KERN_INFO "Removing task list parse module\n");
   unregister_chrdev(majNo, devname);
   return;
}

module_init(myInit);
module_exit(myExit);


// Experiment : KernelModule to print the sched policy of all the tasks.
#if 0
char *findPolicy (unsigned long policy)
{
	switch (policy)
	{
		case 0:
			return "SCHED_NORMAL";
		case 1:
			return "SCHED_FIFO";
		case 2:
			return "SCHED_RR";
		case 3:
			return "SCHED_BATCH";
		default:
			return "UNKNOWN";
	}

} // End of findPolicy()

int myIoctl (struct inode *inode, struct file *filep, unsigned int pid,
             unsigned long A)
{
	struct task_struct *tskParse, *curTask;

	curTask = find_task_by_pid(pid);
	list_for_each_entry(tskParse, &(curTask->tasks), tasks)
	{
		printk(KERN_INFO "pid = %d \t tgid = %d \t Policy = %s ", 
			         tskParse->pid, tskParse->tgid, 
					 findPolicy(tskParse->policy) );

		if (tskParse->pid != tskParse->tgid)
			printk(KERN_INFO "-> THREAD \n");
		else
			printk(KERN_INFO "\n");

	}

	return 0;
}

#endif

// Experiment : KM to print the members of runqueue structures

//#define rqs		(&__get_cpu_var(runqueues))
//
int myIoctl (struct file *filep, unsigned int who, unsigned long pid)
{	

	struct task_struct *myTask;
	unsigned long pending, blocked;
//	printk (KERN_INFO "Number of running processes = %lu \n", 
//				(rqs)->nr_running);	

	myTask = current;
	//printk (KERN_INFO "Entering Ioctl \n");
	//myTask = find_task_by_pid(pid);
	#if 0
	printk (KERN_INFO "size of sigset_t = %d \n", sizeof (sigset_t));	
	printk (KERN_INFO "St Dy Rt priority of task %d = %d %d %d \n",
			myTask->pid, myTask->static_prio, myTask->prio,
			myTask->rt_priority);
	//printk (KERN_INFO "pending = %u \n", myTask->pending.signal);
	printk (KERN_INFO "Address of \n");
	printk ("signal of %d = 0x%08x \n", myTask->pid, myTask->signal);
	printk ("sighand of %d = 0x%08x \n", myTask->pid, myTask->sighand);
	printk ("pending of %d = 0x%08x \n", myTask->pid, &(myTask->pending));
	printk ("blocked of %d = 0x%08x \n", myTask->pid, &(myTask->blocked));
	#endif
	
	blocked = myTask->blocked.sig[0];
	pending = myTask->signal->shared_pending.signal.sig[0];

	//printk (KERN_INFO "who = %d \n", who);
	printk(KERN_INFO "From %s %d ",
			(who & PROCESS) ? "PROCESS":
            (who & THREAD) ? "THREAD":
			(who & SIGHAND) ? "SIGHAND":"UNKNOWN", myTask->pid);

	
	if (blocked)
		printk ("myTask->blocked of %d = 0x%08x ", myTask->pid, blocked);

	if (pending)
		printk ("shared_pending of %d = 0x%08x ", myTask->pid, pending);
	
	printk ("\n");
	//printk ("blocked of %d = 0x%08x \n", myTask->pid, &(myTask->blocked));

	return 0;
} // End of myIoctl
