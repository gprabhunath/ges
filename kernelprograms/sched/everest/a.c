
#include <stdio.h>

struct p {
	int a;
	int b;
};

struct p c;
struct p *d = &c;

int main()
{

	c.a = 34;
	printf ("Address of c = 0x%08x, 0x%08x \n", c, d->a);
	return 0;
}


