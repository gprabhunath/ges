
#include <stdio.h>

struct p {
	int a;
	int b;
};

struct p c, d;

int main()
{
	c.a = 45;
	d.a = 12;

	printf ("Address of structures = 0x%08x, 0x%08x \n", &c, &d);
	printf ("Diff of structures = %u \n", (char *)&c - (char *)&d);

	return 0;
}
