#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <sys/types.h>

int main()
{

	int fd;

	fd = open("/dev/mysched", O_RDWR);
	
	if (fd < 0)
	{
	   perror("Unable to open the device");
		exit (1);
	}
	else
	   printf("File opened Successfully %d\n", fd);

	ioctl (fd, getpid(), 0);
	printf ("PID of current process is %d \n", getpid());

	close(fd);

	return 0;
}
