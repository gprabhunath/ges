
// List of experiments to parse the data structures related to 
// tasks and scheduler

#include <linux/init.h>
#include <linux/module.h>
#include <linux/sched.h>
#include <linux/fs.h>
#include <asm-generic/percpu.h>

MODULE_LICENSE("Dual BSD/GPL");

char *devname;
int majNo;

module_param(devname, charp, 0000);
// Function Declarations for syscall definitions
int myOpen (struct inode *inode, struct file *filep);
int myRelease (struct inode *in, struct file *fp);
int myIoctl (struct inode *inode, struct file *filep, unsigned int pid,
             unsigned long A);

// Local Function declarations
char *findPolicy (unsigned long policy);

// Initialization routines
static int myInit (void);
static void myExit (void);

struct file_operations fops = {open:myOpen,release:myRelease,unlocked_ioctl:myIoctl};

static int myInit (void)
{
   printk(KERN_INFO "Initializing task list parse module \n");

   majNo = register_chrdev(0,devname,&fops);
   if (majNo < 0)
   {
      printk(KERN_INFO "register_chrdev failed \n");
   } else {
      printk(KERN_INFO "Major Number of the device = %d \n", majNo);
   }
   
   return 0;
}

int myOpen (struct inode *inode, struct file *filep)
{
        printk(KERN_INFO "Opened task list parse module \n");
        return 0;
}

int myRelease (struct inode *in, struct file *fp)
{
   printk(KERN_INFO "Released task list parse module \n");
   return 0;
}

static void myExit (void)
{
   printk (KERN_INFO "Removing task list parse module\n");
   unregister_chrdev(majNo, devname);
   return;
}

module_init(myInit);
module_exit(myExit);


// Experiment : KernelModule to print the sched policy of all the tasks.
#if 0
char *findPolicy (unsigned long policy)
{
	switch (policy)
	{
		case 0:
			return "SCHED_NORMAL";
		case 1:
			return "SCHED_FIFO";
		case 2:
			return "SCHED_RR";
		case 3:
			return "SCHED_BATCH";
		default:
			return "UNKNOWN";
	}

} // End of findPolicy()
#endif

#if 0
// Experiment : KM to print the members of runqueue structures

//#define rqs		(&__get_cpu_var(runqueues))
int myIoctl (struct inode *inode, struct file *filep, unsigned int pid, 
             unsigned long A)
{	
	struct task_struct *myTask;
//	printk (KERN_INFO "Number of running processes = %lu \n", 
//				(rqs)->nr_running);	

	myTask = current;
	//myTask = find_task_by_vpid(pid);
	printk (KERN_INFO "St Dy Rt priority of task %d = %d %d %d \n",
			myTask->pid, myTask->static_prio, myTask->prio,
			myTask->rt_priority);
	printk (KERN_INFO "pending = %u \n", myTask->pending.signal);
	
	return 0;
}

#endif

int myIoctl (struct inode *inode, struct file *filep, unsigned int pid,
             unsigned long A)
{
	struct task_struct *tskParse, *curTask;

	curTask = find_task_by_pid(pid);
	list_for_each_entry(tskParse, &(curTask->tasks), tasks)
	{
#if 0
		printk(KERN_INFO "pid = %d \t tgid = %d \t Policy = %s ", 
			         tskParse->pid, tskParse->tgid, 
					 findPolicy(tskParse->policy) );
#endif
		printk(KERN_INFO "pid = %d \t tgid = %d ", 
			         tskParse->pid, tskParse->tgid);

		if (tskParse->pid != tskParse->tgid)
			printk(KERN_INFO "-> THREAD \n");
		else
			printk(KERN_INFO "\n");

	}

	return 0;
}
