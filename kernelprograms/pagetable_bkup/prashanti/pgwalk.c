#include<linux/init.h>
#include<linux/module.h>
#include<linux/fs.h>
#include<linux/highmem.h>

MODULE_LICENSE ("Dual BSD/GPL");

char *devname;
int majno;
struct task_struct *mytask;

module_param(devname, charp, 0000);
//Function Declaration for Syscall definition
int myOpen (struct inode *inode, struct file *filep);
int myRelease (struct inode *in, struct file *fp);
int myIoctl (struct inode *in,struct file *fp,unsigned int add,unsigned long a);

static int myInit (void);
static void myExit (void);

struct file_operations fops = {open:myOpen,release:myRelease,ioctl:myIoctl};

static int myInit (void)
{
	printk (KERN_INFO "Initializing the World \n");
	majno = register_chrdev (0,devname, &fops);
	if (majno < 0)
	{
		printk (KERN_INFO "register_chrdev failed \n");
	}
	else
	{
	printk (KERN_INFO "major number of the device is %d \n", majno);
	}
	return 0;
}

int myOpen (struct inode *inode, struct file *filep)
{
//	mytask=current;
	printk (KERN_INFO "Open Successful \n");
	printk(KERN_INFO "kern pid= %d\n",current->pid);
	return 0;
}

int myRelease (struct inode *in,struct file *fp)
{
	printk (KERN_INFO "File released \n");
	return 0;
}
int myIoctl (struct inode *inode, struct file *fp,unsigned int a,unsigned long add)
{
	struct task_struct *task_struct_ptr;
	struct mm_struct *mm_struct_ptr;
    pgd_t *pgd_ptr;
    unsigned int *pgd_virtual_addr;
    unsigned int pgd_offset;   // whether to declare it as a pointer variable?
    unsigned int *pt_base;
    unsigned int *pt_virtual_addr;
    unsigned int pt_offset; // whether to declare it as a poiter variable ?
    unsigned int *page_base;
    struct page *page_ptr;
    unsigned int page_offset;
    unsigned int *virtual_addr;
	unsigned int ptbase,pfn;

    printk(KERN_INFO"In ioctl function a=%d\n",a);

    task_struct_ptr = current; //find_task_by_vpid(pid);
    mm_struct_ptr = task_struct_ptr->mm;
    pgd_ptr = mm_struct_ptr->pgd;

    printk(KERN_INFO "Virtual Address of variable. 0x%08x\n",(unsigned int)add);

	// Operation on PGD
    pgd_virtual_addr = (unsigned int *)pgd_ptr; 
    pgd_offset = add >> 22; 
    pt_base = pgd_virtual_addr + pgd_offset; //add first 10 bits to get pt_base

    printk(KERN_INFO "VA of PGD base = 0x%08x\n",
							(unsigned int)pgd_virtual_addr);
    printk(KERN_INFO "VA of PGD entry = 0x%08x \n", (unsigned int)pt_base);
    printk(KERN_INFO "Value of PGD Entry = 0x%08x \n\n", *pt_base);

	// Operation on Page Table
	ptbase = *pt_base & 0xfffff000;
    pt_virtual_addr = (unsigned int *)(ptbase + 0xc0000000);
    pt_offset = (add << 10) >> 22; 
	page_base = (pt_virtual_addr + pt_offset);

    printk(KERN_INFO "PA of PT Base = 0x%08x,\n",ptbase);
    printk(KERN_INFO "VA of PT Base = 0x%08x \n", (unsigned int)pt_virtual_addr);
    printk(KERN_INFO "VA of PT entry = 0x%08x \n", (unsigned int)page_base);
    printk(KERN_INFO "Value of PT entry = 0x%08x \n\n", *page_base);


   	// Operation on Page
	pfn = *page_base & 0xfffff000;
    page_offset = (add & 0x00000FFF);
    page_ptr= pfn_to_page(pfn>>12);
    virtual_addr = kmap(page_ptr) + page_offset;

    printk(KERN_INFO "PA of Page Base = 0x%08x\n", pfn);
    printk(KERN_INFO"Kernel VA of Page entry = 0x%08x \n",
											(unsigned int)virtual_addr);
	printk(KERN_INFO "Value of Page Entry = %d\n",*virtual_addr);
    *virtual_addr = *virtual_addr + 4;
    printk(KERN_INFO "Updated Value of Page Entry = %d\n",*virtual_addr);
	
	return 0;
}	

static void myExit (void)
{
	printk (KERN_INFO "Exiting the world\n");
	unregister_chrdev (majno, devname);
	return;
}
	
module_init (myInit);
module_exit (myExit);
	

	
