#include <stdio.h>
#include <fcntl.h>

int a = 4;

int main ()
{
	int fd;
	int ret;


	fd = open ("/dev/myChar",O_RDWR);

	if (fd < 0)
		perror ("Unable to open the device");
	else
	{
		printf ("File open successful\n");
		printf("Value of a = %d \n", a);

		ret = ioctl(fd, getpid(), &a);
		if(ret != 0)
			printf("Error in executing ioctl\n");

		printf("Value of a = %d \n", a);
	}

	close (fd);
	return 0;
}

