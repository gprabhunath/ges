#include <linux/init.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/sched.h>
#include <linux/mm.h>

MODULE_LICENSE("Dual BSD/GPL");

struct task_struct *task_ptr;

char *devname;

int majNo;

module_param(devname,charp,0000);

int myOpen(struct inode *inode,struct file *filep);

int myRelease(struct inode *in,struct file *fp);

int myIoctl (struct inode *inode, struct file *f,unsigned int,unsigned long);

void vmaDump(struct vm_area_struct *, int);
void pgtDump(void);

static int myInit(void);
static void myExit(void);

struct file_operations fops = {open:myOpen,release:myRelease,ioctl:myIoctl};

// Function : Initialization routine 
static int myInit(void)
{
	printk(KERN_INFO "\nInitializing the World\n");
	majNo = register_chrdev(0,devname,&fops);
	if(majNo < 0)
	{
		printk(KERN_INFO "register_chrdev failed\n");
	}
	else
	{
		printk(KERN_INFO "Major Number of the Device = %d\n",majNo);
	}

	return 0;
}

// Function : To open the device myOpen()
int myOpen(struct inode *inode,struct file *filep)
{
	printk(KERN_INFO "Open Successful\n");
	return 0;
}

// Function : To close the device
int myRelease(struct inode *in,struct file *fp)
{
	printk(KERN_INFO "File Released\n\n");
	return 0;
}

// Function : To dump vm area structs
void vmaDump(struct vm_area_struct *vma, int vmaCount)
{
	int i = 0;
	int totalPages = 0;
	int pagesCnt = 0;
	
	printk(KERN_INFO "\n No. of VMAs = %d\n\n", vmaCount);

	while(i < task_ptr->mm->map_count)
	{	
		pagesCnt = (vma->vm_end - vma->vm_start) >> 12;
		printk(KERN_INFO "No of pages = %d \n",pagesCnt); 	
		printk(KERN_INFO "vma       = %08x\n",(unsigned int)vma);	
		printk(KERN_INFO "vma_start = %08x\n",(unsigned int)vma->vm_start);	
		printk(KERN_INFO "vma_end   = %08x\n",(unsigned int)vma->vm_end);
		printk(KERN_INFO "vma_file  = %08x\n",(unsigned int)vma->vm_file);
		printk(KERN_INFO "vma_flags  = %08x\n",(unsigned int)vma->vm_flags);
		printk(KERN_INFO "pgprot  = %08x\n",(unsigned int)vma->vm_page_prot.pgprot);
		printk(KERN_INFO "vma_next  = %08x\n\n",(unsigned int)vma->vm_next);		
		vma = vma->vm_next;
		i++;	
		totalPages = totalPages + pagesCnt;
	}

	printk(KERN_INFO "Total No of pages = %d \n\n\n",totalPages); 	

} // End of vmaDump()

// Function : To Dump Page Table Entries
void pgtDump(void)
{
	int i = 0; 
	int j = 0;
	int temp;

	unsigned int vfn;
	unsigned int *pgd_base;
	unsigned int pt_base;
	unsigned int *vir_pt;
	unsigned int *vir_pgd;
	int totalPgEntries = 0;
	int activePages = 0;

    pgd_base = (unsigned int *)task_ptr->mm->pgd;

    printk(KERN_INFO "--------------------------------------------------\n\n");
    printk(KERN_INFO "\nPGD BASE ADDRESS = 0x%08x\n\n",(unsigned int)pgd_base);

	printk(KERN_INFO "PGD     PGD\n");
	printk(KERN_INFO "INDX   ENTRY \n");
	printk(KERN_INFO "--------------\n");
    vir_pgd = pgd_base;

    while(i < 768)
    {
		if(*vir_pgd == 0x00000000)
		{	
			i++;
			vir_pgd++;
			continue;
		}

        printk(KERN_INFO "%03x  %08x\n\n",i, *vir_pgd);

		pt_base = * vir_pgd;
		pt_base = pt_base & 0xfffff000;
		pt_base = pt_base + 0xc0000000;
		vir_pt = (unsigned int *)pt_base;
		vfn = i;
		//vfn = vfn << 10;
		vfn = vfn << 22;
		
		printk(KERN_INFO "\tPT      PT \n");
		printk(KERN_INFO "\tINDX   ENTRY \t VNF \n");
		printk(KERN_INFO "\t--------------------\n");

		while(j < 1024)
		{
			if(*vir_pt == 0x00000000)
			{
				j++;
				vir_pt++;
				continue;
			}
			
			temp = j;
			temp = temp << 12;
			
			vfn = vfn | temp;
			vfn = vfn >> 12;
	
			printk(KERN_INFO "\t%03x  %08x \t%05x\n",j, *vir_pt,vfn);
	
			vfn = i;
			vfn = vfn << 22;	
	
			j++;
			vir_pt++;
			totalPgEntries = totalPgEntries + 1;
			activePages++;

		} // End of while (j <	
	
		printk (KERN_INFO "Total number of page entries = %d \n\n", totalPgEntries);
        
	    vir_pgd++;
        i++;
		j = 0;
		totalPgEntries = 0;

    } // End of while (i 
	
	printk(KERN_INFO "\nTOTAL NO. OF ACTIVE PAGES = %d \n",activePages);
        
	return;

} // End of pgtDump()

// Function : To invoke vmaDump and pgtDump
int myIoctl (struct inode *inode, struct file *f,unsigned int pid,unsigned long addr)
{
	struct vm_area_struct *vma;
	
//	task_ptr = current;
	task_ptr = find_task_by_pid(pid);		
	vma = task_ptr->mm->mmap;

	vmaDump(vma, task_ptr->mm->map_count);  // Dump vm_area structures
	pgtDump(); 							 	// Dump Page tables
	
	return 0;

} // End of myIoctl

// Function : Cleanup routine
static void myExit(void)
{
	printk(KERN_INFO "\nExiting the World\n");
	unregister_chrdev(majNo,devname);
	return;
}

module_init(myInit);
module_exit(myExit);

