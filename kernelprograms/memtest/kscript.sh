#!/bin/sh

# Remove the kernel module 
rmmod memtest
rm -f /dev/memtest 
make clean
dmesg -c > /dev/null

# Build the kernel module and insmod
make
insmod memtest.ko devname="memtest"
majNo=`grep 'memtest' /proc/devices | cut -b 1,2,3`
mknod /dev/memtest c $majNo 0

# Build the application and execute
gcc -o amod amod.c
./amod 
echo "------------------"
echo "Dumping of dmesg"
echo "------------------"
dmesg > dump
dmesg



