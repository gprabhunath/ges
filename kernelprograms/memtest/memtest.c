
#include <linux/init.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/mm.h>
#include <linux/vmalloc.h>
//#include <asm/pgtable.h>
//#include <asm/highmem.h>

MODULE_LICENSE("Dual BSD/GPL");

#define MB	(1024 * 1024)

char *devname;
int pid;
int majorno;
dev_t dev = 0;
ssize_t s = 0;



module_param(devname, charp, 0000);

int myopen (struct inode *, struct file *);

int myioctl (struct inode *, struct file *, unsigned int, unsigned long);

struct file_operations fp={open:myopen,ioctl:myioctl};


int myioctl (struct inode *x, struct file *y, unsigned int req, unsigned long add)
{
	void *addr, *save;
	unsigned int fcom_size;

//  int k = 0;
  	int i = 4;

	addr = vmalloc(10 * MB);
  	save = addr;
	i = i / 10;
	fcom_size = ((unsigned int)high_memory - PAGE_OFFSET) / MB;

  	printk(KERN_INFO " Address allocated by vmalloc = 0x%08x \n", (unsigned int)addr);

  	printk (KERN_INFO " PAGE OFFSET = 0x%08x \n", (unsigned int)PAGE_OFFSET );
  	printk (KERN_INFO " PAGE SIZE = 0x%08x \n", (unsigned int)PAGE_SIZE );
  	printk (KERN_INFO " HIGH MEMORY = 0x%08x %u MB\n", (unsigned int)high_memory,
						fcom_size);

	printk (KERN_INFO " VMALLOC_OFFSET = 0x%08x \n", (unsigned int)VMALLOC_OFFSET);
	printk (KERN_INFO " VMALLOC_START = 0x%08x \n", (unsigned int)VMALLOC_START);
	printk (KERN_INFO " VMALLOC_END = 0x%08x \n", (unsigned int)VMALLOC_END);
	printk (KERN_INFO " PKMAP_BASE = 0x%08x \n", (unsigned int)PKMAP_BASE);
	printk (KERN_INFO " LAST_PKMAP = 0x%08x \n", (unsigned int)LAST_PKMAP);
	printk (KERN_INFO " FIXADDR_START = 0x%08x \n", (unsigned int)FIXADDR_START);



  /*
  while(i<10000)
    {
      *addr='a';
       addr++;
       i++
    }
  addr=save;
  while(i<10000)
    {
      
     i++;
    }       
	*/

  	vfree(save);
  	return 0;

}

int myopen(struct inode * in, struct file *fp)
 {
   return 0;
 }


int init(void)
 {
   printk(KERN_INFO "\n Initializing MEMTEST \n");
   majorno = register_chrdev(dev, devname, &fp);
   return 0;
 }

void cleanup(void)
 {
   unregister_chrdev(majorno, devname);
   printk(KERN_INFO "\n Exiting MEMTEST \n");
 }


module_init(init);
module_exit(cleanup);

