#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/kdev_t.h>
#include <linux/device.h>
#include <linux/interrupt.h>

MODULE_LICENSE("Dual BSD/GPL");

int Majno;
int minor;
char *devname;
struct class *char_class = 0;
struct cdev *pseudodev ;
dev_t mydev;
struct module mymod;
ssize_t myread (struct file *, char __user *, size_t, loff_t *);
int myioctl (struct inode *inode, struct file *filep, 
		unsigned int command, unsigned long arg);
int myopen (struct inode *inode, struct file *filep);
struct file_operations fops = { ioctl:myioctl,open:myopen,read:myread};
//struct file_operations fops = { ioctl:myioctl,read:myread};
char *x;
//module_param (Majno,int,0000);
module_param (devname,charp,0000);

EXPORT_SYMBOL(Majno);
static int module_initialization(void)
{
	int size;
	pseudodev=cdev_alloc();
	pseudodev->ops=&fops;
	alloc_chrdev_region(&mydev,0,1,devname);
	char_class = class_create(THIS_MODULE, devname);
  	/*connect the file operations with the cdev */
  	cdev_init(pseudodev, &fops);
  	pseudodev->owner = THIS_MODULE;
  	/* connect the major/minor number to the cdev */
	cdev_add(pseudodev,mydev,1);
	device_create(char_class, NULL, mydev, NULL, devname);
	//Majno = register_chrdev (0,devname,&fops);
	printk(KERN_INFO "Hello world..in init\n");
	size=sizeof(struct module);
	printk("<0> size of struct module = %d",size);	
	return 0;
}

static void  module_cleanup(void)
{
	printk("<0> BYE\n");
	//unregister_chrdev(Majno,devname);
	device_destroy(char_class,mydev);
	cdev_del(pseudodev);
	class_destroy(char_class);
	unregister_chrdev_region(mydev,1);
}

int myopen (struct inode *inode, struct file *filep)
{
	minor=MINOR(inode->i_rdev);
	printk("<0> minor number =%d\n",minor);
	printk("<0> open successful\n");
	return 0;
}

ssize_t myread (struct file *f, char __user *usr, size_t size, loff_t *off)
{
	printk("<0> __user * =%x\n",(unsigned int)usr);
	printk("<0> size  =%d\n",(int)size);
        strcpy(usr,"Hello read");
	return (10);
}

int myioctl (struct inode *inode, struct file *filep, 
		unsigned int command, unsigned long arg)
{
	
	struct softirq_action *sirq;
	int i =0;
	unsigned int add = (unsigned int *)0xc07e5a00;
	sirq	= (struct softirq_action *)add;
	for(i=0;i<9;i++)
	{
		printk("<0>value = %08x\n", sirq[i].action);
	}
	printk("<0> HELLO WORLD\n");
	return 0;
}

module_init(module_initialization);
module_exit(module_cleanup);
