
#include <linux/init.h>
#include <linux/module.h>
#include <linux/fs.h>

MODULE_LICENSE("Dual BSD/GPL");


// Initialization routines
static int myInit (void);
static void myExit (void);

static int myInit (void)
{
	int a[4];
	printk(KERN_INFO "Initializing my Kernel Module\n");
	memset (a, 9, 0xff);
	return 0;
}


static void myExit (void)
{
   printk (KERN_INFO "Exiting the World \n");
   //unregister_chrdev(majNo, devname);
   return;
}

module_init(myInit);
module_exit(myExit);
