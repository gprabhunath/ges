
#include <linux/init.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/sched.h>
#include "mysched.h"

MODULE_LICENSE("Dual BSD/GPL");

struct pan {
	int a;
	int b;
};
extern unsigned int muxData;
extern struct pan *getpan(void);
struct pan *tx;

char *devname;
int majNo;

//module_param(devname, charp, 0000);

// Function Declarations for syscall definitions
int myOpen (struct inode *inode, struct file *filep);
int myRelease (struct inode *in, struct file *fp);

// Initialization routines
static int myInit (void);
static void myExit (void);

struct file_operations fops = {open:myOpen,release:myRelease};

static int myInit (void)
{
   printk(KERN_INFO "Initializing the World \n");

   majNo = register_chrdev(0,"myChar1",&fops);
   if (majNo < 0)
   {
      printk(KERN_INFO "register_chrdev failed \n");
   } else {
      printk(KERN_INFO "Major Number of the device = %d \n", majNo);
   }
   
   return 0;
}

int myOpen (struct inode *inode, struct file *filep)
{

	printk(KERN_INFO "Open successful\n");
	printk(KERN_INFO "Value of muxData = %d \n", muxData);
	tx = getpan();
	printk (KERN_INFO "Value of tx.a = %d \n", tx->a);

    return 0;
}

int myRelease (struct inode *in, struct file *fp)
{
   printk(KERN_INFO "File released \n");
   return 0;
}


static void myExit (void)
{
   printk (KERN_INFO "Exiting the World \n");
   unregister_chrdev(majNo, "myChar");
   return;
}

module_init(myInit);
module_exit(myExit);
