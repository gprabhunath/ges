
#include <linux/init.h>
#include <linux/module.h>
#include <linux/fs.h>

MODULE_LICENSE("Dual BSD/GPL");

char *devname;
int majNo;
unsigned muxData;
EXPORT_SYMBOL(muxData);

struct pan {
	int a;
	int b;
};

struct pan fax, *tax;

struct pan *getpan(void);
struct pan *getpan(void)
{
	fax.a = 34;
	fax.b = 23;
	tax = &fax;
	return &fax;
}
EXPORT_SYMBOL(getpan);
EXPORT_SYMBOL(tax);

module_param(devname, charp, 0000);
// Function Declarations for syscall definitions
int myOpen (struct inode *inode, struct file *filep);
int myRelease (struct inode *in, struct file *fp);

// Initialization routines
static int myInit (void);
static void myExit (void);

struct file_operations fops = {open:myOpen,release:myRelease};

static int myInit (void)
{
   printk(KERN_INFO "Initializing the World \n");

   majNo = register_chrdev(0, "myChar", &fops);
   if (majNo < 0)
   {
      printk(KERN_INFO "register_chrdev failed \n");
   } else {
      printk(KERN_INFO "Major Number of the device = %d \n", majNo);
   }
   
   return 0;
}

int myOpen (struct inode *inode, struct file *filep)
{
        printk(KERN_INFO "Open successful\n");
		muxData = 243;
		printk (KERN_INFO "muxData written\n");
		getpan();
		printk (KERN_INFO "Address of tax = 0x%08x \n", tax);
        return 0;
}

int myRelease (struct inode *in, struct file *fp)
{
   printk(KERN_INFO "File released \n");
   return 0;
}


static void myExit (void)
{
   printk (KERN_INFO "Exiting the World \n");
   unregister_chrdev(majNo, devname);
   return;
}

module_init(myInit);
module_exit(myExit);
