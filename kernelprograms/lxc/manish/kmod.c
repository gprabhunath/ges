
/** System Includes **/
#include <linux/init.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/sched.h>
#include <linux/nsproxy.h>
#include <linux/pid_namespace.h>
#include <linux/cdev.h>
#include <asm/uaccess.h>
#include <linux/device.h>
#include <linux/slab.h>

MODULE_LICENSE("Dual BSD/GPL");

/** Constants **/
#define FIRST_MINOR 	10
#define NR_DEVS	1 // Number of device numbers



// Function Declarations for syscall definitions
int myOpen (struct inode *inode, struct file *filep);
int myRelease (struct inode *in, struct file *fp);
long myIoctl (struct file *, unsigned int, unsigned long);

// Initialization routines
static int myInit (void);
static void myExit (void);

struct file_operations fops = {
		.owner = THIS_MODULE,
		.open = myOpen,
		.release = myRelease,
		.unlocked_ioctl = myIoctl
};

/* Global variables */
char *devname; // contains device name
int majNo;     
static dev_t mydev; // encodes major number and minor number
struct cdev *my_cdev; // holds character device driver descriptor

/* To accept input from the command line */
module_param(devname, charp, 0000);

// class and device structures 
static struct class *mychar_class;
static struct device *mychar_device;


/*
 * myOpen: open function of the pseudo driver
 *
 */

int myOpen (struct inode *inode, struct file *filep)
{
        printk(KERN_INFO "Open successful\n");
        return 0;
}

/*
 * myRelease: close function of the pseudo driver
 *
 */

int myRelease (struct inode *in, struct file *fp)
{
   printk(KERN_INFO "File released \n");
   return 0;
}
/*
 * myInit : init function of the kernel module
 *
 */
static int __init myInit (void)
{
	int ret = -ENODEV;
	int status;

	printk(KERN_INFO "Initializing Character Device \n");

	// Allocating Device Numbers
	status = alloc_chrdev_region (&mydev, FIRST_MINOR, NR_DEVS, devname);
	if (status < 0)
	{
		printk (KERN_NOTICE "Device numbers allocation failed: %d \n", 
								status);
		goto err;
	}

	printk (KERN_INFO "Major number allocated = %d \n", MAJOR(mydev));
	my_cdev = cdev_alloc(); // Allocate memory for my_cdev
	if (my_cdev == NULL) {
		printk (KERN_ERR "cdev_alloc failed \n");	
		goto err_cdev_alloc;
	}

	
	cdev_init(my_cdev, &fops); // Initialize my_cdev with fops
	my_cdev->owner = THIS_MODULE;

	status = cdev_add (my_cdev, mydev, NR_DEVS);// Add my_cdev to the list
	if (status) {
		printk (KERN_ERR "cdev_add failed  \n");
		goto err_cdev_add;
	}

	// Create a class and an entry in sysfs
	mychar_class = class_create(THIS_MODULE, devname);
	if (IS_ERR(mychar_class)) {
		printk (KERN_ERR "class_create() failed \n");
		goto err_class_create;
	}

	// creates mychar_device in sysfs and an 
	// device entry will be made in /dev directory
	mychar_device = device_create (mychar_class, NULL, mydev, NULL, devname);
	if (IS_ERR(mychar_device)) {
		printk (KERN_ERR "device_create() failed \n");
		goto err_device_create;
		
	}

	return 0;

err_device_create:
	class_destroy (mychar_class);

err_class_create:
	cdev_del(my_cdev);

err_cdev_add:
	kfree (my_cdev);
	
err_cdev_alloc:
	unregister_chrdev_region (mydev, NR_DEVS);

err:
   return ret;
}

/*
 * myExit : cleanup function of the kernel module
 */

static void myExit (void)
{
	printk (KERN_INFO "Exiting the Character Driver \n");
	
	device_destroy (mychar_class, mydev);
	class_destroy(mychar_class);
	cdev_del(my_cdev);
	unregister_chrdev_region(mydev, NR_DEVS);	

	return;
}

module_init(myInit);
module_exit(myExit);

long myIoctl (struct file *filep, unsigned int pid, unsigned long A)
{
	struct task_struct *tskParse, *curTask;
	struct pid *pidPtr ;

	//curTask = find_task_by_vpid(pid);
	curTask = current;
	printk(KERN_INFO "\n param in ioctl is %lu\n", A );
	printk(KERN_INFO "\n for current process:\n");
	pidPtr = curTask->pids[PIDTYPE_PID].pid ;

	printk(KERN_INFO "pid = %d \t tgid = %d PID_NS = %p, nsproxy = %p,nsproxy PID_NS = %p, ppid is %d, struct pid : %p, struct upid : %p, level %d",
			curTask->pid, curTask->tgid, task_active_pid_ns(curTask), curTask->nsproxy, curTask->nsproxy->pid_ns_for_children, curTask->real_parent->pid, pidPtr, &pidPtr->numbers[pidPtr->level], pidPtr->level);

#if 0
	if (curTask->nsproxy->pid_ns_for_children)
	{
		printk(KERN_INFO "\t last_pid = %d, hide_pid = %d ",curTask->nsproxy->pid_ns_for_children->last_pid, curTask->nsproxy->pid_ns_for_children->hide_pid );
		if (curTask->nsproxy->pid_ns_for_children->child_reaper)
			printk(KERN_INFO "\n oldCpid is %d, oldCtgid is %d ", curTask->nsproxy->pid_ns_for_children->child_reaper->pid, curTask->nsproxy->pid_ns_for_children->child_reaper->tgid);
	}
#endif

	if (A == 1)
{
	list_for_each_entry(tskParse, &(curTask->tasks), tasks)
	{
			pidPtr = tskParse->pids[PIDTYPE_PID].pid ;
			printk(KERN_INFO "\n process: \n pid = %d \t tgid = %d PID_NS = %p, nsproxy = %p, nsproxy PID_NS = %p, struct pid addr : %p , struct upid %p level %d, nr %d \n", 
							tskParse->pid, tskParse->tgid, task_active_pid_ns(tskParse), tskParse->nsproxy, tskParse->nsproxy->pid_ns_for_children, pidPtr, &pidPtr->numbers[pidPtr->level], pidPtr->level, pidPtr->numbers[pidPtr->level].nr);
			if (pidPtr -> level)
			{
					int count = pidPtr -> level;
					for ( ; count ; count--)
							printk(KERN_INFO "new upid addr is %p, nr %d\n", &pidPtr->numbers[count - 1], pidPtr->numbers[count - 1].nr);
			}

	}

}

#if 0
pidPtr = find_pid_ns(task_tgid_vnr(current), task_active_pid_ns(current));
rcu_read_lock();                                                          
tskParse = pid_task(pidPtr, PIDTYPE_PID);                                           
rcu_read_unlock();  
for (curTask = tskParse; tskParse; ) {
   printk(KERN_INFO "## pid is %d\n", tskParse->tgid);
   rcu_read_lock();                                                          
   tskParse = pid_task(pidPtr, PIDTYPE_PID);                                           
   rcu_read_unlock();
   if (curTask == tskParse)
      break;
}

if (A == 1)
	{

			list_for_each_entry(tskParse, &(curTask->children), children)
		{

			printk(KERN_INFO "\n child process: \n Cpid = %d \t Ctgid = %d Cnsproxy = %p, Cppid is %d ", 
					tskParse->pid, tskParse->tgid, tskParse->nsproxy, tskParse->real_parent->pid);

			if (tskParse->nsproxy->pid_ns_for_children)
			{
				printk(KERN_INFO "\t Clast_pid = %d, Chide_pid = %d ",tskParse->nsproxy->pid_ns_for_children->last_pid, tskParse->nsproxy->pid_ns_for_children->hide_pid );
				if (tskParse->nsproxy->pid_ns_for_children->child_reaper)
					printk(KERN_INFO "\n oldCpid is %d, oldCtgid is %d ", tskParse->nsproxy->pid_ns_for_children->child_reaper->pid, tskParse->nsproxy->pid_ns_for_children->child_reaper->tgid);
			}
			if (tskParse->pid != tskParse->tgid)
				printk(KERN_INFO "-> THREAD \n");
			else
				printk(KERN_INFO "\n");
		}
	}
	//else
	//{
	list_for_each_entry(tskParse, &(curTask->tasks), tasks)
	{
#if 0
		printk(KERN_INFO "pid = %d \t tgid = %d \t Policy = %s ", 
				tskParse->pid, tskParse->tgid, 
				findPolicy(tskParse->policy) );
#endif
		printk(KERN_INFO "pid = %d \t tgid = %d nsproxy = %p , ppid is %d ", 
				tskParse->pid, tskParse->tgid, tskParse->nsproxy, tskParse->real_parent->pid);
		if (tskParse->nsproxy->pid_ns_for_children)
		{
			printk(KERN_INFO "\t last_pid = %d, hide_pid = %d ",tskParse->nsproxy->pid_ns_for_children->last_pid, tskParse->nsproxy->pid_ns_for_children->hide_pid );
			if (tskParse->nsproxy->pid_ns_for_children->child_reaper)
				printk(KERN_INFO "\n oldCpid is %d, oldCtgid is %d ", tskParse->nsproxy->pid_ns_for_children->child_reaper->pid, tskParse->nsproxy->pid_ns_for_children->child_reaper->tgid);
		}
		if (tskParse->pid != tskParse->tgid)
			printk(KERN_INFO "-> THREAD \n");
		else
			printk(KERN_INFO "\n");

	}
	//}

#endif
	return 0;
}
