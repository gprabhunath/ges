#define _GNU_SOURCE
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/mount.h>
#include <sys/ioctl.h>
#include <pthread.h>
#include <stdio.h>
#include <sched.h>
#include <signal.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>

#define STACK_SIZE (1024 * 64)

int fd[2];
int stack[65536];
char* args[] = {
			  "/bin/bash",	
			    NULL 
				};

void myIoctl(int param)
{
    int fd;

    fd = open("/dev/myChar", O_RDWR);

    if (fd < 0)
    {
       perror("Unable to open the device");
        exit (1);
    }
    else
       printf("File opened Successfully %d\n", fd);

    ioctl (fd, getpid(), param);
    printf ("PID of current process is %d \n", getpid());

    close(fd);

    return;

}

void *child_thread(void *args)
{
   char ch = 'a';
	printf("\n inside child thread\n");
	myIoctl(0);
   write(fd[1], &ch, 1);
   getchar();
   printf("exiting %s\n", __func__);
	pthread_exit(NULL);
}
	

int child_function(void* arg)
{
		pthread_t threadId;
		pid_t pid;
		int ret;
		char ch = 'a';

		close(fd[0]);
		printf(" Pid of child= %d, \n", getpid());
		sethostname("my_ns", 12);
		//myIoctl(0);

		ret = mount("/", "/opt/lxc1/", NULL, MS_BIND|MS_PRIVATE, NULL);
		if (ret) {
		perror("mount:");
		exit(1);
		}
		ret = fork();
		if (ret == 0) {
				execv(args[0],args);
				exit(1);
		}
		else if (ret > 0) {
				waitpid(ret, NULL, 0);
				write(fd[1], &ch, 1);
				printf("exiting %s\n", __func__);
		}
#if 0
		ret = pthread_create(&threadId, NULL, child_thread, NULL);
		if (ret)
		{
				printf("pthread_create error : %d\n", ret);
				exit(1);
		}
		pthread_join(threadId, NULL);

		pid = fork();
		if (0 == pid)
		{
				execl("/bin/bash", "/bin/bash", NULL);
				return 1;
		}
		else if (pid < 0)
		{
				printf("\n error in fork\n");
				return 1;
		}
		waitpid(-1, NULL, 0);
#endif
		//    execv(args[0],args);
		return 0;
}

int main()
{
		int pid;
		char ch;

		pipe(fd);
		printf(" pid of main - %d  \n", getpid());
		pid = clone(child_function, stack+STACK_SIZE,
						CLONE_NEWUTS | CLONE_NEWIPC | CLONE_NEWPID |CLONE_NEWNS | SIGCHLD, NULL);
		close(fd[1]);
		read(fd[0], &ch, 1 );
		//myIoctl(1);
		waitpid(pid, NULL, 0);
		printf("exiting %s\n", __func__);
		return 0;
}
