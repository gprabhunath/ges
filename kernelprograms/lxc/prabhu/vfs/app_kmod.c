#define _GNU_SOURCE
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <sched.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/ioctl.h>
#include <sys/mount.h>

#define DEVFILE	"/dev/newns"
#define STACK_SIZE (8 * 1024)
#define NS_CLONE_FLAGS (CLONE_NEWUTS | CLONE_NEWNS | CLONE_NEWIPC | \
				CLONE_NEWPID | CLONE_NEWNET | SIGCHLD)
unsigned char stack[STACK_SIZE];
int fd;

void openPseudo_cdev(void)
{
	fd = open(DEVFILE, O_RDWR);
	if (fd < 0)
	   perror("open failed");

	return;
}

void ioctlPseudo_cdev(int n)
{
	int ret;
	ret = ioctl (fd, getpid(), n);
	if (ret < 0)
		perror ("ioctl failed");
	
	return;
	
}

void do_mounts(void)
{
	int status;

	status = mount ("", "/", "", MS_REC|MS_PRIVATE, NULL);
	if (status < 0) {
		perror ("mount PVT failed");
		exit (1);
	}
#if 1
	status = mount ("/opt/lxc1", "/opt/lxc1", NULL, MS_REC|MS_BIND, NULL);
	if (status < 0) {
		perror ("mount BIND failed");
		exit (1);
	}
#endif
	status = mount ("proc", "/proc", "proc", 
					MS_NOSUID|MS_NODEV|MS_NOEXEC, NULL);
	if (status < 0) {
		perror ("mount proc failed");
		exit (1);
	}

	status = mount ("/dev/sdb1", "/media/usb", "vfat", 0, NULL);
	if (status < 0) {
		perror ("mount failed");
		exit (1);
	}
}
int child_start_fn(void *p)
{
	int status;
	
	sethostname ("my_ns", 8);
	do_mounts();	
	ioctlPseudo_cdev(1);
	execlp("bash", "bash", NULL);
//	system("ls /media/usb");

	return 0;	
}

void nextgen(void)
{
	int status;

	status = clone (child_start_fn, stack+STACK_SIZE, NS_CLONE_FLAGS, NULL);
	if (status < 0) {
		perror ("clone failed");
		exit (1);
	}

	return;
}
int main(void)
{
	printf ("PID = %d \n", getpid());
	openPseudo_cdev();
	ioctlPseudo_cdev(0);
	nextgen();

	wait(NULL);	

	return 0;
}
