
#include "ns.h"

extern int fd[2];

#define MERGED "/opt/lxc/merged"
#define LOWER "/opt/lxc/lower"
#define UPPER  "/opt/lxc/upper"
#define WORKDIR "/opt/lxc/work"

void do_mounts()
{
	int ret;
	int info = 0;

#if 1
	ret = mount ("overlay", MERGED, "overlay", MS_RELATIME, "lowerdir=/opt/lxc/lower,upperdir=/opt/lxc/upper,workdir=/opt/lxc/work");
	if (ret < 0)
	{
		perror ("mount overlay failed:");
		exit (1);
	}
#endif
	ret = mount("", "/", "", MS_REC|MS_PRIVATE, NULL);
	if (ret) {
		perror("mount root:");
		write(fd[1], &info, 4);
		exit(1);
	}

	ret = mount(MERGED, MERGED, "bind", MS_BIND|MS_REC, NULL);
	if (ret) {
		perror("mount MERGED");
		write(fd[1], &info, 4);
		exit(1);
	}

	ret = mount("proc", "/opt/lxc/merged/proc", "proc", 0, NULL);
	if (ret) {
		perror("mount proc:");
		write(fd[1], &info, 4);
		exit(1);
	}

	ret = chdir (MERGED);
	if (ret < 0) {
		perror("chdir failed");
		write(fd[1], &info, 4);
		exit(1);
     }
 
	return;	
}
