#include "ns.h"


#define STACK_SIZE (8 * 1024)
#define CLONE_FLAGS (CLONE_NEWNS | CLONE_NEWUTS | CLONE_NEWIPC | CLONE_NEWPID | CLONE_NEWNET)

int fd[2];
unsigned char stack[STACK_SIZE];
char* args[] = {
			  "/bin/bash",	"-i", 
			    NULL 
				};

void myIoctl(int param)
{
    int fd;

    fd = open("/dev/myChar", O_RDWR);

    if (fd < 0)
    {
       perror("Unable to open the device");
        exit (1);
    }
    else
       printf("File opened Successfully %d\n", fd);

    ioctl (fd, getpid(), param);
    printf ("PID of current process is %d \n", getpid());

    close(fd);

    return;

}

void createNewEnv (void)
{
		int ret;
		int info ;
		int status;

		printf(" Pid of child= %d, \n", getpid());

		do_mounts();
		sethostname("my_ns", 12);

	ret = syscall (SYS_pivot_root, ".", ".");	
	if (ret) {
		perror("syscall failed:");
		write(fd[1], &info, 4);
		exit(1);
	 }
	execv(args[0],args);

	return ;
}

int child_function(void* arg)
{
		pid_t pid;
		int ret;
		int info ;
		int status;

	

#if 1
		ret = fork();
		if (ret == 0) {
			//	execv(args[0],args);
			createNewEnv();
				exit(1);
		}
		else if (ret > 0) {
				waitpid(ret, NULL, 0);
		      	write(fd[1], &info, 4);
				printf("exiting %s\n", __func__);
		}
#endif 
		return 0;
}

int main()
{
	int pid;
	int info;
	int status;

	pipe(fd);
	printf(" pid of main = %d  \n", getpid());
	status = unshare (CLONE_FLAGS);
	if (status != 0)
	{
		perror ("unshare failed");
		exit (1);
	}
	pid = clone(child_function, stack+STACK_SIZE, CLONE_PARENT | SIGCHLD, NULL);
	read(fd[0], &info, 4);
	waitpid(pid, NULL, 0);
	printf("exiting %s\n", __func__);
	return 0;
}
