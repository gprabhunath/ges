
/** System Includes **/
#include <linux/init.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <asm/uaccess.h>
#include <asm/atomic.h>
#include <linux/device.h>
#include <linux/slab.h>
#include <linux/sched.h>
#include <linux/nsproxy.h>
#include "/usr/src/kernels/linux-4.6/fs/mount.h"
#include <linux/mount.h>

MODULE_LICENSE("Dual BSD/GPL");

/** Constants **/
#define FIRST_MINOR 	10
#define NR_DEVS	1 // Number of device numbers



// Function Declarations for syscall definitions
int myOpen (struct inode *inode, struct file *filep);
int myRelease (struct inode *in, struct file *fp);
long myIoctl (struct file *fp, unsigned int, unsigned long);

// Initialization routines
static int myInit (void);
static void myExit (void);

struct file_operations fops = {
		.owner = THIS_MODULE,
		.open = myOpen,
		.release = myRelease,
		.unlocked_ioctl = myIoctl
};

/* Global variables */
char *devname; // contains device name
int majNo;     
static dev_t mydev; // encodes major number and minor number
struct cdev *my_cdev; // holds character device driver descriptor

/* To accept input from the command line */
module_param(devname, charp, 0000);

// class and device structures 
static struct class *mychar_class;
static struct device *mychar_device;


/*
 * myOpen: open function of the pseudo driver
 *
 */

int myOpen (struct inode *inode, struct file *filep)
{
        printk(KERN_INFO "Open successful\n");
        return 0;
}

/*
 * myRelease: close function of the pseudo driver
 *
 */

int myRelease (struct inode *in, struct file *fp)
{
   printk(KERN_INFO "File released \n");
   return 0;
}
/*
 * myInit : init function of the kernel module
 *
 */
static int __init myInit (void)
{
	int ret = -ENODEV;
	int status;

	printk(KERN_INFO "Initializing Character Device \n");

	// Allocating Device Numbers
	status = alloc_chrdev_region (&mydev, FIRST_MINOR, NR_DEVS, devname);
	if (status < 0)
	{
		printk (KERN_NOTICE "Device numbers allocation failed: %d \n", 
								status);
		goto err;
	}

	printk (KERN_INFO "Major number allocated = %d \n", MAJOR(mydev));
	my_cdev = cdev_alloc(); // Allocate memory for my_cdev
	if (my_cdev == NULL) {
		printk (KERN_ERR "cdev_alloc failed \n");	
		goto err_cdev_alloc;
	}

	
	cdev_init(my_cdev, &fops); // Initialize my_cdev with fops
	my_cdev->owner = THIS_MODULE;

	status = cdev_add (my_cdev, mydev, NR_DEVS);// Add my_cdev to the list
	if (status) {
		printk (KERN_ERR "cdev_add failed  \n");
		goto err_cdev_add;
	}

	// Create a class and an entry in sysfs
	mychar_class = class_create(THIS_MODULE, devname);
	if (IS_ERR(mychar_class)) {
		printk (KERN_ERR "class_create() failed \n");
		goto err_class_create;
	}

	// creates mychar_device in sysfs and an 
	// device entry will be made in /dev directory
	mychar_device = device_create (mychar_class, NULL, mydev, NULL, devname);
	if (IS_ERR(mychar_device)) {
		printk (KERN_ERR "device_create() failed \n");
		goto err_device_create;
		
	}

	return 0;

err_device_create:
	class_destroy (mychar_class);

err_class_create:
	cdev_del(my_cdev);

err_cdev_add:
	kfree (my_cdev);
	
err_cdev_alloc:
	unregister_chrdev_region (mydev, NR_DEVS);

err:
   return ret;
}

/*
 * myExit : cleanup function of the kernel module
 */

static void myExit (void)
{
	printk (KERN_INFO "Exiting the Character Driver \n");
	
	device_destroy (mychar_class, mydev);
	class_destroy(mychar_class);
	cdev_del(my_cdev);
	unregister_chrdev_region(mydev, NR_DEVS);	

	return;
}

module_init(myInit);
module_exit(myExit);



/*
	Experiment : To look into the vfsmount from task_struct
*/
char *print_path(struct dentry *my_dentry)
{
    static char buf[254] = {0};
    if(my_dentry == my_dentry->d_parent) {
        memset (buf, 0, sizeof (buf));
        return buf;
    }
    else {
        print_path(my_dentry->d_parent);
       // name = my_dentry->d_name.name;
        strcat(buf, "/");
        strcat (buf, my_dentry->d_name.name);
        return buf;
    }
}

void list_mnt_child(struct mount *rootmnt)
{
	struct mount *r_mnt = NULL;
#if 0
	if (list_empty(&rootmnt->mnt_mounts))
	{	
		return;
	}
	list_for_each_entry(r_mnt, &rootmnt->mnt_mounts, mnt_child) {
		list_mnt_child(r_mnt);	
		if (r_mnt->mnt_devname)
			printk ("%s : %s : %s \n", r_mnt->mnt_parent->mnt_devname, 
										r_mnt->mnt_devname,
									print_path(r_mnt->mnt_mountpoint));
	}
#endif		
	list_for_each_entry(r_mnt, &rootmnt->mnt_mounts, mnt_child) {
		printk (KERN_INFO "Dev Name |  Major:Minor | Super block\n");
		printk (KERN_INFO "%s \t %d:%d \t %p \n", r_mnt->mnt_devname,
			MAJOR(r_mnt->mnt.mnt_sb->s_dev), MINOR(r_mnt->mnt.mnt_sb->s_dev),
			r_mnt->mnt.mnt_sb );
	}
	return;
}

#if 1
long myIoctl (struct file *fp, unsigned int pid, unsigned long u_val)
{
	struct task_struct *myTask;
	struct mount *rootmnt;
//	struct mount *r_mnt = NULL;
	int p = 98;

#if 0
	struct vfsmount *mntHead;
	struct vfsmount *pwdmnt;
	struct vfsmount *mnt_parent;
#endif
	//myTask = find_task_by_pid(pid);
	myTask = current;
	rootmnt = myTask->nsproxy->mnt_ns->root;
	printk ("Information of pid %d \n", pid);

#if 0
	rootmnt = myTask->fs->rootmnt;
	pwdmnt = myTask->fs->pwdmnt;
	mnt_parent = rootmnt->mnt_parent;
	printk ("task_struct = %p \n", myTask);
	printk ("nsproxy = %p \n", myTask->nsproxy);
	printk ("Count in nsproxy = %d \n", atomic_read (&myTask->nsproxy->count));
	printk ("mnt_namespace = %p \n", myTask->nsproxy->mnt_ns);
	printk ("root mnt point = %p \n", rootmnt);
	printk (KERN_INFO "Dev filename of the mounted filesystem descriptor of the root directory = %s \n", rootmnt->mnt_devname);
	printk (KERN_INFO "Name of the mount point directory where this file system is mounted = %s \n", rootmnt->mnt.mnt_root->d_iname);
	printk (KERN_INFO "Super block = %p \n", rootmnt->mnt.mnt_sb);
	printk ("Major:Minor = %d:%d \n", MAJOR(rootmnt->mnt.mnt_sb->s_dev),
							MINOR(rootmnt->mnt.mnt_sb->s_dev));
#endif
	list_mnt_child(rootmnt);


#if 0
	printk (KERN_INFO "\n");
	if (rootmnt->mnt.mnt_root->d_iname)
		p = 100;
	printk ("%d \n", p);
	printk (KERN_INFO "Name of the mount point directory where this file system is mounted = %s \n", rootmnt->mnt.mnt_root->d_iname);
	printk ("Major:Minor = %d:%d \n", MAJOR(rootmnt->mnt.mnt_sb->s_dev),
							MINOR(rootmnt->mnt.mnt_sb->s_dev));

	list_mnt_child(rootmnt);
		if (r_mnt && r_mnt->mnt_devname)
			printk ("Dev file name = %s \n", r_mnt->mnt_devname);
		if (r_mnt->mnt_mp && r_mnt->mnt_mp->m_dentry)
			printk ("Parent mount point is %s \n", r_mnt->mnt_mp->m_dentry->d_iname);
		printk ("Major:Minor = %d:%d \n", MAJOR(r_mnt->mnt.mnt_sb->s_dev),
							MINOR(r_mnt->mnt.mnt_sb->s_dev));

	printk (KERN_INFO "Name of the root directory of this file system is = %s \n", rootmnt->mnt_root->d_name.name);

//	printk (KERN_INFO "Dev filename of the mounted filesystem descriptor of the cwd = %s \n", pwdmnt->mnt_devname);

	//printk (KERN_INFO "Name of the mount point directory where the file system is mounted = %s \n", mnt_mntpoint->d_name.name);

	printk (KERN_INFO "\n");

	printk (KERN_INFO "Dev filename of parent filesystem on which this filesystem is mounted = %s \n", mnt_parent->mnt_devname);
	
	printk (KERN_INFO "Name of the mount point directory where this file system is mounted = %s \n", mnt_parent->mnt_mountpoint->d_name.name);

	printk (KERN_INFO "Name of the root directory of the parent file system = %s \n", mnt_parent->mnt_mountpoint->d_name.name);


	// List of dev filename, mnt point directory and root directory of 
	// the filesystem descriptors mounted on this filesystem
	printk (KERN_INFO "\nList of dev filename, mount point directory and root directory of the filesystem descriptors mounted on the native filesystem \n\n");

	list_for_each_entry(mntHead, &(rootmnt->mnt_mounts), mnt_child)
	{
		printk (KERN_INFO "Dev name = %-9s | ", mntHead->mnt_devname);
		printk ("Mnt pt. dir = %-7s | ", mntHead->mnt_mountpoint->d_name.name);
		printk ("Root dir = %s \n", mntHead->mnt_root->d_name.name);
		
	} // End of list_for_each_entry

	// List of dev filename, mnt point directory and root directory of 
	// the filesystem descriptors mounted on rootfs filesystem
	printk (KERN_INFO "\nList of dev filename, mount point directory and root directory of the filesystem descriptors mounted on rootfs filesystem \n\n");
	
	list_for_each_entry(mntHead, &(mnt_parent->mnt_mounts), mnt_child)
	{
		printk (KERN_INFO "Dev name = %s \t", mntHead->mnt_devname);
		printk ("Mnt pt. dir = %s \t", mntHead->mnt_mountpoint->d_name.name);
		printk ("Root dir = %s \n", mntHead->mnt_root->d_name.name);
		
	} // End of list_for_each_entry

	// List of dev filename, mnt point directory and root directory of 
	// the mounted filesystem descriptors belonging to the namespace.

	printk (KERN_INFO "\nList of dev filename, mount point directory and root directory of the mounted filesystem descriptors belonging to the namespace \n\n");
	
	list_for_each_entry(mntHead, &(myTask->namespace->list), mnt_list)
	{
		printk (KERN_INFO "Dev name = %-13s | ", mntHead->mnt_devname);
		printk ("Mnt pt. dir = %-11s | ", mntHead->mnt_mountpoint->d_name.name);
		printk ("Root dir = %s \n", mntHead->mnt_root->d_name.name);
		
	} // End of list_for_each_entry

	printk (KERN_INFO "\n");
#endif

	return 0;

} // End of myIoctl

#endif
