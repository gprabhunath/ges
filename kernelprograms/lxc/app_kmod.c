#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/types.h>


int main()
{

   int fd;

   fd = open("/dev/myChar", O_RDWR);
   
   if (fd < 0)
      perror("Unable to open the device");
   else
      printf("File opened Successfully %d\n", fd);

	ioctl (fd, getpid(), 0);
   close(fd);

   return 0;
}
