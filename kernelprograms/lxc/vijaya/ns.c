#define _GNU_SOURCE
#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>
#include <sched.h>
#include <signal.h>
#include <unistd.h>

#define STACK_SIZE (1024 * 64)

int fd[2];
int stack[65536];
char* args[] = {
			  "/bin/bash",	
			    NULL 
				};

void myIoctl()
{
    int fd;

    fd = open("/dev/mysched", O_RDWR);

    if (fd < 0)
    {
       perror("Unable to open the device");
        exit (1);
    }
    else
       printf("File opened Successfully %d\n", fd);

    ioctl (fd, getpid(), 0);
    printf ("PID of current process is %d \n", getpid());

    close(fd);

    return;

}
int child_function(void* arg)
{
    char pipe_data;

    close(fd[1]);
    read(fd[0], &pipe_data, 1);
    printf(" Pid of child= %d, \n", getpid());
    sethostname("my_ns", 12);
	myIoctl();
//    execv(args[0],args);
}

int main()
{
    int pid;

    pipe(fd);
    printf(" pid of main - %d  \n", getpid());
    pid = clone(child_function, stack+STACK_SIZE,
       CLONE_NEWUTS | CLONE_NEWIPC | CLONE_NEWPID| CLONE_NEWNS | SIGCHLD, NULL);
    sleep(4);
    close(fd[1]);
  
    waitpid(pid, NULL, 0);
    return 0;

}
