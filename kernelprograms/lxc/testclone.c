#define _GNU_SOURCE
#include <stdio.h>
#include <errno.h>
#include <fcntl.h>
#include <stdlib.h>
#include <sched.h>
#include <sys/types.h>
#include <sys/mount.h>
#include <unistd.h>
#include <sys/wait.h>


/*
	Experiment : To understand mnt namespace. 
	Definition : mnt_namespace is the list of the mounted filesystems visible
				 associated to the task. If a particular task is cloned, then
				 the new tasks inherits the mnt_namespace of the parent task.
				 If the new task is cloned with CLONE_NEWNS, then a copy of
				 the mounted filesystem list is created for the new task,
				 though they will be accessing the same superblocks accessed
				 thru the parent's task, with only difference that if the
				 new task mounts any new filesystem, then it is only visible
				 to the new task and the tasks created by this new task but
				 not to the parent task.

	1. Created a new task using clone system call with CLONE_NEWNS flag  
	   and myfunc function.

	2. Using ioctl in myfunc function, found that there was a new address
	   for the namespace in the task_struct of the newly created task and
	   also new addresses for the vfsmount lists headed by the namespace.
	   But the superblock pointed to by the vfsmount was same as that of the
	   parent. Thus infering that a copy of the newspace with the duplicated 
	   vfsmount lists has been created for the new task.

	3. Thus, any file created/modified/deleted by myfunc in the filesystem 
	   would be reflected on the native filesystem and thus visible in the 
	   parent process filesystem view also. 

 	4. But any filesystem mounted on to the child task (myfunc) would be 
	   explicitly visible to the child task only and not visible to the 
	   parent task or any other task which has a filesystem view other 
	   than the child task.

*/
int myfunc (void *);

int myfunc(void *A)
{
	int fd;
	int ret;
	int data;

	printf ("I am in child process %d \n", getpid());
#if 1
    fd = open("/dev/myLXC", O_RDWR);
    if (fd < 0)
		perror("Unable to open the device");
    else
	    printf("File opened in myfunc() Successfully %d\n", fd);
#endif

#if 0
	//ret = mount ("/dev/sdb1", "/mnt", "vfat", 0, "posix");
	//ret = mount ("", "/", "", MS_REC|MS_PRIVATE, NULL);
#endif
    ioctl (fd, getpid(), 0);
	ret = mount ("", "/", "", MS_REC|MS_PRIVATE, NULL);
	if (ret < 0)
		perror("Mount failed");
	else
		printf ("Mount 1 success \n");

    ioctl (fd, getpid(), 0);

	ret = mount ("/var/lib/docker/manish", "/home", "", MS_REC|MS_BIND, NULL);
	if (ret < 0)
		perror("Mount failed");
	else
		printf ("Mount 2 success \n");

    ioctl (fd, getpid(), 0);
	sethostname("MANISH", sizeof ("manish"));
	execv ("/bin/bash", NULL);
#if 0
	system ("touch /mnt/myfile");
	system ("ls /mnt");
	system ("rm -f /mnt/myfile");
#endif

    close(fd);
	getchar();

	if (!umount ("/var/lib/docker"))
		printf ("Umount successfull \n");
	else
		perror("Unmount failed:");
	return 0;

} // End of myfunc

int B = 3;
void *cstack;

int main()
{
	int cret = 0;
	int fd;
	
	cstack = malloc(4096 * 2);


	printf ("Main func %d \n", getpid());

	// clone sys call with the flag CLONE_NEWNS
	//cret = clone(myfunc, cstack + 8191, CLONE_NEWNS|SIGCHLD, (void *)&B );
	//cret = clone(myfunc, cstack + 8191, SIGCHLD, (void *)&B );
	//cret = clone(myfunc, cstack + 8191, (void *)&B );
	cret = clone(myfunc, cstack + 8191, CLONE_NEWNS|CLONE_NEWUTS|CLONE_NEWIPC|CLONE_NEWPID|CLONE_NEWNET|SIGCHLD, (void *)&B );

	if (cret > 0)
	{
		printf ("Pid of the child = %d \n", cret);
#if 1
    	fd = open("/dev/myLXC", O_RDWR);

    	if (fd < 0)
			perror("Unable to open the device");
	    else
		    printf("File opened Successfully %d\n", fd);

 	   	ioctl (fd, getpid(), 0);
   		close(fd);
#endif
		wait(NULL);

	}
	else if (cret == 0)
		printf ("Pid of the parent = %d \n", cret);
	else
		perror("clone failed");

	return 0;

}
