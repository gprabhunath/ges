

#include <stdio.h>

unsigned int *a, *b, *c;

int main()
{
	a = (unsigned int *) malloc(sizeof (unsigned int));
	b = (unsigned int *) malloc(sizeof (unsigned int));
	c = (unsigned int *) malloc(sizeof (unsigned int));

	*a = 4;
	*b = 5;
	*c = 6;

	printf ("The value of a = %d 0x%08x 0x%08x \n", *a, a, &a);
	printf ("The value of b = %d 0x%08x 0x%08x \n", *b, b, &b);
	printf ("The value of c = %d 0x%08x 0x%08x \n", *c, c, &c);

	printf ("The value of int = %d and unsigned int = %d \n", 
			sizeof (int), sizeof (unsigned int));
	
	free (a);
	free (b);
	free (c);

	return 0;
	
}
