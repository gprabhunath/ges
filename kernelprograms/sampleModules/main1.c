
// Main Kernel Module - man1

#include <linux/init.h>
#include <linux/module.h>

MODULE_LICENSE("Dual BSD/GPL");
extern void func_in_file_2(void);

// Initialization Routines
static int __init myInit1(void);
static void __exit myExit1(void);



static int __init myInit1(void)
{	
	printk(KERN_INFO "Entering the module of man1.ko myInit \n");
	func_in_file_2();

	return 0;
}

static void __exit myExit1(void)
{
	printk (KERN_INFO "Exiting the module man1.ko \n");

	return;
}

module_init(myInit1);
module_exit(myExit1);

