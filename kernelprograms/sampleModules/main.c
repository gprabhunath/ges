
// Main Kernel Module

#include <linux/init.h>
#include <linux/module.h>

MODULE_LICENSE("Dual BSD/GPL");
extern void func_in_file_2(void);

// Initialization Routines
static int __init myInit(void);
static void __exit myExit(void);



static int __init myInit(void)
{	
	printk(KERN_INFO "I am in myInit \n");
	func_in_file_2();

	return 0;
}

static void __exit myExit(void)
{
	printk (KERN_INFO "Exiting the module main.ko \n");

	return;
}

module_init(myInit);
module_exit(myExit);

