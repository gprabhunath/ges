#include <unistd.h>
#include <pthread.h>
#include <sched.h>
#include <fcntl.h>
#include "therr.h"

/*
 * Thread start routine. 
*/
struct buff {
    unsigned char dest[6];
    unsigned char src[6];
    unsigned short type;
} buf;

void * thread_routine (void *arg)
{
	int my_policy;
	struct sched_param my_param;
	int status;
	int fd;
	int i = 0;

	status = pthread_getschedparam (
				pthread_self(), &my_policy, &my_param);

	if (status != 0)
			err_abort(status, "Get sched");
	printf ("Thread routine running at %s/%d \n",
			(my_policy == SCHED_FIFO ? "FIFO"
				: (my_policy == SCHED_RR ? "RR"
				: (my_policy == SCHED_OTHER ? "OTHER"
				: "unknown"))),
			my_param.sched_priority);

    fd = open("/dev/myChar", O_RDWR);

    if (fd < 0)
       perror("Unable to open the device");
    else
       printf("File opened Successfully %d\n", fd);

    while (1)
    {
        i = 0;
        read(fd, &buf, sizeof (buf));
        #if 1
        printf ("dest addr = ");
        while (i < 6)
            printf ("%2x:", buf.dest[i++]);
        printf ("  ");
        printf ("src addr = ");
        i = 0;
        while (i < 6)
            printf ("%2x:", buf.src[i++]);
        printf ("  ");
        printf ("TYPE = ");
        printf ("%x ", buf.type);
        #endif
        printf ("\n");
    }

    close(fd);

	printf ("Exiting thread routine \n");

	return NULL;
}

