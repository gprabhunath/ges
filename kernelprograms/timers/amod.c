#include <stdio.h>
#include <fcntl.h>
#include <errno.h>

//char buf[60];

struct buff {
	unsigned char dest[6];
	unsigned char src[6];
	unsigned short type;
} buf;
int main()
{

	int fd, ret;
	int i = 0;

	fd = open("/dev/myChar", O_RDWR);
	
	if (fd < 0)
	   perror("Unable to open the device");
	else
	   printf("File opened Successfully %d\n", fd);
#if 1
	while (1)
	{
		i = 0;
		ret = read(fd, &buf, sizeof (buf));
		if (ret < 0)
			perror("read failed:");
		#if 1
		printf ("dst addr = ");
		while (i < 6)
			printf ("%2x:", buf.dest[i++]);	
		printf ("  ");
		printf ("src addr = ");
		i = 0;
		while (i < 6)
			printf ("%2x:", buf.src[i++]);	
		printf ("  ");
		printf ("TYPE = ");
		printf ("%x ", buf.type);
		#endif
		printf ("\n");
	}
#endif

	close(fd);

	return 0;
}
