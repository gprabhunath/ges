
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <asm/uaccess.h>
#include <linux/init.h>
#include <linux/sched.h>
#include <linux/types.h>
#include <linux/cdev.h>
#include <linux/device.h>
#include <linux/version.h>
#include <linux/wait.h>
#include <linux/smp.h>

MODULE_LICENSE("Dual BSD/GPL");

#define TIMEOUT	(2)	// 500 milliseconds

static DECLARE_WAIT_QUEUE_HEAD(myQ);
static int flag = 0;
int myOpen(struct inode *in, struct file *fp);
int myRelease(struct inode *in, struct file *fp);
int myRead(struct file *fp, char __user *buf, size_t bufsize, loff_t *loff);

char *devname;
int majNo=0;
module_param(devname, charp, 0000);
struct file_operations fops = {open:myOpen, release:myRelease, read:myRead};
struct timer_list mytimer;


void myaction (unsigned long d)
{
	int next_timeout;
	//printk(KERN_DEBUG "Prabhu's timeout... at %d %lu %lu \n",
	  //                smp_processor_id(), jiffies, mytimer.expires);
	//next_timeout = jiffies + 1 * HZ; // timeout = 1 sec
	next_timeout = jiffies + TIMEOUT; 
	mod_timer(&mytimer,next_timeout);
	flag = 1;
	wake_up_interruptible(&myQ);
	return;
}

int timer_init(void)
{
	unsigned long mydata = 0xaa55;
	printk (KERN_INFO "Initializing timer \n");
	printk (KERN_INFO "No. of interrupts/sec = %d \n", HZ);
	// Initialize the timer
	init_timer(&mytimer);
	mytimer.data = (unsigned long)&mydata;
	mytimer.function = myaction;
	mytimer.expires = jiffies + 1 * HZ; // 1 millisecond
	strcpy(mytimer.start_comm, "myTimer");
	add_timer(&mytimer);
	
	majNo = register_chrdev(0, devname, &fops);
	if (majNo < 0)
		printk(KERN_ERR "register_chrdev failed:\n");
	
	return 0;
}

int myOpen(struct inode *in, struct file *fp)
{	
	printk (KERN_INFO "Open Successfull \n");

	return 0;
}

int myRelease(struct inode *in, struct file *fp)
{
	printk (KERN_INFO "Close Successfull \n");
	return 0;
}

void timer_exit(void)
{
	int status;
	status = del_timer_sync(&mytimer);
	unregister_chrdev(majNo, devname);
	printk ("Exited from timer %d\n",status);
}
module_init(timer_init);
module_exit(timer_exit);

int myRead(struct file *fp, char __user *buf, size_t bufsize, loff_t *loff)
{
	int i = 65;
	int k = 0;
	//printk(KERN_DEBUG "Entering read function 0x%08x\n", buf);
	
	if (wait_event_interruptible(myQ, flag != 0))
	{
		printk (KERN_INFO "Restartsys \n");
		return -ERESTARTSYS;
	}
	flag = 0;

#if 0
	while (i >= 65 && i <= 122)
	{
		*(buf++) = i++;
	}
#endif

	while (k < 12)
		buf[k++] = i++;
	buf[12] = 0x00;
	buf[13] = 0x08;
	
	return 0;
}

