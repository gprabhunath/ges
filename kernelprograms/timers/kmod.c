
#include <linux/init.h>
#include <linux/module.h>
#include <linux/fs.h>

MODULE_LICENSE("Dual BSD/GPL");

char *devname;
int majNo;

module_param(devname, charp, 0000);
// Function Declarations for syscall definitions
int myOpen (struct inode *inode, struct file *filep);
int myRelease (struct inode *in, struct file *fp);

// Initialization routines
static int myInit (void);
static void myExit (void);

struct file_operations fops = {open:myOpen,release:myRelease};

static int myInit (void)
{
   printk(KERN_INFO "Initializing the World \n");

   majNo = register_chrdev(0,devname,&fops);
   if (majNo < 0)
   {
      printk(KERN_INFO "register_chrdev failed \n");
   } else {
      printk(KERN_INFO "Major Number of the device = %d \n", majNo);
   }

   printk (KERN_INFO "No. of timer interrupts/second: HZ = %d \n", HZ);
   printk (KERN_INFO "CLOCK_TICK_RATE = %d \n", CLOCK_TICK_RATE);
   printk (KERN_INFO "LATCH = %d \n", LATCH);
   
   return 0;
}

int myOpen (struct inode *inode, struct file *filep)
{
        printk(KERN_INFO "Open successful\n");
   		printk (KERN_INFO "sizeof jiffies = %d \n", sizeof(jiffies));
   		printk (KERN_INFO "sizeof jiffies_64 = %d \n", sizeof(jiffies_64));
   		printk (KERN_INFO "jiffies = 0x%08x \n", jiffies);
   		printk (KERN_INFO "jiffies_64 = %Lu \n", jiffies_64);
        return 0;
}

int myRelease (struct inode *in, struct file *fp)
{
   printk(KERN_INFO "File released \n");
   return 0;
}


static void myExit (void)
{
   printk (KERN_INFO "Exiting the World \n");
   unregister_chrdev(majNo, devname);
   return;
}

module_init(myInit);
module_exit(myExit);
