#!/bin/sh

# Usage : sh kscript.sh myChar

MOD=brkTest
APP=brkApp

dmesg -c > /tmp/dmesg
make clean
make
gcc $APP.c -o $APP
insmod $MOD.ko devname=$1
x=`cat /proc/devices | grep $1 | cut -b 1,2,3`
echo $x
mknod /dev/$1 c $x 0
echo ""; echo "";
echo  ---- Application Logs ----- 
./$APP

rmmod $MOD 
rm -f /dev/$1
rm -f $APP

echo ""; echo "";
echo " ----- Kernel Module Logs ------ "
dmesg

