#include <stdio.h>
#include <fcntl.h>

int *p;
int brkAddr = 0x0804b000;
int main()
{

	int fd;
	int ret;

	printf ("PID = %d \n", getpid());
	fd = open("/dev/myChar", O_RDWR);
   
	if (fd < 0)
		perror("Unable to open the device");
	else
		printf("File opened Successfully %d\n", fd);

	p = malloc(sizeof (int));
	if (ioctl(fd, getpid(), 0))
		perror("ioctl failed:");

	if (brk(brkAddr))
		perror("brk failed:");
	if (ioctl(fd, getpid(), 0))
		perror("ioctl failed:");

	getchar();
	getchar();

	close(fd);

	return 0;
}
