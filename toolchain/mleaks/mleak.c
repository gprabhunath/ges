#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

#define LEN	140
#define MB	(1 << 20)	

unsigned char *p[LEN];

unsigned char *buf;

int main(void)
{
	int i;

	printf ("PID = %d \n", getpid());
#if 0
	for (i=0; i < LEN; i++)
		p[i] = (unsigned char *) malloc (1 * 1024 * sizeof (unsigned char));

	for (i=0; i < LEN; i++)
		memset (p[i], 0x11, (1 * 1024));
#if 0	
	for (i=0; i < LEN; i++)
		free (p[i]);
#endif
#endif

	buf = malloc (1 * MB);
	free (buf);
	return 0;
}
