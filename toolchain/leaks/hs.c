#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define MAX 3	
int *p;

int main()
{
	printf ("PID = %d \n", getpid());

	p = malloc (4);

	printf ("Address of p = 0x%08x \n", p);
	printf ("Address of p+%d = 0x%08x \n", MAX, p+MAX);

	*(p+MAX) = 12;

	getchar();

	printf ("Value of *(p+%d) = %d \n", MAX, *(p+MAX));

	free (p);

	//*p = 12;

	return 0;
}
