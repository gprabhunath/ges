#include <stdio.h>
#include <unistd.h>

#define MAX -7

int b = 5;
int a[3] = {1, 2, 3};

int main()
{
	printf ("PID = %d \n", getpid());
	printf ("Address of a = 0x%08x \n", a);
	printf ("Address of a[%d] = 0x%08x \n", MAX, a+MAX);

	getchar();

	a[MAX] = 5;

	printf ("Value of a[%d] = %d \n", MAX, a[MAX]);

	
	return 0;
}
