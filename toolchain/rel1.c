#include <stdio.h>

int A, B, C;
int D = 12;
int E = 20;
static int F, G, H;
static int I = 92;
static int J = 29;
const int M = 88;
const int N = 65;
char *gstr = "Global String";

extern void add (int, int);

int main()
{
	static int K = 24;	
	static int L;
	char *lstr = "Local String";
	const int P = 89;
	int R;
	int S = 4;

	A = 34;
	B = 19;
	C = 29;
	F = 62;
	G = 26;
	H = 18;
	
	add (2, 9);

	return 0;

}
