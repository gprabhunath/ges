#include <stdio.h>
#include <mathops.h>
#include <sys/types.h>
#include <unistd.h>

int main()
{
	int c, d;

	printf ("PID = %d \n", getpid());

	c = sum (9, 4);
	d = diff (2, 8);
	getchar();
	printf ("Value of c = %d \n", c);
	printf ("Value of d = %d \n", d);

	return 0;

}
