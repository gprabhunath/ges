
#ifndef __MATHOPS_H
#define __MATHOPS_H

#include <stdio.h>

#define HMI_BT HMI

struct hmi_t 
{
	int a;
	void (*fn)(void);
};

extern int sum (int, int);
extern int diff (int, int);

#endif





