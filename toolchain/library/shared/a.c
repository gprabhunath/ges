#include <stdio.h>
#include <dlfcn.h>
#include <stdlib.h>
#include "mathops.h"

void *handle;
struct hmi_t *hm;

int main()
{
	handle = dlopen ("./libmathops.so", RTLD_NOW);	
	if (!handle)
	{
		fprintf (stderr, "%s \n", dlerror());
		exit (1);
	}


	hm = (struct hmi_t *)dlsym (handle, "HMI");
	if (!handle)
	{
		fprintf (stderr, "%s \n", dlerror());
		exit (1);
	}

	hm->fn();

	return 0;
}
