#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <mathops.h>

int main()
{
	int c, d;

	printf ("PID = %d \n", getpid());

	c = sum (9, 4);
	d = diff (2, 8);

	printf ("Value of c = %d \n", c);
	printf ("Value of d = %d \n", d);

	printf ("Address of sum = 0x%08x \n", sum);
	getchar();

	return 0;

}
