#include <stdio.h>
#include <stdlib.h>

struct zone {
	int a;
	char ch;
};

struct zone *zone;

int main(int argc, char **argv)
{
	printf ("0x%08x 0x%08x \n", &argc, argv);
	zone = malloc (sizeof (struct zone));
	zone->ch = 4;
	zone->a = 12;

	free (zone);

	printf ("zone->a = %d \n", zone->a);
	printf ("zone->ch = %d \n", zone->ch);

	return 0;
}

