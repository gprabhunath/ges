#include <stdio.h>
#include <stdlib.h>

#define dec(b, c)	\
	typeof(*b) c; \
	b = malloc (sizeof (c))

struct abc {
	int a;
	int b;
} *p;

int main()
{
	
	dec(p, s);
	p->a = 9;
	printf ("Size of s = %d \n", sizeof (s));
	printf ("Value of p->a = %d \n", p->a);

	return 0;
}
