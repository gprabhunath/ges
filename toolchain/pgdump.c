#include <stdio.h>

#define PAGE_OFFSET	0x10000000
#define PAGE_SIZE	0x1000

#define INDEX	0x001

int main()
{
	int i;

#if 0
	for (i=10; i>=0; i--)	
		printf ("0x%08x \n", PAGE_OFFSET + (PAGE_SIZE * i) );
#endif

	for (i=10; i>=0 ; i--)
		printf ("0x%03x \n", i);


	return 0;
}
