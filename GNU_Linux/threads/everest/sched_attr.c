
#include "sched_attr.h"


int main(int argc, int *argv[])
{
	pthread_t thread_id;
	pthread_attr_t thread_attr;
	int thread_policy;
	struct sched_param thread_param;
	int status, rr_min_priority, rr_max_priority;
	int i = 0, ch;
	FILE *fp;

	pid = getpid();
	printf ("PID of main thread is %d \n", pid);
	
	status = pthread_attr_init(&thread_attr);
	if (status != 0)
		err_abort(status, "Init attr");
	
#if defined (_POSIX_THREAD_PRIORITY_SCHEDULING) && !defined (sun)
	status = pthread_attr_getschedpolicy(&thread_attr, &thread_policy);
	if (status != 0)
		err_abort (status, "Get policy");
	
	status = pthread_attr_getschedparam(&thread_attr, &thread_param);
	if (status != 0)
		err_abort (status, "Get sched param");
	
	printf ("Default policy is %s, priority is %d\n",
		(thread_policy == SCHED_FIFO ? "FIFO"
		: (thread_policy == SCHED_RR ? "RR"
		: (thread_policy == SCHED_OTHER ? "OTHER"
		: "unknown"))), 
		thread_param.sched_priority);
	
	status = pthread_attr_setschedpolicy(&thread_attr, SCHED_RR);
	if (status != 0)
		printf ("Unable to set SCHED_RR policy \n");
	else {
		rr_min_priority = sched_get_priority_min (SCHED_RR);
		if (rr_min_priority == -1)
			errno_abort ("Get SCHED_RR min priority");

		rr_max_priority = sched_get_priority_max (SCHED_RR);
		if (rr_max_priority == -1)
			errno_abort ("Get SCHED_RR max priority");

		//thread_param.sched_priority = (rr_min_priority + rr_max_priority) / 2;
		thread_param.sched_priority = 98;
		
		printf ("SCHED_RR priority range is %d to %d: using %d \n",
				rr_min_priority, rr_max_priority, thread_param.sched_priority);

		status = pthread_attr_setschedparam(&thread_attr, &thread_param);
		if (status != 0)
			err_abort (status, "Set params");

		printf ("Creating thread at RR/%d\n",thread_param.sched_priority);

		status = pthread_attr_setinheritsched(
					&thread_attr,PTHREAD_EXPLICIT_SCHED);
		if (status != 0)
			err_abort (status, "Set inherit");

	}
#else
	printf ("Priority scheduling not supported \n");
#endif


	//setTimer();
	//signal(SIGALRM,alarm_wakeup); /* set the Alarm signal capture */


	// Device File open 
    fd = open("/dev/myChar", O_RDWR);
    if (fd < 0)
        perror("Unable to open the device");

	fpnull = fopen("/dev/null", "w");

	status = pthread_create (
				&thread_id, &thread_attr, thread_routine, NULL);
	if (status != 0)
		err_abort (status, "Create thread");

	// Open a log file to be used by signal handler
	flog = fopen("proclog.txt", "w");	
	if (flog == NULL)	
		perror("fopen proclog failed:");
	
	status = pthread_join(thread_id, NULL);
	if (status != 0)
		err_abort (status, "Join thread");


    while(i++ < 5)
    {
        fp = fopen("messages", "rw");
        while ((ch = fgetc(fp) != EOF))
                putc(ch, fpnull);
		myIoctl(PROCESS);
        fclose(fp);
		process_cnt++;
    }
	
	close(fd); // Close Device file
	fclose(fpnull);
	fclose(flog);
	printf ("Exiting main routine %d \n", pid);

	return 0;
		
} // End of main
