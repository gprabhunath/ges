
	fprintf (fpAnalyselog,
	"%d \t\t\t: Task id \n
	%s  \t\t\t: Executable name \n 
	%c   \t\t\t: Task State  \n 
	%d   \t\t\t: pid of the parent \n 
	%d   \t\t\t: The process group ID of the process \n 
	%d   \t\t\t: The session ID of the process \n 
	%d   \t\t\t: The  controlling  terminal  of  the  process \n 
	%d   \t\t\t: pid of the foreground process group \n 
	%u   \t\t\t: Kernel flags of the process \n 
	%lu   \t\t\t: The  number  of  minor faults the process has \n 
				  made which have not required loading a memory \n
				  page from disk \n
	%lu   \t\t\t: The number of minor faults that the process's \n 
				  waited-for children have made
	%lu   \t\t\t: The  number  of  major faults the process has \n 
				  made which have required loading a memory \n
				  page from disk \n
	%lu   \t\t\t: The number of major faults that the process's \n 
				  waited-for children have made
	%lu   \t\t\t: Amount of time that this process has been \n 
				  scheduled in user mode, measured in clock ticks \n
	%lu   \t\t\t: Amount of time that this process has been \n 
				  scheduled in kernel mode, measured in clock ticks \n
	%ld   \t\t\t: Amount of time that this process's waited-for \n 
				  children  have  been  scheduled in user mode \n
	%ld   \t\t\t: Amount of time that this process's waited-for \n 
				  children have been scheduled in kernel mode \n
	%ld   \t\t\t: priority for SCHED_OTHER \n 
	%ld   \t\t\t: Nice Value \n 
	%ld   \t\t\t: Number of  threads  in  this  process \n 
	%ld   \t\t\t: This is always zero \n 
	%llu  \t\t\t: The time in jiffies the process started after \n
				  system boot \n 
	%lu   \t\t\t: Virtual address size in bytes \n 
	%ld   \t\t\t: number of pages the process has in the real memory \n 
	%lu   \t\t\t: Current soft limit in bytes on the rss of the \n
				  process \n 
	%lu   \t\t\t: The start virtual address of the program \n 
	%lu   \t\t\t: The end virtual address of the program \n 
	%lu   \t\t\t: The start address of the stack \n 
	%lu   \t\t\t: Current value of stack pointer - esp \n 
	%lu   \t\t\t: Current instruction pointer - eip \n 
	%lu   \t\t\t: bitmap of pending signals \n 
	%lu   \t\t\t: bitmap of blocked signals \n 
	%lu   \t\t\t: bitmap of ignored signals \n 
	%lu   \t\t\t: bitmap of caught signals \n 
	%lu   \t\t\t: Address of the system call at which the process
				  is waiting \n 
	%lu   \t\t\t: Number of pages swapped (not maintained) \n 
	%lu   \t\t\t: Cumulative nswap (not maintained)\n 
	%d   \t\t\t:  Signal to be sent to parent when we die \n 
	%d   \t\t\t:  CPU number last executed on \n 
	%u   \t\t\t:  Real-time  scheduling  priority \n 
	%u   \t\t\t:  Scheduling   policy \n 
	%llu  \t\t\t: Aggregated  block  I/O  delays \n 
	%lu   \t\t\t: Guest time of the process \n 
	%ld   \t\t\t: Guest time of the  process's children \n" 
	);

} // End of main
