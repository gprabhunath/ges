#include "sched_attr.h"

void sigBlk(int sig)
{
	sigset_t set, oldset;
	int ret;
	
	set.__val[0] = sig;
	ret = pthread_sigmask(SIG_SETMASK, &set, &oldset);	
	if (ret)
		perror("sigprocmask:");
	printf ("Blocked signals = 0x%08x \n", oldset.__val[0]);
	return;
	
}
void setTimer(void)
{
	struct itimerval tout_val;
	int ret;

	// Set the timer value
    tout_val.it_interval.tv_sec = 0;
    tout_val.it_interval.tv_usec = INTERVAL;
    tout_val.it_value.tv_sec = 1;
    tout_val.it_value.tv_usec = 0; // Set 20 ms interval

    ret = setitimer(ITIMER_REAL, &tout_val,0);
    if (ret)
    	perror("setitimer failed:");

	return;
}

void myIoctl(int flag)
{
	int ret;
#if 1
    ret = ioctl (fd, flag, &A);
	if (ret < 0)
		perror("Ioctl failed:");
#endif

}

void alarm_wakeup (int i)
{
	char pidpath[20];
	char tidpath[30];
	int ch;
	FILE *fpPID, *fpTID;

	myIoctl(SIGHAND);
/*
    printf(" Wakeup!!! TC = %d ; PC = %d ; pid = %d ; tid = %d \n", 
			thread_cnt, process_cnt, pid, tid);
*/
	sprintf (pidpath, "/proc/%d/stat", pid);
	sprintf (tidpath, "/proc/%d/task/%d/stat", pid, tid);

	fprintf (flog, "\n--------------------------------------------\n");
	fprintf (flog, " Wakeup!!! TC = %d ; PC = %d ; pid = %d ; tid = %d \n",
					thread_cnt, process_cnt, pid, tid);

	fpPID = fopen(pidpath, "r");
	if (fpPID != NULL)
	{
		while ((ch = fgetc(fpPID)) != EOF)
			fputc(ch, flog);
		fclose(fpPID);
	}
	
	fprintf (flog, "\n");	

	fpTID = fopen(tidpath, "r");
	if (fpTID != NULL)
	{
		while ((ch = fgetc(fpTID)) != EOF)
			fputc(ch, flog);
		fclose(fpTID);
	}

	fprintf (flog, "--------------------------------------------\n");
		
    return;
}

/*
 * Thread start routine. If priority scheduling is supported, 
 * report the thread scheduling attributes
*/

void * thread_routine (void *arg)
{
	int my_policy;
	struct sched_param my_param;
	int status;
	int fd, ch;
	unsigned long long i = 0;
	char str[10];
	FILE *fp;

	tid = syscall(SYS_gettid);
	printf ("Starting thread routine %d \n", tid);
	/*
	 * If the priority scheduling option is not defined , then we can do 
	 * nothing with the output of pthread_getschedparam, so just report
	 * that the thread ran, and exit
	*/

	#if defined (_POSIX_THREAD_PRIORITY_SCHEDULING) && !defined(sun)
		status = pthread_getschedparam (
				pthread_self(), &my_policy, &my_param);

		if (status != 0)
			err_abort(status, "Get sched");
		printf ("Thread routine running at %s/%d \n",
			(my_policy == SCHED_FIFO ? "FIFO"
				: (my_policy == SCHED_RR ? "RR"
				: (my_policy == SCHED_OTHER ? "OTHER"
				: "unknown"))),
			my_param.sched_priority);
	
	
	//sigBlk(SIGALRMBLK);
	setTimer();
	signal(SIGALRM,alarm_wakeup);

	while(i++ < 2)
	{
		myIoctl(THREAD);
#if 1
    	while(i++ < 5)
    	{
			printf ("Inside loop \n");
        	fp = fopen("msgthread", "rw");
        	while ((ch = fgetc(fp) != EOF))
                putc(ch, fpnull);
        	myIoctl(THREAD);
        	fclose(fp);
			thread_cnt++;
    	}

#endif

	}

	#else
		printf ("thread_routine running \n");
	#endif
	
	printf ("Exiting thread routine %d \n", tid);
	return NULL;
}
