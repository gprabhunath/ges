#include <stdio.h>
#include <sys/time.h>
#include <errno.h>
#include <stdlib.h>
#include <signal.h>

#define INTERVAL 2

int i=0;
void alarm_wakeup (int p)
{
	//signal(SIGALRM,alarm_wakeup);
	//printf("%d sec up partner, Wakeup!!!\n",INTERVAL);
	printf ("Finished %d times \n", i);
	//signal(SIGALRM,alarm_wakeup); /* set the Alarm signal capture */

	return;
}

int main()
{
	struct itimerval t_interval;
	struct itimerval tout_val;
	int ret, ch;
	FILE *fp, *fpnull;

	printf ("pid = %d \n", getpid());
	ret = getitimer(ITIMER_REAL, &t_interval);
	if (ret)
		perror("getitimer failed:");
	
	printf ("tv_sec = %lu \t tv_usec = %lu \n", 
	        t_interval.it_value.tv_sec, t_interval.it_value.tv_usec);

	// Set the timer value
	tout_val.it_interval.tv_sec = 0;
	tout_val.it_interval.tv_usec = 1000 * 500;
	tout_val.it_value.tv_sec = 5; 
	tout_val.it_value.tv_usec = 0; // Set 20 ms interval

	ret = setitimer(ITIMER_REAL, &tout_val,0);
	if (ret)
		perror("setitimer failed:");

	// Get the timer value
	ret = getitimer(ITIMER_REAL, &t_interval);
	if (ret)
		perror("getitimer failed:");
	
	printf ("TV_sec = %lu \t tv_usec = %lu \n", 
	        t_interval.it_interval.tv_sec, t_interval.it_interval.tv_usec);


	signal(SIGALRM,alarm_wakeup); /* set the Alarm signal capture */
	
	fpnull = fopen("/dev/null", "w");			  
    while(i++ < 100)
    {
    	fp = fopen("messages", "rw");
        while ((ch = fgetc(fp) != EOF))
        	putc(ch, fpnull);
        fclose(fp);
    }
	fclose (fp);
                                                          
	return 0;
}

