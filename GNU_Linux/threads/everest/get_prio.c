
#include <stdio.h>
#include <sched.h>

int main()
{
	int rr_min_priority, rr_max_priority;

	rr_min_priority = sched_get_priority_min(SCHED_RR);
	rr_max_priority = sched_get_priority_max(SCHED_RR);

	printf ("Min priority = %d \n", rr_min_priority);
	printf ("Max priority = %d \n", rr_max_priority);

	return 0;
}
