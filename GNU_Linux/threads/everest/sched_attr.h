#ifndef __sched_attr_h
#define __sched_attr_h

#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <sched.h>
#include <fcntl.h>
#include <signal.h>
#include <syscall.h>
#include <sys/time.h>

#include "errors.h"



#define INTERVAL    (1000 * 100)
#define SIGALRMBLK  0x0000
#define PROCESS	1
#define THREAD	2
#define SIGHAND 4

#define NUM 999999999

int A; // for ioctl
int fd; // for device operations
FILE *fpnull, *flog;
unsigned int thread_cnt, process_cnt;
int tid, pid;

void alarm_wakeup (int i);
void * thread_routine (void *arg);
void myIoctl(int flag);
void setTimer(void);

#endif
