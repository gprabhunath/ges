
#include <stdio.h>
#include <linux/time.h>

struct timespec cur;

int main()
{
	unsigned int sec, ms;
	
	clock_gettime (CLOCK_REALTIME, &cur);
	sec = cur.tv_sec;
	ms = cur.tv_nsec/1000;
	printf ("Sec:microsec = %u:%u \n", sec, ms);

	return 0;
}
