#include <stdio.h>
#include <sys/time.h>
#include <errno.h>
#include <stdlib.h>
#include <signal.h>

#define INTERVAL 20000

void alarm_wakeup (int i)
{
	signal(SIGALRM,alarm_wakeup);
	printf("%d milli sec up partner, Wakeup!!!\n",INTERVAL);
	//signal(SIGALRM,alarm_wakeup); /* set the Alarm signal capture */

	return;
}

int main()
{
	struct itimerval t_interval;
	struct itimerval tout_val;
	int ret;

	ret = getitimer(ITIMER_REAL, &t_interval);
	if (ret)
		perror("getitimer failed:");
	
	printf ("tv_sec = %lu \t tv_usec = %lu \n", 
	        t_interval.it_value.tv_sec, t_interval.it_value.tv_usec);

#if 0
	// Set the timer value
	tout_val.it_interval.tv_sec = 0;
	tout_val.it_interval.tv_usec = 0;
	tout_val.it_value.tv_sec = 0; 
	tout_val.it_value.tv_usec = INTERVAL; // Set 20 ms interval

	ret = setitimer(ITIMER_REAL, &tout_val,0);
	if (ret)
		perror("setitimer failed:");

	// Get the timer value
	ret = getitimer(ITIMER_REAL, &t_interval);
	if (ret)
		perror("getitimer failed:");
	
	printf ("TV_sec = %lu \t tv_usec = %lu \n", 
	        t_interval.it_value.tv_sec, t_interval.it_value.tv_usec);


	signal(SIGALRM,alarm_wakeup); /* set the Alarm signal capture */
			  
#endif
	return 0;
}

