#include <stdio.h>
#include <time.h>
//#include <inttypes.h>
#include <stdint.h>

time_t result;

int main()
{
	result = time(NULL);	
	printf ("%s%ju secs since the Epoch \n",
				asctime(localtime(&result)),
				(uintmax_t)result);

	return 0;
}
