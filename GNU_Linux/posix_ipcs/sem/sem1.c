#include <stdio.h>
#include <stdlib.h>
#include <semaphore.h>

sem_t sem;

int main()
{
	int status;

	status = sem_init (&sem, 0, 0);
	if (status < 0)
	{
		perror ("sem_init failed");
		exit (1);
	}

	status = sem_wait (&sem);
	if (status < 0)
	{
		perror ("sem_init failed");
		exit (1);
	}


	return 0;	

}


