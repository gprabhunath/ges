#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>

#define MODE   (S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH)
#define PAGE_SIZE	4096

unsigned int *sm;
unsigned int ms = 5;

void f1(void)
{
	while (*sm == 5)
	{
		printf ("Global Edge Software Limited \n");
	}

	return;
}

int main()
{

	int fd;
	int status;

	fd = shm_open ("/cshm", O_RDWR, MODE);
	if (fd < 0)
	{
		perror ("shm_open failed");
		exit (1);
	}
#if 0
	status = ftruncate (fd, PAGE_SIZE);
	if (status < 0)
	{
		perror ("ftruncate failed");
		exit (1);
	}
	
#endif
	sm = mmap (NULL, PAGE_SIZE, PROT_READ|PROT_WRITE, MAP_SHARED,fd, 0);
	if (sm == MAP_FAILED)
	{
		perror ("mmap failed");
		exit (1);
	}

	printf ("Value of *sm = %u \n", *sm) ;

	f1();

	return 0;
	
}

