#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>

//#define MODE   (S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH)
#define MODE   (S_IRUSR|S_IWUSR)
#define PAGE_SIZE	4096

unsigned int *sm;

int main(int argc, char *argv[])
{

	int fd;
	int status;

	if (argc < 2)
	{
		printf ("Too few command line arguments \n");
		printf ("usage: ./t1 <num> \n");
		exit (1);
	}
	
	fd = shm_open ("/cshm", O_CREAT|O_RDWR, MODE);
	if (fd < 0)
	{
		perror ("shm_open failed");
		exit (1);
	}
#if 0
	status = ftruncate (fd, PAGE_SIZE);
	if (status < 0)
	{
		perror ("ftruncate failed");
		exit (1);
	}
#endif
	
	sm = mmap (NULL, PAGE_SIZE, PROT_READ|PROT_WRITE, MAP_SHARED,fd, 0);
	if (sm == MAP_FAILED)
	{
		perror ("mmap failed");
		exit (1);
	}

	*sm = atoi(argv[1]);

	return 0;
	
}

