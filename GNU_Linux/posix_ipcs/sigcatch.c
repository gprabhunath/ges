#include <stdio.h>
#include <sys/types.h>
#include <signal.h>
#include <unistd.h>

unsigned int b;
int p = 5;

void catch_ctlc( int sig_num )
{
	p = 6;
  	fflush( stdout );

  return;
}

int main()
{

  signal( SIGUSR1, catch_ctlc );

  printf("Go ahead, make my day.\n");

	while (p == 5)
	{
		b++;
		if (b > 100)
		{
			b = 0;
		}
	}

	while (b-- > 0)
	{
		printf ("Global Edge Software Limited %u \n", b);
	}

  return 0;
}
