#include <stdio.h>
#include <pthread.h>
#include <string.h>
#include <stdlib.h>

#if 0
	f1(); // To test int volatile 
	f2(); // To test int volatile *
	f3(); // To test int *volatile		
	f4(); // To test int volatile *volatile 		
#endif


typedef void(*func)(void);

int volatile f1_v = 5;
int tv = 5;
int volatile *f2_v = &tv;
int *volatile f3_v;
int volatile *volatile f4_v;


void get_user_input(void)
{
	int p;

	printf ("Enter the value of p: ");		
	scanf ("%d", &p);
	return;
}

void f4_thread_func (void)
{
	get_user_input();
	f4_v = malloc (sizeof (int));
	*f4_v = 5;

	sched_yield();

	get_user_input();
	*f4_v = 6;

	return;
}

void f4(void)
{
	int b = 0;

	while (f4_v == NULL)
	{
		b++;
		if (b > 100)
			b = 0;
	}
	while (b-- > 0)
	{
		printf ("Attachments are main hurdles for progress %d \n", b);
	}
	b = 0;
	while (*f4_v == 5)
	{
		b++;
		if (b > 100)
			b = 0;
	}
	while (b-- > 0)
	{
		printf ("With consistent efforts we can get rid of attachment - Gita %d \n", b);
	}

	free ((int *)f4_v);
	f4_v = NULL;

	return ;	
}

void f3_thread_func (void)
{

	get_user_input();
	f3_v  = malloc (sizeof (int));

	return;
}

void f3(void)
{
	int b = 0;

	while (f3_v == NULL)
	{
		b++;
		if (b > 100)
			b = 0;
	}
	while (b-- > 0)
	{
		printf ("Attachments are main hurdles for progress %d \n", b);
	}

	free ((int *)f3_v);
	f3_v = NULL;

	return ;	
}

void f2_thread_func (void)
{
	get_user_input();
	*f2_v = 6;

	return;
}

void f2(void)
{
	int b = 0;

	while (*f2_v == 5)
	{
		b++;
		if (b > 100)
			b = 0;
	}

	while (b-- > 0)
	{
		printf ("Attachments are main hurdles for progress %d \n", b);
	}

	return;	
}

void f1_thread_func (void)
{
	get_user_input();
	f1_v = 6;

	return;
}

void f1(void)
{
	int b = 0;

	while (f1_v == 5)
	{
		b++;
		if (b > 100)
			b = 0;
	}

	while (b-- > 0)
	{
		printf ("Attachments are main hurdles for progress %d \n", b);
	}

	return;
}

func vfunc[9] = {NULL, f1, f2, f3, f4, 
				f1_thread_func, f2_thread_func, 
				f3_thread_func, f4_thread_func};

static int vfunc_index;

void *thread_func (void *s)
{

	vfunc[vfunc_index + 4]();	

	pthread_exit (NULL);	
}

int main()
{
	pthread_t th_id;
	int ret;

	printf ("Enter user input to test: \n \
			1: int volatile \n \
			2: int volatile \n \
			3: int volatile \n \
			4: int volatile \n"
		   );
	printf ("Enter choice:");
	scanf ("%d", &vfunc_index);
	
	
	ret = pthread_create (&th_id, NULL, thread_func, NULL);
	if (ret != 0)
	{
		printf ("pthread_create failed: \n", strerror (ret));
		exit (1);
	}

	vfunc[vfunc_index]();	

	pthread_exit (NULL);
}
