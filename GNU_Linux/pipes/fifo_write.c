#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <stdlib.h>
#include <fcntl.h>


#define BUFSIZE	(1 * 1024 * 1024)

const char *pathname = "/home/prabhu/training/GNU_Linux/pipes/mypipe";


void hexdump (char buf[], int cnt)
{
	int i;
	for (i=0; i <= cnt; i++)
		printf ("%02x ", buf[i]);

	printf ("\n");
	
}
int main()
{
	char buf[BUFSIZE];
	int fd;
	int cnt;


	fd = open (pathname, O_WRONLY);
	if ( fd == -1 )
	{
		perror ("open failed");
	}
	
	printf ("pid = %d \n", getpid());
	cnt = read (0, buf, sizeof (buf));
	//hexdump(buf, cnt+2);
	printf ("No of bytes read is %d \n", cnt);
	
	cnt = write (fd, buf, cnt);
	printf ("No of bytes written to the pipe is %d \n", cnt);
	
	return 0;		
}
