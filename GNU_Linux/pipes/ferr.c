
#include <stdio.h>

char one[] = {"Writing to stdout\n"};
char two[] = {"Writing to stderr\n"};
char rbuf[5];
int main()
{
	int fd;

	write (1, one, sizeof (one))	;
	write (2, two, sizeof (two))	;
	//fprintf (stdout, "I\n");
	read (0, rbuf, sizeof (rbuf));
	printf ("Buffer read is %s", rbuf);

	return 0;
}
