#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <stdlib.h>

#define MODE S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH


const char *pathname = "/home/prabhu/training/GNU_Linux/pipes/mypipe";


int main()
{
	int status;

	status = mkfifo (pathname, MODE);
	if (status != 0)
	{
		perror ("mkfifo fail");
		exit (1);
	}


	return 0;
}
