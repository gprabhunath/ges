#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

char *cmd[] = { "ls", "-1", (char *)0 };

int main()
{
  int pfds[2];
  int ret;

  if ( pipe(pfds) == 0 ) {

    if ( fork() == 0 ) {

      close(1);
      close( pfds[0] );
      dup2( pfds[1], 1 );
      //execlp( "ls", "ls", "-l", NULL );
      execlp( "ls", "ls -1", NULL );
      //execve( "/bin/ls", cmd, NULL );

    } else {

      close(0);
      close( pfds[1] );
      dup2( pfds[0], 0 );
      execlp( "wc", "wc", "-l", NULL );

    }

  }

  return 0;
}
