#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <stdlib.h>
#include <fcntl.h>


#define BUFSIZE	4096

const char *pathname = "/home/prabhu/training/GNU_Linux/pipes/mypipe";

char buf[BUFSIZE];

int main()
{
	int fd;

	fd = open (pathname, O_RDONLY);
	if ( fd == -1 )
	{
		perror ("open failed");
	}

	read (fd, buf, sizeof (buf));
	
	write (1, buf, sizeof (buf));
	
	return 0;	
}
