
#include <stdio.h>
#include <sys/msg.h>
#include <errno.h>
#include "common.h"

key_t	myKey;
int main()
{
  int msgid;

  /* Create the message queue with the id MY_MQ_ID */
/*
  myKey = ftok("/home/prabhu/training/IPC/mq/myqueue", 2);
  msgid = msgget( myKey, 0666 | IPC_CREAT );
*/
  msgid = msgget( MY_MQ_ID, 0666 | IPC_CREAT );

  if (msgid >= 0) 
  {
    printf( "Created a Message Queue %d\n", msgid );
  } else
  {	
  	perror ("MQ not created");
  }

  return 0;
}

