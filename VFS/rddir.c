#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>

FILE *fp;

char buf[1024];

int main()
{
	int cnt;
	int fd;

#if 0
	fp = fopen ("/home/prabhu/training", "r");
	if (fp == NULL)
	{
		perror ("fopen failed");
		exit (-1);
	}

	cnt = fread (buf, sizeof (buf), 1, fp);
//	printf ("No. of bytes read = %d \n", cnt);
	

	fwrite (buf, sizeof (buf), 1, stdout);

	fclose (fp);

#endif

	fd = open ("/home/prabhu/training", O_RDONLY);
	if (fd < 0)
	{
		perror ("open failed:");
		exit (-1);
	}

	printf ("File descriptor = %d \n", fd);

	cnt = read (fd, buf, sizeof (buf));
	if (cnt < 1)
	{
		perror ("read failed:");
		exit (-1);
		
	}

	return 0;
	
}
