#!/bin/bash

# This is untested but you get the basic idea
# Why? Cause fuck dependencies that's why ;)

if [[ $# -lt 1 ]]; then
    echo "Usage: $0 INFILE"
    exit 1
fi

if [[ $1 =~ \.tar\.bz2$ ]] ; then tar xfj $1; fi
if [[ $1 =~ \.tar\.gz$ ]] ; then tar xzf $1; fi
if [[ $1 =~ \.tar$ ]] ; then tar xf $1; fi
