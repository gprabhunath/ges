#!/bin/sh

# explode.sh: takes 1 argument, the initrd.img. Creates an initrd
# directory, extracts the initrd and changes directory into it.

rm -rf initrd
mkdir initrd
cp $1 initrd
cd initrd
#gzip -dc $1 | cpio -id
xz $1 | cpio -id
rm -rf $1
ls -l
